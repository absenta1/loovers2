package com.appdupe.flamer.pojo;

import com.android.slidingmenu.objects.Match;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class UserLikes 
{

    @SerializedName("success")
    public Boolean  success;

    @SerializedName("data")
	public ArrayList<UserLikesData> userlikesDataList;

    @SerializedName("currentLikes")
    public String currentLikes;

    @SerializedName("userLiked")
    public String userliked ;

    @SerializedName("newMatch")
    public ArrayList<Match> newMatch;

   
}
