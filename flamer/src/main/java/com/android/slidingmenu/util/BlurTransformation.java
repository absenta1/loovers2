package com.android.slidingmenu.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v8.renderscript.Allocation;
import android.support.v8.renderscript.Element;
import android.support.v8.renderscript.RenderScript;
import android.support.v8.renderscript.ScriptIntrinsicBlur;

/**
 * Created by Juan Martin Bernal on 1/4/16.
 */
public class BlurTransformation implements com.squareup.picasso.Transformation {
    private final int radius;
    private Context cxt;

    // radius is corner radii in dp
    public BlurTransformation(Context cxt, final int radius) {
        this.cxt = cxt;
        this.radius = radius;
    }

    @Override
    public Bitmap transform(final Bitmap bitmap) {

        if (radius > 0) {
            Bitmap outBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
            RenderScript rs = RenderScript.create(cxt);
            ScriptIntrinsicBlur blurScript = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs));
            Allocation allIn = Allocation.createFromBitmap(rs, bitmap,Allocation.MipmapControl.MIPMAP_NONE, Allocation.USAGE_SCRIPT);
            Allocation allOut = Allocation.createFromBitmap(rs, outBitmap);
            blurScript.setRadius(radius);
            blurScript.setInput(allIn);
            blurScript.forEach(allOut);
            allOut.copyTo(outBitmap);
            bitmap.recycle();
            rs.destroy();

            return outBitmap;
        }else
            return  bitmap;
    }

    @Override
    public String key() {
        return "blur"+Float.toString(radius);
    }
}