package com.android.slidingmenu.fragments;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.slidingmenu.Analitycs.MyApplication;
import com.android.slidingmenu.MainActivity;
import com.android.slidingmenu.adapter.GeneralAdapter;
import com.android.slidingmenu.objects.UserData;
import com.android.slidingmenu.util.Common;
import com.android.slidingmenu.util.GlobalData;
import com.android.slidingmenu.util.TypesLetter;
import com.android.slidingmenu.util.Visibility;
import com.android.slidingmenu.webservices.Services;
import com.appdupe.flamer.pojo.MatchesData;
import com.appdupe.flamer.utility.Constant;
import com.google.gson.Gson;
import com.loovers.app.R;
import com.wang.avi.AVLoadingIndicatorView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import jp.co.recruit_lifestyle.android.widget.WaveSwipeRefreshLayout;
import okhttp3.Call;
import okhttp3.FormBody;
import okhttp3.Response;

public class FragmentWallas extends Fragment
        implements WaveSwipeRefreshLayout.OnRefreshListener{

    public MainActivity mainActivity;
    public GeneralAdapter adapter;
    public LinearLayout containerExplanedSection;
    public Button btnPLayWalla;
    public ImageView imgUser;
    public TextView txtTitle,txtSubTitle,txtCantPersons,txtNumberPersons,txtDesc;
    public WaveSwipeRefreshLayout mWaveSwipeRefreshLayout;
    public AVLoadingIndicatorView progressBarFavoritesWallas;
    public static final int SHOW_DIALOG = 1;
    public static final int HIDE_DIALOG = 0;
    public ArrayList<MatchesData> matchesDatas = new ArrayList<>();

    public FragmentWallas()
    {}
    public FragmentWallas(MainActivity mainActivity)
    {
        this.mainActivity = mainActivity;
    }
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.flechazosactivity, null);

        if(mainActivity == null)
            mainActivity = (MainActivity)getActivity();


        Visibility.visible(mainActivity.tvTitle);

        mainActivity.tvTitle.setText(""+mainActivity.getResources().getString(R.string.likes));

        mWaveSwipeRefreshLayout             = (WaveSwipeRefreshLayout)view.findViewById(R.id.main_swipe);
        mainActivity.gridview               = (GridView)view.findViewById(R.id.search_gridview);
        mainActivity.containerMatchEmpty    = (RelativeLayout)view.findViewById(R.id.containerMatchEmpty);
        containerExplanedSection            = (LinearLayout)view.findViewById(R.id.containerExplanedSection);
        progressBarFavoritesWallas          = (AVLoadingIndicatorView)view.findViewById(R.id.progressBarFavoritesWallas);
        btnPLayWalla                        = (Button)view.findViewById(R.id.btnPLayWalla);
        imgUser                             = (ImageView)view.findViewById(R.id.imageView13);
        txtTitle                            = (TextView)view.findViewById(R.id.txtTitle);
        txtSubTitle                         = (TextView)view.findViewById(R.id.txtSubTitle);
        txtCantPersons                      = (TextView)view.findViewById(R.id.txtCantPersons);
        txtNumberPersons                    = (TextView)view.findViewById(R.id.txtNumberPersons);
        txtDesc                             = (TextView)view.findViewById(R.id.txtDesc);

        mWaveSwipeRefreshLayout.setColorSchemeColors(Color.WHITE, Color.WHITE);
        mWaveSwipeRefreshLayout.setWaveColor(Color.parseColor("#e2156c"));
        mWaveSwipeRefreshLayout.setOnRefreshListener(this);

        btnPLayWalla.setTransformationMethod(null);
        txtTitle.setTypeface(TypesLetter.getSansBold(mainActivity));
        txtSubTitle.setTypeface(TypesLetter.getSansRegular(mainActivity));
        btnPLayWalla.setTypeface(TypesLetter.getSansRegular(mainActivity));
        txtNumberPersons.setTypeface(TypesLetter.getSansRegular(mainActivity));
        txtDesc.setTypeface(TypesLetter.getSansRegular(mainActivity));
        txtCantPersons.setTypeface(TypesLetter.getSansRegular(mainActivity), Typeface.BOLD);

        String genere = (GlobalData.isKey(Constant.GENERE,mainActivity))?GlobalData.get(Constant.GENERE, mainActivity):Constant.MAN;

        //  Log.d("app2you","genere: "+genere);

        imgUser.setImageResource(genere.equals(Constant.WOMAN)?R.drawable.hombre:R.drawable.mujer);


        btnPLayWalla.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mainActivity.replaceFragment(new FragmentGame(mainActivity));
            }
        });


		return view;
	}

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        if(GlobalData.isKey(Constant.REQUEST_WANT_TO_ME,mainActivity))
        {
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    loadWantToMe();
                }
            }, 260);

        }
        else {
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    getWallasOkhttp3(SHOW_DIALOG);
                }
            }, Constant.TIME_EXCUTE_FRAGMENT);

        }



    }

    /**
     * Cargar las personas que están en memoria que me quieren conocer
     */
    public void loadWantToMe()
    {
        String resultCards = GlobalData.get(Constant.REQUEST_WANT_TO_ME,mainActivity);
        //Log.d("app2you","quien te quiere conocer: " + resultCards);
        Gson gson = new Gson();
        MatchesData []datas = gson.fromJson(resultCards, MatchesData[].class);

        matchesDatas = new ArrayList<MatchesData>(Arrays.asList(datas));
        int size = matchesDatas.size();
        if (matchesDatas != null && size > 0) {

            String genere = (GlobalData.isKey(Constant.GENERE,mainActivity))?GlobalData.get(Constant.GENERE, mainActivity):Constant.MAN;
            String userStatus = (GlobalData.isKey(Constant.USER_STATUS,mainActivity))?GlobalData.get(Constant.USER_STATUS, mainActivity):"";
            if(genere.equals(Constant.WOMAN) /*|| userStatus.equals(Constant.GOLD_TRIAL)*/ || userStatus.equals(Constant.GOLD))
            {
                //normal
            }
            else
            {
                if(size == 1)
                {
                    //un solo registro
                    txtNumberPersons.setText(" "+"nueva persona le has gustado.");
                    txtDesc.setText("Descúbrela jugando a loovers o desbloqueala ahora.");
                }
                else
                {
                    //varios registros
                    txtNumberPersons.setText(" "+ "personas le gustas.");
                    txtDesc.setText("Descúbrelas jugando a loovers o desbloquealas ahora.");
                }
                txtCantPersons.setText(""+size);
                Visibility.visible(containerExplanedSection);
            }


            adapter = new GeneralAdapter(mainActivity, matchesDatas, Constant.WALLAS);
            mainActivity.gridview.setAdapter(adapter);
            Visibility.visible(mainActivity.gridview);
            Visibility.gone(mainActivity.containerMatchEmpty);

        } else {
            Visibility.gone(containerExplanedSection);
            Visibility.gone(mainActivity.gridview);
            Visibility.visible(mainActivity.containerMatchEmpty);
        }
    }


    /**
     * Metodo encargado de obtener los wallas que ha recibido un usuario
     */
    public void getWallasOkhttp3(final int option)
    {

        if(option == SHOW_DIALOG) {
            Visibility.visible(progressBarFavoritesWallas);
        }
        okhttp3.OkHttpClient client = new okhttp3.OkHttpClient();

        okhttp3.RequestBody formBody = new FormBody.Builder()
                .add("sessionToken", "" + GlobalData.get(Constant.SESSION_TOKEN, mainActivity))
                .add("serviceId", "" + Constant.WALLAS)
                .build();

        okhttp3.Request request = new okhttp3.Request.Builder()
                .url(Services.GET_MY_USERS)
                .post(formBody)
                .build();


        client.newCall(request).enqueue(new okhttp3.Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    Looper.prepare();

                    mainActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Common.errorMessage(mainActivity,"",mainActivity.getResources().getString(R.string.check_internet));
                            Visibility.gone(progressBarFavoritesWallas);
                            GlobalData.set(Constant.SERVICE_EXECUTION, "N", mainActivity);
                            mWaveSwipeRefreshLayout.setRefreshing(false);
                        }
                    });

                    Looper.loop();
                }

                @Override
                public void onResponse(Call call, final Response response) throws IOException {
                    final String result = response.body().string();
                    Looper.prepare();
                    if(response.isSuccessful()) {

                        mainActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    Gson gson = new Gson();
                                    final UserData userData = gson.fromJson(result, UserData.class);
                                    GlobalData.set(Constant.SERVICE_EXECUTION, "N", mainActivity);
                                    mWaveSwipeRefreshLayout.setRefreshing(false);
                                    if (userData.success) {
                                        //true
                                        matchesDatas = userData.results;
                                        int size = matchesDatas.size();
                                        if (matchesDatas != null && size > 0) {

                                            String genere = (GlobalData.isKey(Constant.GENERE, mainActivity)) ? GlobalData.get(Constant.GENERE, mainActivity) : Constant.MAN;
                                            String userStatus = (GlobalData.isKey(Constant.USER_STATUS, mainActivity)) ? GlobalData.get(Constant.USER_STATUS, mainActivity) : "";
                                            if (genere.equals(Constant.WOMAN) /*|| userStatus.equals(Constant.GOLD_TRIAL) */|| userStatus.equals(Constant.GOLD)) {
                                                //normal
                                            } else {
                                                if (size == 1) {
                                                    //un solo registro
                                                    txtNumberPersons.setText(" " + "nueva persona le has gustado.");
                                                    txtDesc.setText("Descúbrela jugando a loovers o desbloqueala ahora.");
                                                } else {
                                                    //varios registros
                                                    txtNumberPersons.setText(" " + "personas le gustas.");
                                                    txtDesc.setText("Descúbrelas jugando a loovers o desbloquealas ahora.");
                                                }
                                                txtCantPersons.setText("" + size);
                                                Visibility.visible(containerExplanedSection);
                                            }


                                            adapter = new GeneralAdapter(mainActivity, matchesDatas, Constant.WALLAS);
                                            mainActivity.gridview.setAdapter(adapter);
                                            Visibility.visible(mainActivity.gridview);
                                            Visibility.gone(mainActivity.containerMatchEmpty);

                                        } else {
                                            Visibility.gone(containerExplanedSection);
                                            Visibility.gone(mainActivity.gridview);
                                            Visibility.visible(mainActivity.containerMatchEmpty);
                                        }
                                        Visibility.gone(progressBarFavoritesWallas);
                                    } else {
                                        //false
                                        Toast.makeText(mainActivity, R.string.check_internet, Toast.LENGTH_SHORT).show();
                                    }

                                    Visibility.gone(progressBarFavoritesWallas);
                                } catch (Exception ex) {
                                    Visibility.gone(progressBarFavoritesWallas);
                                    ex.printStackTrace();
                                    Toast.makeText(mainActivity, R.string.error_reponse_server, Toast.LENGTH_SHORT).show();
                                } finally {
                                    response.body().close();
                                }
                            }
                        });

                    }
                    else
                        Toast.makeText(mainActivity, R.string.check_internet, Toast.LENGTH_SHORT).show();

                    Looper.loop();
                }
            });
    }
    @Override
    public void onPause() {
        super.onPause();

        if(matchesDatas != null && matchesDatas.size() > 0)
            GlobalData.set(Constant.REQUEST_WANT_TO_ME,new Gson().toJson(matchesDatas),mainActivity);
    }


    @Override
    public void onRefresh() {
        if(GlobalData.isKey(Constant.SERVICE_EXECUTION,mainActivity)) {
            if (!GlobalData.get(Constant.SERVICE_EXECUTION, mainActivity).equals("Y"))
                if(mainActivity.containerMatchEmpty.getVisibility() == View.GONE) {
                    GlobalData.set(Constant.SERVICE_EXECUTION, "Y", mainActivity);
                    getWallasOkhttp3(HIDE_DIALOG);
                }
        }
        else {

            if(mainActivity.containerMatchEmpty.getVisibility() == View.GONE) {
                GlobalData.set(Constant.SERVICE_EXECUTION, "Y", mainActivity);
                getWallasOkhttp3(HIDE_DIALOG);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        MyApplication.getInstance().trackScreenView("fragment wallas");
    }
}

