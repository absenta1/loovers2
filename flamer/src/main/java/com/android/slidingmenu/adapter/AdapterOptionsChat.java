package com.android.slidingmenu.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.slidingmenu.objects.ItemOptionChat;
import com.android.slidingmenu.util.TypesLetter;
import com.loovers.app.R;

import java.util.ArrayList;

/**
 * Created by Juan Martin Bernal on 4/4/16.
 */
public class AdapterOptionsChat extends ArrayAdapter<ItemOptionChat> {
    private Context context;
    private ArrayList<ItemOptionChat> values;

    public AdapterOptionsChat(Context context, ArrayList<ItemOptionChat> values) {
        super(context, R.layout.row_option_chat, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);


        View rowView = inflater.inflate(R.layout.row_option_chat, parent, false);
        TextView txtOption     = (TextView)rowView.findViewById(R.id.txtReason);
        ImageView imgOption    = (ImageView)rowView.findViewById(R.id.imgReason);
        final ItemOptionChat option = values.get(position);

        if(option != null) {
            imgOption.setImageResource(option.icon);
            txtOption.setText(""+option.name);
            txtOption.setTypeface(TypesLetter.getSansRegular(context));
        }

        return rowView;
    }

}