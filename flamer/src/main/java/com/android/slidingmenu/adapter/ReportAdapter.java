package com.android.slidingmenu.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.slidingmenu.reports.Report;
import com.android.slidingmenu.util.TypesLetter;
import com.loovers.app.R;

import java.util.ArrayList;

/**
 * Created by Juan Martin Bernal on 24/2/16.
 */
public class ReportAdapter extends ArrayAdapter<Report> {
    private  Context context;
    private  ArrayList<Report> values;

    public ReportAdapter(Context context, ArrayList<Report> values) {
        super(context, R.layout.row_report, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.row_report, parent, false);
        TextView txtReason = (TextView) rowView.findViewById(R.id.txtReason);
        ImageView imgReason = (ImageView) rowView.findViewById(R.id.imgReason);
        txtReason.setTypeface(TypesLetter.getSansRegular(context));
        Report report = values.get(position);

        if(report != null)
        {
            txtReason.setText(""+report.reason);
            imgReason.setImageResource(report.image);
        }



        return rowView;
    }
}