package com.appdupe.androidpushnotifications;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

import com.android.slidingmenu.MainActivity;
import com.android.slidingmenu.util.Common;
import com.android.slidingmenu.util.GlobalData;
import com.android.slidingmenu.util.Notificacion;
import com.android.slidingmenu.webservices.Services;
import com.android.vending.billing.IInAppBillingService;
import com.appdupe.flamer.utility.Constant;
import com.google.android.gms.gcm.GcmListenerService;
import com.loovers.app.R;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


/**
 * Service used for receiving GCM messages. When a message is received this service will log it.
 */
public class GcmService extends GcmListenerService {

    private static final String WALLA           = "like";
    private static final String MATCHES         = "match";
    private static final String FAVORITES       = "favorite";
    private static final String PREMIUM         = "premium";
    private static final String MESSAGES        = "message";
    private static final String NOTIFICATION    = "notification";
    private static final String EXPIRED_ACCOUNT = "expired";

    IInAppBillingService mService;

    ServiceConnection mServiceConn = new ServiceConnection() {
        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService = null;
        }

        @Override
        public void onServiceConnected(ComponentName name,
                                       IBinder service) {
            mService = IInAppBillingService.Stub.asInterface(service);
        }
    };

    @Override
    public void onCreate() {
        super.onCreate();

        Intent intent = new Intent("com.android.vending.billing.InAppBillingService.BIND");
        // This is the key line that fixed everything for me
        intent.setPackage("com.android.vending");
        bindService(intent, mServiceConn, Context.BIND_AUTO_CREATE);
    }

    public GcmService() {
    }

    @Override
    public void onMessageReceived(String from, Bundle extras) {

        sendNotification("Received GCM Message: " + extras.toString());

        String message              = (extras.containsKey("payload"))?extras.getString("payload"):"";
        String action               = (extras.containsKey("action"))?extras.getString("action"):"";
        String strMessageType       = (extras.containsKey("mt"))?extras.getString("mt"):"";
        String strMessageID         = (extras.containsKey("mid"))?extras.getString("mid"):"";
        String strSenderName        = (extras.containsKey("sname"))?extras.getString("sname"):"";
        String strDateTime          = (extras.containsKey("dt"))?extras.getString("dt"):"";
        String myId                 = (extras.containsKey("sfid"))?extras.getString("sfid"):"";
        String strSenderId          = (extras.containsKey("senderId"))?extras.getString("senderId"):"";
        String avatar               = (extras.containsKey("image"))?extras.getString("image"):"";
        String messageTypeId        = (extras.containsKey("messageTypeId"))?extras.getString("messageTypeId"):"";
        String imageUrl             = (extras.containsKey("imageUrl"))?extras.getString("imageUrl"):"";
        String picId                = (extras.containsKey("picId"))?extras.getString("picId"):"";
        String privatePic           = (extras.containsKey("privatePicStatusId"))?extras.getString("privatePicStatusId"):"";
        String chatStatusId         = (extras.containsKey("chatStatusId"))?extras.getString("chatStatusId"):"";
        String accountTypeId        = (extras.containsKey("accountTypeId"))?extras.getString("accountTypeId"):"";
        String type                 = (extras.containsKey("type"))?extras.getString("type"):"";
        String notification         = (extras.containsKey("notification"))?extras.getString("notification"):"";
        boolean requestToMe         = (extras.containsKey("requestToMe"))?extras.getBoolean("requestToMe"):false;

        Log.d("app2you","type: "+ type + " message: "+ message + " notification: "+ notification + "****** "+" accountId "+accountTypeId + " mi id: " +myId + " id otro usurio: "+ strSenderId);
        Log.d("app2you","chatstatusid: " +chatStatusId + " " + " messageTypeId: " + messageTypeId);
        if(type.equalsIgnoreCase(WALLA))
        {
            //Todo: queda pendiente el contador cada que llegue un "te quiere conocer" y pintarla en el menú

            GlobalData.delete(Constant.REQUEST_WANT_TO_ME,this);
            //from 2
            try {
                if(!accountTypeId.equals("")) {
                    if (Integer.parseInt(accountTypeId) > 2) {

                        if (GlobalData.isKey(Constant.NOTIFICATION_WALLAS, this))
                            if (GlobalData.get(Constant.NOTIFICATION_WALLAS, this).equals("Y"))
                                Notificacion.create(this, getResources().getString(R.string.app_name), getResources().getString(R.string.message_notification_like_text), "", R.drawable.logo_parejita_2, null, null, MainActivity.class, 2);

                    } else
                        GlobalData.set(Constant.COUNTER_WALLAS, "0", this);
                }

                //Intent i = new Intent("LOCATION_UPDATED");
                //i.putExtra("name", "" + strSenderName);
                //i.putExtra("type","1");
                //sendBroadcast(i);
            }
            catch(Exception ex)
            {
                ex.printStackTrace();
            }

        }

        else if(type.equalsIgnoreCase(MATCHES))
        {
            GlobalData.delete(Constant.REQUEST_MATCH,this);
            //from 3
            Bitmap bitmap = null;
            if(avatar != null)
                bitmap = Common.getBitmapFromURL(avatar);


            Log.d("app2you","match de "+strSenderName + " "+ strSenderId + " avatar: "+avatar );
            String tmp = (GlobalData.isKey(Constant.COUNTER_MATCHS,this))?GlobalData.get(Constant.COUNTER_MATCHS,this):null;
            String walla = (GlobalData.isKey(Constant.COUNTER_WALLAS,this))?GlobalData.get(Constant.COUNTER_WALLAS,this):"0";
            int numWallas = Integer.parseInt(walla);

            String countWalla =  String.valueOf((numWallas > 0) ? numWallas - 1 : "0");

            GlobalData.set(Constant.COUNTER_WALLAS, "" + countWalla, this);

            String countMatch = String.valueOf((tmp != null) ? Integer.parseInt(tmp) + 1 : "1");
            GlobalData.set(Constant.COUNTER_MATCHS, "" + countMatch, this);

            Intent i = new Intent("LOCATION_UPDATED");
            //i.putExtra("name", "" + strSenderName);
            i.putExtra("type","1");
            sendBroadcast(i);

            if(GlobalData.isKey(Constant.NOTIFICATION_MATCHES, this))
                if(GlobalData.get(Constant.NOTIFICATION_MATCHES,this).equals("Y"))
                    Notificacion.create(this, getResources().getString(R.string.app_name),getResources().getString(R.string.message_notification_match_text), "", R.drawable.logo_parejita_2, bitmap, null, MainActivity.class,3);
        }
        else if(type.equalsIgnoreCase(FAVORITES))
        {
            GlobalData.delete(Constant.REQUEST_FAVORITES,this);

            //from 4 apagado temporalmente
                    /*Log.d("app2you","favorito de "+strSenderName + " "+ strSenderId );
                    String tmp = (GlobalData.isKey(Constant.COUNTER_FAVORITES,this))?GlobalData.get(Constant.COUNTER_FAVORITES,this):null;
                    String countFavorite = String.valueOf((tmp != null) ? Integer.parseInt(tmp) + 1 : "1");
                    GlobalData.set(Constant.COUNTER_FAVORITES,""+countFavorite,this);

                    Notificacion.create(this, strSenderName +" te hizo favorito", "favorite", "", R.drawable.ic_launcher_wallamatch, null, null, MainActivity.class,4);*/
        }
        else if(type.equalsIgnoreCase(PREMIUM))
        {
            /*Intent i = new Intent("LOCATION_UPDATED");
            i.putExtra("type","3");
            sendBroadcast(i);*/
            verifiedSubsProduct();
        }
        else if(type.equalsIgnoreCase(EXPIRED_ACCOUNT))
        {
            //expiro la cuenta del usuario cambiar estado
           // Log.d("app2you","entro aqui expired: " + accountTypeId );

            GlobalData.set(Constant.USER_STATUS,""+accountTypeId,this);
        }
        else if(type.equalsIgnoreCase(NOTIFICATION))
        {
            Log.d("app2you","notification : " + message );
            Notificacion.create(this, getResources().getString(R.string.app_name), getResources().getString(R.string.app_name), "", R.drawable.logo_parejita_2, null, null, MainActivity.class,99);
        }
        else if(type.equalsIgnoreCase(MESSAGES))
        {
            //message 1
            if(!chatStatusId.equals(""+Constant.REJECT_CHAT_DIRECT)) {
                String tmp = (GlobalData.isKey(Constant.COUNTER_MESSAGES, this)) ? GlobalData.get(Constant.COUNTER_MESSAGES, this) : "1";
                String countMessage = String.valueOf((tmp != null) ? Integer.parseInt(tmp) + 1 : "1");
                GlobalData.set(Constant.COUNTER_MESSAGES, "" + countMessage, this);
            }

            Intent homeIntent = new Intent("com.embed.anddroidpushntificationdemo11.push");

            homeIntent.putExtra("MESSAGE_FOR_PUSH", message);
            homeIntent.putExtra("MESSAGE_FOR_PUSH_ACTION", "");
            homeIntent.putExtra("MESSAGE_FOR_PUSH_MESSAGETYPE", strMessageType);
            homeIntent.putExtra("MESSAGE_FOR_PUSH_MESSAGEID", strMessageID);
            homeIntent.putExtra("MESSAGE_FOR_PUSH_SENDERNAME", strSenderName);
            homeIntent.putExtra("MESSAGE_FOR_PUSH_DATETIME", strDateTime);
            homeIntent.putExtra("MESSAGE_FOR_PUSH_FACEBOOKID", strSenderId);
            homeIntent.putExtra("MESSAGE_FOR_PUSH_SENDERID", strSenderId);

            //tipo de mensaje
            homeIntent.putExtra("MESSAGE_FOR_PUSH_MESSAGE_TYPE",messageTypeId);
            homeIntent.putExtra("MESSAGE_FOR_PUSH_MESSAGE_IMAGE",imageUrl);
            homeIntent.putExtra("MESSAGE_FOR_PUSH_MESSAGE_PIC_ID",picId);
            homeIntent.putExtra("MESSAGE_FOR_PUSH_MESSAGE_PRIVATE_PIC",privatePic);

            //request chat
            homeIntent.putExtra("MESSAGE_FOR_PUSH_MESSAGE_CHAT_STATUS_ID",chatStatusId);
            homeIntent.putExtra("MESSAGE_REQUEST_TO_ME",requestToMe);

            Log.d("app2youParamsMessage","messageType :"+ messageTypeId + " image: " + imageUrl + " picId: " + picId + " privatePic: " + privatePic);
            sendBroadcast(homeIntent);


            Intent i = new Intent("LOCATION_UPDATED");
            i.putExtra("type", "0");
            i.putExtra("otherUserId", strSenderId);
            i.putExtra("lastestMessage", message);
            i.putExtra("date", strDateTime);
            i.putExtra("counter_messages", "1");
            sendBroadcast(i);



            if(GlobalData.isKey(Constant.NOTIFICATION_MESSAGES, this)) {
                if (GlobalData.get(Constant.NOTIFICATION_MESSAGES, this).equals("Y")) {
                    if (strSenderId.equals(Constant.ID_WALLAMATCH))
                        Notificacion.create(this, getResources().getString(R.string.app_name), message, "", R.drawable.logo_parejita_2, null, null, MainActivity.class, 1);
                    else {
                        if(chatStatusId.equals(""+Constant.ACCEPT_CHAT_DIRECT) && messageTypeId.equals(Constant.REQUEST_CHAT))
                            Notificacion.create(this, getResources().getString(R.string.app_name), strSenderName + " acepto tu solicitud de chat.", "", R.drawable.logo_parejita_2, null, null, MainActivity.class, 1);
                        else if(chatStatusId.equals(""+Constant.REJECT_CHAT_DIRECT) && messageTypeId.equals(Constant.REQUEST_CHAT))
                        {
                            //
                        }
                        else
                            Notificacion.create(this, getResources().getString(R.string.app_name), getResources().getString(R.string.message_notification_message_text) + " " + strSenderName, "", R.drawable.logo_parejita_2, null, null, MainActivity.class, 1);
                    }

                }
            }

        }//close type messages

    }

    @Override
    public void onDeletedMessages() {
        sendNotification("Deleted messages on server");
    }

    @Override
    public void onMessageSent(String msgId) {
        sendNotification("Upstream message sent. Id=" + msgId);
    }

    @Override
    public void onSendError(String msgId, String error) {
        sendNotification("Upstream message send error. Id=" + msgId + ", error" + error);
    }

    // Put the message into a notification and post it.
    // This is just one simple example of what you might choose to do with
    // a GCM message.
    private void sendNotification(String msg) {
        //logger.log(Log.INFO, msg);
        Log.d("app2you","mensaje recibido: " + msg);
    }

    /**
     * Método encargado de obtener la información de las subscripciones del usuario
     */
    public void verifiedSubsProduct() {
        try {

            Bundle subsDetails = mService.getPurchases(3,"com.appwallamatch.app","subs",null);
            int response = subsDetails.getInt("RESPONSE_CODE");
            if (response == 0) {
                ArrayList<String>  purchaseDataList = subsDetails.getStringArrayList("INAPP_PURCHASE_DATA_LIST");
                sendDataSubscriptionToServer(purchaseDataList.toString());
            }
            else
            {
                sendDataSubscriptionToServer(""+response);
            }

        } catch (RemoteException e) {
            e.printStackTrace();
            sendDataSubscriptionToServer("error al traer datos de la compra");
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mServiceConn != null) {
            unbindService(mServiceConn);
        }

    }

    /**
     * Método encargado de enviar información de los productos adquiridos por el usuario
     * @param data
     */
    public void sendDataSubscriptionToServer(String data)
    {
        //implementar lógica

        OkHttpClient client = new OkHttpClient();
        RequestBody formBody = new FormBody.Builder()
                .add("sessionToken",GlobalData.isKey(Constant.SESSION_TOKEN,this)?GlobalData.get(Constant.SESSION_TOKEN, this):"")
                .add("data",""+data.toString())
                .build();

        Request request = new Request.Builder()
                .url(Services.UPDATE_PURCHASE_DATA)
                .post(formBody)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                final String result = response.body().string();
                Log.d("app2you", "respuesta: " + result);
            }
        });
    }

}