package com.android.slidingmenu.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.slidingmenu.util.TypesLetter;
import com.loovers.app.R;

/**
 * Created by Juan Martín Bernal on 29/4/16.
 */
public class FragmentAdvantages extends Fragment {

    public static final String EXTRA_TITLE      = "EXTRA_TITLE";
    public static final String EXTRA_MESSAGE    = "EXTRA_MESSAGE";
    public static final String EXTRA_IMAGE      = "EXTRA_IMAGE";
    public TextView txtAdvantagesGold;

    public FragmentAdvantages(){}

    public static final FragmentAdvantages newInstance(String title, String message,int image)
    {
        FragmentAdvantages f = new FragmentAdvantages();
        Bundle bdl           = new Bundle();
        bdl.putString(EXTRA_TITLE, title);
        bdl.putString(EXTRA_MESSAGE, message);
        bdl.putInt(EXTRA_IMAGE,image);
        f.setArguments(bdl);
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        String title                    = getArguments().getString(EXTRA_TITLE);
        String message                  = getArguments().getString(EXTRA_MESSAGE);
        int image                       = getArguments().getInt(EXTRA_IMAGE);
        View v                          = inflater.inflate(R.layout.register_or_session_viewpager_page, container, false);
        ImageView imageAdvantagesGold   = (ImageView)v.findViewById(R.id.imageAdvantagesGold);
        TextView txtTitleGold           = (TextView)v.findViewById(R.id.txtTitleGold);
        txtAdvantagesGold               = (TextView)v.findViewById(R.id.txtAdvantagesGold);

        txtTitleGold.setText(title);
        txtAdvantagesGold.setText(message);
        imageAdvantagesGold.setImageResource(image);
        txtAdvantagesGold.setTypeface(TypesLetter.getSansRegular(getActivity()));
        txtTitleGold.setTypeface(TypesLetter.getSansBold(getActivity()));


        return v;
    }
    public void startAnimation()
    {
        Animation animation = new TranslateAnimation(500, 0,0, 0);
        animation.setDuration(800);
        animation.setFillAfter(true);
        txtAdvantagesGold.startAnimation(animation);
    }

}