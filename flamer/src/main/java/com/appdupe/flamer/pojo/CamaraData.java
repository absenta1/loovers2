package com.appdupe.flamer.pojo;

import com.google.gson.annotations.SerializedName;

public class CamaraData
{
    @SerializedName("success")
    public Boolean success;

	@SerializedName("errorMsg")
	public String errorMsg;

    @SerializedName("errorNum")
	public int errNum;

    @SerializedName("errFlag")
	public int errFlag;

    @SerializedName("status")
    public int status;

    @SerializedName("picture")
    public int picture;

}
