package com.android.slidingmenu.util;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;

import com.loovers.app.R;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

/**
 * Created by Juan Martin Bernal on 16/11/15.
 */
public class Notificacion {
    // A integer, that identifies each notification uniquely
    public static final int NOTIFICATION_ID = 1;
    public static final int NOTIFICATION_MESSAGE = 8961;
    public static final int NOTIFICATION_ID_MATCH = 7459;
    private static NotificationManager notificationManager;

    /**
     *
     * @param cxt
     * @param title
     * @param smallText
     * @param subText
     * @param smallIcon
     * @param largeIcon
     * @param sound
     * @param activity
     */
    public static void create(Context cxt, String title, String smallText, String subText, int smallIcon, Bitmap largeIcon,Uri sound ,Class activity, int from) {

        // Use NotificationCompat.Builder to set up our notification.
        NotificationCompat.Builder builder = new NotificationCompat.Builder(cxt);

        //icon appears in device notification bar and right hand corner of notification

        builder.setSmallIcon(R.drawable.icono_ticker_noti);

        builder.setTicker(cxt.getResources().getString(R.string.app_name));
       // builder.setSubText("sub text");

        // This intent is fired when notification is clicked
        if(activity != null) {
            Intent intent = new Intent(cxt, activity);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);

            if(from == 99) {
                GlobalData.set("from",""+from, cxt);
            }
            else {
                Log.d("app2you","creo el from con id: " + from);
                intent.putExtra("from", from);
                GlobalData.set("from",""+from, cxt);

            }
            PendingIntent pendingIntent = PendingIntent.getActivity(cxt, from, intent, 0);
            // Set the intent that will fire when the user taps the notification.
            builder.setContentIntent(pendingIntent);
        }

        // Large icon appears on the left of the notification
        if(largeIcon != null) {
            /*float multiplier= getImageFactor(cxt.getResources());
            largeIcon=Bitmap.createScaledBitmap(largeIcon, (int)(largeIcon.getWidth()*multiplier), (int)(largeIcon.getHeight()*multiplier), false);

            Log.d("app2you", "entro noti bitmap");*/
            builder.setLargeIcon(largeIcon);
            //builder.setStyle(new NotificationCompat.BigPictureStyle().bigPicture(largeIcon));
        }


        // Content title, which appears in large type at the top of the notification
        builder.setContentTitle(""+title);

        // Content text, which appears in smaller text below the title
        String messageSmallText = smallText.trim();
        try {
            messageSmallText = URLDecoder.decode(messageSmallText, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        builder.setContentText(""+ Html.fromHtml(messageSmallText));

        //builder.setStyle(new NotificationCompat.BigPictureStyle(new NotificationCompat.Builder()))

        // The subtext, which appears under the text on newer devices.
        // This will show-up in the devices with Android 4.2 and above only

        if(subText != null) {
            String messageSubText = subText.trim();
            try {
                messageSubText = URLDecoder.decode(messageSubText, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            builder.setSubText("" + Html.fromHtml(messageSubText));
        }

        builder.setLights(Color.WHITE,1000,5000);

        builder.setAutoCancel(true);

        if(sound != null)
             builder.setSound(sound);

        NotificationManager notificationManager = (NotificationManager) cxt.getSystemService(Context.NOTIFICATION_SERVICE);

        // Will display the notification in the notification bar
       // if(from == 3)
         //   notificationManager.notify(NOTIFICATION_ID_MATCH,builder.build());
         //else
            notificationManager.notify(0, builder.build());
    }


    /**
     *
     * @param context
     * @param title
     * @param text
     * @param info
     * @param ticker

     * @param activity
     */
    public static void createImage(Context context, String title, String text,
                                   String info, String ticker, Bitmap largeIcon, int smallIcon,
                                   Class<?> activity, Uri soundUri, String message, Bitmap aBigBitmap) {
        try {


                notificationManager = (NotificationManager) context
                        .getSystemService(Context.NOTIFICATION_SERVICE);
                NotificationCompat.Builder builder = new NotificationCompat.Builder(
                        context);


                builder.setSmallIcon(smallIcon)
                        .setLargeIcon(largeIcon)
                        .setContentTitle(title).setContentText(text)

                        .setContentInfo(info).setTicker(ticker)
                        .setLights(Color.YELLOW, 1000, 5000)

                        .setAutoCancel(true);

                if(aBigBitmap != null)
                {
                    builder.setStyle(new NotificationCompat.BigPictureStyle().bigPicture(aBigBitmap).setBigContentTitle(title).setSummaryText(text));
                }
                else
                {
                    builder.setContentTitle(title);
                    builder.setStyle(new NotificationCompat.BigTextStyle().bigText(message));
                }



                 if(soundUri != null)
                     builder.setSound(soundUri);


                if (activity != null) {
                    PendingIntent pendingIntent = PendingIntent.getActivity(
                            context, 0, new Intent(context, activity), 0);
                    builder.setContentIntent(pendingIntent);
                }

                notificationManager.notify(0, builder.build());


        } catch (Exception e) {
            Log.d("Exception: ", e.getMessage());
        }
    }
    public static float getImageFactor(Resources r){
        DisplayMetrics metrics = r.getDisplayMetrics();
        float multiplier=metrics.density/3f;
        return multiplier;
    }
}
