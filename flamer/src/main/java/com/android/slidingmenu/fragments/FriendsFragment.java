package com.android.slidingmenu.fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.slidingmenu.Analitycs.MyApplication;
import com.android.slidingmenu.MainActivity;
import com.android.slidingmenu.adapter.FriendsAdapter;
import com.android.slidingmenu.landings.DialogFragmentChat;
import com.android.slidingmenu.landings.DialogFragmentHighlightsPhoto;
import com.android.slidingmenu.landings.DialogFragmentShareFacebook;
import com.android.slidingmenu.nearyou.NearFragment;
import com.android.slidingmenu.objects.Store;
import com.android.slidingmenu.objects.UserChats;
import com.android.slidingmenu.store.DialogStore;
import com.android.slidingmenu.util.Common;
import com.android.slidingmenu.util.GlobalData;
import com.android.slidingmenu.util.Visibility;
import com.android.slidingmenu.webservices.Services;
import com.appdupe.androidpushnotifications.ChatActivity;
import com.appdupe.flamer.pojo.MatchesData;
import com.appdupe.flamer.utility.Constant;
import com.gitonway.lee.niftymodaldialogeffects.lib.Effectstype;
import com.gitonway.lee.niftymodaldialogeffects.lib.NiftyDialogBuilder;
import com.google.gson.Gson;
import com.loovers.app.R;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Random;

import jp.co.recruit_lifestyle.android.widget.WaveSwipeRefreshLayout;
import okhttp3.Call;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by Juan Martin Bernal on 23/09/15.
 */
public class FriendsFragment  extends Fragment {

    public View view;
    public ListView listFriends;
    public ArrayList<MatchesData> chats = new ArrayList<>();
    public ArrayList<MatchesData> chatsTmp;

    public static final int SHOW_DIALOG = 1;
    public static final int HIDE_DIALOG = 0;
    public MainActivity mainActivity;
    public AVLoadingIndicatorView progressBarAllChats;
    public WaveSwipeRefreshLayout mWaveSwipeRefreshLayout;
    //public String userId = "";

    public FriendsFragment(){}
    public FriendsFragment(MainActivity mainActivity)
    {
        this.mainActivity = mainActivity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.person_fragment,container, false);
        if(mainActivity == null)
            mainActivity = (MainActivity)getActivity();

        Visibility.visible(mainActivity.tvTitle);

        listFriends = (ListView)view.findViewById(R.id.listView);

        progressBarAllChats = (AVLoadingIndicatorView)view.findViewById(R.id.progressBarAllChats);

        mWaveSwipeRefreshLayout = (WaveSwipeRefreshLayout)view.findViewById(R.id.main_swipe);
        mWaveSwipeRefreshLayout.setColorSchemeColors(Color.WHITE, Color.WHITE);
        mWaveSwipeRefreshLayout.setWaveColor(Color.parseColor("#e2156c"));
       // userId = GlobalData.isKey(Constant.MY_USER_ID,mainActivity)?GlobalData.get(Constant.MY_USER_ID,mainActivity):"";

        return view;
    }



    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        GlobalData.set(Constant.COUNTER_MESSAGES,"0",mainActivity);

        listFriends.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int positon, long l) {
                MatchesData matchesData     = (MatchesData) listFriends.getItemAtPosition(positon);
                if(matchesData.userId != null && matchesData.userId.equals(Constant.PROMOTION_ID))
                {
                    //Common.errorMessage(mainActivity,"","Abrir promoción");
                    logicPromotions(matchesData.featureId);
                }
                else {
                    matchesData.count_messages = "0";
                    matchesData.userId = matchesData.id_user;
                    matchesData.firstName = matchesData.name;
                    matchesData.avatarUrl = matchesData.picture;

                    ChatActivity dialog = new ChatActivity(mainActivity, matchesData, Constant.MATCH, mainActivity.friendsAdapter, 1);
                    dialog.show(mainActivity.mFragmentManager, "ChatActivity.TAG");
                }

            }
        });
        listFriends.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                MatchesData matchesData = (MatchesData) listFriends.getItemAtPosition(position);
                RelativeLayout container = (RelativeLayout)view.findViewById(R.id.containerRowChat);
                if(matchesData.userId != null && !matchesData.userId.equals(Constant.PROMOTION_ID)){}
                else if(!matchesData.id_user.equals(Constant.ID_SUPPORT) && !matchesData.id_user.equals(Constant.ID_WALLAMATCH) )
                {
                    if(matchesData.isMatch == 1)
                        dialogDeleteRelation(mainActivity.getString(R.string.safe_delete_match)+" "+matchesData.name+"?",Constant.DELETE_MATCH,matchesData.id_user,position,container);
                    else
                        dialogDeleteRelation(mainActivity.getString(R.string.safe_delete_direct_chat)+" "+ matchesData.name+"?",Constant.DELETE_DIRECT_CHAT,matchesData.id_user,position,container);

                }


                return true;
            }
        });


        mWaveSwipeRefreshLayout.setOnRefreshListener(new WaveSwipeRefreshLayout.OnRefreshListener() {
            @Override public void onRefresh() {
                // Do work to refresh the list here.
                if (!GlobalData.isKey(Constant.SERVICE_EXECUTION,mainActivity)) {
                    GlobalData.set(Constant.SERVICE_EXECUTION,"Y",mainActivity);
                    getAllChats(HIDE_DIALOG);
                }
            }
        });

    }

    /**
     * Lógica para cuando seleccione una de las caracteristicas - promociones
     * @param featureId
     */
    public void logicPromotions(int featureId)
    {
        switch (featureId)
        {
            case Constant.FEATURE_1:
                if(!GlobalData.isKey(Constant.SHARE_FACEBOOK,mainActivity)) {
                    DialogFragmentShareFacebook dialogFragmentShareFacebook = new DialogFragmentShareFacebook(mainActivity);
                    dialogFragmentShareFacebook.show(mainActivity.mFragmentManager, "DialogFragmentShareFacebook.TAG");
                }
                else
                    Common.errorMessage(mainActivity,"",""+mainActivity.getString(R.string.share_facebook_done));
                break;
            case Constant.FEATURE_2:
                String status = GlobalData.get(Constant.USER_STATUS,mainActivity);

                if (status.equals(Constant.NOT_GOLD_BEFORE_TRIAL_NO_PICTURE))
                {
                    final NiftyDialogBuilder dialog =  NiftyDialogBuilder.getInstance(mainActivity);
                    dialog.withTitle(null);
                    dialog.withMessage(null);
                    dialog.isCancelableOnTouchOutside(false);
                    dialog.withDialogColor(Color.TRANSPARENT);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    dialog.withDuration(Constant.DURATION_ANIMATION);
                    dialog.withEffect(Effectstype.Shake);
                    dialog.setCustomView(R.layout.modal_general,mainActivity);

                    TextView title = (TextView) dialog.findViewById(R.id.title);
                    TextView text = (TextView) dialog.findViewById(R.id.text);
                    title.setText(Common.getNickName(mainActivity));
                    text.setText(""+mainActivity.getString(R.string.found_near_person_upload_photo));

                    Button exit = (Button) dialog.findViewById(R.id.exit);
                    exit.setText(mainActivity.getResources().getString(R.string.exit));
                    Button accept = (Button) dialog.findViewById(R.id.accept);
                    // if button is clicked, close the custom dialog
                    accept.setText(mainActivity.getResources().getString(R.string.update_photo));
                    exit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });
                    accept.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();

                            mainActivity.replaceFragment(new FragmentProfile(mainActivity));
                        }
                    });

                    dialog.show();
                }
                else {
                    NearFragment nearFragment = new NearFragment(mainActivity);

                    if (nearFragment != null)
                        mainActivity.replaceFragment(nearFragment);


                }
                break;
            case Constant.FEATURE_3://Destaca tu foto sobre las demás
                DialogFragmentHighlightsPhoto dialogFragmentHighlightsPhoto = new DialogFragmentHighlightsPhoto(mainActivity);
                dialogFragmentHighlightsPhoto.show(mainActivity.mFragmentManager,"DialogFragmentHighlightsPhoto.TAG");

                break;
            case Constant.FEATURE_4://se el más popular
                goToStore();
                break;
            case Constant.FEATURE_5:
                mainActivity.replaceFragment(new FragmentProfile(mainActivity,1));
                break;
            case Constant.FEATURE_6:

                try {
                    mainActivity.getPackageManager().getPackageInfo("com.facebook.katana", 0);
                    mainActivity.startActivity( new Intent(Intent.ACTION_VIEW, Uri.parse("fb://facewebmodal/f?href=https://www.facebook.com/weareloovers/?fref=ts")));
                } catch (Exception e) {
                    mainActivity.startActivity( new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/weareloovers/?fref=ts")));
                }

                break;
            case Constant.FEATURE_7://Chatea con las más populares
                DialogFragmentChat dialogFragmentChat = new DialogFragmentChat(mainActivity);
                dialogFragmentChat.show(mainActivity.mFragmentManager,"DialogFragmentChat.TAG");
                break;
            case Constant.FEATURE_8:
                if (!GlobalData.isKey(Constant.RATE_APP, mainActivity))
                    mainActivity.openDialogRateWallamatch();
                break;
            default:
                break;
        }
    }
    /**
     * Dialogo para eliminar relación
     */
    public void dialogDeleteRelation(String message, final int relationId,final String userId, final int position, final RelativeLayout container) {
        // custom dialog
        final Dialog dialog =  new Dialog(mainActivity,R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        dialog.setContentView(R.layout.modal_general);

        TextView text = (TextView) dialog.findViewById(R.id.text);
        text.setText(""+message);


        Button exit = (Button) dialog.findViewById(R.id.exit);
        Button accept = (Button) dialog.findViewById(R.id.accept);
        // if button is clicked, close the custom dialog
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

                deleteRelationship(""+relationId,userId,position,container);

            }
        });

        dialog.show();

    }
    /**
     * Metodo encargado de eliminar la relación de chat Mathc o  chat directo
     * @param userId
     * @param position
     * @param container
     */
    public void deleteRelationship(final String relationId, final String userId, final int position, final RelativeLayout container)
    {
        final ProgressDialog progressDialog = new ProgressDialog(mainActivity);
        progressDialog.setMessage(""+mainActivity.getResources().getString(R.string.delete_relation));
        progressDialog.setCancelable(false);
        progressDialog.show();
        OkHttpClient client = new OkHttpClient();
        RequestBody formBody = new FormBody.Builder()
                .add("sessionToken", "" + GlobalData.get(Constant.SESSION_TOKEN, mainActivity))
                .add("serviceId", relationId )
                .add("otherUserId",userId)
                .build();

        Request request = new Request.Builder()
                .url(Services.DELETE_RELATIONSHIP)
                .post(formBody)
                .build();

        client.newCall(request).enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                progressDialog.dismiss();
                Looper.prepare();
                mainActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Common.errorMessage(mainActivity, "", mainActivity.getResources().getString(R.string.check_internet));
                    }
                });

                Looper.loop();
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                progressDialog.dismiss();
                final String result = response.body().string();
                //  Log.d("app2you", "respuesta eliminar wallas: " + result);

                if(response.isSuccessful()) {
                    Looper.prepare();
                    mainActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            JSONObject json = null;
                            try {
                                json = new JSONObject(result);

                                String success = json.getString("success");
                                if (success.equals("true")) {
                                    //al ingresar a cerca de mi se piden nueva mente
                                    GlobalData.delete("listPersons", mainActivity);
                                    GlobalData.delete("listnewpersons", mainActivity);
                                    GlobalData.delete(Constant.REQUEST_MATCH, mainActivity);

                                    Animation scale = AnimationUtils.loadAnimation(mainActivity, R.anim.scale_remove_view);
                                    scale.setAnimationListener(new Animation.AnimationListener() {
                                        @Override
                                        public void onAnimationStart(Animation animation) {

                                        }

                                        @Override
                                        public void onAnimationEnd(Animation animation) {

                                            chatsTmp.remove(position);
                                            if (mainActivity.friendsAdapter != null)
                                                mainActivity.friendsAdapter.notifyDataSetChanged();

                                        }

                                        @Override
                                        public void onAnimationRepeat(Animation animation) {

                                        }
                                    });
                                    container.startAnimation(scale);
                                } else {
                                    Common.errorMessage(mainActivity, "", mainActivity.getResources().getString(R.string.check_internet));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } finally {
                                response.body().close();
                            }
                        }
                    });

                    Looper.loop();
                }
                else
                    Toast.makeText(mainActivity, R.string.check_internet, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        MyApplication.getInstance().trackScreenView("fragment all chats");

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                getAllChats(SHOW_DIALOG);
            }
        }, Constant.TIME_EXCUTE_FRAGMENT);
        mainActivity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    }

    /**
     * obtiene los chats del usuario
     */
    public void getAllChats(int option)
    {
        if(option == SHOW_DIALOG)
            Visibility.visible(progressBarAllChats);

        OkHttpClient client = new OkHttpClient();

        RequestBody formBody = new FormBody.Builder()
                .add("sessionToken", "" + GlobalData.get(Constant.SESSION_TOKEN, mainActivity))

                .build();

        Request request = new Request.Builder()
                .url(Services.GET_ALL_CHATS)
                .post(formBody)
                .build();

        client.newCall(request).enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

                Looper.prepare();

                mainActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mWaveSwipeRefreshLayout.setRefreshing(false);
                        Visibility.gone(progressBarAllChats);
                        Common.errorMessage(mainActivity,"",mainActivity.getResources().getString(R.string.check_internet));
                        GlobalData.delete(Constant.SERVICE_EXECUTION,mainActivity);
                    }
                });

                Looper.loop();
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {

                final String result = response.body().string();
                Log.d("app2you", "respuesta req chats todo el listado  " + result);
                Looper.prepare();
                if(response.isSuccessful()) {

                    mainActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                mWaveSwipeRefreshLayout.setRefreshing(false);
                                GlobalData.delete(Constant.SERVICE_EXECUTION, mainActivity);
                                Gson gson = new Gson();
                                final UserChats userChats = gson.fromJson(result, UserChats.class);

                                if (userChats.success) {
                                    chats = userChats.chats;
                                    chatsTmp = new ArrayList<>();

                                    mainActivity.listRequestChat.clear();

                                    for (MatchesData chat : chats) {
                                        if (chat.chatTypeId == 1 || chat.id_user.equals(Constant.ID_SUPPORT) || chat.id_user.equals(Constant.ID_WALLAMATCH))
                                            chatsTmp.add(chat);
                                        else {
                                            mainActivity.listRequestChat.add(chat);

                                            int size = mainActivity.listRequestChat.size();
                                            if (size > 0) {
                                                Visibility.visible(mainActivity.badgeTab);
                                                mainActivity.badgeTab.setText("" + size);
                                            } else
                                                Visibility.gone(mainActivity.badgeTab);

                                        }
                                    }

                                    if (chatsTmp != null && chatsTmp.size() > 0) {
                                        Collections.sort(chatsTmp, Collections.reverseOrder());
                                        ArrayList<Store> stores = Common.loadPromotions(mainActivity);
                                        Store store = anyStore(stores);
                                        MatchesData matchesData = new MatchesData();
                                        matchesData.userId          = Constant.PROMOTION_ID;
                                        matchesData.featureId       = store.featureId;
                                        matchesData.imageResource   = store.image;
                                        matchesData.picture         = store.urlImage;
                                        matchesData.lastestMessage  = store.text;
                                        matchesData.name = "app";

                                        if(chatsTmp.size() > 2)
                                        {

                                            matchesData.date = chatsTmp.get(2).date;
                                            chatsTmp.add(2,matchesData);
                                        }
                                        else
                                        {
                                            final String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
                                            matchesData.date = date;
                                            chatsTmp.add(0,matchesData);
                                        }

                                        mainActivity.friendsAdapter = new FriendsAdapter(mainActivity, chatsTmp);
                                        listFriends.setAdapter(mainActivity.friendsAdapter);
                                        mainActivity.friendsAdapter.notifyDataSetChanged();
                                        Visibility.visible(listFriends);
                                    }


                                } else {
                                    Toast.makeText(mainActivity, R.string.try_again, Toast.LENGTH_SHORT).show();
                                }
                                Visibility.gone(progressBarAllChats);
                            } catch (Exception ex) {
                                Visibility.gone(progressBarAllChats);
                                ex.printStackTrace();
                                Toast.makeText(mainActivity, R.string.error_reponse_server, Toast.LENGTH_SHORT).show();
                            }
                            finally {
                                response.body().close();
                            }

                        }
                    });


                }
                else
                    Toast.makeText(mainActivity, R.string.error_reponse_server, Toast.LENGTH_SHORT).show();

                Looper.loop();
            }
        });
    }

    /**
     * Método encargado devolver un objecto aleatorio tipo store para mostrar en la lista
     * @param stores
     * @return
     */
    public Store anyStore(ArrayList<Store> stores)
    {
        Random randomGenerator = new Random();
        int index = randomGenerator.nextInt(stores.size());
        Store item = stores.get(index);
       // System.out.println("Managers choice this week" + item + "our recommendation to you");
        return item;
    }

    /**
     * Método encargado de abrir la tienda de la app
     */
    public void goToStore()
    {
        DialogStore dialogStore = new DialogStore(mainActivity,1);
        dialogStore.show(mainActivity.mFragmentManager,"DialogStore.TAG");
    }

}
