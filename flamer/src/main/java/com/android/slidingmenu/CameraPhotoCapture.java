package com.android.slidingmenu;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Looper;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.slidingmenu.interfaces.Iphotolistener;
import com.android.slidingmenu.util.AppAnimation;
import com.android.slidingmenu.util.Common;
import com.android.slidingmenu.util.ConnectionDetector;
import com.android.slidingmenu.util.GlobalData;
import com.android.slidingmenu.util.ReceiveUriScaledBitmapTask;
import com.android.slidingmenu.util.TypesLetter;
import com.android.slidingmenu.util.Visibility;
import com.android.slidingmenu.webservices.Services;
import com.appdupe.flamer.pojo.CamaraData;
import com.appdupe.flamer.utility.Constant;
import com.google.gson.Gson;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.soundcloud.android.crop.Crop;
import com.loovers.app.R;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Date;

import okhttp3.Call;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.Response;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseView;

public class CameraPhotoCapture extends Activity implements Iphotolistener ,ReceiveUriScaledBitmapTask.ReceiveUriScaledBitmapListener{

    public  static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 1;
    public  static final int CAPTURE_GALLERY = 2345;
    public static final int CROP_PIC_REQUEST_CODE = 5698;
    public static final String TEMP_PHOTO_FILE = "temporary_holder.jpg";
    public static final int  MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 21;


    public Uri imageUri                      = null;
    public static final int SIZE_IMAGE  = 640;//1280
    public ImageView showImg ;
    public CameraPhotoCapture CameraActivity = null;

    private String picturePath = "" ;
    private String token;
    private int type;
    private Button upload;
    private ImageView btn_rotate;
    public Bitmap resizedBitmap;
    public int temporal = 100;
    public int openPhotoMode = 100;
    private static final MediaType MEDIA_TYPE_JPG = MediaType.parse("image/jpg");
    public ProgressWheel progressBarRotateImage;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.camera_activity);
        CameraActivity = this;
        token = GlobalData.get(Constant.SESSION_TOKEN, CameraPhotoCapture.this);//Settings.getToken(this);


        showImg                 = (ImageView) findViewById(R.id.showImg);
        progressBarRotateImage  = (ProgressWheel)findViewById(R.id.progressBarRotateImage);
        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            type = extras.getInt("TYPE");
            openPhotoMode = extras.getInt("OPEN");
            // and get whatever type user account id is
        }


        Button photo = (Button) findViewById(R.id.photo);
        Button gallery = (Button) findViewById(R.id.gallery);
        btn_rotate = (ImageView)findViewById(R.id.btn_rotate);

        upload = (Button) findViewById(R.id.upload);

        upload.setTypeface(TypesLetter.getSansRegular(CameraPhotoCapture.this));
        photo.setTypeface(TypesLetter.getSansRegular(CameraPhotoCapture.this));
        gallery.setTypeface(TypesLetter.getSansRegular(CameraPhotoCapture.this));

        upload.setVisibility(View.GONE);
        Visibility.gone(btn_rotate);

        gallery.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                openGallery();

            }
        });

        photo.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {

                if (ActivityCompat.checkSelfPermission(CameraPhotoCapture.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED&& ActivityCompat.checkSelfPermission(CameraPhotoCapture.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ) {
                    ActivityCompat.requestPermissions( CameraPhotoCapture.this, new String[] {  android.Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA  },MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
                }
                else {
                    openCamera();
                }


            }

        });
        upload.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {


                if(ConnectionDetector.isConnectingToInternet(CameraPhotoCapture.this)) {
                    //Log.d("app2you", "ruta de la imagen a subir: " + picturePath);
                    if (!picturePath.equals("")) {
                         //BackGroundTaskForUploadPicture uploadService;

                        if (type == Constant.TYPE_PROFILE) {
                            BackGroundTaskForUploadPicture uploadServiceProfile = new BackGroundTaskForUploadPicture(picturePath, token, true,Constant.PUBLIC_PHOTO);
                            uploadServiceProfile.setListener(CameraActivity);
                            uploadServiceProfile.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                        } else {
                            //pregunta el tipo de foto de galeria que desea subir privada o normal
                            final Dialog dialog =  new Dialog(CameraPhotoCapture.this,R.style.PauseDialog);
                            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before

                            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                            dialog.setContentView(R.layout.modal_general);

                            TextView title = (TextView)dialog.findViewById(R.id.title);
                            TextView text = (TextView) dialog.findViewById(R.id.text);
                            title.setText("Privacidad");
                            text.setText("Define la privacidad para está foto.\nLas fotos privadas, solo las podrán ver quien tu quieras.");


                            Button exit = (Button) dialog.findViewById(R.id.exit);
                            exit.setText("PÚBLICA");
                            Button accept = (Button) dialog.findViewById(R.id.accept);
                            accept.setText("PRIVADA");
                            // if button is clicked, close the custom dialog
                            exit.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog.dismiss();
                                    BackGroundTaskForUploadPicture uploadService = new BackGroundTaskForUploadPicture(picturePath, token, false,Constant.PUBLIC_PHOTO);
                                    uploadService.setListener(CameraActivity);
                                    uploadService.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                                }
                            });
                            accept.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog.dismiss();
                                    BackGroundTaskForUploadPicture uploadService = new BackGroundTaskForUploadPicture(picturePath, token, false,Constant.PRIVATE_PHOTO);
                                    uploadService.setListener(CameraActivity);
                                    uploadService.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);


                                }
                            });

                            dialog.show();

                        }


                        /*if(type == Constant.TYPE_PROFILE) {
                            Bitmap bm = BitmapFactory.decodeFile(picturePath);

                            Bitmap scaledBitmap = scaleDown(bm, SIZE_IMAGE, true);

                            ByteArrayOutputStream bos = new ByteArrayOutputStream();
                            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bos);
                            byte[] data = bos.toByteArray();

                            uploadPictureOkhttp3(picturePath, data,true, CameraActivity);
                        }
                        else {
                            Bitmap bm = BitmapFactory.decodeFile(picturePath);

                            Bitmap scaledBitmap = scaleDown(bm, SIZE_IMAGE, true);

                            ByteArrayOutputStream bos = new ByteArrayOutputStream();
                            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bos);
                            byte[] data = bos.toByteArray();

                            uploadPictureOkhttp3(picturePath, data, false, CameraActivity);
                        }*/
                    }
                }
                else
                    Common.errorMessage(CameraPhotoCapture.this,"",getResources().getString(R.string.check_internet));
            }
        });

        if(openPhotoMode == 1)
        {
            if (ActivityCompat.checkSelfPermission(CameraPhotoCapture.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(CameraPhotoCapture.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions( CameraPhotoCapture.this, new String[] {  android.Manifest.permission.WRITE_EXTERNAL_STORAGE , Manifest.permission.CAMERA },MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
            }
            else {
                openGallery();
            }

        }
        else if(openPhotoMode == 2)
        {
            if (ActivityCompat.checkSelfPermission(CameraPhotoCapture.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(CameraPhotoCapture.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions( CameraPhotoCapture.this, new String[] {  android.Manifest.permission.WRITE_EXTERNAL_STORAGE , Manifest.permission.CAMERA },MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
            }
            else {
                openCamera();
            }

        }
        btn_rotate.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                rotateImage();
            }
        });

    }

    /**
     * Método encargado de girar la imagen
     */
    public void rotateImage()
    {
        AppAnimation.scale(CameraPhotoCapture.this,btn_rotate);
        new AsynckRotateImage().execute();
       /* if(resizedBitmap != null)
        {
            resizedBitmap = Common.rotateImage(resizedBitmap,90);
            showImg.setImageBitmap(resizedBitmap);
            File dest = new File(picturePath);
            FileOutputStream out = null;
            try {
                out = new FileOutputStream(dest);
                resizedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
                out.flush();
                out.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }*/
    }
    public class AsynckRotateImage extends AsyncTask<String, String, String>
    {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Visibility.visible(progressBarRotateImage);
        }

        @Override
        protected String doInBackground(String... params) {

            if(resizedBitmap != null)
                resizedBitmap = Common.rotateImage(resizedBitmap,90);

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            showImg.setImageBitmap(resizedBitmap);
            File dest = new File(picturePath);
            FileOutputStream out = null;
            try {
                out = new FileOutputStream(dest);
                resizedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
                out.flush();
                out.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            Visibility.gone(progressBarRotateImage);
        }
    }

    /**
     * Método para abrir la camara del dispositivo
     */
    private void openCamera() {
        /*************************** Camera Intent Start ************************/

        // Define the file-name to save photo taken by Camera activity

        String fileName = "Camera_Example.jpg";

        // Create parameters for Intent with filename

        ContentValues values = new ContentValues();

        values.put(MediaStore.Images.Media.TITLE, fileName);

        values.put(MediaStore.Images.Media.DESCRIPTION,"Image capture by camera");

        // imageUri is the current activity attribute, define and save it for later usage

        imageUri = getContentResolver().insert(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

        /**** EXTERNAL_CONTENT_URI : style URI for the "primary" external storage volume. ****/


        // Standard Intent action that can be sent to have the camera
        // application capture an image and return it.

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE );
        //  Uri imageUri = getOutputMediaFileUri();

        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);

        //intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);

        intent.putExtra("return-data", true);

        startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);

        /*************************** Camera Intent End ************************/

    }

    /**
     * Método para abrir la galeria del dispositivo
     */
    private void openGallery() {
        try
        {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult( intent, CAPTURE_GALLERY );

            } else {
                showKitKatGallery();
            }
        }
        catch ( ActivityNotFoundException e )
        {
            e.printStackTrace();
        }
    }


    /**
     * Método para abrir galeria de la version KitKat Android (4.4)
     */
    private void showKitKatGallery() {
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("image/*");
        startActivityForResult(intent, CAPTURE_GALLERY);

    }

    @Override
    public void onUriScaledBitmapReceived(Uri uri) {
        startCropActivity(uri);
    }
    private void startCropActivity(Uri originalUri) {
        imageUri = Uri.fromFile(getTempFile());
        new Crop(originalUri).output(imageUri).asSquare()
                .start(this);
    }

    /**
     * Método que define el archivo temporal donde se guardara la imagen
     * @return
     */
    public static File getTempFile() {

        if (Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {

            File file = new File(Environment.getExternalStorageDirectory(),
                    TEMP_PHOTO_FILE);
            try {
                file.createNewFile();
            } catch (IOException e) {
            }

            return file;
        } else {

            return null;
        }
    }

    /**
     * Clase encargada de procesar la imagen y mostrarla en la vista.
     */
    class AsyncTaskProcessImage extends AsyncTask<String, String, String>
    {
        public Uri imageUri;
        public String path;

        public ProgressDialog progressDialog;
        public boolean status = true;
        public int from = 0;

        public AsyncTaskProcessImage(Uri imageUri, String path)
        {
            this.imageUri  = imageUri;
            this.path = path;
        }
        public AsyncTaskProcessImage(Uri imageUri, String path, int from)
        {
            this.imageUri  = imageUri;
            this.path = path;
            this.from = from;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(CameraPhotoCapture.this,R.style.CustomDialogTheme);
            progressDialog.setCancelable(false);
           // progressDialog.setMessage(getResources().getString(R.string.proccessing_image));
            progressDialog.show();
            progressDialog.setContentView(R.layout.custom_progress_dialog);
            TextView txtView = (TextView) progressDialog.findViewById(R.id.txtProgressDialog);
            txtView.setText(""+getResources().getString(R.string.proccessing_image));
        }

        @Override
        protected String doInBackground(String... strings) {

            try {
                //Uri selectedImage = data.getData();

                InputStream is = getContentResolver().openInputStream(imageUri);
                Bitmap bitmap = BitmapFactory.decodeStream(is);
                is.close();

                Date d = new Date();
                String filename = d.getTime() + ".jpg";
                File sd = Environment.getExternalStorageDirectory();
                File dest = new File(sd, filename);

                picturePath = dest.getPath();

                FileOutputStream out = new FileOutputStream(dest);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 95, out);

                int height = bitmap.getHeight();
                int width = bitmap.getWidth();

                float scaleWidth = ((float) 1024) / width;
                float newHeight = height * scaleWidth;

                float scaleHeight = ((float) newHeight) / height;
                Matrix matrix = new Matrix();

                matrix.postScale(scaleWidth, scaleHeight);
                //recreate the new Bitmap
                resizedBitmap = Common.decodeFile(picturePath, from);//Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, true);

                out.flush();
                out.close();


            } catch (Exception e) {
                e.printStackTrace();
                status = false;
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
            if(status) {
                if (resizedBitmap != null) {
                    showImg.setImageBitmap(resizedBitmap);
                    showImg.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    //Picasso.with(CameraPhotoCapture.this).load(new File(picturePath)).into(showImg);
                }
                else
                    Toast.makeText(CameraPhotoCapture.this, "error foto", Toast.LENGTH_SHORT).show();
            }
            else
                Toast.makeText(CameraPhotoCapture.this, R.string.try_again, Toast.LENGTH_SHORT).show();

        }
    }


    @Override
    protected void onActivityResult( int requestCode, int resultCode, Intent imageReturnedIntent)
    {
        //Toast.makeText(this,"de vuelta " + resultCode,Toast.LENGTH_LONG).show();
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        //Log.d("loovers", "data: " + requestCode + " " + resultCode);

        if (requestCode == Crop.REQUEST_CROP) {
            handleCrop(resultCode, imageReturnedIntent);
        }
         else if ( requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {

            if ( resultCode == RESULT_OK) {
                upload.setVisibility(View.VISIBLE);
                Visibility.visible(btn_rotate);
                showCase(btn_rotate);
                if(imageUri != null) {

                    if(type == Constant.TYPE_PROFILE) {
                        temporal = 1;
                        startCropActivity(imageUri);
                    }
                    else
                        new AsyncTaskProcessImage(imageUri, picturePath,1).execute();
                }
                else {
                    showImg.setImageBitmap(null);
                    Visibility.gone(upload);
                    Visibility.gone(btn_rotate);
                    Toast.makeText(this, R.string.try_again_upload_photo, Toast.LENGTH_SHORT).show();
                }

            } else if ( resultCode == RESULT_CANCELED) {
                showImg.setImageBitmap(null);
                Visibility.gone(upload);
                Visibility.gone(btn_rotate);
                Toast.makeText(this, R.string.canceled_photo, Toast.LENGTH_SHORT).show();
            } else {
                showImg.setImageBitmap(null);
                Visibility.gone(upload);
                Visibility.gone(btn_rotate);
                Toast.makeText(this, R.string.canceled_photo, Toast.LENGTH_SHORT).show();
            }
        }
        else if ( requestCode == CAPTURE_GALLERY) {

            if ( resultCode == RESULT_OK) {

                Uri selectedImage = imageReturnedIntent.getData();
                if(selectedImage != null) {
                    upload.setVisibility(View.VISIBLE);
                    Visibility.visible(btn_rotate);
                    showCase(btn_rotate);
                    if(type == Constant.TYPE_PROFILE) {
                        temporal = 0;
                        startCropActivity(selectedImage);
                    }
                    else
                        new AsyncTaskProcessImage(selectedImage, picturePath,0).execute();
                }
                else {
                    Visibility.gone(upload);
                    Visibility.gone(btn_rotate);
                    Toast.makeText(this, R.string.try_again_upload_photo, Toast.LENGTH_SHORT).show();
                }
            }
            else if(resultCode == RESULT_CANCELED)
            {
                Toast.makeText(this, R.string.canceled_photo, Toast.LENGTH_SHORT).show();
                showImg.setImageBitmap(null);
                Visibility.gone(upload);
                Visibility.gone(btn_rotate);
            }
            else {
                Toast.makeText(this, R.string.canceled_photo, Toast.LENGTH_SHORT).show();
                Visibility.gone(upload);
                Visibility.gone(btn_rotate);
            }
        }
    }

    /**
     * Método encargado de llamar a la actividad para hacer recorte cuadrado de la imagen
     * @param resultCode
     * @param result
     */
    private void handleCrop(int resultCode, Intent result) {
        if (resultCode == Activity.RESULT_OK) {
            showCase(btn_rotate);

            new AsyncTaskProcessImage(imageUri, picturePath,temporal).execute();

        }
        else if(resultCode == Activity.RESULT_CANCELED)
        {
            Toast.makeText(this, R.string.canceled_photo, Toast.LENGTH_SHORT).show();
            showImg.setImageBitmap(null);
            Visibility.gone(upload);
            Visibility.gone(btn_rotate);
        }
        else if (resultCode == Crop.RESULT_ERROR) {
            Toast.makeText(this, R.string.canceled_photo, Toast.LENGTH_SHORT).show();
            showImg.setImageBitmap(null);
            Visibility.gone(upload);
            Visibility.gone(btn_rotate);
            //Toast.makeText(this, Crop.getError(result).getMessage(),Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void OnPhotoMadeOK() {

        if(type == Constant.TYPE_PROFILE) {

            final Dialog dialog =  new Dialog(CameraPhotoCapture.this,R.style.PauseDialog);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
            dialog.setCancelable(false);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setContentView(R.layout.modal_general);

            TextView title = (TextView)dialog.findViewById(R.id.title);
            title.setText(""+Common.getNickName(CameraPhotoCapture.this));
            TextView text = (TextView) dialog.findViewById(R.id.text);
            String message = getResources().getString(R.string.advert_photo_pending_welcome);
            text.setText(""+message);

            Button exit = (Button) dialog.findViewById(R.id.exit);
            Visibility.gone(exit);
            Button accept = (Button) dialog.findViewById(R.id.accept);
            // if button is clicked, close the custom dialog
            exit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            accept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    GlobalData.set(Constant.MESSAGE_PHOTO_PENDING, "Y", CameraPhotoCapture.this);
                    finish();

                }
            });

            dialog.show();
        }
        else
        {
            finish();
        }

    }

    @Override
    public void OnPhotoMadeFail() {

    }
    public  Bitmap scaleDown(Bitmap realImage, float maxImageSize,
                                   boolean filter) {
        float ratio = Math.min(
                (float) maxImageSize / realImage.getWidth(),
                (float) maxImageSize / realImage.getHeight());
        int width = Math.round((float) ratio * realImage.getWidth());
        int height = Math.round((float) ratio * realImage.getHeight());

        Bitmap newBitmap = Bitmap.createScaledBitmap(realImage, width,
                height, filter);
        return newBitmap;
    }

    /**
     * Clase encargada de enviar la imagen al servidor, pra que sea guardada
     */
    private class BackGroundTaskForUploadPicture extends
            AsyncTask<String, Void, Void> {

        private String imageFile;
        private String token;
        private boolean isProfile;
        private String isPrivate = "";
        private Iphotolistener listener;
        public CamaraData camaraData;
        public ProgressDialog progressDialog;
        public boolean status = true;


        public BackGroundTaskForUploadPicture(String imageFile,String token, boolean isProfile, String isPrivate) {
            this.imageFile = imageFile;
            this.token = token;
            this.isProfile = isProfile;
            this.isPrivate = isPrivate;
        }

        public void setListener(Iphotolistener listener) {
            this.listener = listener;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(CameraPhotoCapture.this,R.style.CustomDialogTheme);
            progressDialog.setCancelable(false);
            progressDialog.show();
            progressDialog.setContentView(R.layout.custom_progress_dialog);
            TextView txtView = (TextView) progressDialog.findViewById(R.id.txtProgressDialog);
            txtView.setText(""+getResources().getString(R.string.updating_photo));
        }

        @Override
        protected Void doInBackground(String... params) {

            try {


                Bitmap bm = BitmapFactory.decodeFile(imageFile);

                Bitmap scaledBitmap = scaleDown(bm, SIZE_IMAGE, true);

                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bos);
                    byte[] data = bos.toByteArray();
                    HttpClient httpClient = new DefaultHttpClient();
                    HttpPost postRequest = new HttpPost(Services.UPLOAD_PICTURE_SERVICE);
                    ByteArrayBody bab = new ByteArrayBody(data, "profile.jpg");

                    MultipartEntity reqEntity = new MultipartEntity(
                            HttpMultipartMode.BROWSER_COMPATIBLE);

                    if (isProfile)
                        reqEntity.addPart("isProfile", new StringBody("1"));
                    else
                        reqEntity.addPart("isProfile", new StringBody("2"));

                    reqEntity.addPart("imageUrl", bab);

                    reqEntity.addPart("sessionToken", new StringBody(this.token));
                    reqEntity.addPart("sdcardPath", new StringBody(this.imageFile));
                    reqEntity.addPart("isFacebook",new StringBody("2"));
                    //datos del dispositivo
                    reqEntity.addPart("so", new StringBody("1"));
                    reqEntity.addPart("deviceVersion", new StringBody(""+Common.getVersionDevice(CameraPhotoCapture.this)));
                    reqEntity.addPart("appVersion",new StringBody(CameraPhotoCapture.this.getResources().getString(R.string.app_version)));
                    reqEntity.addPart("isPrivate",new StringBody(isPrivate));
                    reqEntity.addPart("model", new StringBody(""+Build.MODEL));
                    reqEntity.addPart("brand", new StringBody(""+Build.BRAND));

                    postRequest.setEntity(reqEntity);
                    HttpResponse response = httpClient.execute(postRequest);
                    BufferedReader reader = new BufferedReader(new InputStreamReader(
                            response.getEntity().getContent(), "UTF-8"));
                    String sResponse;
                    StringBuilder s = new StringBuilder();

                    while ((sResponse = reader.readLine()) != null)
                        s = s.append(sResponse);


                    Log.d("app2you","datos upload picture "+s.toString());
                    Gson gson = new Gson();
                    camaraData = gson.fromJson(s.toString(), CamaraData.class);

            } catch (Exception e) {
                status = false;
                e.printStackTrace();
                Log.e(e.getClass().getName(), e.getMessage());
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            progressDialog.dismiss();

            if(status)
            {
                if(!camaraData.success)
                {
                    Common.errorMessage(CameraPhotoCapture.this, "", getResources().getString(R.string.try_again));
                }
                else {
                    //peticion con exito
                    if (camaraData.errNum == 0) {
                        if (listener != null)
                            listener.OnPhotoMadeOK();

                    } else {
                        if (listener != null)
                            listener.OnPhotoMadeFail();

                    }
                   // getMyUserProfile(imageFile);

                }
            }
            else
                Common.errorMessage(CameraPhotoCapture.this,"",getResources().getString(R.string.check_internet));

        }

    }

    /**
     * Metodo  para enviar mensaje de chat al servidor
     */
    public void uploadPictureOkhttp3(String imageFile, byte[] dataImage, boolean isProfile,final Iphotolistener listener)
    {
        final ProgressDialog progressDialog = new ProgressDialog(CameraPhotoCapture.this,R.style.CustomDialogTheme);
        progressDialog.setCancelable(false);
        progressDialog.show();
        progressDialog.setContentView(R.layout.custom_progress_dialog);
        TextView txtView = (TextView) progressDialog.findViewById(R.id.txtProgressDialog);
        txtView.setText(""+getResources().getString(R.string.updating_photo));
        okhttp3.OkHttpClient client = new okhttp3.OkHttpClient();

        okhttp3.RequestBody formBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("sessionToken", "" + GlobalData.get(Constant.SESSION_TOKEN, CameraPhotoCapture.this))

                .addFormDataPart("sdcardPath", ""+imageFile)
                .addFormDataPart("isFacebook","2")
                //datos del dispositivo
                .addFormDataPart("so", "1")
                .addFormDataPart("deviceVersion", ""+Common.getVersionDevice(CameraPhotoCapture.this))
                .addFormDataPart("appVersion",CameraPhotoCapture.this.getResources().getString(R.string.app_version))
                .addFormDataPart("model", ""+Build.MODEL)
                .addFormDataPart("brand", ""+Build.BRAND)
                .addFormDataPart("isProfile",(isProfile)?"1":"2")

                .addPart(RequestBody.create(MEDIA_TYPE_JPG,dataImage))
                .build();

        okhttp3.Request request = new okhttp3.Request.Builder()
                .url(Services.UPLOAD_PICTURE_SERVICE)
                .post(formBody)

                .build();


        client.newCall(request).enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                progressDialog.dismiss();
                Looper.prepare();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Common.errorMessage(CameraPhotoCapture.this, "", getResources().getString(R.string.check_internet));

                    }
                });
                Looper.loop();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                progressDialog.dismiss();
                //final String result = response.body().string();
                InputStream in = response.body().byteStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                String result = "";
                String line = reader.readLine();
                result = line;
                while((line = reader.readLine()) != null) {
                    result += line;
                }
                System.out.println(result);
                final String resultFinal = result;
                response.body().close();
                Log.d("app2you", "respuesta upload photo: " + resultFinal);

                Looper.prepare();

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Gson gson = new Gson();
                            CamaraData camaraData = gson.fromJson(resultFinal, CamaraData.class);

                            if(!camaraData.success)
                            {
                                Common.errorMessage(CameraPhotoCapture.this, "", getResources().getString(R.string.try_again));
                            }
                            else {
                                //peticion con exito
                                if (camaraData.errNum == 0) {
                                    if (listener != null)
                                        listener.OnPhotoMadeOK();

                                } else {
                                    if (listener != null)
                                        listener.OnPhotoMadeFail();

                                }
                                // getMyUserProfile(imageFile);

                            }
                        } catch (Exception ex) {

                            ex.printStackTrace();
                        }
                    }
                });


                Looper.loop();
            }
        });

    }

    @Override
    protected void onPause() {
        super.onPause();
        GlobalData.set(Constant.ACTIVITY_FROM_CAMERA,"Y",CameraPhotoCapture.this);
    }

    /**
     * Método para mostrar tutorial de uso de una vista - widget
     * @param view
     */
    public void showCase(View view)
    {
        // single example
       new MaterialShowcaseView.Builder(this)
                .setTarget(view)
                .setDismissText("ENTENDIDO")
                .setDismissTextColor(Color.parseColor("#43b5b6"))

                .setContentText("Este botón, sirve para rotar la foto.")
                .setDelay(0) // optional but starting animations immediately in onCreate can make them choppy
                .singleUse("200") // provide a unique ID used to ensure it is only shown once
                .show();

    }
    //Callback para gestión de permisos en Android >= 6.0
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    if(openPhotoMode == 1)
                        openGallery();
                    else
                        openCamera();
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
}