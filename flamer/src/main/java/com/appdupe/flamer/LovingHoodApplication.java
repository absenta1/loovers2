package com.appdupe.flamer;

import android.content.Context;
import android.support.multidex.MultiDex;

/**
 * Created by manoli on 13/2/15.
 */
public class LovingHoodApplication extends BaseApplication {

   // private static final String IN_APP_BILLING_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnGEI/4UWh223J1gORLPQ+l0J3ssYrgT6zXwmLEyeVWL5vnhcYZTV0w9vTZ2LeSb9QC0Nt13zDgjMGHVYhL2zNBoe1ndH/liYmIsTXSpcO+nOG6fSCzJ9NAHuUUdHc2IA76A12ECRzNPNF54FgI2EEsT/Tzwy8HElYyD0znHt3s4FWIsw3O5B+KNqcC1ZRicGO70uMlCMOCdq0RbHxzrlOuS5IjTsz9KOBksHLjmBgxogfUsrDpt/ewDDnkOLI8vC59AVBd5xcy7t/c8QVizqRnXzTb8i7Nvi26a1EtgdNdX5sG5/852l/xYESACcujwqWvKSC7gQId1pWytVKS8T5wIDAQAB" ;
    private static final String IN_APP_BILLING_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAk/7pl9bzluSqi3DJS3RZxkE1whMIhVxnpsgyvTZzBPCWqEIkPgJMqWfwFtehVyUYlgdqxvQxrbCcPf0Mvxj7QhfZCxzsUGuPnU8MIOAUBY8AEbkyJmstilJ6K8VcnFbOfcvLPZp8cl2uBVJpth0/GZGv847Dh09KyC+hiXL1cRaWVjAhjvD1/uYF5vGKPczO33SDiYPzpbvXrqnYS03i/s7Abe6aE2IWllbo4zNPc2sUqfdORkTezHew7u5BvyS/leEmtiDBY7RsDi/jklJkk8O6eprkugsEKvD1mUmKDgV/y2NwSVCOo5WLm2l0a/Xn3pIou48r4TT0CWF4bc88ZwIDAQAB";
    private static final boolean FAKE_PURCHASES = false;
    //private PaymentManager mPaymentManager;

    @Override
    public void onCreate() {
        MultiDex.install(this);
        setCurrentApplication(this);
        // initBillingService();
        super.onCreate();
    }
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
    /*private void initBillingService() {
        String appkey = IN_APP_BILLING_KEY;
        mPaymentManager = new PaymentManager(this,appkey);

    }*/
   /* public PaymentManager getPaymentManager() {
        return mPaymentManager;
    }*/
    public static LovingHoodApplication getCurrent() {
        return (LovingHoodApplication)BaseApplication.getCurrent();
    }

}
