package com.android.slidingmenu.objects;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Juan Martin Bernal on 17/12/15.
 */
public class Match implements Serializable {

    @SerializedName("idUser")
    public String idUser;

    @SerializedName("username")
    public String username;

    @SerializedName("date_birthday")
    public String date_birthday;

    @SerializedName("latitude")
    public String latitude;

    @SerializedName("longitude")
    public String longitude;

    @SerializedName("picture")
    public String picture;

    @SerializedName("date")
    public String date;

    @SerializedName("message")
    public String message;

    public int chatStatusId;

    public boolean requestToMe;



    @Override
    public String toString() {
        return "idUser: "+ idUser + " username: " + username + " date_birthday: " + date_birthday + " picture: " +picture + " chatStatusId " +chatStatusId;
    }
}
