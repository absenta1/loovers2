package com.android.slidingmenu.fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.slidingmenu.Analitycs.MyApplication;
import com.android.slidingmenu.MainActivity;
import com.android.slidingmenu.adapter.ReportAdapter;
import com.android.slidingmenu.reports.Report;
import com.android.slidingmenu.security.FragmentSecurity;
import com.android.slidingmenu.util.Common;
import com.android.slidingmenu.util.GlobalData;
import com.android.slidingmenu.util.KeyBoard;
import com.android.slidingmenu.util.TypesLetter;
import com.android.slidingmenu.util.Visibility;
import com.android.slidingmenu.webservices.Services;
import com.appdupe.flamer.utility.Constant;
import com.gitonway.lee.niftymodaldialogeffects.lib.Effectstype;
import com.gitonway.lee.niftymodaldialogeffects.lib.NiftyDialogBuilder;
import com.loovers.app.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by Juan Martin Bernal on 19/2/16.
 */
public class FragmentDeleteAccount extends DialogFragment implements View.OnClickListener {

    private TextView senderName,txtTitle, txtSubText,txtDeleteAccount;
    private ImageView imgBack;
    private Button btnPLayWalla;
    public LinearLayout containerOptionsBackChat;

    public MainActivity mainActivity;
    public FragmentSettings fragmentSettings;

    public FragmentDeleteAccount(MainActivity mainActivity, FragmentSettings fragmentSettings)
    {
        this.mainActivity = mainActivity;
        this.fragmentSettings = fragmentSettings;
    }

    public FragmentDeleteAccount(){}

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_delete_account, null);

        if(mainActivity == null)
            mainActivity = (MainActivity)getActivity();

        initComponents(view);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_FRAME, android.R.style.Theme_Holo_Light_DarkActionBar);
    }

    public void initComponents(View view)
    {
        senderName = (TextView)view.findViewById(R.id.senderName);
        senderName.setText(""+mainActivity.getResources().getString(R.string.delete_account));
        senderName.setTypeface(TypesLetter.getAvenir(mainActivity));
        containerOptionsBackChat = (LinearLayout)view.findViewById(R.id.containerOptionsBackChat);
        imgBack                  = (ImageView)view.findViewById(R.id.imgBack);
        txtTitle                 = (TextView)view.findViewById(R.id.txtTitle);
        txtSubText               = (TextView)view.findViewById(R.id.txtSubText);
        txtDeleteAccount         = (TextView)view.findViewById(R.id.txtDeleteAccount);
        btnPLayWalla             = (Button)view.findViewById(R.id.btnPLayWalla);

        txtTitle.setTypeface(TypesLetter.getSansBold(mainActivity));
        txtSubText.setTypeface(TypesLetter.getSansRegular(mainActivity));
        btnPLayWalla.setTypeface(TypesLetter.getSansBold(mainActivity));

        txtTitle.setText((GlobalData.isKey(Constant.FIRST_MATCH,mainActivity))?mainActivity.getResources().getString(R.string.title_delete_account):mainActivity.getResources().getString(R.string.title_delete_account_match));
        txtSubText.setText((GlobalData.isKey(Constant.FIRST_MATCH,mainActivity))?mainActivity.getResources().getString(R.string.subtitle_delete_account):mainActivity.getResources().getString(R.string.subtitle_delete_account_match));
        btnPLayWalla.setText(mainActivity.getResources().getString(R.string.play_loovers));

        imgBack.setOnClickListener(this);
        containerOptionsBackChat.setOnClickListener(this);
        btnPLayWalla.setOnClickListener(this);
        txtDeleteAccount.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        if(v.getId() == R.id.containerOptionsBackChat)
        {
            dismiss();

        }
        else if(v.getId() == R.id.imgBack)
        {
            dismiss();

        }
        else if(v.getId() == R.id.btnPLayWalla)
        {

                dismiss();
                if(fragmentSettings != null)
                    fragmentSettings.dismiss();

                if(mainActivity.menu.isMenuShowing())
                    mainActivity.menu.toggle();

                mainActivity.replaceFragment(new FragmentGame(mainActivity));


        }
        else if(v.getId() == R.id.txtDeleteAccount)
        {
             /*final Dialog dialog = new Dialog(mainActivity);
			 dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			 dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
			 dialog.setContentView(R.layout.modal_general);

			 TextView text = (TextView) dialog.findViewById(R.id.text);
			 text.setText(""+mainActivity.getResources().getString(R.string.message_delete_account));


			 Button exit = (Button) dialog.findViewById(R.id.exit);
			 Button accept = (Button) dialog.findViewById(R.id.accept);
			 // if button is clicked, close the custom dialog
			 exit.setOnClickListener(new View.OnClickListener() {
				 @Override
				 public void onClick(View v) {
					 dialog.dismiss();
				 }
			 });
			 accept.setOnClickListener(new View.OnClickListener() {
				 @Override
				 public void onClick(View v) {
					 dialog.dismiss();
					 serviceDeleteAccount();


				 }
			 });

			 dialog.show();*/
            openDialogReport();

        }
    }

    /**
     * Abrir cuadro de dialogo con las opciones por las que se elimina la cuenta
     */
    private void openDialogReport() {

        final Dialog dialog = new Dialog(mainActivity,R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_report_user);

        TextView txtTitleReport = (TextView) dialog.findViewById(R.id.txtTitleReport);
        final ListView listReport     = (ListView)dialog.findViewById(R.id.listReport);
        txtTitleReport.setText(""+mainActivity.getString(R.string.because_delete_account));
        txtTitleReport.setTypeface(TypesLetter.getSansBold(mainActivity));


        listReport.setAdapter(new ReportAdapter(mainActivity,Common.loadReasonsDeleteAccount(mainActivity)));

        listReport.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //final ProgressBar progressBar = (ProgressBar)view.findViewById(R.id.progressBarReportUser);
                //Visibility.visible(progressBar);

                final Report report = (Report)listReport.getItemAtPosition(position);
                switch (report.idCategory)
                {
                    case Constant.OTHER_REASON_DELETE_APP:
                        //abrir cuadro de dialog para escribir la razones
                        dialog.dismiss();
                        openDialogFeedBack(report);

                        break;
                    default:
                        //llamar al servicio para enviar razon por la cual se elimina la cuenta, no olvidar enconder el progressBar y el dialog al terminar
                        serviceDeleteAccount(""+report.idCategory,""+report.reason,dialog);
                        break;
                }

              //  Delete account by "+ report.reason

            }
        });

        dialog.show();
    }
    /**
     * Abrir dialogo para enviar feedback
     */
    public void openDialogFeedBack(final Report report)
    {

        final Dialog dialog = new Dialog(mainActivity,R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_dontlike_app);

        TextView txtTitle = (TextView) dialog.findViewById(R.id.txtTitleReport);
        TextView txtDescription = (TextView) dialog.findViewById(R.id.textView13);
        Button  btnCancel = (Button)dialog.findViewById(R.id.btnCancel);
        Button  btnSend = (Button)dialog.findViewById(R.id.btnSend);
        final EditText fieldComment = (EditText)dialog.findViewById(R.id.fieldComment);
        txtTitle.setText(""+mainActivity.getResources().getString(R.string.title_reason_delete_account));
        txtTitle.setTypeface(TypesLetter.getSansBold(mainActivity));
        txtDescription.setTypeface(TypesLetter.getSansRegular(mainActivity));
        btnSend.setTypeface(TypesLetter.getSansBold(mainActivity));
        btnSend.setText(""+mainActivity.getResources().getString(R.string.txt_btn_delete_account));
        btnCancel.setTypeface(TypesLetter.getSansRegular(mainActivity));
        fieldComment.setTypeface(TypesLetter.getSansRegular(mainActivity));

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                KeyBoard.hide(fieldComment);
                dialog.dismiss();
            }
        });
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String comment = fieldComment.getText().toString().trim();
                if(comment.length() > 0) {
                    KeyBoard.hide(fieldComment);
                    serviceDeleteAccount(""+report.idCategory,comment,dialog);
                }
                else
                    Toast.makeText(mainActivity, R.string.empty_fields, Toast.LENGTH_SHORT).show();

            }
        });
        dialog.show();
    }
    /**
     * Metodo encargado de obtener los matches que tiene un usuario
     */
    public void serviceDeleteAccount(String deleteReasonId, String message, final Dialog dialog)
    {

        final ProgressDialog progressDialog = new ProgressDialog(mainActivity,R.style.CustomDialogTheme);
        progressDialog.setCancelable(false);
        progressDialog.show();
        progressDialog.setContentView(R.layout.custom_progress_dialog);
        TextView txtView = (TextView) progressDialog.findViewById(R.id.txtProgressDialog);
        txtView.setText(""+mainActivity.getResources().getString(R.string.deleting_account));

        OkHttpClient client = new OkHttpClient();
        RequestBody formBody = new FormBody.Builder()
                .add("sessionToken", "" + GlobalData.get(Constant.SESSION_TOKEN, mainActivity))
                .add("deleteReasonId",deleteReasonId)
                .add("message",""+message)
                .build();

        Request request = new Request.Builder()
                .url(Services.DELETE_ACCOUNT)
                .post(formBody)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                if (progressDialog != null)
                    progressDialog.dismiss();
                Looper.prepare();

                mainActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(dialog != null)
                            dialog.dismiss();

                        Common.errorMessage(mainActivity, "", mainActivity.getResources().getString(R.string.check_internet));
                    }
                });
                Looper.loop();
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {

                final String result = response.body().string();
                Log.d("app2you", "respuesta delete account: " + result);

                Looper.prepare();

                mainActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if(dialog != null)
                                dialog.dismiss();

                            JSONObject json = new JSONObject(result);

                            String success = json.getString("success");
                            int errorNum = json.has("errorNum")?json.getInt("errorNum"):0;
                            if (success.equals("true")) {
                                Common.closeSessionWallamatch(mainActivity,progressDialog);
                            } else {
                                if (progressDialog != null)
                                    progressDialog.dismiss();

                                if(errorNum == Constant.ERROR_OPEN_SESSION_OTHER_DEVICE)
                                    otherSessionOpen();
                                else
                                    Toast.makeText(mainActivity, R.string.try_again, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            if (progressDialog != null)
                                progressDialog.dismiss();
                            Toast.makeText(mainActivity, R.string.error_reponse_server, Toast.LENGTH_SHORT).show();
                        }
                        finally {
                            response.body().close();
                        }


                    }


                });

                Looper.loop();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if (GlobalData.isKey(Constant.PIN_SECURITY, mainActivity)) {
            FragmentSecurity fragmentSecurity = new FragmentSecurity(mainActivity,1);
            fragmentSecurity.setCancelable(false);
            fragmentSecurity.show(mainActivity.mFragmentManager,"FragmentSecurity.TAG");
        }

        MyApplication.getInstance().trackScreenView("fragment delete account");
    }
    /**
     * Cerrar session actual porque la han abierto en otro dispositivo
     */
    private void otherSessionOpen() {
        final NiftyDialogBuilder dialog =  NiftyDialogBuilder.getInstance(mainActivity);
        dialog.withTitle(null);
        dialog.withMessage(null);
        dialog.isCancelableOnTouchOutside(false);
        dialog.withDialogColor(Color.TRANSPARENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.withDuration(Constant.DURATION_ANIMATION);
        dialog.withEffect(Effectstype.Slidetop);
        dialog.setCustomView(R.layout.modal_general,mainActivity);

        TextView text = (TextView) dialog.findViewById(R.id.text);
        text.setText(""+mainActivity.getResources().getString(R.string.error_session));

        Button exit = (Button) dialog.findViewById(R.id.exit);
        Visibility.gone(exit);
        Button accept = (Button) dialog.findViewById(R.id.accept);
        // if button is clicked, close the custom dialog
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Common.closeSessionWallamatch(mainActivity);
            }
        });

        dialog.show();
    }
}
