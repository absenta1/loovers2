package com.android.slidingmenu.messages;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TabHost;
import android.widget.TextView;

import com.android.slidingmenu.MainActivity;
import com.android.slidingmenu.fragments.FriendsFragment;
import com.android.slidingmenu.util.TypesLetter;
import com.loovers.app.R;

/**
 * Created by Juan Martin Bernal on 17/3/16.
 */
public class MessageFragment extends Fragment {


    private FragmentTabHost mTabHost;
    public MainActivity mainActivity;
   // public TextView titleTab, badgeTab;
    public MessageFragment() {
        // Required empty public constructor
    }

    public MessageFragment(MainActivity mainActivity)
    {
        this.mainActivity = mainActivity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if(mainActivity == null)
            mainActivity = (MainActivity)getActivity();

        mainActivity.tvTitle.setText(""+mainActivity.getResources().getString(R.string.messages));

        mTabHost = new FragmentTabHost(getActivity());
        mTabHost.setup(getActivity(), getChildFragmentManager(), R.layout.fragment_near);

        mTabHost.addTab(mTabHost.newTabSpec("Messages").setIndicator(getTabIndicator(mainActivity,"Chats",0)),
                FriendsFragment.class, null);
        mTabHost.addTab(mTabHost.newTabSpec("Request").setIndicator(getTabIndicator(mainActivity,"Solicitudes",1)),
                RequestChatsFragment.class, null);


        return mTabHost;


    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mTabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String tabId) {
                if(tabId.equals("Messages"))
                {

                    setStyleTextTabActive(mTabHost.getTabWidget().getChildAt(0));
                    setStyleTabHide(mTabHost.getTabWidget().getChildAt(1));

                }
                else
                {
                    setStyleTextTabActive(mTabHost.getTabWidget().getChildAt(1));
                    setStyleTabHide(mTabHost.getTabWidget().getChildAt(0));
                }
            }
        });

    }

    /**
     * Tab activa
     * @param view
     */
    public void setStyleTextTabActive(View view)
    {
        mainActivity.titleTab = (TextView) view.findViewById(R.id.title);
        mainActivity.titleTab.setTextColor(Color.WHITE);
        mainActivity.titleTab.setTypeface(TypesLetter.getSansRegular(mainActivity), Typeface.BOLD);
    }

    /**
     * tab desactivada
     * @param view
     */
    public void setStyleTabHide(View view)
    {
        mainActivity.titleTab = (TextView) view.findViewById(R.id.title);
        mainActivity.titleTab.setTextColor(Color.parseColor("#c3c3c3"));
        mainActivity.titleTab.setTypeface(TypesLetter.getSansRegular(mainActivity));
    }
    /**
     * Método que establece el estilo de las tabs
     * @param context
     * @param title
     * @param tabId
     * @return
     */

    private View getTabIndicator(Context context, String title, int tabId) {
        View view = LayoutInflater.from(context).inflate(R.layout.tab_layout, null);

        mainActivity.titleTab = (TextView) view.findViewById(R.id.title);
        mainActivity.badgeTab  = (TextView) view.findViewById(R.id.badge);
        if(tabId == 0) {
            mainActivity.titleTab.setTextColor(Color.WHITE);
            mainActivity.titleTab.setTypeface(TypesLetter.getSansRegular(mainActivity), Typeface.BOLD);
        }
        else
        {
            mainActivity.titleTab.setTextColor(Color.parseColor("#c3c3c3"));
            mainActivity.titleTab.setTypeface(TypesLetter.getSansRegular(mainActivity));
        }

        mainActivity.badgeTab.setTypeface(TypesLetter.getSansRegular(mainActivity));
        mainActivity.titleTab.setTextSize(TypedValue.COMPLEX_UNIT_SP,14);
        mainActivity.titleTab.setText(title);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mTabHost = null;
    }

}
