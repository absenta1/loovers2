package com.android.slidingmenu.landings;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.android.slidingmenu.MainActivity;
import com.android.slidingmenu.util.TypesLetter;
import com.appdupe.flamer.utility.Constant;
import com.loovers.app.R;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;

/**
 * Created by Martin Bernal on 12/5/16.
 */
public class DialogFragmentShareFacebook extends DialogFragment implements View.OnClickListener {

    private TextView senderName,txtTitle, txtSubText,txtOtherMoment;
    private ImageView imgBack;
    public LinearLayout containerOptionsBackChat;
    public MainActivity mainActivity;

    public DialogFragmentShareFacebook(MainActivity mainActivity)
    {
        this.mainActivity = mainActivity;
    }

    public DialogFragmentShareFacebook(){}

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.landing_share_facebook, null);

        if(mainActivity == null)
            mainActivity = (MainActivity)getActivity();

        initComponents(view);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_FRAME, android.R.style.Theme_Holo_Light_DarkActionBar);
    }

    /**
     * Inicializa los componentes de la vista
     * @param view
     */
    public void initComponents(View view)
    {
        senderName          = (TextView)view.findViewById(R.id.senderName);
        txtOtherMoment      = (TextView)view.findViewById(R.id.txtOtherMoment);
        senderName.setText(""+mainActivity.getString(R.string.share_and_win));
        senderName.setTypeface(TypesLetter.getAvenir(mainActivity));


        containerOptionsBackChat        = (LinearLayout)view.findViewById(R.id.containerOptionsBackChat);
        imgBack                         = (ImageView)view.findViewById(R.id.imgBack);
        txtTitle                        = (TextView)view.findViewById(R.id.txtTitle);
        txtSubText                      = (TextView)view.findViewById(R.id.txtSubText);
        mainActivity.btnShareFacebook   = (Button) view.findViewById(R.id.btnShareFacebook);


        txtTitle.setTypeface(TypesLetter.getSansBold(mainActivity));
        txtSubText.setTypeface(TypesLetter.getSansRegular(mainActivity));
        txtOtherMoment.setTypeface(TypesLetter.getSansRegular(mainActivity));


        imgBack.setOnClickListener(this);
        containerOptionsBackChat.setOnClickListener(this);
        txtOtherMoment.setOnClickListener(this);

        mainActivity.btnShareFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainActivity.callbackManager = CallbackManager.Factory.create();
                ShareDialog shareDialog = new ShareDialog(mainActivity);
                // this part is optional
                shareDialog.registerCallback(mainActivity.callbackManager, new FacebookCallback<Sharer.Result>() {
                    @Override
                    public void onSuccess(Sharer.Result result) {
                        mainActivity.setCredits("", ""+Constant.FACEBOOK_SHARE_CREDITS_TIME);
                        dismiss();

                    }

                    @Override
                    public void onCancel() {
                        Toast.makeText(mainActivity, R.string.canceled_post, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onError(FacebookException error) {
                        Toast.makeText(mainActivity, R.string.error_share_post_facebook, Toast.LENGTH_SHORT).show();
                    }
                });
                if (ShareDialog.canShow(ShareLinkContent.class)) {
                    ShareLinkContent linkContent = new ShareLinkContent.Builder()
                            .setContentTitle(""+mainActivity.getString(R.string.app_name))
                            .setContentDescription(""+mainActivity.getString(R.string.description_app))
                            .setContentUrl(Uri.parse("https://play.google.com/store/apps/details?id=com.loovers.app"))
                            .setImageUrl(Uri.parse("http://ass2.loovers.es/support/piccompartir.png"))
                            .build();
                    shareDialog.show(linkContent);
                }
            }
        });

    }

    @Override
    public void onClick(View v) {

        if(v.getId() == R.id.containerOptionsBackChat) {
            dismiss();
        }
        else if(v.getId() == R.id.imgBack)
        {
            dismiss();
        }
        else if(v.getId() == R.id.txtOtherMoment)
        {
            dismiss();
        }

    }

}
