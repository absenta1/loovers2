package com.android.slidingmenu.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import com.android.slidingmenu.util.Common;
import com.android.slidingmenu.util.GlobalData;
import com.android.slidingmenu.util.TypesLetter;
import com.android.slidingmenu.webservices.Services;
import com.appdupe.flamer.RegisterOrSessionActivityActivity;
import com.appdupe.flamer.utility.ConnectionDetector;
import com.appdupe.flamer.utility.Constant;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.romainpiel.shimmer.Shimmer;
import com.romainpiel.shimmer.ShimmerTextView;
import com.loovers.app.R;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import okhttp3.Call;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


/**
 * Created by Juan Martin Bernal on 7/12/15.
 */
public class IntroActivity extends Activity /*implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener*/ {

    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;
    public final static int MY_PERMISSIONS_REQUEST_READ_GPS = 7851;
    public final static int MY_PERMISSIONS_REQUEST_GET_LOCATION = 8695;

    // Google client to interact with Google API
    private GoogleApiClient mGoogleApiClient;

    private LocationRequest mLocationRequest;

    //public GPSTracker gps;
    public ShimmerTextView shimmer_tv;
    public Shimmer shimmer;
    public static final int SPLASH_TIME_OUT = 3000;
    public Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.splashactivity);
        shimmer_tv = (ShimmerTextView) findViewById(R.id.shimmer_tv);
        shimmer_tv.setTypeface(TypesLetter.getAvenir(IntroActivity.this));
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

       // Common.getHasKey(this);

        if(!GlobalData.isKey(Constant.SESSION_TOKEN,IntroActivity.this)) {
            if (!GlobalData.isKey(Constant.SERVER_PING, IntroActivity.this)) {
                String ip = "false";

                int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                if (currentapiVersion >= Build.VERSION_CODES.M) {
                    sendPingSever(ip);
                } else {
                    ip = (Common.getLocalIpAddress(IntroActivity.this).length() > 0) ? Common.getLocalIpAddress(IntroActivity.this) : "false";
                    sendPingSever(ip);
                }

            }
        }

    }

    @Override
    protected void onResume() {
        super.onResume();

        if (GlobalData.isKey(Constant.FIRST_REGISTER, IntroActivity.this)) {
            //getPositionUser(currentLatitude, currentLongitude);
            shimmer = new Shimmer();
            shimmer.setDuration(400);
            shimmer.start(shimmer_tv);

            handler = new Handler();
            handler.postDelayed(new Runnable() {

                @Override
                public void run() {
                    // getPositionUser(currentLatitude, currentLongitude);
                    goToRegister();
                }
            }, 400);

        } else {
            shimmer = new Shimmer();
            shimmer.setDuration(Constant.TIME_SHIMMER);
            shimmer.start(shimmer_tv);

            handler = new Handler();
            handler.postDelayed(new Runnable() {

                @Override
                public void run() {
                   // getPositionUser(currentLatitude, currentLongitude);
                    goToRegister();
                }
            }, SPLASH_TIME_OUT);
        }
            /*mLocationRequest = LocationRequest.create()
                    .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                    .setInterval(5000)        // 5 seconds, in milliseconds
                    .setFastestInterval(1000); // 1 second, in milliseconds
            connectGoogle();*/



    }
    public void goToRegister()
    {
        Intent intent = new Intent(IntroActivity.this, RegisterOrSessionActivityActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        overridePendingTransition(R.anim.fade_in_slow, R.anim.fade_out);
        finish();
    }

    /**
     * Conectar con el servicio de localización Google
     */
    /*public void connectGoogle() {

        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.M){
            // Do something for M and above versions
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions( this, new String[] {  android.Manifest.permission.ACCESS_COARSE_LOCATION,android.Manifest.permission.ACCESS_FINE_LOCATION  },MY_PERMISSIONS_REQUEST_READ_GPS);
            }
            else
            {
                mGoogleApiClient = new GoogleApiClient.Builder(this)
                        .addApi(LocationServices.API)
                        .addConnectionCallbacks(this)
                        .addOnConnectionFailedListener(this)
                        .build();

                mGoogleApiClient.connect();

                if(mGoogleApiClient != null)
                    LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            }
        } else{
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .build();

            mGoogleApiClient.connect();

            if(mGoogleApiClient != null)
                LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        }
    }*/
  /*  @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        try {
        switch (requestCode) {

                case MY_PERMISSIONS_REQUEST_READ_GPS: {
                    // If request is cancelled, the result arrays are empty.
                    if (grantResults.length > 0
                            && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        //connectGoogle();
                        // permission was granted, yay! Do the
                        // contacts-related task you need to do.

                    } else {

                        // permission denied, boo! Disable the
                        // functionality that depends on this permission.

                    }
                    return;
                }
                case MY_PERMISSIONS_REQUEST_GET_LOCATION: {
                    // If request is cancelled, the result arrays are empty.
                    if (grantResults.length > 0
                            && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                        logicGoogleGps();

                    } else {

                        // permission denied, boo! Disable the
                        // functionality that depends on this permission.
                    }
                    return;
                }


            // other 'case' lines to check for other
            // permissions this app might request
        }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            Toast.makeText(IntroActivity.this, R.string.try_again, Toast.LENGTH_SHORT).show();
        }
    }*/



    @Override
    protected void onPause() {
        super.onPause();
        if (shimmer != null)
            shimmer.cancel();

        if (handler != null)
            handler.removeCallbacksAndMessages(null);

        /*if (mGoogleApiClient != null) {

            if(mGoogleApiClient.isConnected()) {
                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
                mGoogleApiClient.disconnect();
            }
        }*/

    }


    /**
     * Obtener info de de la posicion de la persona
     * @param latAsync
     * @param lonAsync
     */
    public void getPositionUser(double latAsync, double lonAsync) {

        if (GlobalData.isKey(Constant.FIRST_REGISTER, IntroActivity.this)) {
                Log.d("app2you", "values gps: " + latAsync + " lon: " + lonAsync);
                if (latAsync != 0.0) {
                    double latOld = GlobalData.isKey(Constant.LATITUD_USER,IntroActivity.this)?Double.parseDouble(GlobalData.get(Constant.LATITUD_USER,IntroActivity.this)):0.0;
                    double lonOld = GlobalData.isKey(Constant.LONGITUD_USER,IntroActivity.this)?Double.parseDouble(GlobalData.get(Constant.LONGITUD_USER,IntroActivity.this)):0.0;

                    Location locationOld = new Location("One");
                    locationOld.setLatitude(latOld);
                    locationOld.setLongitude(lonOld);

                    Location locationNew = new Location("Two");
                    locationNew.setLatitude(latAsync);
                    locationNew.setLongitude(lonAsync);

                    float distance = locationOld.distanceTo(locationNew);

                    if (distance > 5000) {

                        GlobalData.set(Constant.LATITUD_USER, "" + latAsync ,IntroActivity.this);
                        GlobalData.set(Constant.LONGITUD_USER, "" + lonAsync, IntroActivity.this);
                        getDataLocationPerson(latAsync,lonAsync);
                    }
                    else
                    {
                        goToRegister();
                    }
                } else {
                    goToRegister();
                    //Toast.makeText(IntroActivity.this,R.string.no_found_gps, Toast.LENGTH_SHORT).show();
                }


        } else {

            Log.d("app2you", "Es primera vez que entra a loovers values " + latAsync + " lon: " + lonAsync );
            if (ConnectionDetector.isConnectingToInternet(IntroActivity.this)) {
                if (latAsync != 0.0) {
                        GlobalData.set(Constant.LATITUD_USER, "" + latAsync ,IntroActivity.this);
                        GlobalData.set(Constant.LONGITUD_USER, "" + lonAsync, IntroActivity.this);
                        getDataLocationPerson(latAsync,lonAsync);
                } else
                {
                    goToRegister();
                   //Toast.makeText(IntroActivity.this, R.string.no_found_gps, Toast.LENGTH_SHORT).show();
                }
            }
            else {
                Common.errorMessage(IntroActivity.this,"",getResources().getString(R.string.check_internet));
                //Toast.makeText(IntroActivity.this, R.string.check_internet, Toast.LENGTH_SHORT).show();
                //overridePendingTransition(R.anim.fade_in_slow, R.anim.fade_out);
                //finish();
            }
        }
    }

    /**
     * Método que ejecuta la tarea en background, para recolectar la información de ubicación del dispositivo.
     * @param latAsync
     * @param lonAsync
     */
    private void getDataLocationPerson(double latAsync, double lonAsync) {

       new AsyncTaskLocationData(latAsync, lonAsync).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    /**
     * Método encargado de traer los datos de posición desde el servicio web
     * @param lat
     * @param lng
     * @return
     */
    private JSONObject getLocationInfo(double lat, double lng) {

        HttpGet httpGet = new HttpGet(
                "https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyADSAD0oSLc9h1SeqT15IUVP6iyK-lYox0&latlng="
                        + lat + "," + lng + "&sensor=false");

        final HttpParams httpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParams, 12000);
        HttpClient client = new DefaultHttpClient(httpParams);

        HttpResponse response;
        StringBuilder stringBuilder = new StringBuilder();

        try {
            response = client.execute(httpGet);
            HttpEntity entity = response.getEntity();
            InputStream stream = entity.getContent();
            /*int b;
            while ((b = stream.read()) != -1)
                stringBuilder.append((char) b);*/
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(stream, "UTF-8"), 8);
            //StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line);
            }

        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject = new JSONObject(stringBuilder.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject;
    }

    /*@Override
    public void onConnected(@Nullable Bundle bundle) {

        logicGoogleGps();
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    //localizacion normal antes de lollipop
    @Override
    public void onLocationChanged(Location location) {
        handleNewLocation(location);
    }*/

    public void logicGoogleGps()
    {
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.M){
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions( this, new String[] {  android.Manifest.permission.ACCESS_COARSE_LOCATION,android.Manifest.permission.ACCESS_FINE_LOCATION  },MY_PERMISSIONS_REQUEST_GET_LOCATION);
            }
            else
            {
                Location location = null;
                if(mGoogleApiClient != null)
                    location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

                if (location == null) {
                    Log.d("app2you", "location null");
                    /*gps = new GPSTracker(IntroActivity.this);
                    if (!gps.canGetLocation())
                        gps.showSettingsAlert();

                    if(mGoogleApiClient != null && mGoogleApiClient.isConnected())
                        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);*/
                    goToRegister();
                } else {
                    handleNewLocation(location);
                }
            }
        }
        else {
            Location location = null;

            if(mGoogleApiClient != null && mGoogleApiClient.isConnected())
                location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

            if (location == null) {
                /*gps = new GPSTracker(IntroActivity.this);
                if (!gps.canGetLocation())
                    gps.showSettingsAlert();
                else
                    Toast.makeText(IntroActivity.this, R.string.try_again, Toast.LENGTH_SHORT).show();

                if(mGoogleApiClient != null && mGoogleApiClient.isConnected())
                    LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);*/
                goToRegister();
            } else {
                handleNewLocation(location);
            }
        }
    }
    public void handleNewLocation(Location location) {

        final double currentLatitude = location.getLatitude();
        final double currentLongitude = location.getLongitude();
        shimmer_tv = (ShimmerTextView) findViewById(R.id.shimmer_tv);
        shimmer_tv.setTypeface(TypesLetter.getAvenir(IntroActivity.this));
        shimmer.start(shimmer_tv);

        if (GlobalData.isKey(Constant.FIRST_REGISTER, IntroActivity.this)) {
            getPositionUser(currentLatitude, currentLongitude);
        } else {
            handler = new Handler();
            handler.postDelayed(new Runnable() {

                @Override
                public void run() {
                    getPositionUser(currentLatitude, currentLongitude);
                }
            }, SPLASH_TIME_OUT);
        }
    }

    /**
     * Clase para obtener localización
     */
    public class AsyncTaskLocationData extends AsyncTask<String, String, String>
    {
        double latAsync;
        double lonAsync;
        public AsyncTaskLocationData(double latAsync, double lonAsync)
        {
            this.latAsync = latAsync;
            this.lonAsync = lonAsync;
        }
        @Override
        protected String doInBackground(String... params) {
            String jsonStr = getLocationInfo(latAsync, lonAsync).toString();
            return jsonStr;
        }

        @Override
        protected void onPostExecute(String jsonStr) {
            super.onPostExecute(jsonStr);

            if (jsonStr != null) {

                JSONObject jsonObj;
                try {
                    jsonObj = new JSONObject(jsonStr);
                    Log.d("app2you","json: " + jsonStr);
                    String Status = jsonObj.getString("status");
                    if (Status.equalsIgnoreCase("OK")) {
                        JSONArray Results = jsonObj.getJSONArray("results");
                        JSONObject zero = Results.getJSONObject(0);
                        JSONArray address_components = zero
                                .getJSONArray("address_components");

                        for (int i = 0, len = address_components.length(); i < len; ++i) {
                            JSONObject zero2 = address_components
                                    .getJSONObject(i);
                            String long_name = zero2.getString("long_name");
                            String short_name = zero2.getString("short_name");
                            JSONArray mtypes = zero2.getJSONArray("types");
                            String type = mtypes.getString(0);

                            if (type.equalsIgnoreCase("locality")) {
                                // Address2 = Address2 + long_name + ", ";
                                String city = long_name;

                                GlobalData.set(Constant.CITY_USER, "" + city, IntroActivity.this);
                                //Log.d("app2you", "city async " + city );
                            }

                            if (type.equalsIgnoreCase("country")) {
                                // Address2 = Address2 + long_name + ", ";
                                String codeCountry = short_name;
                                GlobalData.set(Constant.COUNTRY_USER, "" + codeCountry, IntroActivity.this);
                               // Log.d("app2you", "pais async " + codeCountry );
                            }
                            /*if(type.equalsIgnoreCase("postal_code"))
                            {
                                String postal_code = long_name;
                                GlobalData.set(Constant.POSTAL_CODE, "" + postal_code, IntroActivity.this);
                                Log.d("app2you", "postal code async " + postal_code );
                            }*/
                            if(type.equalsIgnoreCase("administrative_area_level_2"))
                            {
                                String province = long_name;

                                GlobalData.set(Constant.PROVINCE, "" + province, IntroActivity.this);
                              //  Log.d("app2you", "province async " + province );
                            }

                        }
                        goToRegister();
                    }
                    else
                    {
                        //Toast.makeText(IntroActivity.this, "error al parsear la info", Toast.LENGTH_SHORT).show();
                        goToRegister();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    goToRegister();

                }

            }
            else
            {
                goToRegister();

            }
        }
    }


    /**
     * Envia datos de instalacion al servidor, por primera vez
     */
    public void sendPingSever(String ip)
    {

        OkHttpClient client = new OkHttpClient();

        RequestBody formBody = new FormBody.Builder()
                .add("brand",""+Build.BRAND)
                .add("model",""+Build.MODEL)
                .add("so","1")
                .add("ip",""+ip)
                .add("deviceVersion", Common.getVersionDevice(IntroActivity.this))
                .add("appVersion",""+getResources().getString(R.string.app_version))
                .add("typeId","1")
                .build();

        Request request = new Request.Builder()
                .url(Services.REGISTER_INSTALL)
                .post(formBody)
                .build();

        client.newCall(request).enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(Call call, IOException e) {


            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                final String result = response.body().string();
                Log.d("app2you", "respuesta  ping  " + result);

                Looper.prepare();

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            JSONObject json = new JSONObject(result);

                            String success = json.getString("success");

                            if (success.equals("true")) {
                                GlobalData.set(Constant.SERVER_PING,"Y",IntroActivity.this);
                                GlobalData.set(Constant.HASH_SESSION,""+json.getString("hashSession"),IntroActivity.this);
                            } else {

                            }
                        } catch (JSONException ex) {
                            ex.printStackTrace();
                        }
                        finally {
                            response.body().close();
                        }

                    }
                });
                Looper.loop();
            }
        });
    }
}