package com.android.slidingmenu;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.RemoteException;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTabHost;
import android.support.v4.app.FragmentTransaction;
import android.support.v8.renderscript.Allocation;
import android.support.v8.renderscript.Element;
import android.support.v8.renderscript.RenderScript;
import android.support.v8.renderscript.ScriptIntrinsicBlur;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.slidingmenu.activities.UploadPhotoProfileActivity;
import com.android.slidingmenu.adapter.FriendsAdapter;
import com.android.slidingmenu.adapter.ProfileGalleryAdapter;
import com.android.slidingmenu.fragments.FragmentFavorites;
import com.android.slidingmenu.fragments.FragmentGame;
import com.android.slidingmenu.fragments.FragmentMatch;
import com.android.slidingmenu.fragments.FragmentProfile;
import com.android.slidingmenu.fragments.FragmentSettings;
import com.android.slidingmenu.fragments.FragmentWallas;
import com.android.slidingmenu.messages.MessageFragment;
import com.android.slidingmenu.nearyou.NearFragment;
import com.android.slidingmenu.security.FragmentSecurity;
import com.android.slidingmenu.store.DialogStore;
import com.android.slidingmenu.util.AppAnimation;
import com.android.slidingmenu.util.Common;
import com.android.slidingmenu.util.ConnectionDetector;
import com.android.slidingmenu.util.GlobalData;
import com.android.slidingmenu.util.IabBroadcastReceiver;
import com.android.slidingmenu.util.IabHelper;
import com.android.slidingmenu.util.IabResult;
import com.android.slidingmenu.util.Inventory;
import com.android.slidingmenu.util.KeyBoard;
import com.android.slidingmenu.util.Purchase;
import com.android.slidingmenu.util.TypesLetter;
import com.android.slidingmenu.util.Visibility;
import com.android.slidingmenu.webservices.Services;
import com.android.vending.billing.IInAppBillingService;
import com.appbrain.AppBrain;
import com.appdupe.flamer.pojo.MatchesData;
import com.appdupe.flamer.utility.CircleTransform;
import com.appdupe.flamer.utility.Constant;
import com.appsflyer.AFInAppEventParameterName;
import com.appsflyer.AFInAppEventType;
import com.appsflyer.AppsFlyerLib;
import com.appyvet.rangebar.RangeBar;
import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.gitonway.lee.niftymodaldialogeffects.lib.Effectstype;
import com.gitonway.lee.niftymodaldialogeffects.lib.NiftyDialogBuilder;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu.OnOpenListener;
import com.squareup.picasso.Picasso;
import com.loovers.app.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


/**
 * Clase principal encargada de controlar los fragmentos de la aplicación, así como gestión de notificiones y mensajes dentro de la app.
 * @author Juan Martín Bernal
 * @date 16/11/2015
 */
public class MainActivity extends FragmentActivity implements
        OnClickListener,IabBroadcastReceiver.IabBroadcastListener,DialogInterface.OnClickListener{

    public ArrayList<Fragment> fragments = new ArrayList<Fragment>();
    public ArrayList<MatchesData> listRequestChat = new ArrayList<>();
    public LinearLayout containerNoRequestChat;
    public Button btMenu, btnShareFacebook;
    public TextView tvTitle;
    private Typeface topbartextviewFont;
    public FragmentManager mFragmentManager;
    private Boolean mrightMenuClicked = false;
    private Dialog mdialog;
    public FriendsAdapter friendsAdapter;public ImageView profileimage, img_preferences,imgBackGroundProfile, btnOkBirthday,imgSecurity,imageNearToYou;
    public ImageView layerimage, imageGame, imageWallas, imageMatch, imageMessages, imageFavorites,likeButton, dislikeButton;
    private TextView profiletextview, txtCounterWallas, txtCounterMatches, txtCounterMessages,txtDulce, txtApasionada, txtAtrevida, txtTraviesa,txtAlertNewMessage,txtBirthday;
    private LinearLayout homelayout, messages,favoritelayout, matchlayout, flechazosLayout,nearLayout,containerCredits;
    private RelativeLayout menu_left;
    public SlidingMenu menu;
    public FrameLayout activity_main_content_fragment, frame_view_shadom;
    public RelativeLayout container_finish_register, container_personal_status, container_button_menu, container_questions,container_new_notification,containerBirthdayUser;
    public Button btn_ok, btn_op1, btn_op2, btn_op3,btn_begin;
    public LinearLayout containerContinue;
    public RangeBar rangeDulce, rangeApasionada, rangeAtrevida, rangeTraviesa;
    public ImageView imgDulce, imgApasionada, imgAtrevida, imgTraviesa, imgBtnOk,imgCredits;

    //pago
    //public static final String base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAk/7pl9bzluSqi3DJS3RZxkE1whMIhVxnpsgyvTZzBPCWqEIkPgJMqWfwFtehVyUYlgdqxvQxrbCcPf0Mvxj7QhfZCxzsUGuPnU8MIOAUBY8AEbkyJmstilJ6K8VcnFbOfcvLPZp8cl2uBVJpth0/GZGv847Dh09KyC+hiXL1cRaWVjAhjvD1/uYF5vGKPczO33SDiYPzpbvXrqnYS03i/s7Abe6aE2IWllbo4zNPc2sUqfdORkTezHew7u5BvyS/leEmtiDBY7RsDi/jklJkk8O6eprkugsEKvD1mUmKDgV/y2NwSVCOo5WLm2l0a/Xn3pIou48r4TT0CWF4bc88ZwIDAQAB";//loovers old
    //public static final String base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnXgu3fnhCFAipMSUdAFno4LpEo1RshNdU1XXHxD6MbykIKN1Oum449709uajjCdTfMEnnGvUzaKXvQs5Zuy25qybaPxieNn4dXY33YgDIuRHa5X+xnwscw+7N84rAZnx8H4Y0b3jdaIlgPtrEwG8Byl7r7MHYCcLxvjp0hELcrCNTvaX325GM0A75gySmfWph05cRAkCCz7gXtG8yOAUVxQEHNjMkX+fb4cAoOj3XPbOD7S/rNE2xyvU26WfBwp08fn4W9PZN1tp2rHw8K3euilsiAiaNP9dLEZKZFXNX0dzFrd9+0IclmS7jxEthSDwcEToKLyqhPrXimDGHTXHRQIDAQAB";//loovers new
   /* public static final String base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAtWLbyRfz8+MeimRYBsRJNzRt1Ny2ozTzq5A9BEyzDhFV6h3nnm+3pRH9X0T91voNYqhHVRM96CDvRo8Wmvp459QKDCPcXvI/vQquazy/3QTriF45iL7ITPS+wI10WpUL4zOl2RjTxmP73mNWZrYeWS0tovoqZ/TnGHMCJ3Y/4lU59Y2+TDYul6zczQbdllliaY340+2ZJQYvAVyYPVxxGEXqGMn3Cf3HNFuUN+XKDbDOml2YoFxooO9Ca7Szyb3zQL9j4afI1asUnI3NHDAGp8to43G2pcVrYwWIXP1MxkpxAh6NFkgYqtSNtsexBrdavXI9eneAjJlLOccLe86xswIDAQAB";
*/
    //key console loovers para compras
    public static final String base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAtWLbyRfz8+MeimRYBsRJNzRt1Ny2ozTzq5A9BEyzDhFV6h3nnm+3pRH9X0T91voNYqhHVRM96CDvRo8Wmvp459QKDCPcXvI/vQquazy/3QTriF45iL7ITPS+wI10WpUL4zOl2RjTxmP73mNWZrYeWS0tovoqZ/TnGHMCJ3Y/4lU59Y2+TDYul6zczQbdllliaY340+2ZJQYvAVyYPVxxGEXqGMn3Cf3HNFuUN+XKDbDOml2YoFxooO9Ca7Szyb3zQL9j4afI1asUnI3NHDAGp8to43G2pcVrYwWIXP1MxkpxAh6NFkgYqtSNtsexBrdavXI9eneAjJlLOccLe86xswIDAQAB";
    // The helper object
    public IabHelper mHelper;

    // Provides purchase notification while this app is running
    public IabBroadcastReceiver mBroadcastReceiver;
    boolean mIsPremium = false;
    // SKUs for our products: the premium upgrade (non-consumable) and gas (consumable)
    public static final String SKU_PREMIUM = "premium";

    public static final String SKU_GAS = "gas";

    static final int RC_REQUEST = 10001;
    // SKU for our subscription (infinite gas)
    //wallamatch_gold_sku_month_001 - producto oficial
    static final String SKU_INFINITE_GAS_MONTHLY = "wallamatch_gold_sku_month_001";//"test_wallamatch_sku_mensual";//"infinite_gas_monthly";
    public static final String SKU_RELOAD_WALLAS = "wallamatch_recarga_wallas_sku_002";

    static final String SKU_INFINITE_GAS_YEARLY = "infinite_gas_yearly";

    // Does the user have an active subscription to the infinite gas plan?
    boolean mSubscribedToInfiniteGas = false;

    // Will the subscription auto-renew?
    boolean mAutoRenewEnabled = false;

    // Tracks the currently owned infinite gas SKU, and the options in the Manage dialog
    public String mInfiniteGasSku = "";
    public String mFirstChoiceSku = "";
    public String mSecondChoiceSku = "";

    // Used to select between purchasing gas on a monthly or yearly basis
    public String mSelectedSubscriptionPeriod = "";
    // How many units (1/4 tank is our unit) fill in the tank.
   // public static final int TANK_MAX = 4;

    // Current amount of gas in tank, in units
    //public int mTank;

    private BroadcastReceiver uiUpdated;
    public IntentFilter filter;
    // Splash screen timer

    //atributos del fragmento match
    public GridView gridview;
    public RelativeLayout containerMatchEmpty;
    public Animation animationCake;

    public TextView titleTab, badgeTab,txtCredits,txtCreditsStore;
    public ImageView imgCreditsStore;

    /*productos loovers ids aplicación suspendida
    public static final String SKU_RELOAD_10_CREDITS            = "wallamatch_recarga_10monedas_sku_003";
    public static final String SKU_RELOAD_60_CREDITS            = "wallamatch_recarga_60monedas_sku_004";
    public static final String SKU_RELOAD_130_CREDITS           = "wallamatch_recarga_130monedas_sku_005";
    public static final String SKU_GOLD_1MONTH_SUBSCRIPTION     = "wallamatch_gold_1mes_sku_month_006";
    public static final String SKU_GOLD_3MONTH_SUBSCRIPTION     = "wallamatch_gold_3meses_sku_month_007";*/
    public static final String SKU_GOLD_24H                     = "wallamatch_gold_1dia_sku_month_008";

    public static final String SKU_RELOAD_10_CREDITS            = "loovers_recarga_10creditos_sku_001";
    public static final String SKU_RELOAD_60_CREDITS            = "loovers_recarga_60creditos_sku_002";
    public static final String SKU_RELOAD_130_CREDITS           = "loovers_recarga_130creditos_sku_003";
    public static final String SKU_GOLD_1MONTH_SUBSCRIPTION     = "loovers_gold_1mes_sku_004";
    public static final String SKU_GOLD_3MONTH_SUBSCRIPTION     = "loovers_gold_3mes_sku_005";

    //public static final String SKU_GOLD_1MONTH_TEST = "wallamatch_gold_1mes_test_sku_month_009";

    //perfil
    public ProfileGalleryAdapter adapter;
    //facebook
    public CallbackManager callbackManager;

    public FragmentTabHost mTabHost;

    IInAppBillingService mService;

    ServiceConnection mServiceConn = new ServiceConnection() {
        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService = null;
        }

        @Override
        public void onServiceConnected(ComponentName name,
                                       IBinder service) {
            mService = IInAppBillingService.Stub.asInterface(service);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Remove title bar
        // While testing call AppBrain.addTestDevice() here.
        AppBrain.init(this);


        AppsFlyerLib.getInstance().startTracking(this.getApplication(),"D5pM9WPraXtQiSnVJuHEu3");
        AppsFlyerLib.getInstance().setGCMProjectID("705758513230");
        //AppsFlyerLib.getInstance().init(this,"D5pM9WPraXtQiSnVJuHEu3");
        //AppsFlyerLib.getInstance().setGCMProjectID(this,"AIzaSyCpC0_ZwM6lsDwCCeCRk1dbzuwsSZbvgPQ");
        AppsFlyerLib.getInstance().setDebugLog(false);

        FacebookSdk.sdkInitialize(MainActivity.this);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.layout_container_main);

        Intent intent = new Intent("com.android.vending.billing.InAppBillingService.BIND");
        // This is the key line that fixed everything for me
        intent.setPackage("com.android.vending");

        bindService(intent, mServiceConn, Context.BIND_AUTO_CREATE);

        //inicializo preferencias
        initPreferences();

        //inicializo contadores
        initCounters();

        //inicializar compras dentro de la app
        onCreateBilling();

        GlobalData.set(Constant.APP_ON, "Y", MainActivity.this);

        container_finish_register       = (RelativeLayout) findViewById(R.id.container_finish_register);
        container_personal_status       = (RelativeLayout) findViewById(R.id.container_personal_status);
        container_questions             = (RelativeLayout) findViewById(R.id.container_questions);
        container_new_notification      = (RelativeLayout) findViewById(R.id.container_new_notification);
        containerBirthdayUser           = (RelativeLayout)findViewById(R.id.containerBirthdayUser);
        container_button_menu           = (RelativeLayout) findViewById(R.id.container_button_menu);
        btnOkBirthday                   = (ImageView)findViewById(R.id.btnOkBirthday);
        imgCredits                      = (ImageView)findViewById(R.id.imgCredits);
        txtBirthday                     = (TextView)findViewById(R.id.txtBirthday);
        containerCredits                = (LinearLayout)findViewById(R.id.containerCredits);
        txtCredits                      = (TextView)findViewById(R.id.txtCredits);
        txtCredits.setTypeface(TypesLetter.getSansRegular(MainActivity.this));
        String numLikes = GlobalData.isKey(Constant.CURRENT_LIKES,MainActivity.this)?GlobalData.get(Constant.CURRENT_LIKES,MainActivity.this):"0";
        txtCredits.setText(""+ numLikes);
        validateNumCredits(numLikes);


        if(!GlobalData.isKey(Constant.BIRTHDAY_VERIFIED,MainActivity.this))
        {
            animationCake = AnimationUtils.loadAnimation(MainActivity.this,R.anim.scale_cake);
            txtBirthday.startAnimation(animationCake);
        }
        //inicializar evento de fecha
        showDialogDate();

        frame_view_shadom       = (FrameLayout) findViewById(R.id.frame_view_shadom);
        Visibility.gone(frame_view_shadom);

        btn_begin               = (Button) findViewById(R.id.btn_begin);
        btn_ok                  = (Button) findViewById(R.id.btn_ok);
        btn_op1                 = (Button) findViewById(R.id.opcion_uno);
        btn_op2                 = (Button) findViewById(R.id.opcion_dos);
        btn_op3                 = (Button) findViewById(R.id.opcion_tres);
        txtDulce                = (TextView)findViewById(R.id.txtDulce);
        txtApasionada           = (TextView)findViewById(R.id.txtApasionada);
        txtAtrevida             = (TextView)findViewById(R.id.txtAtrevida);
        txtTraviesa             = (TextView)findViewById(R.id.txtTraviesa);
        txtAlertNewMessage      = (TextView)findViewById(R.id.txtAlertNewMessage);

        btn_op1.setTransformationMethod(null);
        btn_op2.setTransformationMethod(null);
        btn_op3.setTransformationMethod(null);

        String genere = (GlobalData.isKey(Constant.GENERE,MainActivity.this))?GlobalData.get(Constant.GENERE,MainActivity.this):Constant.MAN;

        btn_op2.setText((genere.equals(Constant.MAN))?getResources().getString(R.string.invite_male):getResources().getString(R.string.invite_female));
        txtDulce.setText((genere.equals(Constant.MAN)) ? getResources().getString(R.string.romantic) : getResources().getString(R.string.sweet));
        txtApasionada.setText((genere.equals(Constant.MAN)) ? getResources().getString(R.string.retailer) : getResources().getString(R.string.passionate));
        txtAtrevida.setText((genere.equals(Constant.MAN)) ? getResources().getString(R.string.daring) : getResources().getString(R.string.bold));
        txtTraviesa.setText((genere.equals(Constant.MAN))?getResources().getString(R.string.seductive):getResources().getString(R.string.naughty));
        containerContinue = (LinearLayout) findViewById(R.id.containerContinue);

        mFragmentManager = getSupportFragmentManager();

        rangeDulce      = (RangeBar) findViewById(R.id.rangebar_dulce);
        rangeApasionada = (RangeBar) findViewById(R.id.rangebar_apasionada);
        rangeAtrevida   = (RangeBar) findViewById(R.id.rangebar_atrevida);
        rangeTraviesa   = (RangeBar) findViewById(R.id.rangebar_traviesa);
        imgDulce        = (ImageView) findViewById(R.id.img_dulce);
        imgApasionada   = (ImageView) findViewById(R.id.img_apasionada);
        imgAtrevida     = (ImageView) findViewById(R.id.img_atrevida);
        imgTraviesa     = (ImageView) findViewById(R.id.img_traviesa);
        imgBtnOk        = (ImageView) findViewById(R.id.btn_questions);

        Common.setPropertiesRange(rangeDulce, 5, true, false);
        Common.setPropertiesRange(rangeApasionada, 5, true, false);
        Common.setPropertiesRange(rangeAtrevida, 5, true, false);
        Common.setPropertiesRange(rangeTraviesa, 5, true, false);

        rangeDulce.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
            @Override
            public void onRangeChangeListener(RangeBar rangeBar, int leftPinIndex, int rightPinIndex, String leftPinValue, String rightPinValue) {
                //marca la position rightPinIndex
                setImageValue(imgDulce, rightPinIndex);

            }
        });
        rangeApasionada.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
            @Override
            public void onRangeChangeListener(RangeBar rangeBar, int leftPinIndex, int rightPinIndex, String leftPinValue, String rightPinValue) {
                //marca la position rightPinIndex
                setImageValue(imgApasionada, rightPinIndex);

            }

        });
        rangeAtrevida.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
            @Override
            public void onRangeChangeListener(RangeBar rangeBar, int leftPinIndex, int rightPinIndex, String leftPinValue, String rightPinValue) {
                //marca la position rightPinIndex
                setImageValue(imgAtrevida, rightPinIndex);

            }
        });
        rangeTraviesa.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
            @Override
            public void onRangeChangeListener(RangeBar rangeBar, int leftPinIndex, int rightPinIndex, String leftPinValue, String rightPinValue) {
                //marca la position rightPinIndex
                setImageValue(imgTraviesa, rightPinIndex);

            }
        });
        imgBtnOk.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    JSONObject option1 = new JSONObject();
                    option1.put("option", "1");
                    option1.put("value", "" + rangeDulce.getRightPinValue());

                    JSONObject option2 = new JSONObject();
                    option2.put("option", "2");
                    option2.put("value", "" + rangeApasionada.getRightPinValue());

                    JSONObject option3 = new JSONObject();
                    option3.put("option", "3");
                    option3.put("value", "" + rangeAtrevida.getRightPinValue());

                    JSONObject option4 = new JSONObject();
                    option4.put("option", "4");
                    option4.put("value", "" + rangeTraviesa.getRightPinValue());

                    JSONArray jsonArray = new JSONArray();

                    jsonArray.put(option1);
                    jsonArray.put(option2);
                    jsonArray.put(option3);
                    jsonArray.put(option4);

                    if(ConnectionDetector.isConnectingToInternet(MainActivity.this))
                        updateDescriptionOptions(jsonArray);
                    else
                        Common.errorMessage(MainActivity.this,"",getResources().getString(R.string.check_internet));



                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

        btnOkBirthday.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                String txtDate = txtBirthday.getText().toString().trim();

                SimpleDateFormat fromUser = new SimpleDateFormat("dd/MM/yyyy");

                SimpleDateFormat myFormat = new SimpleDateFormat("yyyy/MM/dd");
                try {
                    txtDate = myFormat.format(fromUser.parse(txtDate));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                if(!txtDate.isEmpty()) {
                    if(ConnectionDetector.isConnectingToInternet(MainActivity.this))
                        updateProfile(txtDate);
                    else
                        Common.errorMessage(MainActivity.this,"",getResources().getString(R.string.check_internet));
                }
                else
                    Common.errorMessage(MainActivity.this,"",getResources().getString(R.string.cant_birthday));
            }
        });

        containerCredits.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogStore dialogStore = new DialogStore(MainActivity.this,0);
                dialogStore.show(mFragmentManager,"DialogStore.TAG");
            }
        });



        activity_main_content_fragment  = (FrameLayout) findViewById(R.id.activity_main_content_fragment);
        tvTitle                         = (TextView) findViewById(R.id.activity_main_content_title);
        topbartextviewFont              = TypesLetter.getAvenir(MainActivity.this);
        tvTitle.setTypeface(topbartextviewFont);

        menu = new SlidingMenu(this);
        menu.setMode(SlidingMenu.LEFT);
        menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
        menu.setShadowWidthRes(R.dimen.shadow_width);
        menu.setShadowDrawable(R.drawable.shadow);

        menu.setBehindOffsetRes(R.dimen.slidingmenu_offset);
        menu.setFadeDegree(0.35f);
        menu.attachToActivity(this, SlidingMenu.SLIDING_CONTENT);
        //Log.d(TAG, "onCreate  before add menu ");
        menu.setMenu(R.layout.left_menu);
        //menu.setSecondaryMenu(R.layout.rightmenu);

        menu.setSlidingEnabled(false);

        View leftmenuview = menu.getMenu();
        //View rightmenuview = menu.getSecondaryMenu();

        initLayoutComponent(leftmenuview);

        menu.setOnOpenListener(new OnOpenListener() {
            @Override
            public void onOpen() {
                apperBackgroundGradient(true);
            }
        });
        menu.setOnCloseListener(new SlidingMenu.OnCloseListener() {
            @Override
            public void onClose() {
                apperBackgroundGradient(false);
            }
        });
        //menu.setSecondaryOnOpenListner(this);


        btMenu = (Button) findViewById(R.id.button_menu);

        Bundle bundle = getIntent().getExtras();
        if(bundle==null)
        {

        }
        else {


            int optionMenu = bundle.getInt("finish_tutorial");
            if(optionMenu == 1)
            {

                menu.toggle();
                //Common.errorMessage(MainActivity.this,"","Pronto te daremos respuesta acerca de la foto ;)");
                final NiftyDialogBuilder dialog =  NiftyDialogBuilder.getInstance(MainActivity.this);
                dialog.withTitle(null);
                dialog.withMessage(null);
                dialog.isCancelableOnTouchOutside(false);
                dialog.withDialogColor(Color.TRANSPARENT);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.withDuration(500);
                dialog.withEffect(Effectstype.Slidetop);
                dialog.setCustomView(R.layout.modal_general,MainActivity.this);

                TextView title = (TextView)dialog.findViewById(R.id.title);
                title.setText(getResources().getString(R.string.title_modal_welcome)+" "+Common.getNickName(MainActivity.this));
                TextView text = (TextView) dialog.findViewById(R.id.text);
                text.setText(""+getResources().getString(R.string.advert_photo_pending_welcome));


                Button exit = (Button) dialog.findViewById(R.id.exit);
                Visibility.gone(exit);
                Button accept = (Button) dialog.findViewById(R.id.accept);
                // if button is clicked, close the custom dialog
                exit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                accept.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();


                    }
                });

                dialog.show();
            }
            else
                menu.toggle();



        }

        btMenu.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                apperBackgroundGradient(true);
                toggleMenu(v);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        setProfilePick(profileimage);
                    }
                },100);


            }
        });
        container_button_menu.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                apperBackgroundGradient(true);
                    toggleMenu(v);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        setProfilePick(profileimage);
                    }
                },100);


            }
        });

        profileimage.setOnClickListener(this);
        homelayout.setOnClickListener(this);
        favoritelayout.setOnClickListener(this);
        messages.setOnClickListener(this);
        profiletextview.setOnClickListener(this);
        matchlayout.setOnClickListener(this);
        flechazosLayout.setOnClickListener(this);
        nearLayout.setOnClickListener(this);
        imgBackGroundProfile.setOnClickListener(this);
        img_preferences.setOnClickListener(this);
        imgSecurity.setOnClickListener(this);



        //usuario nuevo
        if (GlobalData.isKey(Constant.FIRST_REGISTER, MainActivity.this)) {
            if (GlobalData.get(Constant.FIRST_REGISTER, MainActivity.this).equals("Y")) {
                GlobalData.set(Constant.FIRST_REGISTER, "N", MainActivity.this);
                Visibility.visible(container_finish_register);
                Visibility.invisible(container_personal_status);
                Visibility.invisible(container_questions);
                Visibility.invisible(containerBirthdayUser);
                container_button_menu.setEnabled(false);
                btMenu.setEnabled(false);

            } else {
               // Log.d("app2you","primera vez 4");
                Visibility.gone(container_finish_register);
                Visibility.gone(container_personal_status);
                Visibility.gone(container_questions);
                Visibility.gone(containerBirthdayUser);

                //check version app
                checkVersionApp();

                container_button_menu.setEnabled(true);
                btMenu.setEnabled(true);

            }
        }


        if(container_finish_register.getVisibility() == View.VISIBLE || container_personal_status.getVisibility() == View.VISIBLE ||
                container_questions.getVisibility()== View.VISIBLE || containerBirthdayUser.getVisibility() == View.VISIBLE) {
            //Log.d("app2you","primera vez 2");
            Visibility.enableDisableView(activity_main_content_fragment, false);

        }
        else
        {
            //Log.d("app2you","primera vez 3");
            Visibility.enableDisableView(activity_main_content_fragment, true);

            Bundle b = getIntent().getExtras();
            if(b == null)
            {
              //  Log.d("app2you","extras en null");

                if(GlobalData.isKey(Constant.TUTORIAL_GAME,MainActivity.this))
                {

                        if(menu.isMenuShowing())
                            menu.toggle();


                    //el segundo paramentro me indica si muesto la barra de progreso cuando esta buscando personas alrededor. (1)- NO MUESTRO PROGRESO
                    // Toast.makeText(MainActivity.this, "Entro aqui tercer vez", Toast.LENGTH_SHORT).show();
                    FragmentGame fragment = new FragmentGame(MainActivity.this,1);
                    replaceFragment(fragment);
                    tvTitle.setText(getResources().getString(R.string.app_name));

                }
                else
                {
                    //el segundo paramentro me indica si muesto la barra de progreso cuando esta buscando personas alrededor. (1)- NO MUESTRO PROGRESO

                    FragmentGame fragment = new FragmentGame(MainActivity.this,1);
                    replaceFragment(fragment);
                    tvTitle.setText(getResources().getString(R.string.app_name));
                }

            }
            else
            {

                //Log.d("app2you", "extras no null: " + b.getInt("from"));
                int from = b.getInt("from");

                if(b.containsKey("finish_tutorial"))
                {
                    getIntent().removeExtra("finish_tutorial");
                }
                else {
                    if (menu.isMenuShowing())
                        menu.toggle();
                }

                switch (from)
                {
                    case 1:
                        //FriendsFragment friendsFragment = new FriendsFragment(MainActivity.this);
                        MessageFragment friendsFragment = new MessageFragment(MainActivity.this);
                        replaceFragment(friendsFragment);
                        tvTitle.setText(""+getResources().getString(R.string.messages));

                        break;
                    case 2:
                        FragmentWallas fragmentWallas = new FragmentWallas(MainActivity.this);
                        replaceFragment(fragmentWallas);
                        tvTitle.setText(getResources().getString(R.string.likes));

                        break;
                    case 3:
                        /*FragmentMatch fragmentMatch = new FragmentMatch(MainActivity.this);
                        replaceFragment(fragmentMatch);
                        tvTitle.setText(getResources().getString(R.string.match));*/
                        if (profileimage != null)
                            setProfilePick(profileimage);

                        if (!menu.isMenuShowing())
                            menu.toggle();

                        break;
                    default:
                        FragmentGame fragment = new FragmentGame(MainActivity.this);
                        replaceFragment(fragment);
                        tvTitle.setText(getResources().getString(R.string.app_name));
                        break;
                }
                getIntent().removeExtra("from");


            }

        }

        btn_begin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Visibility.gone(container_finish_register);
                Visibility.gone(container_questions);
                Visibility.visible(container_personal_status);
            }
        });

        //acciones personal status

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(ConnectionDetector.isConnectingToInternet(MainActivity.this))
                    updatePersonalStatus();
                else
                    Common.errorMessage(MainActivity.this,"",getResources().getString(R.string.check_internet));

            }
        });

        btn_op1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GlobalData.set(Constant.PERSONAL_STATUS, "" + Constant.CHATEAR_UN_RATO, MainActivity.this);
                animateContainer();
                btn_op1.setBackgroundResource(R.drawable.slect_hombre);
                btn_op2.setBackgroundResource(R.drawable.fondo_boton_hombre);
                btn_op3.setBackgroundResource(R.drawable.fondo_boton_hombre);

                btn_op2.setTextColor(Color.parseColor("#c3c3c3"));
                btn_op3.setTextColor(Color.parseColor("#c3c3c3"));
                btn_op1.setTextColor(Color.WHITE);

            }
        });
        btn_op2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GlobalData.set(Constant.PERSONAL_STATUS, "" + Constant.INVITARME_A_SALIR, MainActivity.this);
                animateContainer();
                btn_op2.setBackgroundResource(R.drawable.slect_hombre);
                btn_op1.setBackgroundResource(R.drawable.fondo_boton_hombre);
                btn_op3.setBackgroundResource(R.drawable.fondo_boton_hombre);

                btn_op3.setTextColor(Color.parseColor("#c3c3c3"));
                btn_op1.setTextColor(Color.parseColor("#c3c3c3"));
                btn_op2.setTextColor(Color.WHITE);
            }
        });
        btn_op3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GlobalData.set(Constant.PERSONAL_STATUS, "" + Constant.SALTARME_LOS_PRELIMINARES, MainActivity.this);
                animateContainer();
                btn_op3.setBackgroundResource(R.drawable.slect_hombre);
                btn_op1.setBackgroundResource(R.drawable.fondo_boton_hombre);
                btn_op2.setBackgroundResource(R.drawable.fondo_boton_hombre);

                btn_op2.setTextColor(Color.parseColor("#c3c3c3"));
                btn_op1.setTextColor(Color.parseColor("#c3c3c3"));
                btn_op3.setTextColor(Color.WHITE);
            }
        });

        container_new_notification.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                GlobalData.set(Constant.COUNTER_MESSAGES,"0",MainActivity.this);
                //FriendsFragment friendsFragment = new FriendsFragment(MainActivity.this);
                MessageFragment friendsFragment = new MessageFragment(MainActivity.this);
                tvTitle.setText(""+getResources().getString(R.string.messages));
                if (friendsFragment != null)
                    replaceFragment(friendsFragment);
            }
        });
        filter = new IntentFilter();
        filter.addAction("LOCATION_UPDATED");

        uiUpdated = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {

                //TextView.setText(intent.getExtras().getString("<KEY>"))
                //String name = intent.getExtras().getString("name");
                String type = intent.getExtras().getString("type");
                if (type.equals("1")) {
                  //  Log.d("app2you","EJECUTO SERVICIO DE GETWALLAS");
                    //getNumWallas();
                    if(menu != null && menu.isMenuShowing())
                        setProfilePick(profileimage);
                }
                else if (type.equals("3"))
                {
                    verifiedSubsProduct();
                }
                else {
                    if(menu != null && menu.isMenuShowing())
                        setProfilePick(profileimage);

                    String otherUserId      = intent.getExtras().getString("otherUserId");
                    String lastestMessage   = intent.getExtras().getString("lastestMessage");
                    String date             = intent.getExtras().getString("date");
                    String counter_messages = intent.getExtras().getString("counter_messages");

                    MatchesData matchesData     = new MatchesData();
                    matchesData.id_user         = otherUserId;
                    matchesData.lastestMessage  = lastestMessage;
                    matchesData.date            = date;
                    matchesData.count_messages  = counter_messages;
                    matchesData.senderIdLastestMessage = Integer.parseInt(otherUserId);

                    updateAllChats(matchesData);


                    /*Visibility.visible(container_new_notification);
                    txtAlertNewMessage.setText(getResources().getString(R.string.new_message_from)+" " + name);
                    AppAnimation.fadeIn(container_new_notification, 200);
                    new Handler().postDelayed(new Runnable() {

                        public void run() {
                            AppAnimation.fadeOut(container_new_notification, 300);
                            GlobalData.delete("notification_new", MainActivity.this);
                        }
                    }, SPLASH_TIME_OUT);*/
                }
            }
                //Toast.makeText(MainActivity.this, "llego : "+ message, Toast.LENGTH_SHORT).show();

        };



    }

    public void validateNumCredits(String numLikes) {
        if(numLikes.equals("0"))
        {
            imgCredits.setImageResource(R.drawable.no_credito);
            txtCredits.setTextColor(Color.parseColor("#c3c3c3"));
        }
        else
        {
            imgCredits.setImageResource(R.drawable.credito);
            txtCredits.setTextColor(Color.parseColor("#d1a52e"));
        }
    }

    /**
     * Método encargado de mostrar dialogo con opción para seleccionar fecha de nacimiento
     */
    public void showDialogDate()
    {
        final Calendar myCalendar = Calendar.getInstance();

        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                Calendar userAge = new GregorianCalendar(year,monthOfYear,dayOfMonth);
                Calendar minAdultAge = new GregorianCalendar();
                minAdultAge.add(Calendar.YEAR, - 18);
                if (minAdultAge.before(userAge))
                {
                    Common.errorMessage(MainActivity.this,"",getResources().getString(R.string.higher_age));
                   // Toast.makeText(LoginUsingFacebookMailOrGooglev2.this, R.string.higher_age, Toast.LENGTH_SHORT).show();
                }
                else {
                    myCalendar.set(Calendar.YEAR, year);
                    myCalendar.set(Calendar.MONTH, monthOfYear);
                    myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                    Visibility.visible(btnOkBirthday);
                    txtBirthday.clearAnimation();
                    updateLabel(myCalendar);
                }
            }

        };

        txtBirthday.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                setDatePicker(date, myCalendar);
            }
        });

    }
    public void setDatePicker(DatePickerDialog.OnDateSetListener date,Calendar myCalendar)
    {
        int age = 0;
        if(txtBirthday.getText().toString().trim().length() == 0)
            age = 18;

        new DatePickerDialog(MainActivity.this, date, myCalendar
                .get(Calendar.YEAR) - age, myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }
    /**
     * Actualiza el campo de la fecha
     * @param myCalendar
     */
    private void updateLabel(Calendar myCalendar) {

        String myFormat = "dd/MM/yyyy";//"yyyy/MM/dd" ;
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat);
        txtBirthday.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        int sdk = android.os.Build.VERSION.SDK_INT;
        if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            txtBirthday.setBackgroundDrawable(null);
        } else {
            txtBirthday.setBackground(null);
        }

        txtBirthday.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
        txtBirthday.setText(sdf.format(myCalendar.getTime()));
    }
    /**
     * Animacion boton continuar
     */
    public void animateContainer() {
        if (containerContinue.getVisibility() == View.INVISIBLE) {
            Visibility.visible(containerContinue);
            AppAnimation.fadeIn(containerContinue, 400);
        }

    }

    /**
     * Método encargado de actualizar el estado personal de un usuario
     */
    public void updatePersonalStatus() {
        final ProgressDialog progressDialog = new ProgressDialog(this,R.style.CustomDialogTheme);
        progressDialog.setCancelable(false);
       // progressDialog.setMessage(""+getResources().getString(R.string.saving));
        progressDialog.show();

        progressDialog.setContentView(R.layout.custom_progress_dialog);
        TextView txtView = (TextView) progressDialog.findViewById(R.id.txtProgressDialog);
        txtView.setText(""+getResources().getString(R.string.saving));

        OkHttpClient client = new OkHttpClient();

        RequestBody formBody = new FormBody.Builder()
                .add("sessionToken", "" + GlobalData.get(Constant.SESSION_TOKEN, MainActivity.this))
                .add("personalStatus", "" + GlobalData.get(Constant.PERSONAL_STATUS, MainActivity.this))
                .build();

        Request request = new Request.Builder()
                .url(Services.UPDATE_PREFERENCES_SERVICE)
                .post(formBody)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                progressDialog.dismiss();
                Looper.prepare();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Common.errorMessage(MainActivity.this, "", "" + getResources().getString(R.string.check_internet));
                    }
                });

                Looper.loop();
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                progressDialog.dismiss();
                final String result = response.body().string();

                Looper.prepare();

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //apago el dialogo
                            try {
                                JSONObject json = new JSONObject(result);
                                String success = json.getString("success");
                                if (success.equals("true")) {
                                    Visibility.gone(container_personal_status);
                                    Visibility.visible(container_questions);
                                } else {
                                    Visibility.gone(container_personal_status);
                                    Visibility.visible(container_questions);
                                }

                            } catch (JSONException ex) {
                                ex.printStackTrace();
                                Toast.makeText(MainActivity.this, R.string.error_reponse_server, Toast.LENGTH_SHORT).show();
                            }
                            finally {
                                response.body().close();
                            }
                        }
                    });


                Looper.loop();
            }
        });
    }

    /**
     * Método encargado de actualizar la descripción del usuario
     */
    public void updateDescriptionOptions(JSONArray options) {
        final ProgressDialog progressDialog = new ProgressDialog(this,R.style.CustomDialogTheme);
        progressDialog.setCancelable(false);
      //  progressDialog.setMessage(""+getResources().getString(R.string.saving_preferences));
        progressDialog.show();
        progressDialog.setContentView(R.layout.custom_progress_dialog);
        TextView txtView = (TextView) progressDialog.findViewById(R.id.txtProgressDialog);
        txtView.setText(""+getResources().getString(R.string.saving_preferences));


        OkHttpClient client = new OkHttpClient();

        RequestBody formBody = new FormBody.Builder()
                .add("sessionToken", "" + GlobalData.get(Constant.SESSION_TOKEN, MainActivity.this))
                .add("descriptionOptions", "" + options.toString())
                .build();

        Request request = new Request.Builder()
                .url(Services.UPDATE_PREFERENCES_SERVICE)
                .post(formBody)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                progressDialog.dismiss();
                Looper.prepare();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Common.errorMessage(MainActivity.this, "", "" + getResources().getString(R.string.check_internet));
                    }
                });

                Looper.loop();
            }

            @Override
            public void onResponse(Call call,final Response response) throws IOException {
                progressDialog.dismiss();
                final String result = response.body().string();

                Looper.prepare();

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //apago el dialogo
                            try {
                                JSONObject json = new JSONObject(result);
                                String success = json.getString("success");
                                if (success.equals("true")) {
                                    if(GlobalData.isKey(Constant.LOGIN_FACEBOOK, MainActivity.this))
                                    {
                                        Visibility.gone(container_questions);
                                        Visibility.visible(containerBirthdayUser);
                                    }
                                    else {
                                        Intent intent = new Intent(MainActivity.this, UploadPhotoProfileActivity.class);
                                        intent.putExtra("TYPE", 1);
                                        startActivity(intent);
                                        finish();
                                    }

                                } else {
                                    Toast.makeText(MainActivity.this, R.string.check_internet, Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException ex) {
                                ex.printStackTrace();
                                Toast.makeText(MainActivity.this, R.string.error_reponse_server, Toast.LENGTH_SHORT).show();
                            }
                            finally {
                                response.body().close();
                            }
                        }
                    });


                Looper.loop();
            }
        });
    }



    @Override
    protected void onPause() {
        super.onPause();
        if(uiUpdated != null)
            unregisterReceiver(uiUpdated);

//        BaseApplication.getCurrent().applicationStopped();
        GlobalData.set(Constant.APP_ON, "N", MainActivity.this);
        GlobalData.delete(Constant.ACTIVITY_FROM_CAMERA,MainActivity.this);

        GlobalData.delete("listPersons",MainActivity.this);
        GlobalData.delete("listnewpersons",MainActivity.this);
    }


    @Override
    protected void onResume() {
        super.onResume();

        if (GlobalData.isKey(Constant.PIN_SECURITY, MainActivity.this) ) {

            if(GlobalData.isKey(Constant.ACTIVITY_FROM_CAMERA,MainActivity.this))
                GlobalData.delete(Constant.ACTIVITY_FROM_CAMERA,MainActivity.this);
            else {
                FragmentSecurity fragmentSecurity = new FragmentSecurity(MainActivity.this, 1);
                fragmentSecurity.setCancelable(false);
                fragmentSecurity.show(mFragmentManager, "FragmentSecurity.TAG");
            }
        }

        if (uiUpdated != null)
            registerReceiver(uiUpdated, filter);


        if (profiletextview != null)
            profiletextview.setText("" + GlobalData.get(Constant.NICKNAME, MainActivity.this)/*Settings.getUserName(this)*/);

        if (profileimage != null)
            setProfilePick(profileimage);

        //agregar logica de notificaciones

        Bundle b = getIntent().getExtras();
        if(b != null) {
           // Log.d("app2you", "extras no null: " + b.getInt("from"));
            int from = b.getInt("from");
            openFromNoticaction(from);
            getIntent().removeExtra("from");
            if(GlobalData.isKey("from",MainActivity.this))
                GlobalData.delete("from",MainActivity.this);

        }
        else
        {
            int tmp = (GlobalData.isKey("from",MainActivity.this))?Integer.parseInt(GlobalData.get("from", MainActivity.this)):99;
            openFromNoticaction(tmp);
            GlobalData.delete("from", MainActivity.this);
         //   Log.d("app2you", "no hay nada en los extras");
        }

        //esperar confirmacion de email
        final Intent intent = getIntent();
        final String action = intent.getAction();
      //  Log.d("app2you","arranco de una vez la app: accion - " + action);
        if (Intent.ACTION_VIEW.equals(action)) {
            final List<String> segments = intent.getData().getPathSegments();
            if (segments.size() > 1) {
                String idMail = segments.get(1);
             //   Log.d("app2you","llego desde url : " + idMail);
                if(menu != null && menu.isMenuShowing())
                    menu.toggle();

                if(GlobalData.isKey(Constant.EMAIL_VERIFIED,MainActivity.this) && GlobalData.get(Constant.EMAIL_VERIFIED,MainActivity.this).equals("Y")) {
                    // Common.errorMessage(this, "", "Tu correo ya ha sido verificado");
                }
                else
                    confirmEmail(idMail);
            }
        }

        //validar los días
        compareTwoDays();




    }

    /**
     * Metodo encargado de actualizar el menu lateral
     * @param userProfilImage
     */
    public void setProfilePick(ImageView userProfilImage) {

        String status = GlobalData.get(Constant.USER_STATUS, MainActivity.this);
        //Log.d("app2you", "setProfilePick status usr is: " + status);

        if(userProfilImage != null) {
            if (!status.equals(Constant.NOT_GOLD_BEFORE_TRIAL_NO_PICTURE) && GlobalData.isKey(Constant.AVATAR_USER, MainActivity.this) && !GlobalData.get(Constant.AVATAR_USER, MainActivity.this).equals("") ) {
                Picasso.with(MainActivity.this).load(GlobalData.get(Constant.AVATAR_USER, MainActivity.this)).error(R.drawable.no_foto).resize(350, 350).centerCrop().transform(new CircleTransform()).into(userProfilImage);
                Picasso.with(MainActivity.this).load(GlobalData.get(Constant.AVATAR_USER, MainActivity.this)).error(R.drawable.no_foto).transform(new BlurTransformation(25)).into(imgBackGroundProfile);
            }
            else {
                String url = GlobalData.isKey(Constant.AVATAR_USER,MainActivity.this)?GlobalData.get(Constant.AVATAR_USER,MainActivity.this):"";
                if(url.isEmpty()) {
                    Picasso.with(MainActivity.this).load(R.drawable.no_foto).resize(200, 200).transform(new CircleTransform()).into(userProfilImage);
                    Picasso.with(MainActivity.this).load(R.drawable.no_foto).transform(new BlurTransformation(10)).into(imgBackGroundProfile);
                }
                else
                {
                    Picasso.with(MainActivity.this).load(url).error(R.drawable.no_foto).resize(200, 200).transform(new CircleTransform()).into(userProfilImage);
                    Picasso.with(MainActivity.this).load(url).error(R.drawable.no_foto).transform(new BlurTransformation(10)).into(imgBackGroundProfile);
                }

            }
        }
        imgSecurity.setImageResource(GlobalData.isKey(Constant.PIN_SECURITY,MainActivity.this)?R.drawable.lock:R.drawable.unlock);
        //btn_gold.setVisibility((status.equals(Constant.NOT_GOLD_AFTER_TRIAL))?View.VISIBLE:View.GONE);

        if (status.equals(Constant.GOLD) /*|| status.equals(Constant.GOLD_TRIAL)*/) {
            //usuario gold
            layerimage.setImageResource(R.drawable.circulo_foto_gold);
            imageGame.setImageResource(R.drawable.icono_menu_juego_gold);
            imageWallas.setImageResource(R.drawable.icono_menu_conocerte_gold);
            imageMatch.setImageResource(R.drawable.icono_menu_match_gold);
            imageMessages.setImageResource(R.drawable.icono_menu_mensaje_gold);
            imageFavorites.setImageResource(R.drawable.corazon_gold);
            imageNearToYou.setImageResource(R.drawable.icono_menu_cercadeti_gold);
            profiletextview.setTextColor(Color.parseColor("#e8cd7d"));
            menu_left.setBackgroundColor(Color.parseColor("#2b2b2b"));
            img_preferences.setImageResource(R.drawable.configuracion_gold);
            txtCounterMatches.setBackgroundResource(R.drawable.alerta_gold);
            txtCounterMessages.setBackgroundResource(R.drawable.alerta_mensaje);
            txtCounterWallas.setBackgroundResource(R.drawable.alerta_gold);
            //FFD76E
        } else {
            //Normal
            layerimage.setImageResource(R.drawable.circulo_foto_perfil);
            imageGame.setImageResource(R.drawable.icono_menu_juego);
            imageWallas.setImageResource(R.drawable.icono_menu_conocerte);
            imageMatch.setImageResource(R.drawable.icono_menu_match);
            imageMessages.setImageResource(R.drawable.icono_menu_mensaje);
            imageFavorites.setImageResource(R.drawable.icono_menu_favoritos);
            imageNearToYou.setImageResource(R.drawable.icono_menu_cercadeti);
            profiletextview.setTextColor(Color.WHITE);
            menu_left.setBackgroundColor(Color.parseColor("#1d1c2f"));
            img_preferences.setImageResource(R.drawable.ic_settings_white_24dp);
            txtCounterMatches.setBackgroundResource(R.drawable.alerta);
            txtCounterMessages.setBackgroundResource(R.drawable.alerta_mensaje);
            txtCounterWallas.setBackgroundResource(R.drawable.alerta);

        }
        //verificar contadores
        String counterWallas = (GlobalData.isKey(Constant.COUNTER_WALLAS,MainActivity.this))?GlobalData.get(Constant.COUNTER_WALLAS, MainActivity.this):"0";
        String counterMatches = (GlobalData.isKey(Constant.COUNTER_MATCHS,MainActivity.this))?GlobalData.get(Constant.COUNTER_MATCHS, MainActivity.this):"0";
        String counterMessages = (GlobalData.isKey(Constant.COUNTER_MESSAGES,MainActivity.this))?GlobalData.get(Constant.COUNTER_MESSAGES, MainActivity.this):"0";


        if(!counterWallas.equals("0") && (Integer.parseInt(status) > 2))
        {
            Visibility.visible(txtCounterWallas);
            txtCounterWallas.setText(""+counterWallas);
        }
        else {
            Visibility.gone(txtCounterWallas);
        }

        if(!counterMatches.equals("0"))
        {
            Visibility.visible(txtCounterMatches);
            txtCounterMatches.setText(""+counterMatches);
        } else
            Visibility.gone(txtCounterMatches);


        if(!counterMessages.equals("0"))
        {
            Visibility.visible(txtCounterMessages);
        }
        else
            Visibility.gone(txtCounterMessages);

    }

    /**
     * Se inicializan los componentes.
     *
     * @param leftmenu
     */
    private void initLayoutComponent(View leftmenu) {

        profileimage                = (ImageView) leftmenu.findViewById(R.id.profileimage);
        imgBackGroundProfile        = (ImageView) leftmenu.findViewById(R.id.imgBackGroundProfile);
        menu_left                   = (RelativeLayout) leftmenu.findViewById(R.id.menu_left);
        img_preferences             = (ImageView) leftmenu.findViewById(R.id.img_preferences);
        imgSecurity                 = (ImageView) leftmenu.findViewById(R.id.imgSecurity);
        homelayout                  = (LinearLayout) leftmenu.findViewById(R.id.homelayout);
        favoritelayout              = (LinearLayout) leftmenu.findViewById(R.id.favoritelayout);
        matchlayout                 = (LinearLayout) leftmenu.findViewById(R.id.matchlayout);
        flechazosLayout             = (LinearLayout) leftmenu.findViewById(R.id.flechazoslayout);
        nearLayout                  = (LinearLayout)leftmenu.findViewById(R.id.nearLayout);
        messages                    = (LinearLayout) leftmenu.findViewById(R.id.messages);


        Typeface openSans           = TypesLetter.getSansLight(MainActivity.this);

        //iconos y circulo imagen
        layerimage                  = (ImageView) leftmenu.findViewById(R.id.layerimage);
        imageGame                   = (ImageView) leftmenu.findViewById(R.id.imageGame);
        imageWallas                 = (ImageView) leftmenu.findViewById(R.id.imageWallas);
        imageMatch                  = (ImageView) leftmenu.findViewById(R.id.imageMatch);
        imageMessages               = (ImageView) leftmenu.findViewById(R.id.imageMessages);
        imageFavorites              = (ImageView) leftmenu.findViewById(R.id.imageFavorites);
        imageNearToYou              = (ImageView) leftmenu.findViewById(R.id.imageNearToYou);

        profiletextview             = (TextView) leftmenu.findViewById(R.id.profiletextview);
        TextView txtGameWallamatch  = (TextView) leftmenu.findViewById(R.id.hometectview);
        TextView txtMessage         = (TextView) leftmenu.findViewById(R.id.massagetextview);

        TextView favoritestextview  = (TextView) leftmenu.findViewById(R.id.favoriteview);
        TextView matchtextview      = (TextView) leftmenu.findViewById(R.id.matchview);
        TextView lesgustastextview  = (TextView) leftmenu.findViewById(R.id.lesgustastextview);
        TextView txtNearToYou       = (TextView)leftmenu.findViewById(R.id.txtNearToYou);

        txtCounterWallas            = (TextView)leftmenu.findViewById(R.id.txtCounterWallas);
        txtCounterMessages          = (TextView)leftmenu.findViewById(R.id.txtCounterMessages);
        txtCounterMatches           = (TextView)leftmenu.findViewById(R.id.txtCounterMatches);

        profiletextview.setTypeface(openSans);
        txtGameWallamatch.setTypeface(openSans);
        txtMessage.setTypeface(openSans);

        favoritestextview.setTypeface(openSans);
        matchtextview.setTypeface(openSans);
        lesgustastextview.setTypeface(openSans);
        txtNearToYou.setTypeface(openSans);


    }

    /**
     * Método para comprar 10 créditos
     */
    public void buy10Coins() {

        String payload = GlobalData.isKey(Constant.MY_USER_ID,MainActivity.this)?GlobalData.get(Constant.MY_USER_ID,MainActivity.this):"";

        try {
            mHelper.launchPurchaseFlow(this, SKU_RELOAD_10_CREDITS, RC_REQUEST,
                    mPurchaseFinishedListener, payload);
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            Common.errorMessage(MainActivity.this,""+getResources().getString(R.string.app_name),""+getResources().getString(R.string.update_google_play_services));
        }

    }
    /**
     * Método para comprar 60 créditos
     */
    public void buy60Coins() {

        String payload = GlobalData.isKey(Constant.MY_USER_ID,MainActivity.this)?GlobalData.get(Constant.MY_USER_ID,MainActivity.this):"";

        try {
            mHelper.launchPurchaseFlow(this, SKU_RELOAD_60_CREDITS, RC_REQUEST,mPurchaseFinishedListener, payload);
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            Common.errorMessage(MainActivity.this,""+getResources().getString(R.string.app_name),""+getResources().getString(R.string.update_google_play_services));
        }
    }
    /**
     * Método para comprar 130 créditos
     */
    public void buy130Coins() {

        String payload = GlobalData.isKey(Constant.MY_USER_ID,MainActivity.this)?GlobalData.get(Constant.MY_USER_ID,MainActivity.this):"";

        try {
            mHelper.launchPurchaseFlow(this, SKU_RELOAD_130_CREDITS, RC_REQUEST,
                    mPurchaseFinishedListener, payload);
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            Common.errorMessage(MainActivity.this,""+getResources().getString(R.string.app_name),""+getResources().getString(R.string.update_google_play_services));
        }
    }
    /**
     * Método para comprar GOLD por 1 día
     */
    public void buyGOLD24h() {

        String payload = GlobalData.isKey(Constant.MY_USER_ID,MainActivity.this)?GlobalData.get(Constant.MY_USER_ID,MainActivity.this):"";

        try {
            mHelper.launchPurchaseFlow(this, SKU_GOLD_24H, RC_REQUEST,
                    mPurchaseFinishedListener, payload);
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            Common.errorMessage(MainActivity.this,""+getResources().getString(R.string.app_name),""+getResources().getString(R.string.update_google_play_services));
        }
    }
    /**
     * Método para comprar GOLD por un mes
     */
    public void buyGOLD1MonthSubscription() {

        if (!mHelper.subscriptionsSupported()) {
            Common.errorMessage(MainActivity.this, "" + getResources().getString(R.string.app_name), "" + getResources().getString(R.string.update_google_play_services));
            return;
        }

        CharSequence[] options;
        if (!mSubscribedToInfiniteGas || !mAutoRenewEnabled) {
            // Both subscription options should be available
            options = new CharSequence[1];
            options[0] = "mensual";
            //options[1] = "anual";
            mFirstChoiceSku = SKU_GOLD_1MONTH_SUBSCRIPTION;//SKU_GOLD_1MONTH_SUBSCRIPTION;
            //mSecondChoiceSku = SKU_INFINITE_GAS_YEARLY;
        } else {
            // This is the subscription upgrade/downgrade path, so only one option is valid
            options = new CharSequence[1];
            if (mInfiniteGasSku.equals(SKU_GOLD_1MONTH_SUBSCRIPTION)) {
                // Give the option to upgrade to yearly
                options[0] = "anual";
                mFirstChoiceSku = SKU_INFINITE_GAS_YEARLY;
            } else {
                // Give the option to downgrade to monthly
                options[0] = "mensual";
                mFirstChoiceSku = SKU_GOLD_1MONTH_SUBSCRIPTION;
            }
            //mSecondChoiceSku = "";
        }
        //llamar servicio para pago

        String payload = GlobalData.isKey(Constant.MY_USER_ID,MainActivity.this)?GlobalData.get(Constant.MY_USER_ID,MainActivity.this):"";

        if (TextUtils.isEmpty(mSelectedSubscriptionPeriod)) {
            // The user has not changed from the default selection
            mSelectedSubscriptionPeriod = mFirstChoiceSku;
        }

        List<String> oldSkus = null;
        if (!TextUtils.isEmpty(mInfiniteGasSku)
                && !mInfiniteGasSku.equals(mSelectedSubscriptionPeriod)) {
            // The user currently has a valid subscription, any purchase action is going to
            // replace that subscription
            oldSkus = new ArrayList<String>();
            oldSkus.add(mInfiniteGasSku);
        }

        //setWaitScreen(true);

        mHelper.launchPurchaseFlow(this, mSelectedSubscriptionPeriod, IabHelper.ITEM_TYPE_SUBS,
                oldSkus, RC_REQUEST, mPurchaseFinishedListener, payload);
        // Reset the dialog options
        mSelectedSubscriptionPeriod = "";
        mFirstChoiceSku = "";
        mSecondChoiceSku = "";

    }
    /**
     * Método para comprar GOLD por 3 meses
     */
    public void buyGOLD3MonthSubscription() {

        if (!mHelper.subscriptionsSupported()) {
            Common.errorMessage(MainActivity.this, "" + getResources().getString(R.string.app_name), "" + getResources().getString(R.string.update_google_play_services));
            return;
        }

        CharSequence[] options;
        if (!mSubscribedToInfiniteGas || !mAutoRenewEnabled) {
            // Both subscription options should be available
            options = new CharSequence[1];
            options[0] = "mensual";
            //options[1] = "anual";
            mFirstChoiceSku = SKU_GOLD_3MONTH_SUBSCRIPTION;
            //mSecondChoiceSku = SKU_INFINITE_GAS_YEARLY;
        } else {
            // This is the subscription upgrade/downgrade path, so only one option is valid
            options = new CharSequence[1];
            if (mInfiniteGasSku.equals(SKU_GOLD_3MONTH_SUBSCRIPTION)) {
                // Give the option to upgrade to yearly
                options[0] = "anual";
                mFirstChoiceSku = SKU_INFINITE_GAS_YEARLY;
            } else {
                // Give the option to downgrade to monthly
                options[0] = "mensual";
                mFirstChoiceSku = SKU_GOLD_3MONTH_SUBSCRIPTION;
            }
            //mSecondChoiceSku = "";
        }
        //llamar servicio para pago

         /* TODO: for security, generate your payload here for verification. See the comments on
             *        verifyDeveloperPayload() for more info. Since this is a SAMPLE, we just use
             *        an empty string, but on a production app you should carefully generate
             *        this. */
        String payload = GlobalData.isKey(Constant.MY_USER_ID,MainActivity.this)?GlobalData.get(Constant.MY_USER_ID,MainActivity.this):"";

        if (TextUtils.isEmpty(mSelectedSubscriptionPeriod)) {
            // The user has not changed from the default selection
            mSelectedSubscriptionPeriod = mFirstChoiceSku;
        }

        List<String> oldSkus = null;
        if (!TextUtils.isEmpty(mInfiniteGasSku)
                && !mInfiniteGasSku.equals(mSelectedSubscriptionPeriod)) {
            // The user currently has a valid subscription, any purchase action is going to
            // replace that subscription
            oldSkus = new ArrayList<String>();
            oldSkus.add(mInfiniteGasSku);
        }

        //setWaitScreen(true);

        mHelper.launchPurchaseFlow(this, mSelectedSubscriptionPeriod, IabHelper.ITEM_TYPE_SUBS,
                oldSkus, RC_REQUEST, mPurchaseFinishedListener, payload);
        // Reset the dialog options
        mSelectedSubscriptionPeriod = "";
        mFirstChoiceSku = "";
        mSecondChoiceSku = "";
    }

    // Listener that's called when we finish querying the items and subscriptions we own
    IabHelper.QueryInventoryFinishedListener mGotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
        //    Log.d(TAG, "Query inventory finished.");

            // Have we been disposed of in the meantime? If so, quit.
            if (mHelper == null) return;

            // Is it a failure?
            if (result.isFailure()) {
                complain("Failed to query inventory: " + result);
                return;
            }

         //   Log.d(TAG, "Query inventory was successful.");

            /*
             * Check for items we own. Notice that for each purchase, we check
             * the developer payload to see if it's correct! See
             * verifyDeveloperPayload().
             */

            // Do we have the premium upgrade?
            Purchase premiumPurchase = inventory.getPurchase(SKU_PREMIUM);
            mIsPremium = (premiumPurchase != null && verifyDeveloperPayload(premiumPurchase));
          //  Log.d(TAG, "User is " + (mIsPremium ? "PREMIUM" : "NOT PREMIUM"));

            // First find out which subscription is auto renewing
            Purchase gasMonthly = inventory.getPurchase(SKU_INFINITE_GAS_MONTHLY);
            Purchase gasYearly = inventory.getPurchase(SKU_INFINITE_GAS_YEARLY);
            if (gasMonthly != null && gasMonthly.isAutoRenewing()) {
                mInfiniteGasSku = SKU_INFINITE_GAS_MONTHLY;
                mAutoRenewEnabled = true;
            } else if (gasYearly != null && gasYearly.isAutoRenewing()) {
                mInfiniteGasSku = SKU_INFINITE_GAS_YEARLY;
                mAutoRenewEnabled = true;
            } else {
                mInfiniteGasSku = "";
                mAutoRenewEnabled = false;
            }

            // The user is subscribed if either subscription exists, even if neither is auto
            // renewing
            mSubscribedToInfiniteGas = (gasMonthly != null && verifyDeveloperPayload(gasMonthly))
                    || (gasYearly != null && verifyDeveloperPayload(gasYearly));



            // Check for gas delivery -- if we own gas, we should fill up the tank immediately
            Purchase gasPurchase = inventory.getPurchase(SKU_GAS);
            Purchase GOLD1MONTH = inventory.getPurchase(SKU_GOLD_1MONTH_SUBSCRIPTION);
            Purchase GOLD3MONTH = inventory.getPurchase(SKU_GOLD_3MONTH_SUBSCRIPTION);
            if (gasPurchase != null && verifyDeveloperPayload(gasPurchase)) {
                //Log.d(TAG, "We have gas. Consuming it.");
                mHelper.consumeAsync(inventory.getPurchase(SKU_GAS), mConsumeFinishedListener);
                return;
            }
            else if (GOLD1MONTH != null && verifyDeveloperPayload(GOLD1MONTH)) {
                //Log.d(TAG, "We have gas. Consuming it.");
                mHelper.consumeAsync(inventory.getPurchase(SKU_GOLD_1MONTH_SUBSCRIPTION), mConsumeFinishedListener);
                return;
            }
            else if (GOLD3MONTH != null && verifyDeveloperPayload(GOLD3MONTH)) {
               // Log.d(TAG, "We have gas. Consuming it.");
                mHelper.consumeAsync(inventory.getPurchase(SKU_GOLD_3MONTH_SUBSCRIPTION), mConsumeFinishedListener);
                return;
            }

            //updateUi();
           // setWaitScreen(false);
         //   Log.d(TAG, "Initial inventory query finished; enabling main UI.");
        }
    };

    // Called when consumption is complete
    IabHelper.OnConsumeFinishedListener mConsumeFinishedListener = new IabHelper.OnConsumeFinishedListener() {
        public void onConsumeFinished(Purchase purchase, IabResult result) {
           // Log.d(TAG, "Consumption finished. Purchase: " + purchase + ", result: " + result);

            // if we were disposed of in the meantime, quit.
            if (mHelper == null) return;

            // We know this is the "gas" sku because it's the only one we consume,
            // so we don't check which sku was consumed. If you have more than one
            // sku, you probably should check...
            if (result.isSuccess()) {
                /*if(purchase!= null)
                    Toast.makeText(MainActivity.this, "producto comprado en google : " + purchase.getSku(), Toast.LENGTH_LONG).show();
                else
                    Toast.makeText(MainActivity.this, "producto comprado purchase null", Toast.LENGTH_LONG).show();*/
                // successfully consumed, so we apply the effects of the item in our
                // game world's logic, which in our case means filling the gas tank a bit
                GlobalData.delete(Constant.ACTIVITY_FROM_CAMERA,MainActivity.this);
                if (purchase.getSku().equals(SKU_RELOAD_WALLAS))
                {
                    //Compra recarga de wallas

                    serviceBuyProduct(Constant.PRODUCT_RELOAD_WALLAS,purchase);
                    Map<String, Object> eventValue = new HashMap<String, Object>();
                    eventValue.put(AFInAppEventParameterName.REVENUE,0.98);
                    eventValue.put(AFInAppEventParameterName.CONTENT_TYPE,"PRODUCTS");
                    eventValue.put(AFInAppEventParameterName.CONTENT_ID,""+SKU_RELOAD_WALLAS);
                    eventValue.put(AFInAppEventParameterName.CURRENCY,"EURO");
                    AppsFlyerLib.getInstance().trackEvent(MainActivity.this, AFInAppEventType.PURCHASE,eventValue);
                }
                 else if (purchase.getSku().equals(SKU_RELOAD_10_CREDITS)) {
                    serviceBuyProduct(Constant.BUY_COINS_1,purchase);
                    Map<String, Object> eventValue = new HashMap<String, Object>();
                    eventValue.put(AFInAppEventParameterName.REVENUE,0.98);
                    eventValue.put(AFInAppEventParameterName.CONTENT_TYPE,"PRODUCTS");
                    eventValue.put(AFInAppEventParameterName.CONTENT_ID,""+SKU_RELOAD_10_CREDITS);
                    eventValue.put(AFInAppEventParameterName.CURRENCY,"EURO");
                    AppsFlyerLib.getInstance().trackEvent(MainActivity.this, AFInAppEventType.PURCHASE,eventValue);

                }
                else if (purchase.getSku().equals(SKU_RELOAD_60_CREDITS)) {
                    serviceBuyProduct(Constant.BUY_COINS_2,purchase);

                    Map<String, Object> eventValue = new HashMap<String, Object>();
                    eventValue.put(AFInAppEventParameterName.REVENUE,4.90);
                    eventValue.put(AFInAppEventParameterName.CONTENT_TYPE,"PRODUCTS");
                    eventValue.put(AFInAppEventParameterName.CONTENT_ID,""+SKU_RELOAD_60_CREDITS);
                    eventValue.put(AFInAppEventParameterName.CURRENCY,"EURO");
                    AppsFlyerLib.getInstance().trackEvent(MainActivity.this, AFInAppEventType.PURCHASE,eventValue);

                }
                else if (purchase.getSku().equals(SKU_RELOAD_130_CREDITS)) {
                    serviceBuyProduct(Constant.BUY_COINS_3,purchase);

                    Map<String, Object> eventValue = new HashMap<String, Object>();
                    eventValue.put(AFInAppEventParameterName.REVENUE,9.80);
                    eventValue.put(AFInAppEventParameterName.CONTENT_TYPE,"PRODUCTS");
                    eventValue.put(AFInAppEventParameterName.CONTENT_ID,""+SKU_RELOAD_130_CREDITS);
                    eventValue.put(AFInAppEventParameterName.CURRENCY,"EURO");
                    AppsFlyerLib.getInstance().trackEvent(MainActivity.this, AFInAppEventType.PURCHASE,eventValue);

                }
                else if (purchase.getSku().equals(SKU_GOLD_24H)) {

                    serviceBuyProduct(Constant.GOLD_24H,purchase);
                    Map<String, Object> eventValue = new HashMap<String, Object>();
                    eventValue.put(AFInAppEventParameterName.REVENUE,5.98);
                    eventValue.put(AFInAppEventParameterName.CONTENT_TYPE,"PRODUCTS");
                    eventValue.put(AFInAppEventParameterName.CONTENT_ID,""+SKU_GOLD_24H);
                    eventValue.put(AFInAppEventParameterName.CURRENCY,"EURO");
                    AppsFlyerLib.getInstance().trackEvent(MainActivity.this, AFInAppEventType.PURCHASE,eventValue);
                }

                else if (purchase.getSku().equals(SKU_GOLD_3MONTH_SUBSCRIPTION)) {

                    Toast.makeText(MainActivity.this, "se suscribio 3 meses subs", Toast.LENGTH_LONG).show();
                }
                else if (purchase.getSku().equals(SKU_GOLD_1MONTH_SUBSCRIPTION)) {

                    Toast.makeText(MainActivity.this, "se suscribio 1 mes subs", Toast.LENGTH_LONG).show();
                }
                else {
                    //producto GOLD
                    serviceBuyProduct(Constant.PRODUCT_GOLD_SUBSCRIPTION,purchase);
                    Map<String, Object> eventValue = new HashMap<String, Object>();
                    eventValue.put(AFInAppEventParameterName.REVENUE,29.95);
                    eventValue.put(AFInAppEventParameterName.CONTENT_TYPE,"PRODUCTS");
                    eventValue.put(AFInAppEventParameterName.CONTENT_ID,""+SKU_INFINITE_GAS_MONTHLY);
                    eventValue.put(AFInAppEventParameterName.CURRENCY,"EURO");
                    AppsFlyerLib.getInstance().trackEvent(MainActivity.this, AFInAppEventType.PURCHASE,eventValue);
                    //updateUserStatus(Constant.GOLD);

                }
            }

            //updateUi();
            //setWaitScreen(false);
          //  Log.d(TAG, "End consumption flow.");
        }
    };
    /** Verifies the developer payload of a purchase. */
    boolean verifyDeveloperPayload(Purchase p) {
        String payload = p.getDeveloperPayload();

        /*
         * TODO: verify that the developer payload of the purchase is correct. It will be
         * the same one that you sent when initiating the purchase.
         *
         * WARNING: Locally generating a random string when starting a purchase and
         * verifying it here might seem like a good approach, but this will fail in the
         * case where the user purchases an item on one device and then uses your app on
         * a different device, because on the other device you will not have access to the
         * random string you originally generated.
         *
         * So a good developer payload has these characteristics:
         *
         * 1. If two different users purchase an item, the payload is different between them,
         *    so that one user's purchase can't be replayed to another user.
         *
         * 2. The payload must be such that you can verify it even when the app wasn't the
         *    one who initiated the purchase flow (so that items purchased by the user on
         *    one device work on other devices owned by the user).
         *
         * Using your own server to store and verify developer payloads across app
         * installations is recommended.
         */

        return true;
    }
    // Callback for when a purchase is finished
    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult result, Purchase purchase) {

           /* if(purchase != null)
                Log.d(TAG, "Purchase finished: " + " hola : " + purchase.getSku());
            else
                Log.d(TAG, "Purchase finished: " + result + ", purchase: " + purchase + " nullWalla");*/

            // if we were disposed of in the meantime, quit.
            if (mHelper == null) return;

            if (result.isFailure()) {

                GlobalData.set(Constant.ACTIVITY_FROM_CAMERA,"Y",MainActivity.this);
                Toast.makeText(MainActivity.this, R.string.canceled_buy, Toast.LENGTH_SHORT).show();
                //Toast.makeText(MainActivity.this, "compra "+purchase.getSku() + " cancelada", Toast.LENGTH_SHORT).show();
                return;
            }
            if (!verifyDeveloperPayload(purchase)) {
                complain("Error purchasing. Authenticity verification failed.");
                //setWaitScreen(false);
                return;
            }

          //  Log.d(TAG, "Purchase successful.");
           /* if(purchase != null)
                Toast.makeText(MainActivity.this, "Purchase finished con: "+ purchase.getSku(), Toast.LENGTH_LONG).show();
            else
                Toast.makeText(MainActivity.this, "Purchase null", Toast.LENGTH_SHORT).show();*/

            if (purchase.getSku().equals(SKU_GAS)) {
                // bought 1/4 tank of gas. So consume it.
              //  Log.d(TAG, "Purchase is gas. Starting gas consumption.");
                mHelper.consumeAsync(purchase, mConsumeFinishedListener);
            }
            else if (purchase.getSku().equals(SKU_PREMIUM)) {
                // bought the premium upgrade!
             //   Log.d(TAG, "Purchase is premium upgrade. Congratulating user.");
               // alert("Thank you for upgrading to premium!");
                mIsPremium = true;

            }
            else if (purchase.getSku().equals(SKU_RELOAD_WALLAS)) {
                // Compra de wallas
                //serviceBuyCredits();
                mHelper.consumeAsync(purchase, mConsumeFinishedListener);

            }
            else if (purchase.getSku().equals(SKU_RELOAD_10_CREDITS)) {
                // Compra de wallas
                //serviceBuyCredits();
                mHelper.consumeAsync(purchase, mConsumeFinishedListener);

            }
            else if (purchase.getSku().equals(SKU_RELOAD_60_CREDITS)) {
                // Compra de wallas
                //serviceBuyCredits();
                mHelper.consumeAsync(purchase, mConsumeFinishedListener);

            }
            else if (purchase.getSku().equals(SKU_RELOAD_130_CREDITS)) {
                // Compra de wallas
                //serviceBuyCredits();
                mHelper.consumeAsync(purchase, mConsumeFinishedListener);

            }
            else if (purchase.getSku().equals(SKU_GOLD_24H)) {
                // Compra de wallas
                //serviceBuyCredits();
                mHelper.consumeAsync(purchase, mConsumeFinishedListener);

            }
            else if (purchase.getSku().equals(SKU_INFINITE_GAS_MONTHLY)
                    || purchase.getSku().equals(SKU_INFINITE_GAS_YEARLY)) {
                // bought the infinite gas subscription
             //   Log.d(TAG, "Infinite gas subscription purchased.");
                alert("Thank you for subscribing to infinite gas!");
                mSubscribedToInfiniteGas = true;
                mAutoRenewEnabled = purchase.isAutoRenewing();
                mInfiniteGasSku = purchase.getSku();
                //mTank = TANK_MAX;
                //updateUi();
                //setWaitScreen(false);
            }
            else if(purchase.getSku().equals(SKU_GOLD_1MONTH_SUBSCRIPTION))
            {
                //Toast.makeText(MainActivity.this, "se suscribio a un mes mPurchaseFinishedListener", Toast.LENGTH_LONG).show();
                serviceBuyProduct(Constant.GOLD_1_MONTH,purchase);
                Map<String, Object> eventValue = new HashMap<String, Object>();
                eventValue.put(AFInAppEventParameterName.REVENUE,18.98);
                eventValue.put(AFInAppEventParameterName.CONTENT_TYPE,"PRODUCTS");
                eventValue.put(AFInAppEventParameterName.CONTENT_ID,""+SKU_GOLD_1MONTH_SUBSCRIPTION);
                eventValue.put(AFInAppEventParameterName.CURRENCY,"EURO");
                AppsFlyerLib.getInstance().trackEvent(MainActivity.this, AFInAppEventType.PURCHASE,eventValue);
            }
            else if(purchase.getSku().equals(SKU_GOLD_3MONTH_SUBSCRIPTION))
            {
                serviceBuyProduct(Constant.GOLD_3_MONTHS,purchase);
                Map<String, Object> eventValue = new HashMap<String, Object>();
                eventValue.put(AFInAppEventParameterName.REVENUE,35.88);
                eventValue.put(AFInAppEventParameterName.CONTENT_TYPE,"PRODUCTS");
                eventValue.put(AFInAppEventParameterName.CONTENT_ID,""+SKU_GOLD_3MONTH_SUBSCRIPTION);
                eventValue.put(AFInAppEventParameterName.CURRENCY,"EURO");
                AppsFlyerLib.getInstance().trackEvent(MainActivity.this, AFInAppEventType.PURCHASE,eventValue);
            }
        }
    };

    @Override
    public void receivedBroadcast() {
       // Log.d(TAG, "Received broadcast notification. Querying inventory.");
        mHelper.queryInventoryAsync(mGotInventoryListener);
    }
    void complain(String message) {
     //   Log.e(TAG, "**** TrivialDrive Error: " + message);
        alert("Error: " + message);
    }
    void alert(String message) {
        AlertDialog.Builder bld = new AlertDialog.Builder(this);
        bld.setMessage(message);
        bld.setNeutralButton("OK", null);
    //    Log.d(TAG, "Showing alert dialog: " + message);
        bld.create().show();
    }
    public void onCreateBilling()
    {
        // Create the helper, passing it our context and the public key to verify signatures with
       // Log.d(TAG, "Creating IAB helper.");
        mHelper = new IabHelper(this, base64EncodedPublicKey);

        // enable debug logging (for a production application, you should set this to false).
        mHelper.enableDebugLogging(false);


        // Start setup. This is asynchronous and the specified listener
        // will be called once setup completes.


        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
              //  Log.d(TAG, "Setup finished.");

                if (!result.isSuccess()) {
                    // Oh noes, there was a problem.
                    complain("Problem setting up in-app billing: " + result);
                    return;
                }

                // Have we been disposed of in the meantime? If so, quit.
                if (mHelper == null) return;

                // Important: Dynamically register for broadcast messages about updated purchases.
                // We register the receiver here instead of as a <receiver> in the Manifest
                // because we always call getPurchases() at startup, so therefore we can ignore
                // any broadcasts sent while the app isn't running.
                // Note: registering this listener in an Activity is a bad idea, but is done here
                // because this is a SAMPLE. Regardless, the receiver must be registered after
                // IabHelper is setup, but before first call to getPurchases().
                mBroadcastReceiver = new IabBroadcastReceiver(MainActivity.this);
                IntentFilter broadcastFilter = new IntentFilter(IabBroadcastReceiver.ACTION);
                registerReceiver(mBroadcastReceiver, broadcastFilter);

                // IAB is fully set up. Now, let's get an inventory of stuff we own.
               // Log.d(TAG, "Setup successful. Querying inventory.");
                mHelper.queryInventoryAsync(mGotInventoryListener);
            }
        });
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int id) {
        if (id == 0 /* First choice item */) {
            mSelectedSubscriptionPeriod = mFirstChoiceSku;
        } else if (id == 1 /* Second choice item */) {
            mSelectedSubscriptionPeriod = mSecondChoiceSku;
        } else if (id == DialogInterface.BUTTON_POSITIVE /* continue button */) {
            /* TODO: for security, generate your payload here for verification. See the comments on
             *        verifyDeveloperPayload() for more info. Since this is a SAMPLE, we just use
             *        an empty string, but on a production app you should carefully generate
             *        this. */
            String payload = "";

            if (TextUtils.isEmpty(mSelectedSubscriptionPeriod)) {
                // The user has not changed from the default selection
                mSelectedSubscriptionPeriod = mFirstChoiceSku;
            }

            List<String> oldSkus = null;
            if (!TextUtils.isEmpty(mInfiniteGasSku)
                    && !mInfiniteGasSku.equals(mSelectedSubscriptionPeriod)) {
                // The user currently has a valid subscription, any purchase action is going to
                // replace that subscription
                oldSkus = new ArrayList<String>();
                oldSkus.add(mInfiniteGasSku);
            }

            //setWaitScreen(true);
         //   Log.d(TAG, "Launching purchase flow for gas subscription.");
            mHelper.launchPurchaseFlow(this, mSelectedSubscriptionPeriod, IabHelper.ITEM_TYPE_SUBS,
                    oldSkus, RC_REQUEST, mPurchaseFinishedListener, payload);
            // Reset the dialog options
            mSelectedSubscriptionPeriod = "";
            mFirstChoiceSku = "";
            mSecondChoiceSku = "";
        } else if (id != DialogInterface.BUTTON_NEGATIVE) {
            // There are only four buttons, this should not happen
            // Log.e(TAG, "Unknown button clicked in subscription dialog: " + id);
        }
    }

    /**
     * Evento para mostrar menu
     * @param v
     */
    public void toggleMenu(View v) {

        if (menu.isMenuShowing()) {
            //Visibility.enableDisableView(activity_main_content_fragment, true);
            if(menu != null)
                menu.toggle(true);
        } else {
            //Visibility.enableDisableView(activity_main_content_fragment, false);
            if(menu != null)
                menu.showMenu();
        }

    }

    /**
     * Metodo que controla la accion back de los dispositivos
     */
    @Override
    public void onBackPressed() {
        if (menu.isMenuShowing()) {
            menu.toggle();
            apperBackgroundGradient(false);
        } else {
            if (!backToFragment())
                super.onBackPressed();

        }
    }

    @Override
    public void onStop() {
        super.onStop();

        if (mdialog != null) {
            mdialog.dismiss();
            mdialog = null;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(callbackManager != null)
            callbackManager.onActivityResult(requestCode, resultCode, data);

      //  Log.d(TAG, "onActivityResult(" + requestCode + "," + resultCode + "," + data);
        // Pass on the activity result to the helper for handling
        if (mHelper == null) return;

        // Pass on the activity result to the helper for handling
        if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
            // not handled, so handle it ourselves (here's where you'd
            // perform any handling of activity results not related to in-app
            // billing...
            super.onActivityResult(requestCode, resultCode, data);
        }

    }

    /**
     * Eventos tap de los botones del MainActivity
     * @param v
     */
    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.homelayout) {
            if(ConnectionDetector.isConnectingToInternet(MainActivity.this)) {

                FragmentGame fragmentGame = new FragmentGame(this);
                tvTitle.setText(getResources().getString(R.string.app_name));

                if (fragmentGame != null)
                    replaceFragment(fragmentGame);

                menu.toggle();

            }
            else
                Common.errorMessage(MainActivity.this,"",getResources().getString(R.string.check_internet));

        } else if (v.getId() == R.id.favoritelayout) {

            if(ConnectionDetector.isConnectingToInternet(MainActivity.this)) {

                FragmentFavorites fragmentFavorites = new FragmentFavorites(this);
                tvTitle.setText(""+getResources().getString(R.string.favorites));

                if (fragmentFavorites != null)
                    replaceFragment(fragmentFavorites);

                menu.toggle();

            }
            else
                Common.errorMessage(MainActivity.this,"",getResources().getString(R.string.check_internet));

        } else if (v.getId() == R.id.profileimage) {

            goToProfile();

        }
        else if (v.getId() == R.id.profiletextview) {

            goToProfile();
        }
        else if(v.getId() == R.id.imgBackGroundProfile)
        {
            goToProfile();
        }
        else if (v.getId() == R.id.matchlayout)
        {

            if(ConnectionDetector.isConnectingToInternet(MainActivity.this)) {
                GlobalData.set(Constant.COUNTER_MATCHS, "0", MainActivity.this);

                FragmentMatch fragmentMatch = new FragmentMatch(this);
                tvTitle.setText(""+getResources().getString(R.string.match));

                if (fragmentMatch != null)
                    replaceFragment(fragmentMatch);

                menu.toggle();
            }
            else
                Common.errorMessage(MainActivity.this,"",getResources().getString(R.string.check_internet));


        } else if (v.getId() == R.id.flechazoslayout) {

            if(ConnectionDetector.isConnectingToInternet(MainActivity.this)) {
                String status = GlobalData.get(Constant.USER_STATUS, MainActivity.this);

                if (Integer.parseInt(status) > 2)
                    GlobalData.set(Constant.COUNTER_WALLAS, "0", MainActivity.this);

                FragmentWallas fragmentWallas = new FragmentWallas(this);
                tvTitle.setText(""+getResources().getString(R.string.title_fragment_likes));

                if (fragmentWallas != null)
                    replaceFragment(fragmentWallas);

                menu.toggle();
            }
            else
                Common.errorMessage(MainActivity.this,"",getResources().getString(R.string.check_internet));


        }
        //preferences
        else if (v.getId() == R.id.img_preferences) {
            FragmentSettings fragmentSettings = new FragmentSettings(this);
            fragmentSettings.show(mFragmentManager, "fragmentSettings.TAG");
        }
        else if(v.getId() == R.id.nearLayout)
        {
            String status = GlobalData.get(Constant.USER_STATUS,MainActivity.this);

            if (status.equals(Constant.NOT_GOLD_BEFORE_TRIAL_NO_PICTURE))
            {
                final NiftyDialogBuilder dialog =  NiftyDialogBuilder.getInstance(MainActivity.this);
                dialog.withTitle(null);
                dialog.withMessage(null);
                dialog.isCancelableOnTouchOutside(false);
                dialog.withDialogColor(Color.TRANSPARENT);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.withDuration(Constant.DURATION_ANIMATION);
                dialog.withEffect(Effectstype.Shake);
                dialog.setCustomView(R.layout.modal_general,MainActivity.this);

                TextView title = (TextView) dialog.findViewById(R.id.title);
                TextView text = (TextView) dialog.findViewById(R.id.text);
                title.setText(Common.getNickName(MainActivity.this));
                text.setText("Para conocer a las personas que están cerca de tí, primero debes subir tu foto de perfil.");

                Button exit = (Button) dialog.findViewById(R.id.exit);
                exit.setText(MainActivity.this.getResources().getString(R.string.exit));
                Button accept = (Button) dialog.findViewById(R.id.accept);
                // if button is clicked, close the custom dialog
                accept.setText(MainActivity.this.getResources().getString(R.string.update_photo));
                exit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                accept.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        menu.toggle(true);
                        replaceFragment(new FragmentProfile(MainActivity.this));
                    }
                });

                dialog.show();
            }
            else {
                NearFragment nearFragment = new NearFragment(this);

                if (nearFragment != null)
                    replaceFragment(nearFragment);

                menu.toggle();
            }

        }
        else if (v.getId() == R.id.imgSecurity) {


            if (GlobalData.isKey(Constant.PIN_SECURITY, MainActivity.this)) {
                FragmentSecurity fragmentSecurity = new FragmentSecurity(MainActivity.this,1,1);
                fragmentSecurity.setCancelable(false);
                fragmentSecurity.show(mFragmentManager,"FragmentSecurity.TAG");
            }
            else {
                FragmentSecurity fragmentSecurity = new FragmentSecurity(this);
                fragmentSecurity.show(mFragmentManager, "FragmentSecurity.TAG");
            }


        }
        else if (v.getId() == R.id.messages) {

            if(ConnectionDetector.isConnectingToInternet(MainActivity.this)) {
                GlobalData.set(Constant.COUNTER_MESSAGES, "0", MainActivity.this);

                MessageFragment messageFragment = new MessageFragment(this);
                tvTitle.setText(""+getResources().getString(R.string.messages));
                if (messageFragment != null)
                    replaceFragment(messageFragment);

                if (menu != null)
                    menu.toggle();
            }
            else
                Common.errorMessage(MainActivity.this,"",getResources().getString(R.string.check_internet));
        }
    }

    /**
     * Método encargado de ir al perfil del usuario
     */
    public void goToProfile()
    {
        FragmentProfile fragmentProfile = new FragmentProfile(this);

        if (fragmentProfile != null)
            replaceFragment(fragmentProfile);

        menu.toggle();

    }
    /**
     * Método encargado de obtener la información de las subscripciones del usuario
     */
    public  void verifiedSubsProduct() {
       try {

            //String payload = GlobalData.isKey(Constant.MY_USER_ID,MainActivity.this)?GlobalData.get(Constant.MY_USER_ID,MainActivity.this):"";
            Bundle subsDetails = mService.getPurchases(3,"com.loovers.app","subs",null);
            int response = subsDetails.getInt("RESPONSE_CODE");
            if (response == 0) {
                //Log.d("app2youPay","El usuario tiene subs");
                ArrayList<String> ownedSkus = subsDetails.getStringArrayList("INAPP_PURCHASE_ITEM_LIST");
                ArrayList<String>  purchaseDataList = subsDetails.getStringArrayList("INAPP_PURCHASE_DATA_LIST");

                Log.d("app2youPay","DATA COMPRA array: " + purchaseDataList.toString() );
                //The purchase state of the order. Possible values are 0 (purchased), 1 (canceled), or 2 (refunded).
               /* for (int i = 0, len = purchaseDataList.size(); i < len; ++i) {
                    String purchaseData = purchaseDataList.get(i);
                    String sku = ownedSkus.get(i);
                    Log.d("app2youPay","DATA COMPRA: " + purchaseData + " sku: "+ sku);


                }*/
                //enviar información al servidor
                //sendDataSubscriptionToServer(purchaseDataList);
            }

        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if ((keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) || (keyCode == KeyEvent.KEYCODE_VOLUME_UP)  ){
            //Do something
            return false;
        }
        else if(container_finish_register.getVisibility() == View.VISIBLE || container_personal_status.getVisibility() == View.VISIBLE ||
                container_questions.getVisibility()== View.VISIBLE || containerBirthdayUser.getVisibility()== View.VISIBLE) {
            return true;
        }
        else {
            if (menu.isMenuShowing()) {
                menu.toggle();
                return true;
            }
            if (this.isTaskRoot() && !menu.isMenuShowing()) {
                closeApp();
            }

            if (mrightMenuClicked) {

                mrightMenuClicked = false;
                backToFragment();
            }
            return true;
    //        return false;
        }

       //return true;
    }

    /**
     * Método para cerrar la app
     */
    public void closeApp() {
        // custom dialog
        final Dialog dialog =  new Dialog(MainActivity.this,R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.modal_general);

        TextView text = (TextView) dialog.findViewById(R.id.text);
        text.setText(""+getResources().getString(R.string.message_close_app));

        Button exit = (Button) dialog.findViewById(R.id.exit);
        Button accept = (Button) dialog.findViewById(R.id.accept);
        // if button is clicked, close the custom dialog
        exit.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        accept.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        MainActivity.this.finish();
                    }
                },350);
            }
        });

        dialog.show();

    }

    /**
     * Metodo encargado de reemplazar un fragmneto y añadirlo al array
     *
     * @param fragment
     */
    public void replaceFragment(Fragment fragment) {
        //fragments.add(fragment);
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        //ft.setCustomAnimations(R.anim.fade_in_slow,R.anim.fade_out);
        ft.replace(R.id.activity_main_content_fragment, fragment);
        ft.commit();

    }

    /**
     * Metodo encargado de visualizar un fragmento anterior
     *
     * @return
     */
    public boolean backToFragment() {
        if (fragments != null && fragments.size() > 1) {
            int maxFragments = fragments.size();
            Fragment fragment = fragments.get((maxFragments) - 2);
            fragments.remove((maxFragments) - 1);
            replaceFragmentWithoutAdd(fragment);
            return true;
        } else
            return false;

    }

    /**
     * Método encargado de reemplazar un fragmento sin añadirlo al array
     *
     * @param frag
     */
    public void replaceFragmentWithoutAdd(Fragment frag) {

        FragmentManager manager = getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction ft = manager.beginTransaction();
        ft.setCustomAnimations(R.anim.fade_in_slow, R.anim.fade_out);
        ft.replace(R.id.activity_main_content_fragment, frag);
        ft.commit();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        GlobalData.delete(Constant.CARDS,MainActivity.this);
        GlobalData.delete(Constant.CARDS_GAME,MainActivity.this);
        serviceIsOnline(Constant.OFFLINE);
        GlobalData.delete("personsMap",MainActivity.this);
        GlobalData.delete(Constant.REQUEST_MATCH,MainActivity.this);
        GlobalData.delete(Constant.REQUEST_WANT_TO_ME,MainActivity.this);
        GlobalData.delete(Constant.REQUEST_FAVORITES,MainActivity.this);

        // very important:
        if (mBroadcastReceiver != null)
            unregisterReceiver(mBroadcastReceiver);


        // very important:
        if (mHelper != null) {
            mHelper.dispose();
            mHelper = null;
        }
        if (mServiceConn != null)
            unbindService(mServiceConn);


    }


    /**
     * Controla el degradado a la hora de mostar menu
     *
     * @param status
     */
    public void apperBackgroundGradient(boolean status) {
        if (status) {

            //AppAnimation.fadeIn(frame_view_shadom, 200);
            menu.setSlidingEnabled(true);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Visibility.visible(frame_view_shadom);
                    AppAnimation.fadeIn(frame_view_shadom, 200);
                    Visibility.enableDisableView(activity_main_content_fragment, false);
                }
            },200);


        } else {

            //AppAnimation.fadeOut(frame_view_shadom, 50);
            menu.setSlidingEnabled(false);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    AppAnimation.fadeOut(frame_view_shadom, 50);
                    Visibility.enableDisableView(activity_main_content_fragment, true);
                }
            },200);

        }
    }


    /**
     * Método de cambiar la imagen de la intensidad del fuego en relacion a rangeBar
     *
     * @param image
     * @param value
     */
    public void setImageValue(ImageView image, int value) {

        switch (value) {
            case 0:
                image.setImageResource(R.drawable.fuego_0);
                break;
            case 1:
                image.setImageResource(R.drawable.fuego_1);
                break;
            case 2:
                image.setImageResource(R.drawable.fuego_2);
                break;
            case 3:
                image.setImageResource(R.drawable.fuego_3);
                break;
            case 4:
                image.setImageResource(R.drawable.fuego_4);
                break;
            case 5:
                image.setImageResource(R.drawable.fuego_5);
                break;
            case 6:
                image.setImageResource(R.drawable.fuego_6);
                break;
            case 7:
                image.setImageResource(R.drawable.fuego_7);
                break;
            case 8:
                image.setImageResource(R.drawable.fuego_8);
                break;
            case 9:
                image.setImageResource(R.drawable.fuego_9);
                break;
            case 10:
                image.setImageResource(R.drawable.fuego_10);
                break;

            default:
                image.setImageResource(R.drawable.fuego_5);
                break;
        }
    }

    /**
     * Clase encargada de hacer transformación blur sobre un bitmap
     */
    public class BlurTransformation implements com.squareup.picasso.Transformation {
        private final int radius;

        // radius is corner radii in dp
        public BlurTransformation(final int radius) {
            this.radius = radius;
        }

        @Override
        public Bitmap transform(final Bitmap bitmap) {

            if (radius > 0) {

                    Bitmap outBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
                if(bitmap != null) {
                    RenderScript rs = RenderScript.create(MainActivity.this);
                    ScriptIntrinsicBlur blurScript = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs));
                    Allocation allIn = Allocation.createFromBitmap(rs, bitmap,Allocation.MipmapControl.MIPMAP_NONE, Allocation.USAGE_SCRIPT);

                    Allocation allOut = Allocation.createFromBitmap(rs, outBitmap);
                    blurScript.setRadius(radius);
                    blurScript.setInput(allIn);
                    blurScript.forEach(allOut);
                    allOut.copyTo(outBitmap);
                    bitmap.recycle();
                    rs.destroy();
                    return outBitmap;
                }
                else
                {
                   // Toast.makeText(MainActivity.this, "bitmap outBitmap vacio", Toast.LENGTH_SHORT).show();
                    return bitmap;
                }

            }else
                return  bitmap;
        }

        @Override
        public String key() {
            return "blur"+Float.toString(radius);
        }
    }


    /**
     * Inicio las preferencias de las notificaciones
     */
    public void initPreferences()
    {
        if(!GlobalData.isKey(Constant.NOTIFICATION_WALLAS,MainActivity.this)) {
            String status = (GlobalData.isKey(Constant.GENERE,MainActivity.this))?GlobalData.get(Constant.GENERE, MainActivity.this):Constant.MAN;
            GlobalData.set(Constant.NOTIFICATION_WALLAS, status.equals(Constant.MAN)?"Y":"N", MainActivity.this);
        }

        if(!GlobalData.isKey(Constant.NOTIFICATION_FAVORITES,MainActivity.this))
            GlobalData.set(Constant.NOTIFICATION_FAVORITES,"Y",MainActivity.this);

        if(!GlobalData.isKey(Constant.NOTIFICATION_MATCHES,MainActivity.this))
            GlobalData.set(Constant.NOTIFICATION_MATCHES,"Y",MainActivity.this);

        if(!GlobalData.isKey(Constant.NOTIFICATION_MESSAGES,MainActivity.this))
            GlobalData.set(Constant.NOTIFICATION_MESSAGES,"Y",MainActivity.this);

    }

    /**
     * Inicio los contadores
     */
    public void initCounters()
    {
        //inicializo los contadores
        if(!GlobalData.isKey(Constant.COUNTER_MESSAGES,MainActivity.this)) {
            if(GlobalData.isKey(Constant.FIRST_REGISTER,MainActivity.this) && GlobalData.get(Constant.FIRST_REGISTER,MainActivity.this).equals("N"))
                GlobalData.set(Constant.COUNTER_MESSAGES, "0", MainActivity.this);
            else
                GlobalData.set(Constant.COUNTER_MESSAGES, "1", MainActivity.this);
        }

        if(!GlobalData.isKey(Constant.COUNTER_WALLAS,MainActivity.this))
            GlobalData.set(Constant.COUNTER_WALLAS, "0", MainActivity.this);

        if(!GlobalData.isKey(Constant.COUNTER_MATCHS,MainActivity.this))
            GlobalData.set(Constant.COUNTER_MATCHS, "0", MainActivity.this);

    }


    public void openFromNoticaction(int from)
    {
        switch (from) {
                case 1:
                    //FriendsFragment friendsFragment = new FriendsFragment(MainActivity.this);
                    MessageFragment friendsFragment = new MessageFragment(MainActivity.this);
                    replaceFragment(friendsFragment);
                    tvTitle.setText(""+getResources().getString(R.string.messages));
                    if (menu.isMenuShowing())
                        menu.toggle();

                    break;
                case 2:
                    FragmentWallas fragmentWallas = new FragmentWallas(MainActivity.this);
                    replaceFragment(fragmentWallas);
                    tvTitle.setText(""+getResources().getString(R.string.title_fragment_likes));
                    if (menu.isMenuShowing())
                        menu.toggle();


                    break;
                case 3:
                    //FragmentMatch fragmentMatch = new FragmentMatch(MainActivity.this);
                    //replaceFragment(fragmentMatch);
                    //tvTitle.setText(""+getResources().getString(R.string.title_fragment_match));
                    if (profileimage != null)
                        setProfilePick(profileimage);

                    if (!menu.isMenuShowing())
                         menu.toggle(true);


                    break;
                default:
                    /*FragmentGame fragment = new FragmentGame(MainActivity.this);
                    replaceFragment(fragment);
                    tvTitle.setText(getResources().getString(R.string.app_name));*/
                    break;
            }


    }

    /**
     * Metodo encargado actualizar el perfil (fecha de nacimiento)
     */
    public void updateProfile(final String dateOfBirth)
    {

        final ProgressDialog	progressDialog = new ProgressDialog(MainActivity.this,R.style.CustomDialogTheme);
       // progressDialog.setMessage(""+getResources().getString(R.string.update_data));
        progressDialog.setCancelable(false);
        progressDialog.show();
        progressDialog.setContentView(R.layout.custom_progress_dialog);
        TextView txtView = (TextView) progressDialog.findViewById(R.id.txtProgressDialog);
        txtView.setText(""+getResources().getString(R.string.update_data));

        OkHttpClient client = new OkHttpClient();
        RequestBody formBody = new FormBody.Builder()
                .add("sessionToken", "" + GlobalData.get(Constant.SESSION_TOKEN, MainActivity.this))
                .add("key","dateOfBirth")
                .add("value",""+dateOfBirth)
                .build();

        Request request = new Request.Builder()
                .url(Services.UPDATE_PROFILE)
                .post(formBody)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                if (progressDialog != null)
                    progressDialog.dismiss();
                Looper.prepare();

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Common.errorMessage(MainActivity.this, "", getResources().getString(R.string.check_internet));
                    }
                });
                Looper.loop();
            }

            @Override
            public void onResponse(Call call,final Response response) throws IOException {
                if (progressDialog != null)
                    progressDialog.dismiss();
                final String result = response.body().string();

                Looper.prepare();

                runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                JSONObject json = new JSONObject(result);

                                String success = json.getString("success");

                                if (success.equals("true")) {
                                    GlobalData.set(Constant.BIRTHDAY_VERIFIED,"Y",MainActivity.this);

                                    Visibility.gone(containerBirthdayUser);
                                    new Handler().postDelayed(new Runnable() {

                                        @Override
                                        public void run() {
                                            container_button_menu.setEnabled(true);
                                            btMenu.setEnabled(true);
                                            menu.toggle();


                                            final NiftyDialogBuilder dialog = NiftyDialogBuilder.getInstance(MainActivity.this);
                                            dialog.withTitle(null);
                                            dialog.withMessage(null);
                                            dialog.isCancelableOnTouchOutside(false);
                                            dialog.withDialogColor(Color.TRANSPARENT);
                                            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                            dialog.withDuration(550);

                                            dialog.withEffect(Effectstype.Slidetop);
                                            dialog.setCustomView(R.layout.modal_general, MainActivity.this);

                                            TextView title = (TextView) dialog.findViewById(R.id.title);
                                            title.setText(getResources().getString(R.string.title_modal_welcome) + " " + Common.getNickName(MainActivity.this));
                                            TextView text = (TextView) dialog.findViewById(R.id.text);
                                            text.setText("" + getResources().getString(R.string.message_welcome_facebook));


                                            Button exit = (Button) dialog.findViewById(R.id.exit);
                                            Visibility.gone(exit);
                                            Button accept = (Button) dialog.findViewById(R.id.accept);
                                            // if button is clicked, close the custom dialog
                                            exit.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    dialog.dismiss();
                                                }
                                            });
                                            accept.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    dialog.dismiss();


                                                }
                                            });

                                            dialog.show();


                                            }
                                    }, 320);
                                    FragmentGame fragment = new FragmentGame(MainActivity.this);
                                    replaceFragment(fragment);
                                    tvTitle.setText(getResources().getString(R.string.app_name));
                                } else {
                                    Toast.makeText(MainActivity.this, R.string.try_again, Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(MainActivity.this, R.string.error_reponse_server, Toast.LENGTH_SHORT).show();
                            }
                            finally {
                                response.body().close();
                            }


                        }


                    });

                Looper.loop();
            }
        });
    }

    /**
     * Servicio que me permite cambiar el estado del usuario a conectado/desconectado
     * @param status
     */
    public void serviceIsOnline(int status)
    {
        OkHttpClient client = new OkHttpClient();
        RequestBody formBody = new FormBody.Builder()
                .add("sessionToken", "" + GlobalData.get(Constant.SESSION_TOKEN, MainActivity.this))
                .add("key","is_logged")
                .add("value",""+status)
                .build();

        final Request request = new Request.Builder()
                .url(Services.UPDATE_PROFILE)
                .post(formBody)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call,Response response) throws IOException {

                if(response.isSuccessful())
                    response.body().close();


            }
        });
    }
    /**
     * Método encargado validacion de email del usuario
     */
    public void confirmEmail(String id) {
     //   Log.d("app2you", "execute CONFIRM EMAIL okHttp *****");
        final ProgressDialog progressDialog = new ProgressDialog(this,R.style.CustomDialogTheme);
        progressDialog.setCancelable(false);
        //progressDialog.setMessage();
        progressDialog.show();
        progressDialog.setContentView(R.layout.custom_progress_dialog);
        TextView txtView = (TextView) progressDialog.findViewById(R.id.txtProgressDialog);
        txtView.setText(""+getResources().getString(R.string.confirm_email_process));

        OkHttpClient client = new OkHttpClient();

        RequestBody formBody = new FormBody.Builder()
                .add("sessionToken", "" + GlobalData.get(Constant.SESSION_TOKEN, MainActivity.this))
                .add("id", "" + id)
                .add("isMobile","1")
                .build();

        Request request = new Request.Builder()
                .url(Services.CONFIRM_EMAIL)
                .post(formBody)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                progressDialog.dismiss();
                Looper.prepare();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Common.errorMessage(MainActivity.this, "", "" + getResources().getString(R.string.check_internet));
                    }
                });

                Looper.loop();
            }

            @Override
            public void onResponse(Call call,final Response response) throws IOException {
                progressDialog.dismiss();
                final String result = response.body().string();

                Looper.prepare();

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //apago el dialogo
                            try {
                                JSONObject json = new JSONObject(result);
                                String success = json.getString("success");
                                if (success.equals("true")) {
                                    GlobalData.set(Constant.EMAIL_VERIFIED,"Y",MainActivity.this);
                                    Common.errorMessage(MainActivity.this,"",""+getResources().getString(R.string.mail_confirm));
                                } else {

                                    Common.errorMessage(MainActivity.this,"",""+getResources().getString(R.string.mail_not_confirm));
                                }

                            } catch (JSONException ex) {
                                ex.printStackTrace();
                                Toast.makeText(MainActivity.this, R.string.error_reponse_server, Toast.LENGTH_SHORT).show();
                            }
                            finally {
                                response.body().close();
                            }
                        }
                    });


                Looper.loop();
            }
        });
    }

    /**
     * Método encargado de obtener el tiempo de espera en mili segundos para recargar wallas
     */
    public void serviceBuyProduct(final int productTypeId, Purchase purchase)
    {
        int purchaseState = 0;
        int renew = 0;
        String itemType = "";
        String time = "";
        String purchaseToken = "";

        if(purchase != null)
        {
            purchaseState = (purchase.getPurchaseState() == 0)?3:purchase.getPurchaseState();
            renew = (purchase.getItemType().equalsIgnoreCase("subs"))?1:2;
            itemType = (purchase.getItemType().equalsIgnoreCase("inapp"))?"1":"2";
            Date date = new Date(purchase.getPurchaseTime());
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            time = formatter.format(date);
            purchaseToken = purchase.getToken();
        }

        OkHttpClient client = new OkHttpClient();
        RequestBody formBody = new FormBody.Builder()
                .add("sessionToken", "" + GlobalData.get(Constant.SESSION_TOKEN, MainActivity.this))
                .add("productTypeId",""+productTypeId)
                .add("appVersion",""+MainActivity.this.getResources().getString(R.string.app_version))
                .add("orderId",  (purchase != null)?purchase.getOrderId():"" )
                .add("purchaseState",""+purchaseState )
                .add("itemType",""+itemType)
                .add("purchaseTime",""+time)
                .add("purchaseToken",""+purchaseToken)
                .add("sku",(purchase != null)?purchase.getSku():"")
                .add("autoRenewing",""+renew)

                .build();

        Request request = new Request.Builder()
                .url(Services.BUY_PRODUCT)
                .post(formBody)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

                Looper.prepare();

               runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        Common.errorMessage(MainActivity.this,"",getResources().getString(R.string.check_internet));
                    }
                });
                Looper.loop();
            }

            @Override
            public void onResponse(Call call,final Response response) throws IOException {

                final String result = response.body().string();
                //Log.d("app2you", "respuesta  espera tiempo: " + result);

                Looper.prepare();

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //apago el dialogo
                        try {
                            JSONObject json = new JSONObject(result);
                            String success = json.getString("success");
                            if (success.equals("true")) {
                                //Toast.makeText(MainActivity.this, "serviceBuyProduct SUCCESS " + productTypeId, Toast.LENGTH_SHORT).show();

                                String nick = (GlobalData.isKey(Constant.NICKNAME, MainActivity.this)) ? GlobalData.get(Constant.NICKNAME, MainActivity.this) : getResources().getString(R.string.app_name);

                                switch (productTypeId)
                                {
                                    case Constant.PRODUCT_RELOAD_WALLAS:
                                        Common.errorMessage(MainActivity.this, "", "Disfruta de tus Likes");
                                        replaceFragment(new FragmentGame(MainActivity.this));
                                        break;
                                    case Constant.PRODUCT_GOLD_SUBSCRIPTION:
                                        GlobalData.set(Constant.USER_STATUS,""+Constant.GOLD,MainActivity.this);
                                        setProfilePick(profileimage);
                                        Common.errorMessage(MainActivity.this, "" + nick, "" + getResources().getString(R.string.success_changing_status_gold));
                                        break;
                                    case Constant.BUY_COINS_1:
                                        setCoins(10);
                                        Common.errorMessage(MainActivity.this, "Enhorabuena" + nick, "Has Comprado 10 monedas");

                                        //setear los creditos
                                         break;
                                    case Constant.BUY_COINS_2:
                                        setCoins(60);
                                        Common.errorMessage(MainActivity.this, "Enhorabuena" + nick, "Has Comprado 60 monedas");
                                        //setear los creditos
                                        break;
                                    case Constant.BUY_COINS_3:
                                        setCoins(130);
                                        Common.errorMessage(MainActivity.this, "Enhorabuena" + nick, "Has Comprado 130 monedas");
                                        //setear los creditos
                                        break;
                                    case Constant.GOLD_24H:
                                        GlobalData.set(Constant.USER_STATUS,""+Constant.GOLD,MainActivity.this);
                                        setProfilePick(profileimage);
                                        Common.errorMessage(MainActivity.this, "Enhorabuena " + nick, "Ya eres usuario GOLD por un dia");
                                        break;
                                    case Constant.GOLD_1_MONTH:
                                        GlobalData.set(Constant.USER_STATUS,""+Constant.GOLD,MainActivity.this);
                                        setProfilePick(profileimage);
                                        Common.errorMessage(MainActivity.this, "Enhorabuena" + nick, "Ya eres usuario GOLD por 1 mes");
                                        break;
                                    case Constant.GOLD_3_MONTHS:
                                        GlobalData.set(Constant.USER_STATUS,""+Constant.GOLD,MainActivity.this);
                                        setProfilePick(profileimage);
                                        Common.errorMessage(MainActivity.this, "Enhorabuena" + nick, "Ya eres usuario GOLD por 3 meses");
                                        break;


                                    default:
                                        break;
                                }
                            }
                            else {
                                Toast.makeText(MainActivity.this, "serviceBuyProduct FALSE " + productTypeId, Toast.LENGTH_SHORT).show();
                                Common.errorMessage(MainActivity.this,"",getResources().getString(R.string.check_internet));
                            }

                        } catch (JSONException ex) {
                            ex.printStackTrace();
                            Toast.makeText(MainActivity.this,R.string.error_reponse_server, Toast.LENGTH_SHORT).show();
                        }
                        finally {
                            response.body().close();
                        }
                    }
                });


                Looper.loop();
            }
        });
    }

    /**
     * Abrir cuadro de dialogo para valorar la app
     *
     */
    public void openDialogRateWallamatch() {

        final Dialog dialog = new Dialog(MainActivity.this,R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.raiting_app);

        TextView txtTitleRaiting = (TextView) dialog.findViewById(R.id.txtTitleRaiting);
        TextView txtDescriptionRaiting = (TextView) dialog.findViewById(R.id.txtDescriptionRaiting);
        final TextView txtShouldApp = (TextView)dialog.findViewById(R.id.textView17);
        final RatingBar ratingBar = (RatingBar)dialog.findViewById(R.id.rateWallamatch);
        Button  btnSendRate = (Button)dialog.findViewById(R.id.btnSendRate);
        txtTitleRaiting.setTypeface(TypesLetter.getSansBold(MainActivity.this));
        txtDescriptionRaiting.setTypeface(TypesLetter.getSansRegular(MainActivity.this));
        btnSendRate.setTypeface(TypesLetter.getSansRegular(MainActivity.this));
        txtShouldApp.setTypeface(TypesLetter.getSansRegular(MainActivity.this));
        LayerDrawable stars = (LayerDrawable) ratingBar.getProgressDrawable();
        stars.getDrawable(2).setColorFilter(Color.parseColor("#e2156c"), PorterDuff.Mode.SRC_ATOP);
     
        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                Visibility.invisible(txtShouldApp);
            }
        });

        btnSendRate.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                float rate = ratingBar.getRating();

                if(rate == 0.0)
                {
                    Visibility.visible(txtShouldApp);
                }
                else if(rate == 1.0 || rate == 2.0 || rate == 3.0)
                {
                    //amanda
                    //Toast.makeText(MainActivity.this, "enviar amanda", Toast.LENGTH_SHORT).show();
                    GlobalData.set(Constant.RATE_APP,"Y",MainActivity.this);
                    openDialogDontLikeApp(dialog, rate);

                }
                else
                {
                    //google play
                    GlobalData.set(Constant.RATE_APP,"Y",MainActivity.this);
                    sendRate(rate);
                    dialog.dismiss();
                    final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                    try {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                    } catch (android.content.ActivityNotFoundException anfe) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                    }
                }
            }
        });



        dialog.show();
    }

    /**
     * Abrir cuadro de dialogo para valorar la app
     */
    public void openDialogDontLikeApp(final Dialog dialogRate, final float rate) {

        final Dialog dialog = new Dialog(MainActivity.this,R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_dontlike_app);

        TextView txtTitle           = (TextView) dialog.findViewById(R.id.txtTitleReport);
        TextView txtDescription     = (TextView) dialog.findViewById(R.id.textView13);
        Button  btnCancel           = (Button)dialog.findViewById(R.id.btnCancel);
        Button  btnSend             = (Button)dialog.findViewById(R.id.btnSend);
        final EditText fieldComment = (EditText)dialog.findViewById(R.id.fieldComment);

        txtTitle.setTypeface(TypesLetter.getSansBold(MainActivity.this));
        txtDescription.setTypeface(TypesLetter.getSansRegular(MainActivity.this));
        btnSend.setTypeface(TypesLetter.getSansBold(MainActivity.this));
        btnCancel.setTypeface(TypesLetter.getSansRegular(MainActivity.this));
        fieldComment.setTypeface(TypesLetter.getSansRegular(MainActivity.this));

        btnCancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                KeyBoard.hide(fieldComment);
                dialog.dismiss();
            }
        });
        btnSend.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                String comment = fieldComment.getText().toString().trim();
                if(comment.length() > 0) {
                    KeyBoard.hide(fieldComment);
                }
                else
                   comment = MainActivity.this.getResources().getString(R.string.not_feedback_comment);

                sendFeedBack( "" + comment, rate,dialog, dialogRate);

            }
        });

        dialog.show();
    }

    /**
     * obtiene los chats del usuario
     */
    public void sendFeedBack( String message, float rate,final Dialog dialog, final Dialog dialogRateApp)
    {
        final ProgressDialog progressDialog = new ProgressDialog(MainActivity.this,R.style.CustomDialogTheme);
        progressDialog.setCancelable(false);
        //progressDialog.setMessage(""+mainActivity.getResources().getString(R.string.sending_message));
        progressDialog.show();
        progressDialog.setContentView(R.layout.custom_progress_dialog);
        TextView txtView = (TextView) progressDialog.findViewById(R.id.txtProgressDialog);
        txtView.setText(""+getResources().getString(R.string.sending_message));

        OkHttpClient client = new OkHttpClient();

        RequestBody formBody = new FormBody.Builder()
                .add("sessionToken", "" + GlobalData.get(Constant.SESSION_TOKEN, MainActivity.this))
                .add("message",""+message)
                .add("type","1")
                .add("deviceVersion",Common.getVersionDevice(MainActivity.this))
                .add("so","1")
                .add("appVersion",""+getResources().getString(R.string.app_version))
                .add("rate",""+rate)
                .build();

        Request request = new Request.Builder()
                .url(Services.SEND_FEEDBACK)
                .post(formBody)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                progressDialog.dismiss();
                Looper.prepare();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Common.errorMessage(MainActivity.this, "", "" + getResources().getString(R.string.check_internet));
                    }
                });
                Looper.loop();
            }

            @Override
            public void onResponse(Call call,final Response response) throws IOException {
                progressDialog.dismiss();
                final String result = response.body().string();

                Looper.prepare();

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            JSONObject json = new JSONObject(result);

                            String success = json.getString("success");

                            if (success.equals("true")) {
                                dialog.dismiss();
                                dialogRateApp.dismiss();
                                GlobalData.set(Constant.RATE_APP,"Y",MainActivity.this);
                                //Toast.makeText(mainActivity, "Tu mensaje ha sido recibido. ;)", Toast.LENGTH_SHORT).show();
                                Common.errorMessage(MainActivity.this, "", ""+MainActivity.this.getResources().getString(R.string.send_message_success));
                            } else {
                                //Toast.makeText(mainActivity, "Intenta de nuevo", Toast.LENGTH_SHORT).show();
                                Common.errorMessage(MainActivity.this,"",""+MainActivity.this.getResources().getString(R.string.send_message_failure));
                            }
                        } catch (JSONException ex) {
                            ex.printStackTrace();
                            Toast.makeText(MainActivity.this, R.string.error_reponse_server, Toast.LENGTH_SHORT).show();
                        }
                        finally {
                            response.body().close();
                        }

                    }
                });
                Looper.loop();
            }
        });
    }

    /**
     * Metodo encargado de enviar la calificacion al servidor
     * @param rate
     */
    public void sendRate(float rate)
    {
        OkHttpClient client = new OkHttpClient();

        RequestBody formBody = new FormBody.Builder()
                .add("sessionToken", "" + GlobalData.get(Constant.SESSION_TOKEN, MainActivity.this))
                .add("type","1")
                .add("message","feedBack redirect Google play")
                .add("so","1")
                .add("deviceVersion",Common.getVersionDevice(MainActivity.this))
                .add("appVersion",""+getResources().getString(R.string.app_version))
                .add("rate",""+rate)
                .build();

        Request request = new Request.Builder()
                .url(Services.SEND_FEEDBACK)
                .post(formBody)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

                Looper.prepare();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Common.errorMessage(MainActivity.this, "", "" + getResources().getString(R.string.check_internet));
                    }
                });
                Looper.loop();
            }

            @Override
            public void onResponse(Call call,final Response response) throws IOException {

                //final String result = response.body().string();
                if(response.isSuccessful())
                  response.body().close();

            }
        });
    }


    /**
     * Metodo que chequea la versión de la aplicación
     */
    public void checkVersionApp()
    {
        OkHttpClient client = new OkHttpClient();

        RequestBody formBody = new FormBody.Builder()
                .add("sessionToken", "" + GlobalData.get(Constant.SESSION_TOKEN, MainActivity.this))
                .add("so", "1")
                .add("appVersion",""+MainActivity.this.getResources().getString(R.string.app_version))
                .build();

        Request request = new Request.Builder()
                .url(Services.NEWEST_APP_VERSION)
                .post(formBody)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {


            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {

                final String result = response.body().string();

                Looper.prepare();

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //apago el dialogo
                        try {
                            JSONObject json = new JSONObject(result);
                            String success = json.getString("success");
                            if (success.equals("true")) {
                                String versionApp = json.getString("versionName");
                                String description = json.getString("description");
                                if(!versionApp.equalsIgnoreCase(MainActivity.this.getResources().getString(R.string.app_version)))
                                {
                                    final Dialog dialog =  new Dialog(MainActivity.this,R.style.PauseDialog);
                                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
                                    dialog.setCancelable(false);
                                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                                    dialog.setContentView(R.layout.modal_general);

                                    TextView title = (TextView)dialog.findViewById(R.id.title);
                                    TextView text = (TextView) dialog.findViewById(R.id.text);
                                    text.setText(""+Html.fromHtml(description));
                                    title.setText("Nueva versión Loovers " + versionApp);

                                    Button exit = (Button) dialog.findViewById(R.id.exit);
                                    Visibility.visible(exit);
                                    Button accept = (Button) dialog.findViewById(R.id.accept);
                                    accept.setText(""+getResources().getString(R.string.update));
                                    exit.setText(""+""+getResources().getString(R.string.exit));
                                    // if button is clicked, close the custom dialog
                                    exit.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            dialog.dismiss();
                                        }
                                    });
                                    accept.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            dialog.dismiss();
                                            final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                                            try {
                                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                                            } catch (android.content.ActivityNotFoundException anfe) {
                                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                                            }

                                        }
                                    });

                                    dialog.show();
                                }

                            }

                        } catch (JSONException ex) {
                            ex.printStackTrace();
                            Toast.makeText(MainActivity.this,R.string.error_reponse_server, Toast.LENGTH_SHORT).show();
                        }
                        finally {
                            response.body().close();
                        }
                    }
                });


                Looper.loop();
            }
        });
    }

    /**
     * Método encargado de actualizar la lista de mensajes, cada que ingresa uno nuevo
     */
    public void updateAllChats(MatchesData matchesData)
    {
        if(friendsAdapter != null) {
            //obtengo los chats
            List<MatchesData> matchesDataList = friendsAdapter.chats;
            String otherUserId = matchesData.id_user;
            for (MatchesData data : matchesDataList) {
                if(data.id_user != null && data.id_user.equalsIgnoreCase(otherUserId))
                {
                    //actualizo los datos del chat
                    data.lastestMessage         = matchesData.lastestMessage;
                    data.date                   = matchesData.date;
                    data.senderIdLastestMessage = matchesData.senderIdLastestMessage;
                    if(!data.count_messages.equals("") && !matchesData.count_messages.equals("")) {
                        int numOld  = Integer.parseInt(data.count_messages);
                        int numNews = Integer.parseInt(matchesData.count_messages);
                        int numTotal = numOld + numNews;
                        data.count_messages = ""+numTotal;
                    }
                    else
                        data.count_messages = "1";
                    //refresco el adapter
                    friendsAdapter.notifyDataSetChanged();
                    break;
                }
            }
        }
    }

    /**
     * Actualiza el numero de créditos luego de hacer la compra
     * @param numCoins
     */
    public void setCoins(int numCoins)
    {
        int coins = GlobalData.isKey(Constant.CURRENT_LIKES,MainActivity.this)?Integer.parseInt(GlobalData.get(Constant.CURRENT_LIKES,MainActivity.this)) + numCoins : numCoins;
        if(txtCreditsStore != null)
            txtCreditsStore.setText(""+coins);

        if(txtCredits != null)
            txtCredits.setText(""+coins);

        if(imgCreditsStore != null)
            imgCreditsStore.setImageResource(R.drawable.credito);

        if(imgCredits != null)
            imgCredits.setImageResource(R.drawable.credito);

        GlobalData.set(Constant.CURRENT_LIKES,""+coins,MainActivity.this);
    }

    /**
     * Compara última fecha de conexión para asignar créditos
     */
    public void compareTwoDays()
    {
        if(GlobalData.isKey(Constant.LAST_CONNECTION,MainActivity.this)) {
            SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");
            String lastConnection1 = GlobalData.get(Constant.LAST_CONNECTION, MainActivity.this);
            Date today = Calendar.getInstance().getTime();
            String currentDate = myFormat.format(today);

            try {
                Date date1 = myFormat.parse(lastConnection1);
                Date date2 = myFormat.parse(currentDate);
                long diff = date2.getTime() - date1.getTime();
                Log.d("app2you", "días: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
                long daysNumber = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);


                if (daysNumber > 0) {
                    Log.d("app2you", "Recargar créditos");
                    setCredits(currentDate, ""+Constant.LOGIN_CREDITS_TIME);
                }
                else
                {
                    //cambiar estado del usuario a conectad0
                    serviceIsOnline(Constant.ONLINE);
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        else
        {
            //cambiar estado del usuario a conectad0
            serviceIsOnline(Constant.ONLINE);

            SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date today = Calendar.getInstance().getTime();
            String currentDate = myFormat.format(today);
            GlobalData.set(Constant.LAST_CONNECTION,currentDate,MainActivity.this);
        }
    }

    /**
     * Metodo que recarga créditos a la cuenta del usuario
     */
    public void setCredits(final String currentDate, final String type)
    {
        OkHttpClient client = new OkHttpClient();

        RequestBody formBody = new FormBody.Builder()
                .add("sessionToken", "" + GlobalData.get(Constant.SESSION_TOKEN, MainActivity.this))
                .add("type",""+type)
                .build();

        Request request = new Request.Builder()
                .url(Services.SET_CREDITS)
                .post(formBody)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {


            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {

                final String result = response.body().string();
                Log.d("app2you","resultado setCredits:  "+ result);
                Looper.prepare();

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //apago el dialogo
                        try {
                            JSONObject json = new JSONObject(result);
                            String success = json.getString("success");
                            if (success.equals("true")) {
                                String currentLikes = json.getString("currentLikes");

                                if(type.equals(Constant.LOGIN_CREDITS_TIME)) {
                                    String creditsLogin = GlobalData.isKey(Constant.CREDITS_LOGIN_DAY,MainActivity.this)?GlobalData.get(Constant.CREDITS_LOGIN_DAY,MainActivity.this):"5";
                                    Common.dialogLoginDaily(MainActivity.this, "Bienvenido "+ Common.getNickName(MainActivity.this)+ ", hemos recargado tu cuenta con " +creditsLogin+" likes,"
                                            + " por ingresar hoy a loovers");

                                    GlobalData.set(Constant.LAST_CONNECTION, "" + currentDate, MainActivity.this);
                                    //setCoins(5);
                                    updateCredits(currentLikes);
                                    //cambiar estado del usuario a conectad0
                                    serviceIsOnline(Constant.ONLINE);
                                }
                                else if(type.equals(Constant.FACEBOOK_SHARE_CREDITS_TIME))
                                {
                                    String creditsShareFacebook = GlobalData.isKey(Constant.CREDITS_SHARE_FACEBOOK,MainActivity.this)?GlobalData.get(Constant.CREDITS_SHARE_FACEBOOK,MainActivity.this):"20";
                                    GlobalData.set(Constant.SHARE_FACEBOOK, "Y", MainActivity.this);
                                    //setCoins(20);
                                    updateCredits(currentLikes);
                                    Common.errorMessage(MainActivity.this, "", "Gracias por compartir loovers, hemos cargado a tu cuenta "+ creditsShareFacebook + " likes");
                                }
                                else
                                {
                                    updateCredits(currentLikes);
                                }


                            }

                        } catch (JSONException ex) {
                            ex.printStackTrace();
                        }
                        finally {
                            response.body().close();
                        }
                    }
                });

                Looper.loop();
            }
        });
    }

    public void updateCredits(String coins)
    {
        if(txtCreditsStore != null)
            txtCreditsStore.setText(""+coins);

        if(txtCredits != null)
            txtCredits.setText(""+coins);

        if(imgCreditsStore != null)
            imgCreditsStore.setImageResource(R.drawable.credito);

        if(imgCredits != null)
            imgCredits.setImageResource(R.drawable.credito);

        GlobalData.set(Constant.CURRENT_LIKES,""+coins,MainActivity.this);
    }



}
