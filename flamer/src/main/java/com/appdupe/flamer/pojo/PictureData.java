package com.appdupe.flamer.pojo;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by manoli on 22/3/15.
 */
public class PictureData implements Serializable {

    @SerializedName("avatarUrl")
    public String avatarUrl;

    @SerializedName("imageUrl")
    public String imageUrl;

    @SerializedName("status")
    public String status;

    @SerializedName("name")
    public String name;

    @SerializedName("id")
    public String id_foto;

    @SerializedName("isPrivate")
    public String isPrivate;

    public boolean isChecked;

    @Override
    public String toString() {
        return name + " " + id_foto;
    }
}
