package com.android.slidingmenu.util;

import android.util.Log;

import java.util.HashMap;
import java.util.Locale;

public class Languages {

    static HashMap<String, String> languages = new HashMap<String, String>();
    public static final String SPANISH = "es";
    public static final String ENGLISH = "en";
    public static final String PORTUGUES = "pt";

    public Languages() {
        // TODO Auto-generated constructor stub
    }
    public static String getLanguage()
    {
        String device_language = Locale.getDefault().getDisplayLanguage();
        Log.d("app2you","IDIOMA DEVICE: " + device_language);
        loadMapLanguages();

        return (languages.containsKey(device_language))?languages.get(device_language):"";
    }
    public static void loadMapLanguages()
    {

        languages.put("español", "es");
        languages.put("english", "en");
        languages.put("português", "pt");

    }
    public static String languageDevice()
    {
        return Locale.getDefault().getDisplayLanguage();
    }

}
