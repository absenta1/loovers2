package com.android.slidingmenu.fragments;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;

import android.os.Bundle;
import android.os.Looper;
import android.support.v4.app.DialogFragment;
import android.text.Html;
import android.text.Spannable;

import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;

import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.slidingmenu.Analitycs.MyApplication;
import com.android.slidingmenu.MainActivity;
import com.android.slidingmenu.security.FragmentSecurity;
import com.android.slidingmenu.util.Common;
import com.android.slidingmenu.util.GlobalData;
import com.android.slidingmenu.util.TypesLetter;
import com.android.slidingmenu.util.Validate;
import com.android.slidingmenu.util.Visibility;
import com.android.slidingmenu.webservices.Services;
import com.appdupe.flamer.pojo.UpdatePrefrence;
import com.appdupe.flamer.utility.Constant;

import com.appyvet.rangebar.RangeBar;
import com.gc.materialdesign.views.Switch;
import com.gitonway.lee.niftymodaldialogeffects.lib.Effectstype;
import com.gitonway.lee.niftymodaldialogeffects.lib.NiftyDialogBuilder;
import com.google.gson.Gson;
import com.loovers.app.R;


import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;


import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class FragmentSettings extends DialogFragment implements OnClickListener {

	private TextView limitotsearchvalue;
	private TextView maxage;
	private TextView senderName;
	private ImageView imgEditAccount,imgBack, imgEditStatus;
	private TextView minagevalue,btnDeleteAccount;
	private TextView limitsearchtextview,txtBirthday;
	private Button logoutButton,btnPrivacity, btnShareWallamatch,btnCodeSecurity;

	private Switch switchWalla,switchMatches,switchMessages;
	private TextView txtWallas, txtMatches, txtMessages, txtMyStatus;
	private EditText fieldEmail;
	public MainActivity mainActivity;
	public LinearLayout containerOptionsBackChat,containerMyAccount,containerMyBirthday;
	public RelativeLayout limittosearchseekbarlayout,containerShowAge;
	public com.appyvet.rangebar.RangeBar range_bar, rangebarAge;

	public FragmentSettings(MainActivity mainActivity)
	{
		this.mainActivity = mainActivity;
	}

	public FragmentSettings(){}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.settingactivity, null);

		if(mainActivity == null)
			mainActivity = (MainActivity)getActivity();

		Visibility.visible(mainActivity.tvTitle);

		initDataList(view);


		logoutButton.setOnClickListener(this);
		btnPrivacity.setOnClickListener(this);
		btnShareWallamatch.setOnClickListener(this);
		imgBack.setOnClickListener(this);
		containerOptionsBackChat.setOnClickListener(this);
		imgEditStatus.setOnClickListener(this);
		txtMyStatus.setOnClickListener(this);
		btnCodeSecurity.setOnClickListener(this);

		return view;
	}



	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setStyle(DialogFragment.STYLE_NO_FRAME, android.R.style.Theme_Holo_Light_DarkActionBar);
	}

	private void initDataList(View view) {

		range_bar 			= (com.appyvet.rangebar.RangeBar)view.findViewById(R.id.rangebar);
		rangebarAge 		= (com.appyvet.rangebar.RangeBar)view.findViewById(R.id.rangebarAge);
		containerMyAccount 	= (LinearLayout)view.findViewById(R.id.containerMyAccount);
		containerMyBirthday = (LinearLayout)view.findViewById(R.id.containerMyBirthday);
		txtBirthday         = (TextView)view.findViewById(R.id.txtBirthday);

		//inicializar evento de fecha
		showDialogDate();

		limittosearchseekbarlayout = (RelativeLayout)view.findViewById(R.id.limittosearchseekbarlayout);
		containerShowAge = (RelativeLayout)view.findViewById(R.id.containerShowAge);
		containerMyAccount.setVisibility(GlobalData.isKey(Constant.LOGIN_FACEBOOK,mainActivity)?View.GONE:View.VISIBLE);
		containerMyBirthday.setVisibility(GlobalData.isKey(Constant.BIRTHDAY_VERIFIED,mainActivity)?View.GONE:View.VISIBLE);

		if(GlobalData.isKey(Constant.EMAIL_VERIFIED,mainActivity) && GlobalData.get(Constant.EMAIL_VERIFIED,mainActivity).equals("Y"))
			Visibility.gone(containerMyAccount);

		int minValue = (GlobalData.isKey(Constant.MIN_AGE_PREFERENCE, mainActivity)) ? Integer.parseInt(GlobalData.get(Constant.MIN_AGE_PREFERENCE, mainActivity)) : 18;
		int maxValue = (GlobalData.isKey(Constant.MAX_AGE_PREFERENCE, mainActivity)) ? Integer.parseInt(GlobalData.get(Constant.MAX_AGE_PREFERENCE, mainActivity)) : 55;
		Log.d("app2you","values range : " + minValue + " - " + maxValue);
		if(maxValue >=55)
			maxValue = 55;

		if(minValue<= 18)
			minValue = 18;

		rangebarAge.setRangePinsByValue(minValue, maxValue);//pines

		int distance = (GlobalData.isKey(Constant.RADIUS_PREFERENCE, mainActivity)) ? Integer.parseInt(GlobalData.get(Constant.RADIUS_PREFERENCE, mainActivity)) - 1  : 499;
		range_bar.setSeekPinByIndex(distance);

		logoutButton = (Button) view.findViewById(R.id.logoutButton);
		logoutButton.setTransformationMethod(null);

		btnDeleteAccount = (TextView) view.findViewById(R.id.btnDeleteAccount);
		//btnDeleteAccount.setText(Html.fromHtml(mainActivity.getResources().getString(R.string.txt_delete_account)));
		String myString = mainActivity.getResources().getString(R.string.txt_delete_account);
		int i1 = myString.indexOf("[");
		int i2 = myString.indexOf("]");
		btnDeleteAccount.setMovementMethod(LinkMovementMethod.getInstance());
		btnDeleteAccount.setText(Html.fromHtml(myString), TextView.BufferType.SPANNABLE);
		Spannable mySpannable = (Spannable)btnDeleteAccount.getText();
		ClickableSpan myClickableSpan = new ClickableSpan()
		{
			@Override
			public void onClick(View widget) {
				/* do something */
				FragmentDeleteAccount fragmentDeleteAccount = new FragmentDeleteAccount(mainActivity, FragmentSettings.this);
				fragmentDeleteAccount.show(mainActivity.mFragmentManager,"FragmentDeleteAccount.TAG");
			}
		};
		mySpannable.setSpan(myClickableSpan, i1, i2 + 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

		//setTextViewHTML(btnDeleteAccount,mainActivity.getResources().getString(R.string.txt_delete_account));
		btnPrivacity     = (Button) view.findViewById(R.id.btnPrivacity);
		btnShareWallamatch = (Button) view.findViewById(R.id.btnShareWallamatch);
		btnCodeSecurity    = (Button)view.findViewById(R.id.btnCodeSecurity);
		btnCodeSecurity.setTransformationMethod(null);
		btnDeleteAccount.setTransformationMethod(null);
		btnPrivacity.setTransformationMethod(null);
		btnShareWallamatch.setTransformationMethod(null);

		maxage = (TextView) view.findViewById(R.id.maxage);
		minagevalue = (TextView) view.findViewById(R.id.minagevalue);
		senderName = (TextView)view.findViewById(R.id.senderName);

		containerOptionsBackChat = (LinearLayout)view.findViewById(R.id.containerOptionsBackChat);
		imgBack                  = (ImageView)view.findViewById(R.id.imgBack);
		minagevalue.setText(""+minValue);
		maxage.setText((maxValue==55)?"55+":""+maxValue);

		limitotsearchvalue = (TextView) view
				.findViewById(R.id.limitotsearchvalue);

		limitotsearchvalue.setText(""+(distance+1));

		limitsearchtextview = (TextView) view
				.findViewById(R.id.limitsearchtextview);

		imgEditAccount =   (ImageView) view.findViewById(R.id.imgEditAccount);
		txtWallas = (TextView)view.findViewById(R.id.txtWalla);
		txtMatches = (TextView)view.findViewById(R.id.txtMatches);
		txtMessages = (TextView)view.findViewById(R.id.txtMessages);

		switchWalla = (Switch)view.findViewById(R.id.switchWalla);
		switchMatches = (Switch)view.findViewById(R.id.switchMatches);
		switchMessages = (Switch)view.findViewById(R.id.switchMessages);

		fieldEmail = (EditText)view.findViewById(R.id.editEmail);
		imgEditStatus = (ImageView)view.findViewById(R.id.imgEditStatus);
		txtMyStatus   = (TextView)view.findViewById(R.id.txtMyStatus);
		setPreferenceStatus();

		fieldEmail.setEnabled(false);
		fieldEmail.setTextColor(Color.parseColor("#c3c3c3"));

		txtWallas.setTypeface(TypesLetter.getSansRegular(mainActivity));
		txtMatches.setTypeface(TypesLetter.getSansRegular(mainActivity));
		txtMessages.setTypeface(TypesLetter.getSansRegular(mainActivity));

		limitsearchtextview.setTypeface(TypesLetter.getSansRegular(mainActivity));
		limitotsearchvalue.setTypeface(TypesLetter.getSansRegular(mainActivity));

		logoutButton.setTypeface(TypesLetter.getSansRegular(mainActivity));
		minagevalue.setTypeface(TypesLetter.getSansRegular(mainActivity));
		maxage.setTypeface(TypesLetter.getSansRegular(mainActivity));
		senderName.setTypeface(TypesLetter.getAvenir(mainActivity));

		switchWalla.setChecked((GlobalData.get(Constant.NOTIFICATION_WALLAS, mainActivity).equals("Y")) ? true : false);
		switchMatches.setChecked((GlobalData.get(Constant.NOTIFICATION_MATCHES, mainActivity).equals("Y")) ? true : false);
		switchMessages.setChecked((GlobalData.get(Constant.NOTIFICATION_MESSAGES, mainActivity).equals("Y")) ? true : false);

		if(GlobalData.isKey(Constant.EMAIL_ADDRESS,mainActivity))
			fieldEmail.setText(""+GlobalData.get(Constant.EMAIL_ADDRESS, mainActivity));

		range_bar.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
			@Override
			public void onRangeChangeListener(RangeBar rangeBar, int leftPinIndex, int rightPinIndex, String leftPinValue, String rightPinValue) {
				limitotsearchvalue.setText(""+rightPinValue);
			}
		});

		rangebarAge.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
			@Override
			public void onRangeChangeListener(RangeBar rangeBar, int leftPinIndex, int rightPinIndex, String leftPinValue, String rightPinValue) {
				minagevalue.setText(""+leftPinValue);
				maxage.setText(rightPinValue.equals("55")?"55+":""+rightPinValue);
			}
		});

		imgEditAccount.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				if(fieldEmail.isEnabled())
				{
					String email = fieldEmail.getText().toString().trim();
					if(!Validate.validEmail2(email))
					{
						fieldEmail.setError(""+mainActivity.getResources().getString(R.string.error_mail));
					}
					else {
						if(GlobalData.get(Constant.EMAIL_ADDRESS,mainActivity).equals(email))
						{
							fieldEmail.setEnabled(false);
							imgEditAccount.setImageResource(R.drawable.edit_email);
							fieldEmail.setTextColor(Color.parseColor("#c3c3c3"));
							GlobalData.set(Constant.EMAIL_ADDRESS, "" +email, mainActivity);
							Toast.makeText(mainActivity, R.string.saved_mail, Toast.LENGTH_SHORT).show();
						}
						else
							updateProfile(email);

					}
				}
				else {
					imgEditAccount.setImageResource(R.drawable.afirm);
					fieldEmail.setTextColor(Color.parseColor("#000000"));
					fieldEmail.setEnabled(true);
				}
			}
		});
		limittosearchseekbarlayout.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				validateStatusUser();
			}
		});
		containerShowAge.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				validateStatusUser();
			}
		});

		String status = GlobalData.get(Constant.USER_STATUS, mainActivity);

		if (status.equals(Constant.NOT_GOLD_BEFORE_TRIAL_NO_PICTURE))
		{
			range_bar.setEnabled(false);
			rangebarAge.setEnabled(false);

		}
		else
		{
			range_bar.setEnabled(true);
			rangebarAge.setEnabled(true);
		}


	}

	/**
	 * Metodo para validar el estado del usuario y mostrar un mensaje para poder modificar las preferencias
	 */
	public void validateStatusUser()
	{
		String status = GlobalData.get(Constant.USER_STATUS, mainActivity);

		if (status.equals(Constant.NOT_GOLD_BEFORE_TRIAL_NO_PICTURE))
		{
			final NiftyDialogBuilder dialog =  NiftyDialogBuilder.getInstance(mainActivity);
			dialog.withTitle(null);
			dialog.withMessage(null);
			dialog.isCancelableOnTouchOutside(false);
			dialog.withDialogColor(Color.TRANSPARENT);
			dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
			dialog.withDuration(400);
			dialog.withEffect(Effectstype.Shake);
			dialog.setCustomView(R.layout.modal_general,mainActivity);

			TextView title = (TextView) dialog.findViewById(R.id.title);
			TextView text = (TextView) dialog.findViewById(R.id.text);
			title.setText(""+ Common.getNickName(mainActivity));
			text.setText(mainActivity.getResources().getString(R.string.message_preferences_no_photo));

			Button exit = (Button) dialog.findViewById(R.id.exit);
			exit.setText(mainActivity.getResources().getString(R.string.exit));
			Button accept = (Button) dialog.findViewById(R.id.accept);
			// if button is clicked, close the custom dialog
			accept.setText(mainActivity.getResources().getString(R.string.update_photo));
			exit.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();
				}
			});
			accept.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if(mainActivity.menu.isMenuShowing())
						mainActivity.menu.toggle();


					dialog.dismiss();
					dismiss();

					mainActivity.replaceFragment(new FragmentProfile(mainActivity));


				}
			});

			dialog.show();

		}

	}


	@Override
	public void onClick(View v) {


		 if (v.getId() == R.id.logoutButton) {

			// custom dialog
			final Dialog dialog = new Dialog(mainActivity);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
			dialog.setContentView(R.layout.modal_general);

			TextView text = (TextView) dialog.findViewById(R.id.text);
			text.setText(""+mainActivity.getResources().getString(R.string.message_close_session));


			Button exit = (Button) dialog.findViewById(R.id.exit);
			Button accept = (Button) dialog.findViewById(R.id.accept);
			// if button is clicked, close the custom dialog
			exit.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();
				}
			});
			accept.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();
					serviceLogout();
					//Common.closeSessionWallamatch(mainActivity);

				}
			});

			dialog.show();

		}
		/*else if (v.getId() == R.id.btnDeleteAccount) {


			FragmentDeleteAccount fragmentDeleteAccount = new FragmentDeleteAccount(mainActivity, this);
			fragmentDeleteAccount.show(mainActivity.mFragmentManager,"FragmentDeleteAccount.TAG");

		}*/
		else if(v.getId() == R.id.btnPrivacity)
		{
			//Toast.makeText(mainActivity, "Enviar a politica de privacidad loovers", Toast.LENGTH_SHORT).show();
			FragmentPrivacity fragmentPrivacity = new FragmentPrivacity(mainActivity);
			fragmentPrivacity.show(mainActivity.mFragmentManager,"FragmentPrivacity.TAG");
		}
		else if(v.getId() == R.id.btnCodeSecurity)
		{
			 FragmentSecurity fragmentSecurity = new FragmentSecurity(mainActivity);
			 fragmentSecurity.show(mainActivity.mFragmentManager,"FragmentSecurity.TAG");
		}
		else if(v.getId() == R.id.btnShareWallamatch)
		{
			shareApp();
		}
		else if(v.getId() == R.id.containerOptionsBackChat)
		{
			dismiss();
			updatePrefere();
		}
		else if(v.getId() == R.id.imgBack)
		{
			dismiss();
			updatePrefere();
		}
		else if(v.getId() == R.id.txtMyStatus)
		{
			selectStatus();
		}
		else if(v.getId() == R.id.imgEditStatus)
		{
			selectStatus();
		}



	}
	public void selectStatus()
	{
		final String genere = GlobalData.get(Constant.GENERE,mainActivity);
		String [] items = null;
		if(genere.equals(Constant.MAN))
			items = mainActivity.getResources().getStringArray(R.array.personal_status_him);
		else
			items = mainActivity.getResources().getStringArray(R.array.personal_status_her);

		new AlertDialog.Builder(mainActivity)
				.setSingleChoiceItems(items, 0, null)
				.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						dialog.dismiss();
						int selectedPosition = ((AlertDialog) dialog).getListView().getCheckedItemPosition();
						switch (selectedPosition) {
							case 0:
								txtMyStatus.setText(""+mainActivity.getResources().getString(R.string.chat));
								GlobalData.set(Constant.PERSONAL_STATUS,""+Constant.CHATEAR_UN_RATO,mainActivity);
								break;
							case 1:
								txtMyStatus.setText((genere.equals(Constant.MAN))?mainActivity.getResources().getString(R.string.invite_male):mainActivity.getResources().getString(R.string.invite_female));
								GlobalData.set(Constant.PERSONAL_STATUS, "" + Constant.INVITARME_A_SALIR, mainActivity);
								break;
							case 2:
								txtMyStatus.setText(""+mainActivity.getResources().getString(R.string.skip_preliminaries));
								GlobalData.set(Constant.PERSONAL_STATUS,""+Constant.SALTARME_LOS_PRELIMINARES, mainActivity);
								break;
							default:
								break;
						}
						// Do something useful withe the position of the selected radio button
					}
				})
				.show();
	}

	public void updatePrefere()
	{
		if(switchWalla.isCheck())
			GlobalData.set(Constant.NOTIFICATION_WALLAS,"Y",mainActivity);
		else
			GlobalData.set(Constant.NOTIFICATION_WALLAS,"N",mainActivity);

		if (switchMatches.isCheck())
			GlobalData.set(Constant.NOTIFICATION_MATCHES, "Y", mainActivity);
		else
			GlobalData.set(Constant.NOTIFICATION_MATCHES, "N", mainActivity);

		if (switchMessages.isCheck()) {

			GlobalData.set(Constant.NOTIFICATION_MESSAGES, "Y", mainActivity);
		}
		else {

			GlobalData.set(Constant.NOTIFICATION_MESSAGES, "N", mainActivity);
		}


		updateUserPrefrence();

		if(!GlobalData.isKey(Constant.BIRTHDAY_VERIFIED,mainActivity)) {
			String txtDate = txtBirthday.getText().toString().trim();

			SimpleDateFormat fromUser = new SimpleDateFormat("dd/MM/yyyy");

			SimpleDateFormat myFormat = new SimpleDateFormat("yyyy/MM/dd");
			try {
				txtDate = myFormat.format(fromUser.parse(txtDate));
				updateProfileBirthday(txtDate);
			} catch (ParseException e) {
				e.printStackTrace();
			}

		}
	}
	/**
	 * Metodo encargado de abrir opciones para compartir enlace de la aplicacion
	 */
	public void shareApp()
	{

		Intent intent = new Intent(Intent.ACTION_SEND);
		//intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
		//intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.setType("text/plain");
		intent.putExtra(android.content.Intent.EXTRA_SUBJECT, mainActivity.getResources()
				.getString(R.string.app_name));

		intent.putExtra(Intent.EXTRA_TEXT, mainActivity.getResources().getString(R.string.download_loovers_google_play)+" " + mainActivity.getResources().getString(R.string.url_google_play));
		mainActivity.startActivity(Intent.createChooser(intent, getResources()
				.getString(R.string.app_name)));
		//setStyleRangeBar(range_bar);
		//setStyleRangeBar(rangebarAge);
		/*final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
		try {
			startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
		} catch (android.content.ActivityNotFoundException anfe) {
			startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
		}*/
	}


	/**
	 * Actualiza las preferencias del usuario
	 */
	private void updateUserPrefrence()
	{
		String loweragePrefrence = "" + rangebarAge.getLeftPinValue();
		String heigherAge = "" + rangebarAge.getRightPinValue();
		String sexPrefrence = ""+GlobalData.get(Constant.WANTED_FOUND,mainActivity); ;

		String userSelectedRadius = ""+range_bar.getRightPinValue();

		//String[] params = { sessionToken,sexPrefrence, loweragePrefrence, heigherAge, userSelectedRadius};

		Log.d("app2you", "parametros a mandar: min age: " + loweragePrefrence + " max age: " + heigherAge + " radius: " + userSelectedRadius + " sex preference: " + sexPrefrence);

		//new BackgroundTaskForUpdatePrefrence().execute(params);
		//Ejecutar servicio para actualizar preferencias
		updateUserPreference(sexPrefrence,loweragePrefrence,heigherAge,userSelectedRadius);
	}

	/**
	 * Metodo encargado de actualizar las preferencias del usuario.
	 * @param sexPreference
	 * @param minAge
	 * @param maxAge
     * @param radius
     */
	private void updateUserPreference(String sexPreference, final String  minAge, final String maxAge, final String radius)
	{
		okhttp3.OkHttpClient client = new okhttp3.OkHttpClient();

		okhttp3.RequestBody formBody = new FormBody.Builder()
				.add("sessionToken", "" + GlobalData.get(Constant.SESSION_TOKEN, mainActivity))
				.add("preferenceSex",""+sexPreference)
				.add("preferenceLowerAge",""+minAge)
				.add("preferenceUpperAge",""+maxAge)
				.add("preferenceRadius",""+radius)
				.add("personalStatus",""+ GlobalData.get(Constant.PERSONAL_STATUS, mainActivity))
				.build();


		okhttp3.Request request = new okhttp3.Request.Builder()
				.url(Services.UPDATE_PREFERENCES_SERVICE)
				.post(formBody)
				.build();


		client.newCall(request).enqueue(new okhttp3.Callback() {
			@Override
			public void onFailure(Call call, IOException e) {
				Looper.prepare();

				mainActivity.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						Common.errorMessage(mainActivity,"",""+mainActivity.getResources().getString(R.string.check_internet));
					}
				});

				Looper.loop();
			}

			@Override
			public void onResponse(Call call, final Response response) throws IOException {
				final String result = response.body().string();
				Log.d("app2you", "respuesta profiles game: " + result);

				Looper.prepare();

				mainActivity.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						try {
							Gson gson = new Gson();
							UpdatePrefrence mUpdatePrefrence = gson.fromJson(result,UpdatePrefrence.class);
							if (mUpdatePrefrence.success) {
								Toast.makeText(mainActivity, R.string.success_preferences ,Toast.LENGTH_SHORT).show();
								GlobalData.delete(Constant.CARDS_GAME,mainActivity);
								GlobalData.set(Constant.MIN_AGE_PREFERENCE, "" + minAge, mainActivity);
								GlobalData.set(Constant.MAX_AGE_PREFERENCE,""+maxAge,mainActivity);
								GlobalData.set(Constant.RADIUS_PREFERENCE,""+radius,mainActivity);

							} else {
								Toast.makeText(mainActivity, R.string.error_reponse_server,Toast.LENGTH_SHORT).show();

							}
						} catch (Exception ex) {

							ex.printStackTrace();
							Toast.makeText(mainActivity, R.string.error_reponse_server, Toast.LENGTH_SHORT).show();
						}
						finally {
							response.body().close();
						}
					}
				});


				Looper.loop();
			}
		});
	}


	/**
	 * Metodo encargado de obtener los matches que tiene un usuario
	 */
	public void serviceLogout()
	{

		final ProgressDialog	progressDialog = new ProgressDialog(mainActivity,R.style.CustomDialogTheme);
			//progressDialog.setMessage(mainActivity.getResources().getString(R.string.closing_session));
			progressDialog.setCancelable(false);
			progressDialog.show();
		progressDialog.setContentView(R.layout.custom_progress_dialog);
		TextView txtView = (TextView) progressDialog.findViewById(R.id.txtProgressDialog);
		txtView.setText(""+mainActivity.getResources().getString(R.string.closing_session));

		OkHttpClient client = new OkHttpClient();
		RequestBody formBody = new FormBody.Builder()
				.add("sessionToken", "" + GlobalData.get(Constant.SESSION_TOKEN, mainActivity))

				.build();

		Request request = new Request.Builder()
				.url(Services.LOG_OUT)
				.post(formBody)
				.build();

		client.newCall(request).enqueue(new Callback() {
			@Override
			public void onFailure(Call call, IOException e) {
				if (progressDialog != null)
					progressDialog.dismiss();
				Looper.prepare();

				mainActivity.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						Common.errorMessage(mainActivity, "", mainActivity.getResources().getString(R.string.check_internet));
					}
				});

				Looper.loop();
			}

			@Override
			public void onResponse(Call call, final Response response) throws IOException {

				final String result = response.body().string();
				Log.d("app2you", "respuesta logout: " + result);

				Looper.prepare();



					mainActivity.runOnUiThread(new Runnable() {
						@Override
						public void run() {
							try {
								JSONObject json = new JSONObject(result);

								String success = json.getString("success");
								int errorNum = json.has("errorNum")?json.getInt("errorNum"):0;
								if (success.equals("true")) {
									Common.closeSessionWallamatch(mainActivity,progressDialog);
								} else {
									if (progressDialog != null)
										progressDialog.dismiss();

									if(errorNum == Constant.ERROR_OPEN_SESSION_OTHER_DEVICE)
										otherSessionOpen();
									else
										Toast.makeText(mainActivity, R.string.try_again, Toast.LENGTH_SHORT).show();
								}
							} catch (JSONException e) {
								if (progressDialog != null)
									progressDialog.dismiss();
								e.printStackTrace();
								Toast.makeText(mainActivity, R.string.error_reponse_server, Toast.LENGTH_SHORT).show();
							}
							finally {
								response.body().close();
							}


						}


					});

				Looper.loop();
			}
		});
	}
	/**
	 * Cerrar session actual porque la han abierto en otro dispositivo
	 */
	private void otherSessionOpen() {

		final NiftyDialogBuilder dialog =  NiftyDialogBuilder.getInstance(mainActivity);
		dialog.withTitle(null);
		dialog.withMessage(null);
		dialog.isCancelableOnTouchOutside(false);
		dialog.withDialogColor(Color.TRANSPARENT);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		dialog.withDuration(Constant.DURATION_ANIMATION);
		dialog.withEffect(Effectstype.Slidetop);
		dialog.setCustomView(R.layout.modal_general,mainActivity);

		TextView text = (TextView) dialog.findViewById(R.id.text);
		text.setText(""+mainActivity.getResources().getString(R.string.error_session));


		Button exit = (Button) dialog.findViewById(R.id.exit);
		Visibility.gone(exit);
		Button accept = (Button) dialog.findViewById(R.id.accept);
		// if button is clicked, close the custom dialog
		exit.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		accept.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
				Common.closeSessionWallamatch(mainActivity);


			}
		});

		dialog.show();
	}


	/**
	 * Metodo encargado actualizar el perfil
	 */
	public void updateProfile(final String email)
	{

		final ProgressDialog	progressDialog = new ProgressDialog(mainActivity,R.style.CustomDialogTheme);
		//progressDialog.setMessage(""+mainActivity.getResources().getString(R.string.checking_email));
		progressDialog.setCancelable(false);
		progressDialog.show();
		progressDialog.setContentView(R.layout.custom_progress_dialog);
		TextView txtView = (TextView) progressDialog.findViewById(R.id.txtProgressDialog);
		txtView.setText(""+mainActivity.getResources().getString(R.string.checking_email));

		OkHttpClient client = new OkHttpClient();
		RequestBody formBody = new FormBody.Builder()
				.add("sessionToken", "" + GlobalData.get(Constant.SESSION_TOKEN, mainActivity))
				.add("key","email")
				.add("value",""+email)
				.build();

		Request request = new Request.Builder()
				.url(Services.UPDATE_PROFILE)
				.post(formBody)
				.build();

		client.newCall(request).enqueue(new Callback() {
			@Override
			public void onFailure(Call call, IOException e) {
				if (progressDialog != null)
					progressDialog.dismiss();
				Looper.prepare();

				mainActivity.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						Common.errorMessage(mainActivity, "", mainActivity.getResources().getString(R.string.check_internet));
					}
				});
				Looper.loop();
			}

			@Override
			public void onResponse(Call call, final Response response) throws IOException {
				if (progressDialog != null)
					progressDialog.dismiss();
				final String result = response.body().string();
				Log.d("app2you", "respuesta logout: " + result);

				Looper.prepare();



					mainActivity.runOnUiThread(new Runnable() {
						@Override
						public void run() {
							try {
								JSONObject json = new JSONObject(result);

								String success = json.getString("success");

								if (success.equals("true")) {
									fieldEmail.setEnabled(false);
									imgEditAccount.setImageResource(R.drawable.edit_email);
									fieldEmail.setTextColor(Color.parseColor("#c3c3c3"));
									GlobalData.set(Constant.EMAIL_ADDRESS, "" +email, mainActivity);
									Toast.makeText(mainActivity, R.string.saved_mail, Toast.LENGTH_SHORT).show();
								} else {
									Toast.makeText(mainActivity, R.string.try_again, Toast.LENGTH_SHORT).show();
								}
							} catch (JSONException e) {
								e.printStackTrace();
								Toast.makeText(mainActivity, R.string.error_reponse_server, Toast.LENGTH_SHORT).show();
							}
							finally {
								response.body().close();
							}


						}


					});

				Looper.loop();
			}
		});
	}


	@Override
	public void onResume() {
		super.onResume();
		Log.d("app2you","onResumen preferences");
		MyApplication.getInstance().trackScreenView("fragment preferences");
		//setStyleRangeBar(range_bar);
		//setStyleRangeBar(rangebarAge);
		getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
		getDialog().setOnKeyListener(new DialogInterface.OnKeyListener() {
			@Override
			public boolean onKey(android.content.DialogInterface dialog, int keyCode,
								 android.view.KeyEvent event) {


				if ((keyCode == android.view.KeyEvent.KEYCODE_BACK)) {
					//This is the filter
					if (event.getAction() != KeyEvent.ACTION_DOWN) {
						dismiss();
						updatePrefere();
						return true;
					}
					else {
						return true; // pretend we've processed it
					}
				} else
					return false; // pass on to be processed as normal
			}
		});
	}

	/**
	 * setea las preferencias
	 */
	public void setPreferenceStatus()
	{
		String status = (GlobalData.isKey(Constant.PERSONAL_STATUS,mainActivity))?GlobalData.get(Constant.PERSONAL_STATUS,mainActivity):"11";
		String genere = (GlobalData.isKey(Constant.GENERE,mainActivity))?GlobalData.get(Constant.GENERE,mainActivity):"1";

		if(status.isEmpty() || status == null) {
			GlobalData.set(Constant.PERSONAL_STATUS,""+Constant.CHATEAR_UN_RATO, mainActivity);
			txtMyStatus.setText(""+mainActivity.getResources().getString(R.string.chat));
		}
		else
		{
			int valueStatus = Integer.parseInt(status);
			try {
				switch (valueStatus) {
					case Constant.CHATEAR_UN_RATO:
						txtMyStatus.setText(""+mainActivity.getResources().getString(R.string.chat));
						//GlobalData.set(Constant.PERSONAL_STATUS,""+Constant.CHATEAR_UN_RATO, mainActivity);
						break;
					case Constant.INVITARME_A_SALIR:
						txtMyStatus.setText((genere.equals(Constant.MAN))?mainActivity.getResources().getString(R.string.invite_male):mainActivity.getResources().getString(R.string.invite_female));
						//GlobalData.set(Constant.PERSONAL_STATUS,""+Constant.INVITARME_A_SALIR, mainActivity);
						break;
					case Constant.SALTARME_LOS_PRELIMINARES:
						txtMyStatus.setText(""+mainActivity.getResources().getString(R.string.skip_preliminaries));
						//GlobalData.set(Constant.PERSONAL_STATUS,""+Constant.CHATEAR_UN_RATO, mainActivity);
						break;
					default:
						break;

				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
	}

	/**
	 * Metodo encargado actualizar el perfil (fecha de nacimiento)
	 */
	public void updateProfileBirthday(final String dateOfBirth)
	{
		OkHttpClient client = new OkHttpClient();
		RequestBody formBody = new FormBody.Builder()
				.add("sessionToken", "" + GlobalData.get(Constant.SESSION_TOKEN, mainActivity))
				.add("key","dateOfBirth")
				.add("value",""+dateOfBirth)
				.build();

		Request request = new Request.Builder()
				.url(Services.UPDATE_PROFILE)
				.post(formBody)
				.build();

		client.newCall(request).enqueue(new Callback() {
			@Override
			public void onFailure(Call call, IOException e) {

			}

			@Override
			public void onResponse(Call call, final Response response) throws IOException {

				final String result = response.body().string();
				Log.d("app2you", "respuesta fecha nacimiento: " + result);

				Looper.prepare();
				mainActivity.runOnUiThread(new Runnable() {
						@Override
						public void run() {
							try {
								JSONObject json = new JSONObject(result);

								String success = json.getString("success");

								if (success.equals("true")) {
									GlobalData.set(Constant.BIRTHDAY_VERIFIED,"Y",mainActivity);
								}
							} catch (JSONException e) {
								e.printStackTrace();

							}
							finally {
								response.body().close();
							}


						}


					});

				Looper.loop();
			}
		});
	}
	/**
	 * Metodo encargado de mostrar dialogo con opcion para seleccionar fecha de nacimiento
	 */
	public void showDialogDate()
	{
		final Calendar myCalendar = Calendar.getInstance();

		final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

			@Override
			public void onDateSet(DatePicker view, int year, int monthOfYear,
								  int dayOfMonth) {
				Calendar userAge = new GregorianCalendar(year,monthOfYear,dayOfMonth);
				Calendar minAdultAge = new GregorianCalendar();
				minAdultAge.add(Calendar.YEAR, - 18);
				if (minAdultAge.before(userAge))
				{
					Common.errorMessage(mainActivity,"",getResources().getString(R.string.higher_age));
					// Toast.makeText(LoginUsingFacebookMailOrGooglev2.this, R.string.higher_age, Toast.LENGTH_SHORT).show();
				}
				else {
					myCalendar.set(Calendar.YEAR, year);
					myCalendar.set(Calendar.MONTH, monthOfYear);
					myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
					updateLabel(myCalendar);
				}
			}

		};

		txtBirthday.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				setDatePicker(date, myCalendar);
			}
		});

	}
	public void setDatePicker(DatePickerDialog.OnDateSetListener date,Calendar myCalendar)
	{
		int age = 0;
		if(txtBirthday.getText().toString().trim().length() == 0)
			age = 18;

		new DatePickerDialog(mainActivity, date, myCalendar
				.get(Calendar.YEAR) - age, myCalendar.get(Calendar.MONTH),
				myCalendar.get(Calendar.DAY_OF_MONTH)).show();
	}
	/**
	 * Actualiza el campo de la fecha
	 * @param myCalendar
	 */
	private void updateLabel(Calendar myCalendar) {

		String myFormat = "dd/MM/yyyy";//"yyyy/MM/dd" ;
		SimpleDateFormat sdf = new SimpleDateFormat(myFormat);
		txtBirthday.setText(sdf.format(myCalendar.getTime()));
	}

}//cierre de la clase preferencias
