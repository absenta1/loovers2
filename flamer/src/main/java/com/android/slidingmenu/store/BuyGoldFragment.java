package com.android.slidingmenu.store;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.android.slidingmenu.MainActivity;
import com.android.slidingmenu.fragments.FragmentAdvantages;
import com.android.slidingmenu.util.Common;
import com.viewpagerindicator.CirclePageIndicator;
import com.loovers.app.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Juan Martin Bernal on 11/3/16.
 */
public class BuyGoldFragment extends Fragment
{
    public MainActivity mainActivity;
    public ListView listCredits;
    public BuyCreditsAdapter adapter;
    public ViewPager viewpagerStore;
    public View view;
    public int counter = -1;
    private Timer timer;
    ViewPageAdapter pageAdapter;

    public BuyGoldFragment() {
        // Required empty public constructor
    }

    public BuyGoldFragment(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.buy_credits_gold, container,false);

        if (mainActivity == null)
            mainActivity = (MainActivity) getActivity();

        listCredits                 = (ListView)view.findViewById(R.id.listCredits);
        viewpagerStore              = (ViewPager)view.findViewById(R.id.viewpagerStore);
        listCredits.setClickable(false);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        adapter = new BuyCreditsAdapter(mainActivity, Common.loadProductsGOLDWallamatch(mainActivity));
        listCredits.setAdapter(adapter);


        //ArrayList<Store> storesAdvantages = Common.loadAdvantagesGold(mainActivity);
        List<Fragment> fragments = getFragments();

        pageAdapter = new ViewPageAdapter(getChildFragmentManager(), fragments);

        viewpagerStore.setAdapter(pageAdapter);
        CirclePageIndicator titleIndicator2 = (CirclePageIndicator) view.findViewById(R.id.titlesStore);
        titleIndicator2.setViewPager(viewpagerStore);

        //CustomPagerAdapter mCustomPagerAdapter2 = new CustomPagerAdapter(mainActivity, storesAdvantages);
        viewpagerStore.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                counter = position - 1;
                FragmentAdvantages fragment = (FragmentAdvantages)pageAdapter.instantiateItem(viewpagerStore,position);
                fragment.startAnimation();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


    }


    /**
     * Mover al siguiente slide
     */
    private void moveToNext(){
        if (viewpagerStore!=null) {

            viewpagerStore.setCurrentItem(counter + 1,true);

            if(viewpagerStore.getCurrentItem() == 4)
                counter = -1;
            else
                counter++;
        }

    }

    /**
     * Metodo encargado de gestionar la animación de las paginas de presentación de las ventajas de ser GOLD.
     */
    private void updateDisplay() {
        timer = new Timer();
        timer.schedule(new TimerTask() {

            @Override
            public void run() {
                mainActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //Your code to run in GUI thread here
                        moveToNext();
                    }//public void run() {
                });

            }

        }, 0, 3300);//Update change view every 3 second
    }

    @Override
    public void onResume() {
        super.onResume();
        updateDisplay();
    }

    @Override
    public void onPause() {
        super.onPause();
        if(timer != null)
            timer.cancel();
    }

    /**
     * Método encargado de agregar los slide (fragments) de las ventajas de ser GOLD
     * @return
     */
    private List<Fragment> getFragments(){
        List<Fragment> fList = new ArrayList<Fragment>();

        fList.add(FragmentAdvantages.newInstance(mainActivity.getString(R.string.title_all_unlock),mainActivity.getString(R.string.description_all_unlock),R.drawable.llave_oro2));
        fList.add(FragmentAdvantages.newInstance(mainActivity.getString(R.string.title_visibility),mainActivity.getString(R.string.description_visibility),R.drawable.potencia_visibilidad));
        fList.add(FragmentAdvantages.newInstance(mainActivity.getString(R.string.title_request_photos),mainActivity.getString(R.string.description_request_photos),R.drawable.fotos_privadas1));
        fList.add(FragmentAdvantages.newInstance(mainActivity.getString(R.string.title_chat_want_to_you),mainActivity.getString(R.string.descrition_chat_want),R.drawable.chatea));
        fList.add(FragmentAdvantages.newInstance(mainActivity.getString(R.string.title_no_wait_more),mainActivity.getString(R.string.description_no_wait_more),R.drawable.desbloquea_todo));

        return fList;
    }

    /**
     * Clase encargada de gestionar los fragmentos para mostrar las ventajas de ser GOLD en la tienda
     */
    private class ViewPageAdapter extends FragmentPagerAdapter {
        private List<Fragment> fragments;

        public ViewPageAdapter(FragmentManager fm, List<Fragment> fragments) {
            super(fm);
            this.fragments = fragments;
        }
        @Override
        public Fragment getItem(int position) {
            return this.fragments.get(position);
        }

        @Override
        public int getCount() {
            return this.fragments.size();
        }
    }
}
