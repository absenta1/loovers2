package com.android.slidingmenu.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.loovers.app.R;

import java.util.List;

/**
 * Created by Juan Martin Bernal on 17/12/15.
 */
public class AdapterListMail extends ArrayAdapter<String> {
    private final Context context;
    private final List<String> values;

    public AdapterListMail(Context context, List<String> values) {
        super(context, R.layout.row_mail, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.row_mail, parent, false);
        TextView mail = (TextView) rowView.findViewById(R.id.txtMail);

        String mailWallaMatch = values.get(position);

        if(mailWallaMatch != null)
            mail.setText(""+mailWallaMatch);


        return rowView;
    }
}
