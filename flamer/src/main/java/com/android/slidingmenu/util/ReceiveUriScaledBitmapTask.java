package com.android.slidingmenu.util;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import com.loovers.app.R;

import java.io.File;
import java.io.IOException;

/**
 * Created by Ernesto on 22/12/15.
 */
public class ReceiveUriScaledBitmapTask extends AsyncTask<Object, Uri, Uri> {

    private ReceiveUriScaledBitmapListener receiveUriScaledBitmapListener;
    private ProgressDialog progressDialog;
    Activity mainActivity;

    public ReceiveUriScaledBitmapTask(ReceiveUriScaledBitmapListener receiveUriScaledBitmapListener, Activity mainActivity) {
        this.receiveUriScaledBitmapListener = receiveUriScaledBitmapListener;
        this.mainActivity = mainActivity;
    }
    public ReceiveUriScaledBitmapTask(ReceiveUriScaledBitmapListener receiveUriScaledBitmapListener) {
        this.receiveUriScaledBitmapListener = receiveUriScaledBitmapListener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    	/*
    	 * cerrar dialog
    	 */

        progressDialog = new ProgressDialog(mainActivity);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(mainActivity.getResources().getString(R.string.app_name));
        progressDialog.show();
    }

    @Override
    protected Uri doInBackground(Object[] params) {
        ImageUtils imageUtils = (ImageUtils) params[0];
        Uri originalUri = (Uri) params[1];

        File bitmapFile = null;
        Uri outputUri = null;

        Bitmap bitmap = imageUtils.getBitmap(originalUri);
        Log.d("eka", "dimen : " + "alto :" + bitmap.getHeight() + " ancho: " + bitmap.getWidth());
        int alto = bitmap.getHeight();
        int ancho  = bitmap.getWidth();
        Bitmap scaledBitmap = null;

        if(alto >= ancho)
            scaledBitmap = Bitmap.createScaledBitmap(bitmap, 480, 600, true);//imageUtils.createScaledBitmap(bitmap);
        else
            scaledBitmap = Bitmap.createScaledBitmap(bitmap, 600, 480, true);//imageUtils.createScaledBitmap(bitmap);


        try {
            if(scaledBitmap != null)
                bitmapFile = imageUtils.getFileFromBitmap(scaledBitmap);
        } catch (IOException error) {
            error.printStackTrace();
        }

        if (bitmapFile != null)
            outputUri = Uri.fromFile(bitmapFile);


        return outputUri;
    }

    @Override
    protected void onPostExecute(Uri uri) {
        receiveUriScaledBitmapListener.onUriScaledBitmapReceived(uri);
        progressDialog.dismiss();
    }

    public interface ReceiveUriScaledBitmapListener {

        public void onUriScaledBitmapReceived(Uri uri);
    }
}