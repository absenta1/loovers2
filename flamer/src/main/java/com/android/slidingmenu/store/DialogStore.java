package com.android.slidingmenu.store;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentTabHost;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.slidingmenu.Analitycs.MyApplication;
import com.android.slidingmenu.MainActivity;
import com.android.slidingmenu.util.GlobalData;
import com.android.slidingmenu.util.TypesLetter;
import com.appdupe.flamer.utility.Constant;
import com.loovers.app.R;

/**
 * Created by Juan Matin Bernal on 11/3/16.
 */
public class DialogStore extends DialogFragment
{
    public MainActivity mainActivity;
    public LinearLayout containerOptionsBackChat;
    public ImageView imgBack;
    public int tab = 0;


    //Constructor
    public DialogStore()
    {}

    public DialogStore(MainActivity mainActivity) {
        this.mainActivity = mainActivity;

    }
    public DialogStore(MainActivity mainActivity, int tab) {
        this.mainActivity = mainActivity;
        this.tab = tab;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.store_layout, container, false);
        if(mainActivity == null)
            mainActivity = (MainActivity)getActivity();

        mainActivity.mTabHost                   = (FragmentTabHost)view.findViewById(R.id.tabs);
        containerOptionsBackChat                = (LinearLayout)view.findViewById(R.id.containerOptionsBackChat);
        imgBack                                 = (ImageView)view.findViewById(R.id.imgBack);
        mainActivity.txtCreditsStore            = (TextView) view.findViewById(R.id.txtCreditsStore);
        mainActivity.imgCreditsStore            = (ImageView)view.findViewById(R.id.imgCreditsStore);
        mainActivity.mTabHost.setup(mainActivity, getChildFragmentManager(), R.layout.fragment_near);

        mainActivity.txtCreditsStore.setText(GlobalData.isKey(Constant.CURRENT_LIKES,mainActivity)?GlobalData.get(Constant.CURRENT_LIKES,mainActivity):"0");

        if(GlobalData.get(Constant.USER_STATUS,mainActivity).equals(Constant.GOLD) || GlobalData.get(Constant.GENERE,mainActivity).equals(Constant.WOMAN)) {
            mainActivity.mTabHost.addTab(mainActivity.mTabHost.newTabSpec("COMPRAR").setIndicator(getTabIndicator(mainActivity, mainActivity.getString(R.string.title_buy_credits))),
                    BuyCreditsFragment.class, null);

        }
        else {
            mainActivity.mTabHost.addTab(mainActivity.mTabHost.newTabSpec("COMPRAR").setIndicator(getTabIndicator(mainActivity, mainActivity.getString(R.string.title_buy_credits))),
                    BuyCreditsFragment.class, null);
            mainActivity.mTabHost.addTab(mainActivity.mTabHost.newTabSpec("GOLD").setIndicator(getTabIndicator(mainActivity, mainActivity.getString(R.string.title_gold))),BuyGoldFragment.class, null);
        }
        String numLikes = GlobalData.isKey(Constant.CURRENT_LIKES,mainActivity)?GlobalData.get(Constant.CURRENT_LIKES,mainActivity):"0";
        if(numLikes.equals("0"))
        {
            mainActivity.imgCreditsStore.setImageResource(R.drawable.no_credito);
            mainActivity.txtCreditsStore.setTextColor(Color.parseColor("#c3c3c3"));
        }
        else
        {
            mainActivity.imgCreditsStore.setImageResource(R.drawable.credito);
            mainActivity.txtCreditsStore.setTextColor(Color.parseColor("#d1a52e"));
        }

        return view;


    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setStyle(DialogFragment.STYLE_NO_FRAME, android.R.style.Theme_Holo_Light_DarkActionBar);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if(mainActivity.mTabHost.getTabWidget().getChildCount() > 0)
            mainActivity.mTabHost.setCurrentTab(tab);
        else
            mainActivity.mTabHost.setCurrentTab(0);

        mainActivity.mTabHost.getTabWidget().setStripEnabled(false);

        containerOptionsBackChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    private View getTabIndicator(Context context, String title) {
        View view = LayoutInflater.from(context).inflate(R.layout.tab_layout, null);

        mainActivity.titleTab = (TextView) view.findViewById(R.id.title);
        mainActivity.badgeTab  = (TextView) view.findViewById(R.id.badge);
        mainActivity.titleTab.setTypeface(TypesLetter.getSansRegular(mainActivity));
        mainActivity.badgeTab.setTypeface(TypesLetter.getSansRegular(mainActivity));
        mainActivity.titleTab.setText(title);
        return view;
    }
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);

        // request a window without the title
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        return dialog;
    }
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mainActivity.mTabHost = null;
    }

    @Override
    public void onResume() {
        super.onResume();
        MyApplication.getInstance().trackScreenView("fragment store");
    }
}
