package com.appdupe.flamer.pojo;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class UserfriendData 
{
	@SerializedName("data")
    private ArrayList<UserFriendsData>data;

	public ArrayList<UserFriendsData> getData() {
		return data;
	}

	public void setData(ArrayList<UserFriendsData> data) {
		this.data = data;
	}
}
