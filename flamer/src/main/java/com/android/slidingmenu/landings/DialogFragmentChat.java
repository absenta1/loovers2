package com.android.slidingmenu.landings;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.android.slidingmenu.MainActivity;
import com.android.slidingmenu.store.DialogStore;
import com.android.slidingmenu.util.TypesLetter;
import com.loovers.app.R;


/**
 * Created by Juan Martín Bernal on 18/5/16.
 */
public class DialogFragmentChat  extends DialogFragment implements View.OnClickListener {

    private TextView senderName,txtTitle, txtSubText,txtOtherMoment;
    private ImageView imgBack;
    public LinearLayout containerOptionsBackChat;
    public Button btnActiveGold;
    public MainActivity mainActivity;


    public DialogFragmentChat(MainActivity mainActivity)
    {
        this.mainActivity = mainActivity;
    }

    public DialogFragmentChat(){}

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.landing_chat, null);

        if(mainActivity == null)
            mainActivity = (MainActivity)getActivity();

        initComponents(view);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_FRAME, android.R.style.Theme_Holo_Light_DarkActionBar);
    }

    public void initComponents(View view)
    {
        senderName          = (TextView)view.findViewById(R.id.senderName);
        txtOtherMoment      = (TextView)view.findViewById(R.id.txtOtherMoment);
        senderName.setText(""+mainActivity.getString(R.string.writes_to_all));
        senderName.setTypeface(TypesLetter.getAvenir(mainActivity));

        btnActiveGold                   = (Button)view.findViewById(R.id.btnActiveGold);
        containerOptionsBackChat        = (LinearLayout)view.findViewById(R.id.containerOptionsBackChat);
        imgBack                         = (ImageView)view.findViewById(R.id.imgBack);
        txtTitle                        = (TextView)view.findViewById(R.id.txtTitle);
        txtSubText                      = (TextView)view.findViewById(R.id.txtSubText);


        txtTitle.setTypeface(TypesLetter.getSansBold(mainActivity));
        txtSubText.setTypeface(TypesLetter.getSansRegular(mainActivity));
        txtOtherMoment.setTypeface(TypesLetter.getSansRegular(mainActivity));
        btnActiveGold.setTypeface(TypesLetter.getSansRegular(mainActivity));

        imgBack.setOnClickListener(this);
        containerOptionsBackChat.setOnClickListener(this);
        txtOtherMoment.setOnClickListener(this);
        btnActiveGold.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.btnActiveGold:
                DialogStore dialogStore = new DialogStore(mainActivity,1);
                dialogStore.show(mainActivity.mFragmentManager,"DialogStore.TAG");
                break;
            case R.id.containerOptionsBackChat:
                dismiss();
                break;
            case R.id.imgBack:
                dismiss();
                break;
            case R.id.txtOtherMoment:
                dismiss();
                break;
            default:
                break;
        }


    }

}