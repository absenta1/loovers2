package com.android.slidingmenu.reports;

/**
 * Created by Juan Martin Bernal  on 24/2/16.
 */
public class Report {

    public int idCategory;
    public int image;
    public String reason;

    public Report(int idCategory, int image, String reason) {
        this.idCategory = idCategory;
        this.image = image;
        this.reason = reason;
    }
}
