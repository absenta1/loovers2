package com.android.slidingmenu.util;


import android.app.Service;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

/**
 * Created by Juan Martin Bernal on 2/12/15.
 */
public class KeyBoard {

    /**
     * Metodo encargado de ocultar el teclado
     * @param view
     */
    public static void hide(View view) {
        InputMethodManager imm = (InputMethodManager) view.getContext()
                .getSystemService(Service.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    /**
     * Metodo encargado de mostrar el teclado
     * @param view
     */
    public static void show(View view) {
        InputMethodManager imm = (InputMethodManager) view.getContext()
                .getSystemService(Service.INPUT_METHOD_SERVICE);
        imm.toggleSoftInputFromWindow(view.getApplicationWindowToken(),InputMethodManager.SHOW_FORCED, 0);

    }
}