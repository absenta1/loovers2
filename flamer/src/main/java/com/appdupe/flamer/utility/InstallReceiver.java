package com.appdupe.flamer.utility;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

public class InstallReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {


        String rawReferrer = intent.getStringExtra("referrer");
        if (rawReferrer != null) {
            trackReferrerAttributes(rawReferrer, context);
        }
    }

    private void trackReferrerAttributes(String rawReferrer, Context context) {
        String referrer = "";

        try {
            referrer = URLDecoder.decode(rawReferrer, "UTF-8");
            System.out.println("QQQQQQQ  referrer " + referrer);
            /*if ( Ultilities.getDeviceId(context).equals(""))
                Ultilities.getDeviceIdFrom0(context);*/

            new BackGroundTaskForSendReferrer(referrer, context).execute();

        } catch (UnsupportedEncodingException e) {
            return;
        }

        if (referrer==null || referrer.equals("")) {
            return;
        }

        Uri uri = Uri.parse('?' + referrer); // appends ? for Uri to pickup query string
    

    }

    private class BackGroundTaskForSendReferrer extends
            AsyncTask<String, Void, Void> {
        private Ultilities mUltilities;
        private String referrer;
        private List<NameValuePair> referrerValuePairList;

        private Context mContext;
        private boolean success = true;

        public BackGroundTaskForSendReferrer(String _referrer, Context context) {

            this.referrer = _referrer;

            this.mContext = context;
        }
        @Override
        protected Void doInBackground(String... params) {
            try {

                mUltilities = new Ultilities(mContext);
                String[] findMatchParamere = { referrer };
                List<NameValuePair> namevaluepairs = new ArrayList<NameValuePair>();
                namevaluepairs.add(new BasicNameValuePair("referrer", referrer));
                namevaluepairs.add(new BasicNameValuePair("regId", "regId"));
                mUltilities.makeHttpRequest( Constant.referrer_url, Constant.methodeName, namevaluepairs);




            } catch (Exception e) {
                System.out.println("ERROR" + e);
                success = false;
            }

            return null;
        }



        }
}