package com.appdupe.androidpushnotifications;

import com.google.gson.annotations.SerializedName;

public class ChatMessageList {

	@SerializedName("msg")
	public String strMessage;

	@SerializedName("sfid")
	public String strSenderFacebookId;

	@SerializedName("mid")
	public String intMessageId;

	@SerializedName("sname")
	public String strSendername;

	@SerializedName("dt")
	public String strDateTime;

	@SerializedName("senderId")
	public String strSenderId;

	@SerializedName("messageTypeId")
	public String messageTypeId;

	public String strReceiverId;

	public  String strFlagForMessageSuccess;

	@SerializedName("imageUrl")
	public String imageUrl;

	@SerializedName("picId")
	public String picId;

	@SerializedName("privatePicStatusId")
	public String privatePicStatusId;

	@SerializedName("chatStatusId")
	public int chatStatusId;

	@Override
	public String toString() {
		return strSendername + " " + strSenderId;
	}
}
