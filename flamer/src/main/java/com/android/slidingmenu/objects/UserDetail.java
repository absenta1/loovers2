package com.android.slidingmenu.objects;

import com.appdupe.flamer.pojo.PictureData;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Juan Martin Bernal on 29/12/15.
 */
public class UserDetail implements Serializable {

    @SerializedName("success")
    public Boolean  success;

    @SerializedName("descriptionOptions")
    public String  descriptionOptions;

    @SerializedName("personalStatus")
    public String  personalStatus;

    @SerializedName("city")
    public String  city;

    @SerializedName("name")
    public String  name;

    @SerializedName("age")
    public String  age;

    @SerializedName("pimgs")
    public ArrayList<PictureData> pimgs;

    @SerializedName("gimgs")
    public ArrayList<PictureData>  gimgs;

    @SerializedName("isFavorite")
    public int  isFavorite;

    @SerializedName("isMatch")
    public int  isMatch;

    @SerializedName("likeToMe")
    public int  likeToMe;

    @SerializedName("distance")
    public String  distance;

    @SerializedName("chatStatusId")
    public int chatStatusId;

}
