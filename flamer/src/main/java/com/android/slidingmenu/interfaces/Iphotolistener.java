package com.android.slidingmenu.interfaces;

/**
 * Created by manoli on 11/3/15.
 */
public interface Iphotolistener {

    void OnPhotoMadeOK();
    void OnPhotoMadeFail();
}
