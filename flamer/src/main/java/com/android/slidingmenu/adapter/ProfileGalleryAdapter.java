package com.android.slidingmenu.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.slidingmenu.util.Visibility;
import com.appdupe.flamer.pojo.PictureData;
import com.appdupe.flamer.utility.Constant;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.loovers.app.R;

import java.util.ArrayList;

public class ProfileGalleryAdapter extends BaseAdapter {
	private Context mContext;
	public ArrayList<PictureData> photolist = new ArrayList<PictureData>();
    private LayoutInflater mInflater;
    private Typeface openSansLight ;


    public ProfileGalleryAdapter(ArrayList<PictureData> _photolist ,Context c) {
		super();

		mContext = c;
	    mInflater = LayoutInflater.from(mContext);
        openSansLight = Typeface.createFromAsset(c.getAssets(),
                "fonts/OpenSans-Light.ttf");

        photolist.clear();
        for (int i = _photolist.size() - 1; i >= 0; --i)
            photolist.add(_photolist.get(i));

        //photolist.addAll(_photolist);
        photolist.add(0,getFakeImageDetail());


	}
    private PictureData getFakeImageDetail (){
        PictureData imageDetail = new PictureData();
        imageDetail.imageUrl = "BAJAR";
        imageDetail.status = "0";
        return  imageDetail;


    }
	@Override
	public int getCount() {

		return photolist.size();

	}

	@Override
	public PictureData getItem(int position) {

		return photolist.get(position);
	}

	@Override
	public long getItemId(int position) {

		return  photolist.get(position).hashCode();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

        final POIHolder holder;

        if (convertView == null){
            convertView = mInflater.inflate(R.layout.search_gridview_adapter, null);
            holder = new POIHolder();
            holder.image   = (ImageView)convertView.findViewById(R.id.search_profile_foto);
            holder.imgLock = (ImageView)convertView.findViewById(R.id.imgLock);
            holder.name = (TextView)convertView.findViewById(R.id.textView2);
            holder.progressBarLoadImage = (ProgressWheel)convertView.findViewById(R.id.progressBarLoadImage);
            holder.name.setTypeface(openSansLight);
            convertView.setTag(holder);
        }
        else
            holder = (POIHolder)convertView.getTag();

        Visibility.gone(holder.imgLock);
        Visibility.gone(holder.name);

        PictureData pictureData = photolist.get(position);

        if (pictureData.imageUrl.equals("BAJAR")){
            holder.image.setImageResource(R.drawable.lh_ic_plus_icon);
            Visibility.gone(holder.name);
            Visibility.gone(holder.progressBarLoadImage);
        }
        else
        {

            //Uri uri = Uri.fromFile(new File(photolist.get(position).getSdpath()));
            String url = pictureData.avatarUrl;
            if(url.isEmpty()) {
                Picasso.with(mContext).load(R.drawable.no_foto).resize(150, 150).centerCrop().into(holder.image);
            }
            else {
                Visibility.visible(holder.progressBarLoadImage);
                Picasso.with(mContext).load(url).error(R.drawable.no_foto).resize(300, 300).centerCrop().into(holder.image, new Callback() {
                    @Override
                    public void onSuccess() {
                        Visibility.gone(holder.progressBarLoadImage);
                    }

                    @Override
                    public void onError() {
                        Visibility.gone(holder.progressBarLoadImage);
                    }
                });
            }
        }

        if(pictureData.isPrivate != null && pictureData.isPrivate.equals(Constant.PRIVATE_PHOTO))
            Visibility.visible(holder.imgLock);

        /*SpannableStringBuilder longDescription = new SpannableStringBuilder();

        String status = pictureData.status;
        String estado = "";
        if (status.equals("1")){
            estado = mContext.getResources().getString(R.string.pending);
        }else if (status.equals("2")){
            estado = "";

        }else if (status.equals("3")){
            estado = "";

        }else if (status.equals("4")){
            estado = "";

        }
        longDescription.append(estado);
        longDescription.setSpan(new ForegroundColorSpan(Color.WHITE), 0, longDescription.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        holder.name.setText(longDescription);*/

        return convertView;
	}


    public class POIHolder{
        public ImageView image,imgLock;
        public TextView  name;
        public ProgressWheel progressBarLoadImage;

    }

}
