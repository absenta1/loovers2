package com.android.slidingmenu.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.android.slidingmenu.util.BlurTransformation;
import com.appdupe.flamer.pojo.PictureData;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.loovers.app.R;

import java.util.ArrayList;

/**
 * Created by Juan Martin Bernal on 1/4/16.
 */
public class AdapterPrivatePhoto extends ArrayAdapter<PictureData> {
    private  Context context;
    private ArrayList<PictureData> values;
    public ArrayList<String> picIds = new ArrayList<>();

    public AdapterPrivatePhoto(Context context, ArrayList<PictureData> values) {
        super(context, R.layout.row_item_grid_private_photos, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);


        View rowView = inflater.inflate(R.layout.row_item_grid_private_photos, parent, false);
        //RelativeLayout containerImage         = (RelativeLayout)rowView.findViewById(R.id.containerImage);
        //TextView txtRatePrivatePhoto          = (TextView)rowView.findViewById(R.id.txtRatePrivatePhoto);
        final RelativeLayout containerDifused   = (RelativeLayout)rowView.findViewById(R.id.containerDifused);
        ImageView privatePhoto                  = (ImageView)rowView.findViewById(R.id.privatePhoto);
        //final ProgressBar progressBarLoadImage  = (ProgressBar)rowView.findViewById(R.id.progressBarLoadImage);

       // #4AAFC8
        final PictureData pictureData = values.get(position);

        if(pictureData != null) {

            //txtRatePrivatePhoto.setText("" + temporal.rate);

           // Visibility.visible(progressBarLoadImage);
            Picasso.with(context).load(pictureData.avatarUrl).error(R.drawable.no_foto).transform(new BlurTransformation(context,25)).into(privatePhoto, new Callback() {
                @Override
                public void onSuccess() {
                   // Visibility.gone(progressBarLoadImage);
                }

                @Override
                public void onError() {
                   // Visibility.gone(progressBarLoadImage);
                }
            });
        }
        containerDifused.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(pictureData.isChecked)
                {
                    containerDifused.setBackgroundColor(Color.parseColor("#00000b"));
                    containerDifused.setAlpha(0.7f);
                    pictureData.isChecked = false;
                    if(picIds.contains(pictureData.id_foto))
                        picIds.remove(picIds.indexOf(pictureData.id_foto));

                }
                else
                {
                    containerDifused.setBackgroundColor(Color.parseColor("#4AAFC8"));
                    pictureData.isChecked = true;
                    picIds.add(pictureData.id_foto);
                }
            }
        });


        return rowView;
    }

}