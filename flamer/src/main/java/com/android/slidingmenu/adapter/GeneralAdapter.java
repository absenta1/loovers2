package com.android.slidingmenu.adapter;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.CountDownTimer;
import android.os.Looper;
import android.support.v8.renderscript.Allocation;
import android.support.v8.renderscript.Element;
import android.support.v8.renderscript.RenderScript;
import android.support.v8.renderscript.ScriptIntrinsicBlur;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.slidingmenu.MainActivity;
import com.android.slidingmenu.fragments.DetailProfileFragment;
import com.android.slidingmenu.fragments.FragmentProfile;
import com.android.slidingmenu.landings.DialogFragmentWantToKnow;
import com.android.slidingmenu.objects.Match;
import com.android.slidingmenu.store.DialogStore;
import com.android.slidingmenu.util.AppAnimation;
import com.android.slidingmenu.util.Common;
import com.android.slidingmenu.util.GlobalData;
import com.android.slidingmenu.util.TypesLetter;
import com.android.slidingmenu.util.Visibility;
import com.android.slidingmenu.webservices.Services;
import com.appdupe.MatchFromFlechazosDialogFragment;
import com.appdupe.androidpushnotifications.ChatActivity;
import com.appdupe.flamer.pojo.MatchesData;
import com.appdupe.flamer.pojo.UserLikes;
import com.appdupe.flamer.utility.Constant;
import com.gitonway.lee.niftymodaldialogeffects.lib.Effectstype;
import com.gitonway.lee.niftymodaldialogeffects.lib.NiftyDialogBuilder;
import com.google.gson.Gson;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.loovers.app.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Clase encargada de la visualización y gestión de los perfiles de los usuarios, así como las acciones dependiendo a que categoria pertenezcan
 * "quien te quiere conocer, match, cerca de ti, favoritos"
 * @author Juan Martín Bernal
 * @date 16/12/2015
 */
public class GeneralAdapter extends BaseAdapter {

    private MainActivity mainActivity;
    private ArrayList<MatchesData> machedataList ;
    private LayoutInflater mInflater;
    private Typeface openSansLight ;
    private int blurred = 0;
    private String status,genere;
    private Animation scale, fadeIn;
    private int type;

    public GeneralAdapter(MainActivity mainActivity, ArrayList<MatchesData> machedataList, int type) {
        super();
        this.machedataList      = machedataList;
        this.mainActivity       = mainActivity;
        this.mInflater          = LayoutInflater.from(mainActivity);
        openSansLight           = TypesLetter.getSansLight(mainActivity);
        scale                   = AnimationUtils.loadAnimation(mainActivity, R.anim.scale_remove_view);
        fadeIn                  = AnimationUtils.loadAnimation(mainActivity, R.anim.fade_in_slow);
        this.type               = type;
        genere                  = (GlobalData.isKey(Constant.GENERE,mainActivity))?GlobalData.get(Constant.GENERE,mainActivity):Constant.MAN;
        status                  = (GlobalData.isKey(Constant.USER_STATUS,mainActivity))?GlobalData.get(Constant.USER_STATUS, mainActivity):Constant.WOMAN;

        if (status.equals(Constant.GOLD) || type == Constant.MATCH || type == Constant.FAVORITE || type == Constant.LIST_PERSONS || genere.equals(Constant.WOMAN))
            blurred = 0;
        else
            blurred = 25;

    }

    @Override
    public int getCount() {
        return machedataList.size();
    }

    @Override
    public Object getItem(int position) {
        return machedataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return  machedataList.get(position).hashCode();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final POIHolder holder;
        View view = convertView;

        if (view == null){
            view = mInflater.inflate(R.layout.general_adapter, parent, false);
            holder = new POIHolder();
            holder.image                        = (ImageView)view.findViewById(R.id.search_profile_foto);
            holder.name                         = (TextView)view.findViewById(R.id.textView2);
            holder.progressBarLoadImage         = (ProgressWheel)view.findViewById(R.id.progressBarLoadImage);
            holder.txtMatchNearToYou            = (TextView)view.findViewById(R.id.txtMatchNearToYou);
            holder.personalStatus               = (TextView)view.findViewById(R.id.txtPersonalStatus);
            holder.txtNumGallery                = (TextView)view.findViewById(R.id.txtNumGallery);
            holder.imgWalla                     = (ImageView)view.findViewById(R.id.imgWalla);
            holder.imgDelete                    = (ImageView)view.findViewById(R.id.imgDelete);
            holder.imgLike                      = (ImageView)view.findViewById(R.id.imgLike);
            holder.imgWallaPerson               = (ImageView)view.findViewById(R.id.imgWallaPerson);
            holder.imgSendMessage               = (ImageView)view.findViewById(R.id.imgSendMessage);
            holder.img_online                   = (ImageView)view.findViewById(R.id.img_online);
            holder.containerDifused             = (RelativeLayout)view.findViewById(R.id.containerDifused);
            holder.containerCard                = (RelativeLayout)view.findViewById(R.id.containerCard);
            holder.containerImage               = (RelativeLayout)view.findViewById(R.id.containerImage);
            holder.containerNumGallery          = (LinearLayout)view.findViewById(R.id.containerNumGallery);
            holder.containerButtonsWallas       = (LinearLayout)view.findViewById(R.id.containerButtonsWallas);
            holder.containerButtonsMach         = (LinearLayout)view.findViewById(R.id.containerButtonsMach);
            holder.containerButtonsListPersons  = (LinearLayout)view.findViewById(R.id.containerButtonsListPersons);
            holder.container_personal_status    = (LinearLayout)view.findViewById(R.id.container_personal_status);
            holder.progressBarImgDelete         = (ProgressBar)view.findViewById(R.id.progressBarImgDelete);
            holder.progressBarImgWalla          = (ProgressBar)view.findViewById(R.id.progressBarImgWalla);
            holder.btnMatch                     = (Button)view.findViewById(R.id.btnMatch);
            holder.btnChat                      = (Button)view.findViewById(R.id.btnChat);

            holder.btnMatch.setTransformationMethod(null);
            holder.btnChat.setTransformationMethod(null);
            holder.imgWallaPerson.setEnabled(false);
            holder.name.setTypeface(openSansLight);
            holder.personalStatus.setTypeface(TypesLetter.getSansRegular(mainActivity));
            holder.name.setSelected(true);
            view.setTag(holder);
        }
        else
            holder = (POIHolder)view.getTag();

        final MatchesData data = machedataList.get(position);

        Visibility.gone(holder.containerButtonsMach);
        Visibility.gone(holder.containerButtonsWallas);
        Visibility.gone(holder.containerButtonsListPersons);
        Visibility.gone(holder.img_online);
        Visibility.gone(holder.containerNumGallery);
        holder.containerDifused.setVisibility((blurred > 0)?View.VISIBLE:View.GONE);

        switch (type)
        {
            case Constant.WALLAS:
                if (genere.equals(Constant.WOMAN) || status.equals(Constant.GOLD)/* || status.equals(Constant.GOLD_TRIAL)*/ )
                    Visibility.visible(holder.containerButtonsWallas);
                break;

            case Constant.MATCH:
                Visibility.visible(holder.containerButtonsMach);
                break;

            case Constant.LIST_PERSONS:
                //habilita el estado de online/offline del usuario
                //holder.img_online.setImageResource((data.isOnline == 1)?R.drawable.icon_online:R.drawable.icon_offline);
                //Visibility.visible(holder.img_online);
                //holder.containerButtonsListPersons.setBackground(null);
                int sdk = android.os.Build.VERSION.SDK_INT;
                if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    holder.containerButtonsListPersons.setBackgroundDrawable(null);
                } else {
                    holder.containerButtonsListPersons.setBackground(null);
                }
                Visibility.gone(holder.container_personal_status);
                Visibility.visible(holder.containerButtonsListPersons);

                holder.imgWallaPerson.setImageResource((data.likeToMe == 0)?R.drawable.icono_like_no2 :R.drawable.icono_like_si2);
                if(data.numImgGallery != 0)
                {
                    Visibility.visible(holder.containerNumGallery);
                    holder.txtNumGallery.setText(""+data.numImgGallery);
                    holder.name.setPadding(16,0,0,0);
                }

                if(data.isMatch == 1)
                {
                    Visibility.gone(holder.imgSendMessage);
                    Visibility.gone(holder.imgWallaPerson);
                    Visibility.visible(holder.txtMatchNearToYou);
                    holder.containerButtonsListPersons.setBackgroundColor(Color.parseColor("#00C300"));
                    holder.txtMatchNearToYou.setTypeface(TypesLetter.getAvenir(mainActivity),Typeface.ITALIC);
                }
                else
                {
                    Visibility.visible(holder.imgSendMessage);
                    Visibility.gone(holder.txtMatchNearToYou);
                    if(data.requestToMe)
                    {
                        switch (data.chatStatusId) {
                            case Constant.NOT_REQUEST_CHAT:
                                holder.imgSendMessage.setImageResource(R.drawable.ic_email_white_24dp);
                                break;
                            case Constant.REQUEST_CHAT_PENDING:
                                Visibility.gone(holder.imgSendMessage);
                                Visibility.gone(holder.imgWallaPerson);
                                Visibility.visible(holder.txtMatchNearToYou);
                                holder.containerButtonsListPersons.setBackgroundColor(Color.parseColor("#0084B4"));
                                holder.txtMatchNearToYou.setTypeface(TypesLetter.getAvenir(mainActivity), Typeface.ITALIC);
                                holder.txtMatchNearToYou.setText("Esperando respuesta");
                                break;
                            case Constant.ACCEPT_CHAT_DIRECT:
                                //holder.imgSendMessage.setImageResource(R.drawable.ic_email_white_24dp);
                                Visibility.gone(holder.imgSendMessage);
                                Visibility.gone(holder.imgWallaPerson);
                                Visibility.visible(holder.txtMatchNearToYou);
                                holder.containerButtonsListPersons.setBackgroundColor(Color.parseColor("#00C300"));
                                holder.txtMatchNearToYou.setTypeface(TypesLetter.getAvenir(mainActivity), Typeface.ITALIC);
                                holder.txtMatchNearToYou.setText(""+mainActivity.getString(R.string.request_accepted));
                                break;
                            case Constant.REJECT_CHAT_DIRECT:
                                holder.imgSendMessage.setImageResource(R.drawable.ic_email_reject);
                                break;
                            default:
                                holder.imgSendMessage.setImageResource(R.drawable.ic_email_white_24dp);
                                break;
                        }
                    }
                    else {
                        switch (data.chatStatusId) {
                            case Constant.NOT_REQUEST_CHAT:
                                holder.imgSendMessage.setImageResource(R.drawable.ic_email_white_24dp);
                                break;
                            case Constant.REQUEST_CHAT_PENDING:
                                holder.imgSendMessage.setImageResource(R.drawable.ic_email_sent);
                                break;
                            case Constant.ACCEPT_CHAT_DIRECT:
                                //holder.imgSendMessage.setImageResource(R.drawable.ic_email_white_24dp);
                                Visibility.gone(holder.imgSendMessage);
                                Visibility.gone(holder.imgWallaPerson);
                                Visibility.visible(holder.txtMatchNearToYou);
                                holder.containerButtonsListPersons.setBackgroundColor(Color.parseColor("#00C300"));
                                holder.txtMatchNearToYou.setTypeface(TypesLetter.getAvenir(mainActivity), Typeface.ITALIC);
                                holder.txtMatchNearToYou.setText(""+mainActivity.getString(R.string.request_accepted));
                                break;
                            case Constant.REJECT_CHAT_DIRECT:
                                holder.imgSendMessage.setImageResource(R.drawable.ic_email_reject);
                                break;
                            default:
                                holder.imgSendMessage.setImageResource(R.drawable.ic_email_white_24dp);
                                break;
                        }
                    }
                }
                break;//Constant.LIST_PERSONS
            default:
                break;

        }
        String name = data.firstName;
        String url  = data.avatarUrl;

       /* if (genere.equals(Constant.WOMAN) || status.equals(Constant.GOLD))
            holder.name.setText(name + ", " + data.age);
        else
            holder.name.setText(name);*/

        holder.name.setText(genere.equals(Constant.WOMAN) || status.equals(Constant.GOLD)?name + ", " + data.age:name);

        setPersonalStatus(""+data.personalStatus,holder.personalStatus, ""+data.sexId);

        Visibility.visible(holder.progressBarLoadImage);
        Picasso.with(mainActivity)
                .load(url)
                .error(R.drawable.no_foto)
                .transform(new BlurTransformation(blurred))
                .into(holder.image, new Callback() {
                    @Override
                    public void onSuccess() {
                        Visibility.gone(holder.progressBarLoadImage);
                    }

                    @Override
                    public void onError() {
                        Visibility.gone(holder.progressBarLoadImage);
                    }
                });

        /*if(Common.getScreen(mainActivity,1) > 480)
        {
            RelativeLayout.LayoutParams paramsCard = new RelativeLayout.LayoutParams(320, RelativeLayout.LayoutParams.WRAP_CONTENT);
            holder.containerCard.setLayoutParams(paramsCard);
            RelativeLayout.LayoutParams paramsImage = new RelativeLayout.LayoutParams(320, 320);
            holder.containerImage.setLayoutParams(paramsImage);
        }*/


        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openProfileOtherUser(data,type);

            }
        });
        holder.containerDifused.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               openProfileOtherUser(data,type);

            }
        });
        holder.imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteRelationship(data.userId,position, holder.containerCard, "4");
            }
        });
        holder.imgWalla.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Match match     = new Match();
                match.username  = data.firstName;
                match.idUser    = data.userId;
                match.picture   = data.avatarUrl;

                sendLike(data, "1", match,position, holder.progressBarImgWalla);

            }
        });
        holder.btnMatch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialog = new Dialog(mainActivity,R.style.PauseDialog);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
                dialog.setCancelable(false);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.setContentView(R.layout.modal_general);

                TextView text = (TextView) dialog.findViewById(R.id.text);
                text.setText(mainActivity.getResources().getString(R.string.delete_match)+" " + data.firstName + "?");

                Button exit = (Button) dialog.findViewById(R.id.exit);
                Button accept = (Button) dialog.findViewById(R.id.accept);
                // if button is clicked, close the custom dialog
                exit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                accept.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        deleteRelationship(data.userId,position, holder.containerCard, "5");

                    }
                });

                dialog.show();
            }
        });
        holder.btnChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AppAnimation.scale(mainActivity,holder.btnChat);
                sendMessage(data, Constant.MATCH);
            }
        });
        holder.imgSendMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AppAnimation.scale(mainActivity,holder.imgSendMessage);

                if(data.isMatch == 0) {

                    if(data.requestToMe)
                    {
                        switch (data.chatStatusId) {
                            case Constant.NOT_REQUEST_CHAT:
                                sendMessage(data, Constant.DIRECT_CHAT);
                                break;
                            case Constant.REQUEST_CHAT_PENDING:
                                sendMessage(data, Constant.DIRECT_CHAT);
                                break;
                            case Constant.ACCEPT_CHAT_DIRECT:
                                sendMessage(data, Constant.DIRECT_CHAT);
                                break;
                            case Constant.REJECT_CHAT_DIRECT:
                                Common.errorMessage(mainActivity, "",(data.meRejected)? ""+mainActivity.getString(R.string.you_rejected_chat):""+mainActivity.getString(R.string.request_rejected));
                                break;
                            default:
                                break;
                        }
                    }
                    else {
                        switch (data.chatStatusId) {
                            case Constant.NOT_REQUEST_CHAT:
                                sendMessage(data, Constant.DIRECT_CHAT);
                                break;
                            case Constant.REQUEST_CHAT_PENDING:
                                String messageRequest = "";
                                messageRequest = genere.equals(Constant.MAN) ? "Tu mensaje fue enviado a " + data.firstName + ", debes esperar a que responda, para poder hablar con ella." : "Tu mensaje fue enviado a " + data.firstName + ", debes esperar a que responda, para poder hablar con él.";
                                Common.errorMessage(mainActivity, "", messageRequest);
                                break;
                            case Constant.ACCEPT_CHAT_DIRECT:
                                sendMessage(data, Constant.DIRECT_CHAT);
                                break;
                            case Constant.REJECT_CHAT_DIRECT:
                                Common.errorMessage(mainActivity, "",(data.meRejected)? ""+mainActivity.getString(R.string.you_rejected_chat):""+mainActivity.getString(R.string.request_rejected));
                                break;
                            default:
                                break;
                        }
                    }
                }
                else
                {
                    //es un usuario que tiene match
                    sendMessage(data, Constant.MATCH);
                }

            }
        });
        holder.txtMatchNearToYou.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //es un usuario que tiene solicitud de chat aceptada
                if(data.isMatch == 1)
                    sendMessage(data, Constant.MATCH);
                else
                    sendMessage(data, (data.requestToMe)?Constant.DIRECT_CHAT:Constant.ACCEPT_CHAT_DIRECT);
            }
        });
        holder.imgWallaPerson.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Toast.makeText(mainActivity, "Ten paciencia, estamos trabajando duro en esta funcionalidad...", Toast.LENGTH_LONG).show();

                if (data.likeToMe == 0) {
                    if(GlobalData.isKey(Constant.CURRENT_LIKES,mainActivity) && GlobalData.get(Constant.CURRENT_LIKES,mainActivity).equals("0"))
                        serviceGetWaitTime();
                    else {
                        AppAnimation.scale(mainActivity,holder.imgWallaPerson);
                        Visibility.visible(holder.imgLike);
                        fadeIn.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {
                                sendLike(data, "1", holder.imgLike, holder.imgWallaPerson, position);
                            }

                            @Override
                            public void onAnimationEnd(Animation animation) {
                                AppAnimation.fadeOut(holder.imgLike, 400);
                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) {

                            }
                        });
                        holder.imgLike.startAnimation(fadeIn);
                    }
                }

            }
        });

        return view;
    }

    /**
     * Método encargado de abrir chat con el usuario
     * @param data
     */
    public void sendMessage(MatchesData data, int type)
    {
        ChatActivity dialog = new ChatActivity(mainActivity, data,type, this);
        dialog.show(mainActivity.mFragmentManager, "ChatActivity.TAG");
    }

    /**
     * Muestra el perfil de un usuario teniendo en cuenta el tipo de cuenta del usuario
     * @param data
     * @param type
     */
    public void openProfileOtherUser(MatchesData data, int type)
    {
        DetailProfileFragment dialog = new DetailProfileFragment(mainActivity,data,type,this);
        switch (type)
        {
            case Constant.WALLAS:
                validateStatus(dialog, data);
                break;
            case Constant.MATCH:
                dialog.show(mainActivity.mFragmentManager, "DetailProfileFragment.TAG");
                break;
            case Constant.FAVORITE:
                dialog.show(mainActivity.mFragmentManager, "DetailProfileFragment.TAG");
                break;
            case Constant.LIST_PERSONS:
                dialog.show(mainActivity.mFragmentManager, "DetailProfileFragment.TAG");
                break;
            default:
                break;

        }
    }
    static class POIHolder
    {
        public ImageView image,imgWalla,imgDelete,img_online,imgWallaPerson,imgSendMessage,imgLike;
        public TextView  name, personalStatus,txtNumGallery;
        public ProgressBar progressBarImgDelete, progressBarImgWalla;
        public RelativeLayout containerCard,containerDifused,containerImage;
        public LinearLayout containerButtonsWallas,containerButtonsMach,containerButtonsListPersons,containerNumGallery,container_personal_status;
        public Button btnMatch, btnChat;
        public ProgressWheel progressBarLoadImage;
        public TextView txtMatchNearToYou;
    }

    /**
     * Método encargado de eliminar la relación walla y eliminarlo del listado
     * @param userId
     * @param position
     * @param container
     */
    public void deleteRelationship(final String userId,final int position, final RelativeLayout container, String status)
    {
        final ProgressDialog progressDialog = new ProgressDialog(mainActivity);
        progressDialog.setMessage(""+mainActivity.getResources().getString(R.string.delete_relation));
        progressDialog.setCancelable(false);
        progressDialog.show();
        OkHttpClient client = new OkHttpClient();
        RequestBody formBody = new FormBody.Builder()
                .add("sessionToken", "" + GlobalData.get(Constant.SESSION_TOKEN, mainActivity))
                .add("serviceId", status )
                .add("otherUserId",userId)
                .build();

        Request request = new Request.Builder()
                .url(Services.DELETE_RELATIONSHIP)
                .post(formBody)
                .build();

        client.newCall(request).enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                progressDialog.dismiss();
                Looper.prepare();
                mainActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Common.errorMessage(mainActivity, "", mainActivity.getResources().getString(R.string.check_internet));
                    }
                });

                Looper.loop();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                progressDialog.dismiss();
                final String result = response.body().string();
                //  Log.d("app2you", "respuesta eliminar wallas: " + result);

                Looper.prepare();


                mainActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        JSONObject json = null;
                        try {
                            json = new JSONObject(result);

                            String success = json.getString("success");
                            if (success.equals("true")) {
                                GlobalData.delete(Constant.REQUEST_MATCH,mainActivity);
                                GlobalData.delete(Constant.REQUEST_WANT_TO_ME,mainActivity);
                                scale.setAnimationListener(new Animation.AnimationListener() {
                                    @Override
                                    public void onAnimationStart(Animation animation) {

                                    }

                                    @Override
                                    public void onAnimationEnd(Animation animation) {

                                        machedataList.remove(position);
                                        notifyDataSetChanged();
                                        if (machedataList != null && machedataList.size() == 0) {
                                            //aparcer imagen
                                            Visibility.gone(mainActivity.gridview);
                                            Visibility.visible(mainActivity.containerMatchEmpty);
                                            AppAnimation.fadeIn(mainActivity.containerMatchEmpty,100);
                                        }
                                    }

                                    @Override
                                    public void onAnimationRepeat(Animation animation) {

                                    }
                                });
                                container.startAnimation(scale);
                            } else {
                                Common.errorMessage(mainActivity, "", mainActivity.getResources().getString(R.string.check_internet));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

                Looper.loop();
            }
        });
    }

    /**
     * Método para validar el estado del usuario
     * @param dialog
     * @param data
     */
    public void validateStatus(DetailProfileFragment dialog, MatchesData data)
    {

        if (status.equals(Constant.GOLD) || genere.equals(Constant.WOMAN))
            dialog.show(mainActivity.mFragmentManager, "DetailProfileFragment.TAG");
        else
        {
            if(GlobalData.isKey(Constant.VIEW_LANDING_WANT_TO_KNOW,mainActivity)) {
                DialogStore dialogStore = new DialogStore(mainActivity, 1);
                dialogStore.show(mainActivity.mFragmentManager, "DialogStore.TAG");
            }
            else
            {
                GlobalData.set(Constant.VIEW_LANDING_WANT_TO_KNOW,"Y",mainActivity);
                DialogFragmentWantToKnow dialogFragmentWantToKnow = new DialogFragmentWantToKnow(mainActivity);
                dialogFragmentWantToKnow.show(mainActivity.mFragmentManager,"DialogFragmentWantToKnow.TAG");
            }
        }

    }

    /**
     * Clase encargada de hacer el efecto blur sobre una imagen usando Picasso
     */
    public class BlurTransformation implements com.squareup.picasso.Transformation {
        private final int radius;

        // radius is corner radii in dp
        public BlurTransformation(final int radius) {
            this.radius = radius;
        }

        @Override
        public Bitmap transform(final Bitmap bitmap) {

            if (radius > 0) {
                Bitmap outBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
                RenderScript rs = RenderScript.create(mainActivity);
                ScriptIntrinsicBlur blurScript = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs));
                Allocation allIn = Allocation.createFromBitmap(rs, bitmap,Allocation.MipmapControl.MIPMAP_NONE, Allocation.USAGE_SCRIPT);
                Allocation allOut = Allocation.createFromBitmap(rs, outBitmap);
                blurScript.setRadius(radius);
                blurScript.setInput(allIn);
                blurScript.forEach(allOut);
                allOut.copyTo(outBitmap);
                bitmap.recycle();
                rs.destroy();

                return outBitmap;
            }else
                return  bitmap;
        }

        @Override
        public String key() {
            return "blur"+Float.toString(radius);
        }
    }

    /**
     * Método encargado de cambiar estado personal del usuario
     * @param status
     */
    public void setPersonalStatus(String status, TextView personalStatus, String sexId)
    {
        int number = 0;
        number = Integer.parseInt(status);

        switch (number) {
            case Constant.CHATEAR_UN_RATO:
                personalStatus.setText(mainActivity.getResources().getString(R.string.chat));
                break;
            case Constant.INVITARME_A_SALIR:
                personalStatus.setText((sexId.equals(Constant.MAN))?mainActivity.getResources().getString(R.string.invite_to_exit):mainActivity.getResources().getString(R.string.invite_to_dinner));
                break;
            case Constant.SALTARME_LOS_PRELIMINARES:
                personalStatus.setText(mainActivity.getResources().getString(R.string.skip_preliminaries));
                break;
            default:
                personalStatus.setText((sexId.equals(Constant.MAN))?mainActivity.getResources().getString(R.string.invite_to_exit):mainActivity.getResources().getString(R.string.invite_to_dinner));
                break;
        }
    }

    /**
     * Método encargado de hacer like a un usuario de la lista
     * @param data
     * @param likeType
     * @param match
     * @param position
     * @param progressBarImgWalla
     */
    public void sendLike(final MatchesData data, final String likeType, final Match match, final int position, final ProgressBar progressBarImgWalla ) {

        Visibility.visible(progressBarImgWalla);
        OkHttpClient client = new OkHttpClient();
        RequestBody formBody = new FormBody.Builder()
                .add("sessionToken", "" + GlobalData.get(Constant.SESSION_TOKEN, mainActivity))
                .add("likeToId", data.userId)
                .add("likeType", likeType)
                .build();

        Request request = new Request.Builder()
                .url(Services.LIKES_SERVICE)
                .post(formBody)
                .build();

        client.newCall(request).enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

                Looper.prepare();
                mainActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Visibility.gone(progressBarImgWalla);
                        Common.errorMessage(mainActivity,"",mainActivity.getResources().getString(R.string.check_internet));
                    }
                });

                Looper.loop();
            }

            @Override
            public void onResponse(Call call,final Response response) throws IOException {

                final String result = response.body().string();
                //    Log.d("app2you", "respuesta: " + result + " id: " + data.userId);

                Looper.prepare();



                mainActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Visibility.gone(progressBarImgWalla);
                            Gson gson = new Gson();
                            final UserLikes likes = gson.fromJson(result, UserLikes.class);

                            if (likes.success) {
                                MatchFromFlechazosDialogFragment dialog = new MatchFromFlechazosDialogFragment(mainActivity, match);
                                dialog.show(mainActivity.mFragmentManager, "CapturePhotoDialogFragment.TAG");

                                if (machedataList != null && machedataList.size() > 0) {
                                    machedataList.remove(position);
                                    notifyDataSetChanged();
                                }
                                if (machedataList != null && machedataList.size() == 0) {
                                    //aparcer imagen
                                    Visibility.gone(mainActivity.gridview);
                                    Visibility.visible(mainActivity.containerMatchEmpty);
                                    AppAnimation.fadeIn(mainActivity.containerMatchEmpty, 100);
                                }

                            } else {
                                Common.errorMessage(mainActivity, "", mainActivity.getResources().getString(R.string.check_internet));
                            }
                        }
                        catch (Exception e){
                            e.printStackTrace();
                        }
                        finally {
                            response.body().close();
                        }

                    }
                });

                Looper.loop();
            }
        });
    }
    /**
     * Método hacer like con okHttp
     */
    public void sendLike(final MatchesData data, final String likeType, final ImageView imgLike,final ImageView imgWallaPerson,final int position)
    {
        OkHttpClient client = new OkHttpClient();
        RequestBody formBody = new FormBody.Builder()
                .add("sessionToken", "" + GlobalData.get(Constant.SESSION_TOKEN, mainActivity))
                .add("likeToId", data.userId)
                .add("likeType",likeType )
                .build();

        Request request = new Request.Builder()
                .url(Services.LIKES_SERVICE)
                .post(formBody)
                .build();

        client.newCall(request).enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

                Looper.prepare();
                mainActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Common.errorMessage(mainActivity,"",mainActivity.getResources().getString(R.string.check_internet));
                    }
                });
                Looper.loop();
            }

            @Override
            public void onResponse(Call call,final Response response) throws IOException {

                final String result = response.body().string();
                Log.d("app2you", "respuesta: " + result + " id: " + data.userId);

                Looper.prepare();

                mainActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {

                            Gson gson = new Gson();
                            final UserLikes likes = gson.fromJson(result, UserLikes.class);
                            if (likes.success) {
                                data.likeToMe = 1;
                                imgWallaPerson.setImageResource(R.drawable.icono_like_si2);

                                if (likes.currentLikes.equals("0")) {

                                    GlobalData.set(Constant.CURRENT_LIKES, "" + likes.currentLikes, mainActivity);
                                    String status = GlobalData.get(Constant.USER_STATUS, mainActivity);

                                    if (status.equals(Constant.NOT_GOLD_BEFORE_TRIAL_PICTURE_PENDING)) {
                                        serviceGetWaitTime();
                                        //Common.errorMessage(mainActivity, "", mainActivity.getResources().getString(R.string.limit_wallas_picture_pending));
                                    }
                                    else {
                                        if (status.equals(Constant.NOT_GOLD_BEFORE_TRIAL_NO_PICTURE)) {
                                            final NiftyDialogBuilder dialog =  NiftyDialogBuilder.getInstance(mainActivity);
                                            dialog.withTitle(null);
                                            dialog.withMessage(null);
                                            dialog.isCancelableOnTouchOutside(false);
                                            dialog.withDialogColor(Color.TRANSPARENT);
                                            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                            dialog.withDuration(500);
                                            dialog.withEffect(Effectstype.Shake);
                                            dialog.setCustomView(R.layout.modal_general,mainActivity);

                                            TextView title = (TextView) dialog.findViewById(R.id.title);
                                            TextView text = (TextView) dialog.findViewById(R.id.text);
                                            title.setText(Common.getNickName(mainActivity));
                                            text.setText(mainActivity.getResources().getString(R.string.limit_likes_no_picture));

                                            Button exit = (Button) dialog.findViewById(R.id.exit);
                                            exit.setText(mainActivity.getResources().getString(R.string.exit));
                                            Button accept = (Button) dialog.findViewById(R.id.accept);
                                            // if button is clicked, close the custom dialog
                                            accept.setText(mainActivity.getResources().getString(R.string.update_photo));
                                            exit.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    dialog.dismiss();
                                                }
                                            });
                                            accept.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    dialog.dismiss();
                                                    mainActivity.replaceFragment(new FragmentProfile(mainActivity));


                                                }
                                            });

                                            dialog.show();
                                        } else {
                                            serviceGetWaitTime();
                                        }
                                    }

                                } else {
                                    GlobalData.set(Constant.CURRENT_LIKES, "" + likes.currentLikes, mainActivity);
                                }
                                // Log.d("loovers", "cantidad de matches: " + likes.newMatch.size());
                                if (likes.newMatch != null && likes.newMatch.size() > 0) {

                                    //machedataList.remove(position);//revisar
                                    //notifyDataSetChanged();
                                    if(!GlobalData.isKey(Constant.FIRST_MATCH,mainActivity))
                                        GlobalData.set(Constant.FIRST_MATCH,"Y",mainActivity);

                                    MatchFromFlechazosDialogFragment dialog = new MatchFromFlechazosDialogFragment(mainActivity, likes.newMatch.get(0));
                                    // dialog.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
                                    dialog.show(mainActivity.mFragmentManager, "CapturePhotoDialogFragment.TAG");
                                }



                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();
                            Toast.makeText(mainActivity, R.string.error_reponse_server, Toast.LENGTH_SHORT).show();

                        }
                        finally {
                            response.body().close();
                        }
                    }
                });

                Looper.loop();
            }
        });
    }

    /**
     * Metodo encargado de obtener el tiempo de espera en mili segundos para recargar wallas
     */
    public void serviceGetWaitTime()
    {
        OkHttpClient client = new OkHttpClient();

        RequestBody formBody = new FormBody.Builder()
                .add("sessionToken", "" + GlobalData.get(Constant.SESSION_TOKEN, mainActivity))
                .build();

        Request request = new Request.Builder()
                .url(Services.GET_WAIT_TIME)
                .post(formBody)
                .build();

        client.newCall(request).enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

                Looper.prepare();
                //Toast.makeText(mainActivity, "Error favorito", Toast.LENGTH_SHORT).show();
                mainActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Common.errorMessage(mainActivity,"",mainActivity.getResources().getString(R.string.check_internet));
                    }
                });
                Looper.loop();
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {

                final String result = response.body().string();
                Log.d("app2you", "respuesta  getTime: " + result);

                Looper.prepare();

                mainActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //apago el dialogo
                        try {
                            JSONObject json = new JSONObject(result);
                            String success = json.getString("success");
                            if (success.equals("true")) {
                                int timeMillis = json.getInt("time");
                                openDialogReloadWallas(timeMillis);
                            }
                            else {

                                Common.errorMessage(mainActivity,"",mainActivity.getResources().getString(R.string.check_internet));
                            }


                        } catch (JSONException ex) {
                            ex.printStackTrace();
                            Toast.makeText(mainActivity,R.string.error_reponse_server, Toast.LENGTH_SHORT).show();
                        }
                        finally {
                            response.body().close();
                        }
                    }
                });


                Looper.loop();
            }
        });
    }
    /**
     * Metod encargado de abrir dialogo para visualizar tiempo para recargar wallas y opciones de compra
     */
    public void openDialogReloadWallas(int millis)
    {
        final Dialog dialog = new Dialog(mainActivity, R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.reload_likes);

        TextView txtWallasExhausted     = (TextView)dialog.findViewById(R.id.txtWallasExhausted);
        TextView txtTime                = (TextView) dialog .findViewById(R.id.txtDate);
        TextView txtPassToGold          = (TextView)dialog.findViewById(R.id.txtPassToGold);
        Button btnWait                  = (Button)dialog.findViewById(R.id.btnWait);
        Button btnPassToGold            = (Button)dialog.findViewById(R.id.btnPassToGold);
        Button btnReloadWallas          = (Button)dialog.findViewById(R.id.btnReloadWallas);
        //String status = GlobalData.get(Constant.USER_STATUS, mainActivity);
        //String genere = GlobalData.get(Constant.GENERE,mainActivity);
        // btnPassToGold.setVisibility(status.equals(Constant.NOT_GOLD_AFTER_TRIAL) && genere.equals(Constant.MAN)?View.VISIBLE:View.GONE);

        // if(status.equals(Constant.NOT_GOLD_AFTER_TRIAL)  && genere.equals(Constant.MAN))
        txtPassToGold.setText(""+mainActivity.getResources().getString(R.string.txt_reload_likes_after_trial));
        /*else
            txtPassToGold.setText(""+mainActivity.getResources().getString(R.string.txt_reload_wallas));*/

        txtTime.setTypeface(TypesLetter.getSansBold(mainActivity));
        txtPassToGold.setTypeface(TypesLetter.getSansRegular(mainActivity));
        txtWallasExhausted.setTypeface(TypesLetter.getSansRegular(mainActivity));

        String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis),
                TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
        txtTime.setText(""+hms);

        final CounterClass timer = new CounterClass(millis,1000,txtTime, dialog);
        timer.start();

        btnWait.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                timer.cancel();
            }
        });
        btnPassToGold.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                timer.cancel();
                DialogStore dialogStore = new DialogStore(mainActivity,1);
                dialogStore.show(mainActivity.mFragmentManager,"DialogStore.TAG");
            }
        });
        btnReloadWallas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                timer.cancel();
                DialogStore dialogStore = new DialogStore(mainActivity,0);
                dialogStore.show(mainActivity.mFragmentManager,"DialogStore.TAG");

            }
        });

        dialog.show();
    }

    /**
     * Clase encargada de llevar el contador de forma regresiva
     */
    public class CounterClass extends CountDownTimer
    {
        public TextView txtTime;
        public Dialog dialog;
        public CounterClass(long millisInFuture, long countDownInterval,TextView txtTime,Dialog dialog)
        {
            super(millisInFuture, countDownInterval);
            this.txtTime = txtTime;
            this.dialog  = dialog;
        }
        @Override public void onFinish()
        {
            txtTime.setText("00:00:00");

            getCurrentLikes(dialog);
        }
        @Override public void onTick(long millisUntilFinished)
        {
            long millis = millisUntilFinished;
            String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis),
                    TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                    TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
            //System.out.println(hms);
            txtTime.setText(hms);
        }
    }
    /**
     * Metodo encargado de recargar likes cuando se termina el tiempo de espera
     */
    public void getCurrentLikes(final Dialog dialog)
    {
        OkHttpClient client = new OkHttpClient();

        RequestBody formBody = new FormBody.Builder()
                .add("sessionToken", "" + GlobalData.get(Constant.SESSION_TOKEN, mainActivity))
                .build();

        Request request = new Request.Builder()
                .url(Services.GET_CURRENT_LIKES)
                .post(formBody)
                .build();

        client.newCall(request).enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                dialog.dismiss();
                Looper.prepare();
                //Toast.makeText(mainActivity, "Error favorito", Toast.LENGTH_SHORT).show();
                mainActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Common.errorMessage(mainActivity,"",mainActivity.getResources().getString(R.string.check_internet));
                    }
                });
                Looper.loop();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                dialog.dismiss();
                final String result = response.body().string();
                //  Log.d("app2you", "respuesta  getCurrentLikes: " + result);

                Looper.prepare();

                mainActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //apago el dialogo
                        try {
                            JSONObject json = new JSONObject(result);
                            String success = json.getString("success");
                            if (success.equals("true")) {
                                String currentLikes = json.getString("currentLikes");
                                GlobalData.set(Constant.CURRENT_LIKES, "" + currentLikes, mainActivity);
                                Log.d("app2you","wallas disponibles: " +currentLikes);
                                String numWallas = currentLikes;
                                if (numWallas.equals("0")) {


                                } else {

                                }
                            }
                            else {

                                Common.errorMessage(mainActivity,"",mainActivity.getResources().getString(R.string.check_internet));
                            }


                        } catch (JSONException ex) {
                            ex.printStackTrace();
                            Toast.makeText(mainActivity,R.string.error_reponse_server, Toast.LENGTH_SHORT).show();
                        }
                    }
                });


                Looper.loop();
            }
        });
    }


}