package com.android.slidingmenu.objects;

import com.appdupe.flamer.pojo.MatchesData;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Juan Martin Bernal on 31/12/15.
 */
public class UserData implements Serializable {

    @SerializedName("success")
    public Boolean  success;

    @SerializedName("results")
    public ArrayList<MatchesData> results;
}
