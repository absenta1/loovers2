package com.android.slidingmenu.objects;

/**
 * Created by Juan Martin Bernal on 29/4/16.
 */
public class Store {

    public int featureId;
    public String urlImage;
    public int image;
    public String text;

    public Store(int featureId,String urlImage, int image,String text) {
        this.featureId  = featureId;
        this.urlImage   = urlImage;
        this.text       = text;
        this.image      = image;
    }
    public Store(int image,String text) {

        this.text       = text;
        this.image      = image;
    }
}
