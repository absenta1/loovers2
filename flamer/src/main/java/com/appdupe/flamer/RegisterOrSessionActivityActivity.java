package com.appdupe.flamer;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.SurfaceTexture;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Base64;
import android.util.Log;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.Window;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.BounceInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.slidingmenu.MainActivity;
import com.android.slidingmenu.activities.GenereActivity;
import com.android.slidingmenu.activities.LoginActivity;
import com.android.slidingmenu.util.Alarm;
import com.android.slidingmenu.util.Common;
import com.android.slidingmenu.util.GlobalData;
import com.android.slidingmenu.util.TypesLetter;
import com.android.slidingmenu.util.Visibility;
import com.android.slidingmenu.webservices.Services;
import com.appdupe.flamer.pojo.CamaraData;
import com.appdupe.flamer.pojo.MyProfileData;
import com.appdupe.flamer.utility.Constant;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.gson.Gson;
import com.loovers.app.R;
import com.wang.avi.AVLoadingIndicatorView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class RegisterOrSessionActivityActivity extends FragmentActivity implements TextureView.SurfaceTextureListener{
	//private static boolean mDebugLog = true;
	//private static String mDebugTag = "RegisterOrSessionActivityActivity";
    private static final String TAG = RegisterOrSessionActivityActivity.class.getName();
    public static final String PROPERTY_REG_ID = "registration_id";
    private static final String PROPERTY_APP_VERSION = "appVersion";
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final String PROPERTY_EXPIRATION_TIME = "onServerExpirationTimeMs";
    private static final String PROPERTY_USER = "user";
    public static final long EXPIRATION_TIME_MS = 1000 * 3600 * 24 * 7;
    //private String SENDER_ID = "705758513230";//WALLAMATCH JUAN MARTIN id de proyecto - login
    private String SENDER_ID = "246265097728";//xtreamsenses
    //246265097728
    public static final int SIZE_IMAGE  = 640;//1280
    // Asset video file name.
    private static final String FILE_NAME = "test1.mp4";

    // MediaPlayer instance to control playback of video file.
    private MediaPlayer mMediaPlayer;
    //ViewPageAdapter pageAdapter;
    private Button mButtonSession;
    private Button mButtonRegister;
    private float mVideoWidth;
    private float mVideoHeight;
    //private Ultilities mUltilities = new Ultilities(RegisterOrSessionActivityActivity.this);
    //private android.app.ProgressDialog mdialog;
    private ViewPager pager;
    private Timer timer;
    private TextureView textureView;
    private int contador = -1;
    public ImageView background;
    public ImageView logo_wallamatch;
    public TextView txt_wallamatch;
    public CallbackManager callbackManager;
    public LoginButton loginButton;
    public GoogleCloudMessaging gcm;
    public String emailFacebook = "", idFacebook = "", nameFacebook = "", birthdayFacebook= "";
    private String regid;
    public String mCity;
    public String mCountry;
    public ProgressDialog progressDialog = null;
    public AVLoadingIndicatorView progressBarLogin;
    public String imageUrlFacebook;

    @Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

        //getKeyReleaseFacebook();
        if(!GlobalData.isKey(Constant.SESSION_TOKEN,RegisterOrSessionActivityActivity.this)) {
            if (!GlobalData.isKey(Constant.SERVER_PING_REGISTER, RegisterOrSessionActivityActivity.this)) {
                String ip = "false";
                int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                if (currentapiVersion >= Build.VERSION_CODES.M) {
                    sendPingSever(ip);
                } else {
                    ip = (Common.getLocalIpAddress(RegisterOrSessionActivityActivity.this).length() > 0) ? Common.getLocalIpAddress(RegisterOrSessionActivityActivity.this) : "false";
                    sendPingSever(ip);
                }

            }
        }

        FacebookSdk.sdkInitialize(this.getApplicationContext());

        callbackManager = CallbackManager.Factory.create();
        //LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email", "user_friends"));
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        // App code
                        progressDialog = new ProgressDialog(RegisterOrSessionActivityActivity.this,R.style.CustomDialogTheme);
                        progressDialog.setCancelable(false);
                        progressDialog.show();
                        progressDialog.setContentView(R.layout.custom_progress_dialog);
                        TextView txtView = (TextView) progressDialog.findViewById(R.id.txtProgressDialog);
                        txtView.setText(""+getResources().getString(R.string.register_user));

                        //Log.d("app2you", "token: " + loginResult.getAccessToken().getToken());
                        //AccessToken accessToken = loginResult.getAccessToken();
                        sendDataClickFacebook("6");
                        final GraphRequest request = new GraphRequest(loginResult.getAccessToken(), "me");
                        Bundle params = new Bundle();
                        params.putString("fields", "id,name,email,gender,age_range");
                        request.setParameters(params);
                        AsyncTask.execute(new Runnable() {
                            public void run() {
                                Log.d(TAG, "LoginCallback.onSuccess() lookup email");
                                GraphResponse response = request.executeAndWait();
                                JSONObject jsonResp = response.getJSONObject();
                                Log.d("app2you", "data :" + jsonResp.toString());
                                try {
                                    emailFacebook = jsonResp.getString("email");
                                    nameFacebook = jsonResp.getString("name");
                                    idFacebook = jsonResp.getString("id"); //url image facebook http://graph.facebook.com/10156394864230024/picture?type=large
                                    String tmpName[] = nameFacebook.split(" ");
                                    if(tmpName.length > 0)
                                        nameFacebook = tmpName[0];

                                    String gender = jsonResp.getString("gender");//male or female
                                    String birthday = "";//(jsonResp.has("birthday"))?jsonResp.getString("birthday"):"1989/07/08";
                                    if (gender.equalsIgnoreCase("male")|| gender.equalsIgnoreCase("hombre"))
                                        GlobalData.set(Constant.GENERE, "" + Constant.MAN, RegisterOrSessionActivityActivity.this);
                                    else
                                        GlobalData.set(Constant.GENERE, "" + Constant.WOMAN, RegisterOrSessionActivityActivity.this);

                                    imageUrlFacebook ="https://graph.facebook.com/"+ idFacebook +"/picture?type=large&redirect=false&width=640&height=640"; //"https://graph.facebook.com/" + idFacebook + "/picture?type=large";


                                    gcm = GoogleCloudMessaging.getInstance(RegisterOrSessionActivityActivity.this);

                                    //Obtenemos el Registration ID guardado
                                    regid = getRegistrationId(RegisterOrSessionActivityActivity.this);

                                    //Si no disponemos de Registration ID comenzamos el registro

                                    new DownloadImageFacebook().execute(imageUrlFacebook);

                                    //GlobalData.set(Constant.URL_AVATAR_FACEBOOK, "" + imageUrlFacebook, RegisterOrSessionActivityActivity.this);


                                   /* if(birthday.equals(""))
                                    {
                                        gcm = GoogleCloudMessaging.getInstance(RegisterOrSessionActivityActivity.this);

                                        //Obtenemos el Registration ID guardado
                                        regid = getRegistrationId(RegisterOrSessionActivityActivity.this);

                                        //Si no disponemos de Registration ID comenzamos el registro

                                        new TareaRegistroGCM(emailFacebook, idFacebook, nameFacebook, birthdayFacebook, imageUrlFacebook, progressDialog).execute();
                                    }
                                    else {

                                        SimpleDateFormat fromUser = new SimpleDateFormat("MM/dd/yyyy");
                                        SimpleDateFormat myFormat = new SimpleDateFormat("yyyy/MM/dd");

                                        try {
                                            //final Calendar myCalendar = Calendar.getInstance();
                                            birthdayFacebook = myFormat.format(fromUser.parse(birthday));
                                            Log.d("app2you", "data :" + jsonResp.toString() + " new date : " + birthdayFacebook);
                                            String[] dateUser = birthdayFacebook.split("/");
                                            final Calendar userAge = new GregorianCalendar(Integer.parseInt(dateUser[0]), Integer.parseInt(dateUser[1]), Integer.parseInt(dateUser[2]));
                                            Calendar minAdultAge = new GregorianCalendar();
                                            minAdultAge.add(Calendar.YEAR, -18);
                                            if (minAdultAge.before(userAge)) {
                                                Looper.prepare();
                                                runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        //Toast.makeText(LoginUsingFacebookMailOrGooglev2.this, "Debes ser mayor de 18 años", Toast.LENGTH_SHORT).show();
                                                        Common.errorMessage(RegisterOrSessionActivityActivity.this, "", "" + getResources().getString(R.string.higher_age));
                                                    }
                                                });
                                                Looper.loop();

                                            } else {

                                                gcm = GoogleCloudMessaging.getInstance(RegisterOrSessionActivityActivity.this);

                                                //Obtenemos el Registration ID guardado
                                                regid = getRegistrationId(RegisterOrSessionActivityActivity.this);

                                                //Si no disponemos de Registration ID comenzamos el registro

                                               new DownloadImageFacebook().execute(imageUrlFacebook);
                                                //Toast.makeText(LoginUsingFacebookMailOrGooglev2.this, "ejecutar registro tienes " + userAge.YEAR, Toast.LENGTH_SHORT).show();

                                            }

                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }
                                    }*/


                                } catch (final Exception ex) {
                                    Log.e(TAG, "LoginCallback.onSuccess(): exception caught while getting identity", ex);
                                    runOnUiThread(new Runnable() {
                                        public void run() {
                                            Toast.makeText(RegisterOrSessionActivityActivity.this, "Exception caught while getting identity: " +
                                                    ex.getMessage(), Toast.LENGTH_LONG).show();

                                        }
                                    });
                                }
                            }
                        });

                    }

                    @Override
                    public void onCancel() {
                        // App code
                        if(progressDialog!=null && progressDialog.isShowing() )
                            progressDialog.dismiss();
                        Toast.makeText(RegisterOrSessionActivityActivity.this, "Cancelo Facebook", Toast.LENGTH_SHORT).show();

                        sendDataClickFacebook("6");

                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                        Log.d("app2you","facebok: " + exception.getMessage());
                        if(progressDialog!=null && progressDialog.isShowing() )
                            progressDialog.dismiss();
                        Toast.makeText(RegisterOrSessionActivityActivity.this, R.string.try_again, Toast.LENGTH_SHORT).show();
                        sendDataClickFacebook("6");

                    }
                });

        if(GlobalData.isKey(Constant.LOGIN_APP, getApplicationContext()))
        {
            Intent i = new Intent(RegisterOrSessionActivityActivity.this, MainActivity.class);
            startActivity(i);
            overridePendingTransition(R.anim.fade_in_slow, R.anim.fade_out);
            finish();
        }
        else
        {
            this.requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(R.layout.register_or_session);
            background                      = (ImageView)findViewById(R.id.background);
            loginButton                     = (LoginButton) findViewById(R.id.login_button);
            TextView txtPrivacityFacebook   = (TextView)findViewById(R.id.txtPrivacityFacebook);
            txtPrivacityFacebook.setTypeface(TypesLetter.getSansRegular(RegisterOrSessionActivityActivity.this));
            loginButton.setReadPermissions(Arrays.asList("public_profile", "email" ));
            initView();
        }


	}

    /**
     * Método para cambiar de slide del paginador
     */
    private void moveToNext(){
        if (pager!=null) {

            pager.setCurrentItem(contador + 1);
            if(pager.getCurrentItem() == 2) {
                contador = -1;
            }
            else
                contador++;
        }

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private static int getAppVersion(Context context)
    {
        try
        {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);

            return packageInfo.versionCode;
        }
        catch (PackageManager.NameNotFoundException e)
        {
            throw new RuntimeException("Error al obtener versión: " + e);
        }
    }




    private String getRegistrationId(Context context)
    {
        SharedPreferences prefs = getSharedPreferences(
                MainActivity.class.getSimpleName(),
                Context.MODE_PRIVATE);

        String registrationId = prefs.getString(PROPERTY_REG_ID, "");

        if (registrationId.length() == 0)
        {
            Log.d(TAG, "Registro GCM no encontrado.");
            return "";
        }

        String registeredUser =
                prefs.getString(PROPERTY_USER, "user");

        int registeredVersion =
                prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);

        long expirationTime =
                prefs.getLong(PROPERTY_EXPIRATION_TIME, -1);

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.getDefault());
        String expirationDate = sdf.format(new Date(expirationTime));

        Log.d(TAG, "Registro GCM encontrado (usuario=" + registeredUser +
                ", version=" + registeredVersion +
                ", expira=" + expirationDate + ")");

        int currentVersion = getAppVersion(context);

        if (registeredVersion != currentVersion)
        {
            Log.d(TAG, "Nueva versión de la aplicación.");
            return "";
        }
        else if (System.currentTimeMillis() > expirationTime)
        {
            Log.d(TAG, "Registro GCM expirado.");
            return "";
        }
        return registrationId;
    }
    private SharedPreferences getGCMPreferences(Context context) {
        // This sample app persists the registration ID in shared preferences,
        // but
        // how you store the regID in your app is up to you.
        return getSharedPreferences(LoginUsingFacebookMailOrGooglev2.class.getSimpleName(),
                Context.MODE_PRIVATE);
    }
    private void calculateVideoSize() {
        try {
            AssetFileDescriptor afd = getAssets().openFd(FILE_NAME);
            MediaMetadataRetriever metaRetriever = new MediaMetadataRetriever();
            metaRetriever.setDataSource(
                    afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
            String height = metaRetriever
                    .extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_HEIGHT);
            String width = metaRetriever
                    .extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_WIDTH);
            mVideoHeight = Float.parseFloat("240");
            mVideoWidth = Float.parseFloat("320");

        } catch (IOException e) {
            Log.d(TAG, e.getMessage());
        } catch (NumberFormatException e) {
            Log.d(TAG, e.getMessage());
        }
    }
    private void updateTextureViewSize(int viewWidth, int viewHeight) {
        float scaleX = 1.0f;
        float scaleY = 1.0f;

        if (mVideoWidth > viewWidth && mVideoHeight > viewHeight) {
            scaleX = mVideoWidth / viewWidth;
            scaleY = mVideoHeight / viewHeight;
        } else if (mVideoWidth < viewWidth && mVideoHeight < viewHeight) {
            scaleY = viewWidth / mVideoWidth;
            scaleX = viewHeight / mVideoHeight;
        } else if (viewWidth > mVideoWidth) {
            scaleY = (viewWidth / mVideoWidth) / (viewHeight / mVideoHeight);
        } else if (viewHeight > mVideoHeight) {
            scaleX = (viewHeight / mVideoHeight) / (viewWidth / mVideoWidth);
        }

        // Calculate pivot points, in our case crop from center
        int pivotPointX = viewWidth / 2;
        int pivotPointY = viewHeight / 2;

        Matrix matrix = new Matrix();
        matrix.setScale(scaleX, scaleY, pivotPointX, pivotPointY);

        textureView.setTransform(matrix);
        textureView.setLayoutParams(new RelativeLayout.LayoutParams(viewWidth, viewHeight));
    }
    private List<Fragment> getFragments(){
        List<Fragment> fList = new ArrayList<Fragment>();

        //fList.add(MyFragment.newInstance(getResources().getString(R.string.lh_frase1)));
        //fList.add(MyFragment.newInstance(getResources().getString(R.string.lh_frase2)));
        //fList.add(MyFragment.newInstance(getResources().getString(R.string.lh_frase3)));

        return fList;
    }

    /**
     * Inicialización de los componentes de la pantalla
     */
    private void initView() {

        textureView         = (TextureView) findViewById(R.id.textureView);
        logo_wallamatch     = (ImageView)findViewById(R.id.logo_wallamatch);
        txt_wallamatch      = (TextView) findViewById(R.id.txt_wallamatch);
        progressBarLogin    = (AVLoadingIndicatorView)findViewById(R.id.progressBarLogin);
        pager               = (ViewPager)findViewById(R.id.viewpager);
        mButtonSession      = (Button)findViewById(R.id.registeroorsession_session);
        mButtonRegister     = (Button)findViewById(R.id.registeroorsession_register);

        animationLogo(logo_wallamatch);
        animationText(txt_wallamatch);
        //textureView.setSurfaceTextureListener(this);
        //List<Fragment> fragments = getFragments();

        //pageAdapter = new ViewPageAdapter(getSupportFragmentManager(), fragments);


        //pager.setAdapter(pageAdapter);
        //CirclePageIndicator titleIndicator = (CirclePageIndicator)findViewById(R.id.titles);
       // titleIndicator.setViewPager(pager);

        mButtonSession.setTransformationMethod(null);
        mButtonRegister.setTransformationMethod(null);


        mButtonSession.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntent=new Intent(RegisterOrSessionActivityActivity.this,
                        LoginActivity.class);
                startActivity(mIntent);
                overridePendingTransition(R.anim.fade_in_slow, R.anim.fade_out);
                finish();


            }
        });

        mButtonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntent=new Intent(RegisterOrSessionActivityActivity.this,
                        GenereActivity.class);
                startActivity(mIntent);
                overridePendingTransition(R.anim.fade_in_slow, R.anim.fade_out);
                finish();

            }
        });
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mMediaPlayer != null) {
            mMediaPlayer.stop();
            mMediaPlayer.release();
            mMediaPlayer = null;
        }

    }

    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i, int i2) {
        Surface surface = new Surface(surfaceTexture);

        try {
            AssetFileDescriptor afd = getAssets().openFd(FILE_NAME);
            mMediaPlayer = new MediaPlayer();
            mMediaPlayer.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
            mMediaPlayer.setSurface(surface);
            mMediaPlayer.setLooping(true);
            mMediaPlayer.prepareAsync();
            calculateVideoSize();
            updateTextureViewSize((int)textureView.getWidth(),(int)textureView.getHeight());
            // Play video when the media source is ready for playback.
            mMediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mediaPlayer) {
                    mediaPlayer.start();
                }
            });

        } catch (IllegalArgumentException e) {
            Log.d(TAG, e.getMessage());
        } catch (SecurityException e) {
            Log.d(TAG, e.getMessage());
        } catch (IllegalStateException e) {
            Log.d(TAG, e.getMessage());
        } catch (IOException e) {
            Log.d(TAG, e.getMessage());
        }
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i, int i2) {
    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
        return true;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {
    }
    private class ViewPageAdapter extends FragmentPagerAdapter {
        private List<Fragment> fragments;

        public ViewPageAdapter(FragmentManager fm, List<Fragment> fragments) {
            super(fm);
            this.fragments = fragments;
        }
        @Override
        public Fragment getItem(int position) {
            return this.fragments.get(position);
        }

        @Override
        public int getCount() {
            return this.fragments.size();
        }
    }


    /**
     * Metodo encargado de gestionar la animación de las paginas de presentación inicio.
     */
    private void updateDisplay() {
        timer = new Timer();
        timer.schedule(new TimerTask() {

            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //Your code to run in GUI thread here
                        moveToNext();
                    }//public void run() {
                });

            }

        }, 2500, 5000);//Update change view every 5 second
    }

    @Override
    protected void onPause() {
        super.onPause();
        AppEventsLogger.deactivateApp(this);
        if(timer != null)
            timer.cancel();

    }

    @Override
    protected void onResume() {
        super.onResume();

        AppEventsLogger.activateApp(this);

    }

    /**
     * Crea alarma de servicio de personas
     */
    public void createAlarmInit()
    {
        if(GlobalData.isKey("alarm",RegisterOrSessionActivityActivity.this))
        {
            if(GlobalData.get("alarm",RegisterOrSessionActivityActivity.this).equals("Y"))
                Alarm.createAlarm(this);
        }
        else
            Alarm.createAlarm(this);
    }

    /**
     * Inicializa la BD
     */
    public void initBD()
    {
        //Configuration dbConfiguration = new Configuration.Builder(this).setDatabaseName("loovers.db").create();
        //ActiveAndroid.initialize(dbConfiguration);
    }

    /**
     * Animación para el logo cuando entra a la pantalla
     * @param view
     */
    public void animationLogo(View view)
    {
        Visibility.visible(view);
        Animation animation = new TranslateAnimation(0, 0,-1000, 0);
        animation.setInterpolator(new BounceInterpolator());
        animation.setStartOffset(100);
        animation.setDuration(1800);
        animation.setFillAfter(true);
        view.startAnimation(animation);

    }
    /**
     * Animación para el texto (nombre de la app - loovers) cuando entra a la pantalla
     * @param view
     */
    public void animationText(View view)
    {
        Visibility.visible(view);
        Animation fadeIn = new AlphaAnimation(0, 1);
        fadeIn.setInterpolator(new DecelerateInterpolator()); //add this
        fadeIn.setStartOffset(1500);
        fadeIn.setDuration(1000);
        view.startAnimation(fadeIn);
    }

    /**
     * Clase encargada de obtener el GCM_ID para las notificaciones push
     */
    class TareaRegistroGCM extends AsyncTask<String,String,String>
    {
        String email;
        String password;
        String nickname;
        String age;
        String urlImageFacebook;
        ProgressDialog progressDialog;
        boolean status = true;

        public TareaRegistroGCM(String email, String password, String nickname,String age,String urlImageFacebook,ProgressDialog progressDialog)
        {
            this.email = email;
            this.password = password;
            this.nickname = nickname;
            this.age = age;
            this.urlImageFacebook = urlImageFacebook;
            this.progressDialog = progressDialog;
        }


        @Override
        protected String doInBackground(String... params)
        {

            String msg = "";
            Log.d("app2you", "entro a background : " + email + " " + password + " " + nickname + " " + age);
            try
            {
                if (gcm == null)
                    gcm = GoogleCloudMessaging.getInstance(RegisterOrSessionActivityActivity.this);


                //Nos registramos en los servidores de GCM
                regid = gcm.register(SENDER_ID);


                Log.d(TAG, "Registrado en GCM: registration_id=" + regid);

            }
            catch (IOException ex)
            {
                status = false;
                ex.printStackTrace();
                Log.d(TAG, "Error registro en GCM:" + ex.getMessage());

            }

            return msg;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(status)
                registerUser(email, password, nickname, age,regid,urlImageFacebook,progressDialog);
            else {
                if(progressDialog != null)
                     progressDialog.dismiss();
                new TareaRegistroGCM(email,password,nickname,age,urlImageFacebook,progressDialog).execute();
                // Toast.makeText(LoginUsingFacebookMailOrGooglev2.this, "error GCM ID", Toast.LENGTH_SHORT).show();
            }
        }
    }
    /**
     * Servicio para registrar un usuario en el sistema genere 0 male, 1 female
     */
    public void
    registerUser(final String email, final String password, final String nickname, String age, final String gcmId,final String urlImageFacebook,final ProgressDialog progressDialog)
    {
        OkHttpClient client = new OkHttpClient();

        final String model = Build.MODEL;
        final String version = Common.getVersionDevice(RegisterOrSessionActivityActivity.this);
        String genere = GlobalData.get(Constant.GENERE, RegisterOrSessionActivityActivity.this);
        String province =  (GlobalData.isKey(Constant.PROVINCE,RegisterOrSessionActivityActivity.this))?GlobalData.get(Constant.PROVINCE, RegisterOrSessionActivityActivity.this):"";

        mCity = (GlobalData.isKey(Constant.CITY_USER,RegisterOrSessionActivityActivity.this))?GlobalData.get(Constant.CITY_USER, RegisterOrSessionActivityActivity.this):"";
        mCountry = (GlobalData.isKey(Constant.COUNTRY_USER,RegisterOrSessionActivityActivity.this))?GlobalData.get(Constant.COUNTRY_USER, RegisterOrSessionActivityActivity.this):"ES";
        Log.d("app2you","gcm: " +gcmId + " size: "+ gcmId.length() );
        regid = gcmId;
        final  String wanted = (genere.equals("1") ? "2" : "1");

        RequestBody formBody = new FormBody.Builder()
                .add("email", "" + email)
                .add("password", "" + password)
                .add("firstName", "" + nickname)
                .add("genere", ""+genere)

                //.add("currentLatitude", "" + GlobalData.get(Constant.LATITUD_USER, RegisterOrSessionActivityActivity.this))
                //.add("currentLongitude",""+GlobalData.get(Constant.LONGITUD_USER, RegisterOrSessionActivityActivity.this))
                //.add("city",(mCity.isEmpty())?"":""+mCity)
                //.add("province",""+province)
                .add("country",(mCountry.isEmpty())?"ES":""+mCountry)
                .add("ip","" + Common.getLocalIpAddress(RegisterOrSessionActivityActivity.this))
                .add("registrationId",""+regid)
                .add("registrationIdOld",GlobalData.isKey(Constant.REGISTRATION_ID_GCM_OLD,RegisterOrSessionActivityActivity.this)?GlobalData.get(Constant.REGISTRATION_ID_GCM_OLD,RegisterOrSessionActivityActivity.this):"")

                .add("so", Constant.SO)
                .add("model",""+model)
                .add("isFacebook","1")
                .add("version",""+version)
                .add("appVersion",""+RegisterOrSessionActivityActivity.this.getResources().getString(R.string.app_version))
                .build();


        Request request = new Request.Builder()
                .url(Services.REGISTER_SERVICE)
                .post(formBody)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                if(progressDialog != null)
                    progressDialog.dismiss();
                Looper.prepare();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Common.errorMessage(RegisterOrSessionActivityActivity.this,"",""+getResources().getString(R.string.check_internet));
                    }
                });
                Looper.loop();
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                //if(progressDialog != null)
                  //  progressDialog.dismiss();

                final String result = response.body().string();
                Log.d("app2you", "respuesta: " + result);
                Looper.prepare();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            JSONObject json = new JSONObject(result);
                            String success = json.getString("success");
                            if (success.equals("true")) {
                                setRegistrationId(RegisterOrSessionActivityActivity.this,nickname, regid);
                                String sessionToken = (json.has("sessionToken")) ? json.getString("sessionToken") : "";
                                String currentLikes = json.getString("currentLikes");
                                String myUserId = json.getString("myUserId");
                                int creditsLoginDaily = json.has("creditsLoginDaily")?json.getInt("creditsLoginDaily"):5;
                                GlobalData.set(Constant.CREDITS_LOGIN_DAY,""+creditsLoginDaily,RegisterOrSessionActivityActivity.this);

                                int creditsShareFacebook    = json.has("creditsShareFacebook")?json.getInt("creditsShareFacebook"):20;
                                GlobalData.set(Constant.CREDITS_SHARE_FACEBOOK,""+creditsShareFacebook,RegisterOrSessionActivityActivity.this);


                                //enviar click
                                sendDataClickFacebook("7");
                                GlobalData.set(Constant.REGISTRATION_ID_GCM_OLD,""+regid,RegisterOrSessionActivityActivity.this);
                                GlobalData.set(Constant.LOGIN_APP, "Y", RegisterOrSessionActivityActivity.this);
                                GlobalData.set(Constant.NICKNAME, "" + nickname, RegisterOrSessionActivityActivity.this);
                                GlobalData.set(Constant.SESSION_TOKEN, "" + sessionToken, RegisterOrSessionActivityActivity.this);
                                GlobalData.set(Constant.WANTED_FOUND, "" + wanted, RegisterOrSessionActivityActivity.this);
                                GlobalData.set(Constant.NAME_MODEL_DEVICE, "" + model, RegisterOrSessionActivityActivity.this);
                                GlobalData.set(Constant.VERSION_SO, "" + version, RegisterOrSessionActivityActivity.this);
                                GlobalData.set(Constant.CURRENT_LIKES, "" + currentLikes, RegisterOrSessionActivityActivity.this);
                                GlobalData.set(Constant.USER_STATUS, Constant.NOT_GOLD_BEFORE_TRIAL_NO_PICTURE, RegisterOrSessionActivityActivity.this); //usuario nuevo
                                GlobalData.set(Constant.FIRST_REGISTER, "Y", RegisterOrSessionActivityActivity.this); //usuario nuevo
                                GlobalData.set(Constant.EMAIL_ADDRESS,""+email,RegisterOrSessionActivityActivity.this);
                                GlobalData.set(Constant.COUNTER_MESSAGES,"1",RegisterOrSessionActivityActivity.this);
                                GlobalData.set(Constant.MY_USER_ID,""+myUserId,RegisterOrSessionActivityActivity.this);
                                GlobalData.set(Constant.EMAIL_VERIFIED,"Y",RegisterOrSessionActivityActivity.this);

                                SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");
                                Date today = Calendar.getInstance().getTime();
                                String currentDate = myFormat.format(today);
                                GlobalData.set(Constant.LAST_CONNECTION,currentDate,RegisterOrSessionActivityActivity.this);

                                //se borran las preferencias antiguas
                                GlobalData.delete(Constant.RADIUS_PREFERENCE,RegisterOrSessionActivityActivity.this);
                                GlobalData.delete(Constant.MIN_AGE_PREFERENCE,RegisterOrSessionActivityActivity.this);
                                GlobalData.delete(Constant.MAX_AGE_PREFERENCE,RegisterOrSessionActivityActivity.this);

                                //ejecuta tarea asicrona para descargar y guardar la foto del perfil de facebook
                                new BackGroundTaskForUploadPicture(sessionToken,true,progressDialog).execute();


                            } else
                            {

                                int errorMessage = Integer.parseInt(json.getString("errorNum"));
                                //Toast.makeText(LoginUsingFacebookMailOrGooglev2.this, "Algo paso registro: "+ errorMessage, Toast.LENGTH_SHORT).show();
                                switch (errorMessage) {
                                    case 1:
                                        //error faltan campos key
                                        LoginManager.getInstance().logOut();
                                        break;
                                    case 2:
                                        //error valor
                                        if(progressDialog != null)
                                            progressDialog.dismiss();

                                        LoginManager.getInstance().logOut();
                                        Toast.makeText(RegisterOrSessionActivityActivity.this, R.string.try_again, Toast.LENGTH_SHORT).show();
                                        break;
                                    case 3:
                                        // Toast.makeText(LoginUsingFacebookMailOrGooglev2.this, "El correo ya esta registrado", Toast.LENGTH_SHORT).show();
                                        loginUser(email,password,gcmId, progressDialog);
                                        break;
                                    case 4:
                                        break;
                                    case 5:
                                        break;
                                    case 6:
                                        break;
                                    case 7:

                                        break;
                                    default:
                                        if(progressDialog != null)
                                            progressDialog.dismiss();
                                        LoginManager.getInstance().logOut();
                                        Toast.makeText(RegisterOrSessionActivityActivity.this, R.string.check_internet, Toast.LENGTH_SHORT).show();
                                        break;

                                }

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            if(progressDialog != null)
                                progressDialog.dismiss();

                            LoginManager.getInstance().logOut();
                            Toast.makeText(RegisterOrSessionActivityActivity.this, R.string.error_reponse_server, Toast.LENGTH_SHORT).show();
                        }
                        finally {
                            response.body().close();
                        }
                    }
                });

                Looper.loop();

            }
        });


    }
    private void setRegistrationId(Context context, String user, String regId)
    {
        SharedPreferences prefs = getSharedPreferences(
                MainActivity.class.getSimpleName(),
                Context.MODE_PRIVATE);

        int appVersion = getAppVersion(context);

        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PROPERTY_USER, user);
        editor.putString(PROPERTY_REG_ID, regId);
        editor.putInt(PROPERTY_APP_VERSION, appVersion);
        editor.putLong(PROPERTY_EXPIRATION_TIME,
                System.currentTimeMillis() + EXPIRATION_TIME_MS);

        editor.commit();
    }

    /**
     * Servicio para login un usuario en el sistema
     */
    public void loginUser(final String email, String password, final String regId,final ProgressDialog progressDialog) {

        //Visibility.visible(progressBarLogin);
        OkHttpClient client = new OkHttpClient();

        RequestBody formBody = new FormBody.Builder()
                .add("email", "" + email)
                .add("password", "" + password)
                .add("registrationId", "" + regId)
                .add("registrationIdOld",GlobalData.isKey(Constant.REGISTRATION_ID_GCM_OLD,RegisterOrSessionActivityActivity.this)?GlobalData.get(Constant.REGISTRATION_ID_GCM_OLD,RegisterOrSessionActivityActivity.this):"")

                .add("isFacebook","1")
                .add("ip", "" + Common.getLocalIpAddress(RegisterOrSessionActivityActivity.this))
                .add("version", "" + Common.getVersionDevice(RegisterOrSessionActivityActivity.this))

                .build();

        Request request = new Request.Builder()

                .url(Services.LOGIN_SERVICE)
                .post(formBody)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

                Looper.prepare();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Visibility.gone(progressBarLogin);
                        Common.errorMessage(RegisterOrSessionActivityActivity.this, "", getResources().getString(R.string.check_internet));
                    }
                });
                //Toast.makeText(LoginActivity.this, "Ocurrio un error en logueo usuario", Toast.LENGTH_SHORT).show();
                Looper.loop();
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {

                if(progressDialog != null)
                    progressDialog.dismiss();

                final String result = response.body().string();
                Log.d("app2you", "respuesta login: " + result);
                Looper.prepare();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {


                        try {
                            JSONObject json = new JSONObject(result);

                            String success = json.getString("success");

                            if (success.equals("true")) {
                                sendPingFacebookLogin();
                                String sessionToken = json.getString("sessionToken");
                                String wantedFound = json.getString("preferedSex");
                                String nickname = json.getString("nickname");
                                String accountTypeID = json.getString("accountTypeID");
                                if(!accountTypeID.equals(Constant.NOT_GOLD_BEFORE_TRIAL_NO_PICTURE))
                                    GlobalData.set(Constant.TUTORIAL_GAME, "Y", RegisterOrSessionActivityActivity.this);

                                String isShareFacebook = json.has("shareFacebook")?json.getString("shareFacebook"):"0";
                                if(isShareFacebook.equals("1"))
                                    GlobalData.set(Constant.SHARE_FACEBOOK,"Y",RegisterOrSessionActivityActivity.this);

                                String currentLikes = json.getString("currentLikes");
                                String city = json.getString("city");
                                String genere = json.getString("sexId");
                                String personalStatus = json.getString("personalStatus");
                                String myUserId = json.getString("myUserId");
                                int creditsLoginDaily = json.has("creditsLoginDaily")?json.getInt("creditsLoginDaily"):5;
                                GlobalData.set(Constant.CREDITS_LOGIN_DAY,""+creditsLoginDaily,RegisterOrSessionActivityActivity.this);

                                int creditsShareFacebook    = json.has("creditsShareFacebook")?json.getInt("creditsShareFacebook"):20;
                                GlobalData.set(Constant.CREDITS_SHARE_FACEBOOK,""+creditsShareFacebook,RegisterOrSessionActivityActivity.this);

                               // String emailVerified = json.has("emailVerified")?json.getString("emailVerified"):"N";
                                String avatar = "";
                                String lastConnection = "";
                                if(json.has("lastConnection"))
                                {
                                    lastConnection = json.getString("lastConnection");
                                }
                                else
                                {
                                    SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");
                                    Date today = Calendar.getInstance().getTime();
                                    lastConnection = myFormat.format(today);
                                }

                                setRegistrationId(RegisterOrSessionActivityActivity.this,""+nickname,regId);
                                Gson gson = new Gson();
                                MyProfileData myProfileData = gson.fromJson(result, MyProfileData.class);
                                if (myProfileData.pimgs != null && myProfileData.pimgs.size() > 0) {
                                    avatar = myProfileData.pimgs.get(0).avatarUrl;
                                    GlobalData.set(Constant.AVATAR_USER, "" + avatar, RegisterOrSessionActivityActivity.this);
                                }

                                GlobalData.set(Constant.REGISTRATION_ID_GCM_OLD,""+regid,RegisterOrSessionActivityActivity.this);
                                GlobalData.set(Constant.EMAIL_VERIFIED,"Y", RegisterOrSessionActivityActivity.this);
                                GlobalData.set(Constant.LOGIN_APP, "Y", RegisterOrSessionActivityActivity.this);
                                GlobalData.set(Constant.FIRST_REGISTER, "N", RegisterOrSessionActivityActivity.this);
                                GlobalData.set(Constant.SESSION_TOKEN, "" + sessionToken, RegisterOrSessionActivityActivity.this);
                                GlobalData.set(Constant.WANTED_FOUND, "" + wantedFound, RegisterOrSessionActivityActivity.this);
                                GlobalData.set(Constant.NICKNAME, "" + nickname, RegisterOrSessionActivityActivity.this);
                                GlobalData.set(Constant.USER_STATUS, "" + accountTypeID, RegisterOrSessionActivityActivity.this);
                                GlobalData.set(Constant.CURRENT_LIKES, "" + currentLikes, RegisterOrSessionActivityActivity.this);
                                GlobalData.set(Constant.MY_USER_ID,""+myUserId,RegisterOrSessionActivityActivity.this);
                                GlobalData.set(Constant.CITY_USER, "" + city, RegisterOrSessionActivityActivity.this);
                                GlobalData.set(Constant.EMAIL_ADDRESS,""+email,RegisterOrSessionActivityActivity.this);
                                GlobalData.set(Constant.GENERE,""+genere,RegisterOrSessionActivityActivity.this);
                                GlobalData.set(Constant.PERSONAL_STATUS,""+personalStatus,RegisterOrSessionActivityActivity.this);
                                GlobalData.set(Constant.LOGIN_FACEBOOK,"Y",RegisterOrSessionActivityActivity.this);
                                GlobalData.set(Constant.EMAIL_VERIFIED,"Y",RegisterOrSessionActivityActivity.this);
                                GlobalData.set(Constant.LAST_CONNECTION,lastConnection,RegisterOrSessionActivityActivity.this);

                                //ejecuta la actividad principal
                                Intent intent = new Intent(RegisterOrSessionActivityActivity.this, MainActivity.class);
                                startActivity(intent);
                                overridePendingTransition(R.anim.fade_in_slow, R.anim.fade_out);
                                finish();

                            }
                            else {

                                int errorMessage = Integer.parseInt(json.getString("errorNum"));

                                switch (errorMessage) {
                                    case 1:
                                        LoginManager.getInstance().logOut();
                                        break;
                                    case 2:
                                        break;
                                    case 3:
                                        LoginManager.getInstance().logOut();
                                        Toast.makeText(RegisterOrSessionActivityActivity.this, R.string.invalid_data, Toast.LENGTH_SHORT).show();
                                        break;
                                    default:
                                        Toast.makeText(RegisterOrSessionActivityActivity.this, R.string.invalid_data, Toast.LENGTH_SHORT).show();
                                        break;

                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            LoginManager.getInstance().logOut();
                            Visibility.gone(progressBarLogin);
                            Toast.makeText(RegisterOrSessionActivityActivity.this, R.string.error_reponse_server, Toast.LENGTH_SHORT).show();
                        }
                        finally {
                            response.body().close();
                        }
                    }
                });
                Looper.loop();
            }
        });


    }
    public  Bitmap scaleDown(Bitmap realImage, float maxImageSize,
                             boolean filter) {
        float ratio = Math.min(
                (float) maxImageSize / realImage.getWidth(),
                (float) maxImageSize / realImage.getHeight());
        int width = Math.round((float) ratio * realImage.getWidth());
        int height = Math.round((float) ratio * realImage.getHeight());

        Bitmap newBitmap = Bitmap.createScaledBitmap(realImage, width,
                height, filter);
        return newBitmap;
    }

    /**
     * Clase encargado de enviar la foto procesada al servidor
     */
    private class BackGroundTaskForUploadPicture extends
            AsyncTask<String, Void, Void> {

        private String token;
        private boolean isProfile;
        public CamaraData camaraData;
        public ProgressDialog progressDialog;
        public boolean status = true;

        public BackGroundTaskForUploadPicture(String token, boolean isProfile,ProgressDialog progressDialog) {

            this.token = token;
            this.isProfile = isProfile;
            this.progressDialog = progressDialog;
        }

        @Override
        protected Void doInBackground(String... params) {

            try
            {

                Bitmap bm = Common.getBitmapFromURL(imageUrlFacebook);
                Bitmap scaledBitmap = scaleDown(bm, SIZE_IMAGE, true);

                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bos);
                byte[] data = bos.toByteArray();

                HttpClient httpClient = new DefaultHttpClient();
                HttpPost postRequest = new HttpPost(Services.UPLOAD_PICTURE_SERVICE);
                ByteArrayBody bab = new ByteArrayBody(data, "profile.jpg");

                MultipartEntity reqEntity = new MultipartEntity(
                        HttpMultipartMode.BROWSER_COMPATIBLE);

                if (isProfile)
                    reqEntity.addPart("isProfile", new StringBody("1"));
                else
                    reqEntity.addPart("isProfile", new StringBody("2"));

                reqEntity.addPart("imageUrl", bab);
                reqEntity.addPart("sessionToken", new StringBody(this.token));
                reqEntity.addPart("sdcardPath", new StringBody("loovers"));
                reqEntity.addPart("isFacebook",new StringBody("1"));

                //datos del dispositivo
                reqEntity.addPart("so", new StringBody("1"));
                reqEntity.addPart("isPrivate",new StringBody(Constant.PUBLIC_PHOTO));
                reqEntity.addPart("deviceVersion", new StringBody(""+Common.getVersionDevice(RegisterOrSessionActivityActivity.this)));
                reqEntity.addPart("appVersion",new StringBody(RegisterOrSessionActivityActivity.this.getResources().getString(R.string.app_version)));
                reqEntity.addPart("model", new StringBody(""+ Build.MODEL));
                reqEntity.addPart("brand", new StringBody(""+Build.BRAND));

                postRequest.setEntity(reqEntity);
                HttpResponse response = httpClient.execute(postRequest);
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        response.getEntity().getContent(), "UTF-8"));
                String sResponse;
                StringBuilder s = new StringBuilder();

                while ((sResponse = reader.readLine()) != null)
                    s = s.append(sResponse);

                Gson gson = new Gson();
                //Log.d("app2you","finalizo : " + s.toString());
                camaraData = gson.fromJson(s.toString(), CamaraData.class);

            } catch (Exception e) {
                status = false;
                e.printStackTrace();
                Log.e(e.getClass().getName(), e.getMessage());
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            Visibility.gone(progressBarLogin);
            if(status)
            {
                if(camaraData.success)
                {
                    GlobalData.set(Constant.AVATAR_USER,""+imageUrlFacebook,RegisterOrSessionActivityActivity.this);
                    GlobalData.set(Constant.LOGIN_FACEBOOK, "Y", RegisterOrSessionActivityActivity.this);

                    if(progressDialog != null)
                        progressDialog.dismiss();

                    Intent intent = new Intent(RegisterOrSessionActivityActivity.this, MainActivity.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.fade_in_slow, R.anim.fade_out);
                    finish();
                }
                else
                {
                    if(progressDialog != null)
                        progressDialog.dismiss();

                    LoginManager.getInstance().logOut();
                    Toast.makeText(RegisterOrSessionActivityActivity.this, R.string.check_internet, Toast.LENGTH_SHORT).show();
                }

            }
            else
            {
                if(progressDialog != null)
                    progressDialog.dismiss();

                LoginManager.getInstance().logOut();
                Toast.makeText(RegisterOrSessionActivityActivity.this, R.string.error_reponse_server, Toast.LENGTH_SHORT).show();
            }

        }

    }

    /**
     * Clase encargada de descargar la foto de perfil desde Facebook
     */
    public class DownloadImageFacebook extends AsyncTask<String, String, String>
    {
        @Override
        protected String doInBackground(String... params) {

            HttpGet httpGet = new HttpGet(params[0]);
            HttpClient client = new DefaultHttpClient();
            HttpResponse response;
            StringBuilder stringBuilder = new StringBuilder();

            try {
                response = client.execute(httpGet);
                HttpEntity entity = response.getEntity();
                InputStream stream = entity.getContent();
                int b;
                while ((b = stream.read()) != -1)
                    stringBuilder.append((char) b);

            } catch (ClientProtocolException e) {
            } catch (IOException e) {
                e.printStackTrace();
            }



            return stringBuilder.toString();
        }

        @Override
        protected void onPostExecute(String url) {
            super.onPostExecute(url);

            try {
                JSONObject jsonObject = new JSONObject(url);
                JSONObject jsonObjectFacebook = new JSONObject(jsonObject.getString("data"));
                imageUrlFacebook = jsonObjectFacebook.getString("url");
//                Log.d("app2you","url image facebook fullhd: "+ imageUrlFacebook);
                new TareaRegistroGCM(emailFacebook, idFacebook, nameFacebook, birthdayFacebook, imageUrlFacebook, progressDialog).execute();

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    /**
     * Envia datos de instalacion al servidor, por primera vez
     */
    public void sendPingSever(String ip)
    {

        OkHttpClient client = new OkHttpClient();

        RequestBody formBody = new FormBody.Builder()
                .add("hashSession",GlobalData.isKey(Constant.HASH_SESSION,RegisterOrSessionActivityActivity.this)?GlobalData.get(Constant.HASH_SESSION,RegisterOrSessionActivityActivity.this):"false")
                .add("brand",""+Build.BRAND)
                .add("model",""+Build.MODEL)
                .add("so","1")
                .add("ip",""+ip)
                .add("deviceVersion", Common.getVersionDevice(RegisterOrSessionActivityActivity.this))
                .add("appVersion",""+getResources().getString(R.string.app_version))
                .add("typeId","2")
                .build();

        Request request = new Request.Builder()
                .url(Services.REGISTER_INSTALL)
                .post(formBody)
                .build();

        client.newCall(request).enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                final String result = response.body().string();

                Looper.prepare();

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            JSONObject json = new JSONObject(result);
                            String success = json.getString("success");

                            if (success.equals("true")) {
                                GlobalData.set(Constant.SERVER_PING_REGISTER,"Y",RegisterOrSessionActivityActivity.this);
                            }
                        } catch (JSONException ex) {
                            ex.printStackTrace();
                        }
                        finally {
                            response.body().close();
                        }

                    }
                });
                Looper.loop();
            }
        });
    }

    /**
     * Envia datos de instalacion al servidor, por primera vez
     */
    public void sendPingSeverClickFacebook(String ip, final String typeId)
    {

        OkHttpClient client = new OkHttpClient();

        RequestBody formBody = new FormBody.Builder()
                .add("hashSession",GlobalData.isKey(Constant.HASH_SESSION,RegisterOrSessionActivityActivity.this)?GlobalData.get(Constant.HASH_SESSION,RegisterOrSessionActivityActivity.this):"false")
                .add("brand",""+ Build.BRAND)
                .add("model",""+Build.MODEL)
                .add("so","1")
                .add("ip",""+ip)
                .add("deviceVersion", Common.getVersionDevice(RegisterOrSessionActivityActivity.this))
                .add("appVersion",""+getResources().getString(R.string.app_version))
                .add("typeId",""+typeId)

                .build();

        Request request = new Request.Builder()
                .url(Services.REGISTER_INSTALL)
                .post(formBody)
                .build();

        client.newCall(request).enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(Call call, IOException e) {


            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                final String result = response.body().string();

                Looper.prepare();

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            JSONObject json = new JSONObject(result);

                            String success = json.getString("success");

                            if (success.equals("true")) {
                                GlobalData.delete(Constant.SERVER_PING_GENERE,RegisterOrSessionActivityActivity.this);
                                GlobalData.delete(Constant.SERVER_PING_VIEW_REGISTER,RegisterOrSessionActivityActivity.this);
                                if(typeId.equals("6"))
                                    GlobalData.set(Constant.SERVER_PING_CLICK_FACEBOOK, "Y", RegisterOrSessionActivityActivity.this);
                                else
                                    GlobalData.set(Constant.SERVER_PING_CLICK_FACEBOOK_SUCCESS,"Y",RegisterOrSessionActivityActivity.this);
                            } else {

                            }
                        } catch (JSONException ex) {
                            ex.printStackTrace();
                        }
                        finally {
                            response.body().close();
                        }

                    }
                });
                Looper.loop();
            }
        });
    }

    public void sendDataClickFacebook(String typeId)
    {

        if(!GlobalData.isKey(Constant.SESSION_TOKEN,RegisterOrSessionActivityActivity.this)) {
            if(typeId.equals("6")) {
                if (!GlobalData.isKey(Constant.SERVER_PING_CLICK_FACEBOOK, RegisterOrSessionActivityActivity.this)) {
                    String ip = "false";

                    int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                    if (currentapiVersion >= Build.VERSION_CODES.M) {
                        sendPingSeverClickFacebook(ip, typeId);
                    } else {
                        ip = (Common.getLocalIpAddress(RegisterOrSessionActivityActivity.this).length() > 0) ? Common.getLocalIpAddress(RegisterOrSessionActivityActivity.this) : "false";
                        sendPingSeverClickFacebook(ip, typeId);
                    }

                }
            }
            else
            {
                if (!GlobalData.isKey(Constant.SERVER_PING_CLICK_FACEBOOK_SUCCESS, RegisterOrSessionActivityActivity.this)) {
                    String ip = "false";

                    int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                    if (currentapiVersion >= Build.VERSION_CODES.M) {
                        sendPingSeverClickFacebook(ip, typeId);
                    } else {
                        ip = (Common.getLocalIpAddress(RegisterOrSessionActivityActivity.this).length() > 0) ? Common.getLocalIpAddress(RegisterOrSessionActivityActivity.this) : "false";
                        sendPingSeverClickFacebook(ip, typeId);
                    }

                }
            }
        }

    }
    //Enviar ping primera vez en login
    public void sendPingFacebookLogin()
    {
        if(!GlobalData.isKey(Constant.SESSION_TOKEN,RegisterOrSessionActivityActivity.this)) {
            if (!GlobalData.isKey(Constant.SERVER_PING_LOGIN_FACEBOOK_SUCCESS, RegisterOrSessionActivityActivity.this)) {
                String ip = "false";

                int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                if (currentapiVersion >= Build.VERSION_CODES.M) {
                    sendPingSeverFacebookLogin(ip);
                } else {
                    ip = (Common.getLocalIpAddress(RegisterOrSessionActivityActivity.this).length() > 0) ? Common.getLocalIpAddress(RegisterOrSessionActivityActivity.this) : "false";
                    sendPingSeverFacebookLogin(ip);
                }

            }
        }
    }

    /**
     * Envia datos de instalacion al servidor, por primera vez
     */
    public void sendPingSeverFacebookLogin(String ip)
    {

        OkHttpClient client = new OkHttpClient();

        RequestBody formBody = new FormBody.Builder()
                .add("hashSession",GlobalData.isKey(Constant.HASH_SESSION,RegisterOrSessionActivityActivity.this)?GlobalData.get(Constant.HASH_SESSION,RegisterOrSessionActivityActivity.this):"false")
                .add("brand",""+Build.BRAND)
                .add("model",""+Build.MODEL)
                .add("so","1")
                .add("ip",""+ip)
                .add("deviceVersion", Common.getVersionDevice(RegisterOrSessionActivityActivity.this))
                .add("appVersion",""+getResources().getString(R.string.app_version))
                .add("typeId","8")
                .build();

        Request request = new Request.Builder()
                .url(Services.REGISTER_INSTALL)
                .post(formBody)
                .build();

        client.newCall(request).enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(Call call, IOException e) {


            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                final String result = response.body().string();
                //Log.d("app2you", "respuesta  ping  " + result);

                Looper.prepare();

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            JSONObject json = new JSONObject(result);

                            String success = json.getString("success");

                            if (success.equals("true")) {
                                GlobalData.set(Constant.SERVER_PING_LOGIN_FACEBOOK_SUCCESS,"Y",RegisterOrSessionActivityActivity.this);
                            } else {

                            }
                        } catch (JSONException ex) {
                            ex.printStackTrace();
                        }
                        finally {
                            response.body().close();
                        }

                    }
                });
                Looper.loop();
            }
        });
    }

    public void getKeyReleaseFacebook()
    {
        // Add code to print out the key hash
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.appwallamatch.app",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("app2you", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
    }

}
