package com.android.slidingmenu.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;

import com.android.slidingmenu.util.AppAnimation;
import com.android.slidingmenu.util.Common;
import com.android.slidingmenu.util.GlobalData;
import com.android.slidingmenu.util.Visibility;
import com.android.slidingmenu.webservices.Services;
import com.appdupe.flamer.LoginUsingFacebookMailOrGooglev2;
import com.appdupe.flamer.RegisterOrSessionActivityActivity;
import com.appdupe.flamer.utility.Constant;
import com.loovers.app.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by Juan Martin Bernal on 18/12/15.
 */
public class GenereActivity extends Activity{


    public Button btn_man, btn_woman, btn_continue;
    public LinearLayout containerContinue;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.genere);
        btn_man     = (Button)findViewById(R.id.btn_man);
        btn_woman   = (Button)findViewById(R.id.btn_woman);
        btn_man.setTransformationMethod(null);
        btn_woman.setTransformationMethod(null);
        btn_continue = (Button)findViewById(R.id.btn_continue);
        containerContinue = (LinearLayout)findViewById(R.id.containerContinue);

        if(!GlobalData.isKey(Constant.SESSION_TOKEN,GenereActivity.this)) {
            if (!GlobalData.isKey(Constant.SERVER_PING_GENERE, GenereActivity.this)) {
                String ip = "false";

                int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                if (currentapiVersion >= Build.VERSION_CODES.M) {
                    sendPingSever(ip);
                } else {
                    ip = (Common.getLocalIpAddress(GenereActivity.this).length() > 0) ? Common.getLocalIpAddress(GenereActivity.this) : "false";
                    sendPingSever(ip);
                }

            }
        }

        if(GlobalData.isKey(Constant.GENERE,GenereActivity.this))
        {
            animateContainer();
            String genere = GlobalData.get(Constant.GENERE, GenereActivity.this);
            if(genere.equals(Constant.MAN))
            {
                //hombre
                optionsMan();
            }
            else {
                //mujer
                optionsWoman();
            }
        }

        containerContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                continueRegister();

            }
        });
        btn_continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                continueRegister();
            }
        });


    }
    public void continueRegister()
    {
        //Toast.makeText(GenereActivity.this, "valor del genero: " + GlobalData.get(Constant.GENERE,GenereActivity.this), Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(GenereActivity.this, LoginUsingFacebookMailOrGooglev2.class);
        startActivity(intent);
        overridePendingTransition(R.anim.fade_in_slow, R.anim.fade_out);
        finish();
    }

    /**
     * Selecciono mujer
     * @param view
     */
    public void woman(View view)
    {
        animateContainer();
        GlobalData.set(Constant.GENERE, "" + Constant.WOMAN, GenereActivity.this);
        optionsWoman();
    }
    /** Selecciono hombre
    * @param view
    */
    public void man(View view)
    {
        animateContainer();
        GlobalData.set(Constant.GENERE, "" + Constant.MAN, GenereActivity.this);
        optionsMan();
    }

    /**
     * Animacion boton continuar
     */
    public void animateContainer()
    {
        if(containerContinue.getVisibility() == View.INVISIBLE)
        {
            Visibility.visible(containerContinue);
            AppAnimation.fadeIn(containerContinue,400);
        }

    }

    /**
     * Volver a la actividad inicial
     */
    public void backToInit()
    {
        GlobalData.delete(Constant.GENERE,GenereActivity.this);
        Intent intent = new Intent(GenereActivity.this,RegisterOrSessionActivityActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.fade_in_slow, R.anim.fade_out);
        finish();
    }
    public void optionsWoman()
    {
        btn_woman.setBackgroundResource(R.drawable.slect_mujer);
        btn_man.setBackgroundResource(R.drawable.fondo_boton_hombre);
        btn_man.setTextColor(Color.parseColor("#c3c3c3"));
        btn_woman.setTextColor(Color.WHITE);
    }
    public void optionsMan()
    {
        btn_woman.setBackgroundResource(R.drawable.fondo_boton_hombre);
        btn_man.setBackgroundResource(R.drawable.slect_hombre);
        btn_woman.setTextColor(Color.parseColor("#c3c3c3"));
        btn_man.setTextColor(Color.WHITE);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        backToInit();
    }

    /**
     * Envia datos de instalacion al servidor, por primera vez
     */
    public void sendPingSever(String ip)
    {

        OkHttpClient client = new OkHttpClient();

        RequestBody formBody = new FormBody.Builder()
                .add("hashSession",GlobalData.isKey(Constant.HASH_SESSION,GenereActivity.this)?GlobalData.get(Constant.HASH_SESSION,GenereActivity.this):"false")

                .add("brand",""+ Build.BRAND)
                .add("model",""+Build.MODEL)
                .add("so","1")
                .add("ip",""+ip)
                .add("deviceVersion", Common.getVersionDevice(GenereActivity.this))
                .add("appVersion",""+getResources().getString(R.string.app_version))
                .add("typeId","3")
                .build();

        Request request = new Request.Builder()
                .url(Services.REGISTER_INSTALL)
                .post(formBody)
                .build();

        client.newCall(request).enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(Call call, IOException e) {


            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                final String result = response.body().string();
                //Log.d("app2you", "respuesta  ping  " + result);

                Looper.prepare();

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            JSONObject json = new JSONObject(result);

                            String success = json.getString("success");

                            if (success.equals("true")) {
                                GlobalData.delete(Constant.SERVER_PING_CLICK_FACEBOOK,GenereActivity.this);
                                GlobalData.set(Constant.SERVER_PING_GENERE,"Y",GenereActivity.this);
                            } else {

                            }
                        } catch (JSONException ex) {
                            ex.printStackTrace();
                        }
                        finally {
                            response.body().close();
                        }

                    }
                });

                Looper.loop();
            }
        });
    }



}
