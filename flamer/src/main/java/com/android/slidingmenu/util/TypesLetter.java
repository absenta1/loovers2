package com.android.slidingmenu.util;

import android.content.Context;
import android.graphics.Typeface;

/**
 * Created by Juan Martin Bernal on 11/1/16.
 */
public class TypesLetter {

    public static Typeface getSansBold(Context cxt)
    {
        return Typeface.createFromAsset(cxt.getAssets(),"fonts/OpenSans-Bold.ttf");
    }
    public static Typeface getSansLight(Context cxt)
    {
        return Typeface.createFromAsset(cxt.getAssets(),"fonts/OpenSans-Light.ttf");
    }
    public static Typeface getSansRegular(Context cxt)
    {
        return Typeface.createFromAsset(cxt.getAssets(),"fonts/OpenSans-Regular.ttf");
    }
    public static Typeface getAvenir(Context cxt)
    {
        return Typeface.createFromAsset(cxt.getAssets(),"fonts/AvenirNextRegular.otf");
    }

}
