package com.appdupe.flamer.pojo;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class AlubumListData
{	
	@SerializedName("data")
    private  ArrayList<FaceBookAlubumData>  facebookArrayList;
	
	public ArrayList<FaceBookAlubumData> getFacebookArrayList() 
	{
		return facebookArrayList;
	}
	
	public void setFacebookArrayList(ArrayList<FaceBookAlubumData> facebookArrayList) 
	{
		this.facebookArrayList = facebookArrayList;
	}
}
