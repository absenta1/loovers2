package com.android.slidingmenu.fragments;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.slidingmenu.MainActivity;
import com.android.slidingmenu.objects.Match;
import com.android.slidingmenu.store.DialogStore;
import com.android.slidingmenu.util.GlobalData;
import com.android.slidingmenu.util.IabBroadcastReceiver;
import com.android.slidingmenu.util.IabHelper;
import com.android.slidingmenu.util.TypesLetter;
import com.android.slidingmenu.util.Visibility;
import com.appdupe.flamer.utility.CircleTransform;
import com.appdupe.flamer.utility.Constant;
import com.squareup.picasso.Picasso;
import com.loovers.app.R;

/**
 * Created by Juan Martin Bernal on 22/1/16.
 */
public class FragmentGOLD extends DialogFragment{

    public static final String TAG = "FragmentGOLD";
    public ImageView backgroundImageUser,imgClose,imgUser;
    public TextView txtTitle;
    public Button btnPLay;
    private Match match;
    public MainActivity mainActivity;

    //google
    //pago
    public static final String base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAk/7pl9bzluSqi3DJS3RZxkE1whMIhVxnpsgyvTZzBPCWqEIkPgJMqWfwFtehVyUYlgdqxvQxrbCcPf0Mvxj7QhfZCxzsUGuPnU8MIOAUBY8AEbkyJmstilJ6K8VcnFbOfcvLPZp8cl2uBVJpth0/GZGv847Dh09KyC+hiXL1cRaWVjAhjvD1/uYF5vGKPczO33SDiYPzpbvXrqnYS03i/s7Abe6aE2IWllbo4zNPc2sUqfdORkTezHew7u5BvyS/leEmtiDBY7RsDi/jklJkk8O6eprkugsEKvD1mUmKDgV/y2NwSVCOo5WLm2l0a/Xn3pIou48r4TT0CWF4bc88ZwIDAQAB";

    // The helper object
    IabHelper mHelper;

    // Provides purchase notification while this app is running
    IabBroadcastReceiver mBroadcastReceiver;
    boolean mIsPremium = false;
    // SKUs for our products: the premium upgrade (non-consumable) and gas (consumable)
    public static final String SKU_PREMIUM = "premium";
    public static final String SKU_GAS = "gas";

    static final int RC_REQUEST = 10001;
    // SKU for our subscription (infinite gas)

    static final String SKU_INFINITE_GAS_MONTHLY = "test_wallamatch_sku_mensual";//"infinite_gas_monthly";
    static final String SKU_INFINITE_GAS_YEARLY = "infinite_gas_yearly";

    // Does the user have an active subscription to the infinite gas plan?
    boolean mSubscribedToInfiniteGas = false;

    // Will the subscription auto-renew?
    boolean mAutoRenewEnabled = false;

    // Tracks the currently owned infinite gas SKU, and the options in the Manage dialog
    String mInfiniteGasSku = "";
    String mFirstChoiceSku = "";
    String mSecondChoiceSku = "";

    // Used to select between purchasing gas on a monthly or yearly basis
    String mSelectedSubscriptionPeriod = "";
    // How many units (1/4 tank is our unit) fill in the tank.
    static final int TANK_MAX = 4;

    // Current amount of gas in tank, in units
    int mTank;


    public FragmentGOLD() {
        // Empty constructor required for DialogFragment
    }
    public FragmentGOLD(MainActivity mainActivity, Match match) {
        // Empty constructor required for DialogFragment
        this.mainActivity = mainActivity;
        this.match = match;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        setStyle(DialogFragment.STYLE_NO_FRAME, android.R.style.Theme_Holo_Light_DarkActionBar);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_gold, container);

        if(mainActivity == null)
            mainActivity = (MainActivity)getActivity();
       // onCreateBilling();
        Typeface openSansBold = TypesLetter.getSansBold(mainActivity);

        backgroundImageUser = (ImageView)view.findViewById(R.id.backgroundImageUser);
        txtTitle            = (TextView)view.findViewById(R.id.txtTitle);
        imgClose            = (ImageView)view.findViewById(R.id.imgClose);
        imgUser             = (ImageView)view.findViewById(R.id.imgUser);
        btnPLay             = (Button)view.findViewById(R.id.btnPlay);
        btnPLay.setTypeface(openSansBold);
        if(match != null) {
            Visibility.visible(txtTitle);
            txtTitle.setText(mainActivity.getResources().getString(R.string.title_pass_gold_free1)+" " + match.username + " " +mainActivity.getResources().getString(R.string.title_pass_gold_free2));
            txtTitle.setTypeface(TypesLetter.getSansBold(mainActivity));
            setBackgroundImage(match.picture, backgroundImageUser);

        }
        else
        {
            Visibility.gone(txtTitle);
            setBackgroundImage(GlobalData.get(Constant.AVATAR_USER, mainActivity),backgroundImageUser);
        }

        // setFriendImage(match.picture, imgMatch);
        setFriendImage(GlobalData.get(Constant.AVATAR_USER, mainActivity),imgUser);

        btnPLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
                //pass to gold logic google
                //mainActivity.buyMonth();
                DialogStore dialogStore = new DialogStore(mainActivity,1);
                dialogStore.show(mainActivity.mFragmentManager,"DialogStore.TAG");

            }
        });
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        return view;
    }
    private void setBackgroundImage(String url, ImageView image) {
        Picasso.with(getActivity()).load(url).resize(200,200).centerCrop().into(image);

    }
    private void setFriendImage(String url, ImageView image) {
        Picasso.with(getActivity()).load(url).resize(200,200).centerCrop().transform(new CircleTransform()).into(image);

    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onActivityResult(" + requestCode + "," + resultCode + "," + data);
        // Pass on the activity result to the helper for handling
        if (mHelper == null) return;

        // Pass on the activity result to the helper for handling
        if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
            // not handled, so handle it ourselves (here's where you'd
            // perform any handling of activity results not related to in-app
            // billing...
            super.onActivityResult(requestCode, resultCode, data);
        }
        else {
            Log.d(TAG, "onActivityResult handled by IABUtil.");
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // very important:
        if (mBroadcastReceiver != null) {
            mainActivity.unregisterReceiver(mBroadcastReceiver);
        }

        // very important:
        Log.d(TAG, "Destroying helper.");
        if (mHelper != null) {
            mHelper.dispose();
            mHelper = null;
        }
    }
}
