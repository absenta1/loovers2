package com.android.slidingmenu.products;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.android.slidingmenu.MainActivity;
import com.android.slidingmenu.util.TypesLetter;
import com.appdupe.flamer.utility.Constant;
import com.loovers.app.R;

import java.util.ArrayList;

/**
 * Created by Juan Martin Bernal on 9/3/16.
 */
public class ProductAdapter extends ArrayAdapter<Product> {
    private  MainActivity mainActivity;
    private ArrayList<Product> products;

    public ProductAdapter(MainActivity mainActivity, ArrayList<Product> products) {
        super(mainActivity, R.layout.row_product, products);
        this.mainActivity = mainActivity;
        this.products = products;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) mainActivity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.row_product, parent, false);
        TextView txtProduct = (TextView) rowView.findViewById(R.id.txtProduct);
        TextView txtNumBuy  = (TextView)rowView.findViewById(R.id.txtNumBuy);
        Button btnPrice     = (Button)rowView.findViewById(R.id.btnPrice);

        txtProduct.setTypeface(TypesLetter.getSansRegular(mainActivity));
        btnPrice.setTypeface(TypesLetter.getSansRegular(mainActivity));

        final Product product = products.get(position);

        if(product != null)
        {
            txtProduct.setText(""+product.description);
            btnPrice.setText(""+product.price);
            switch (product.productId)
            {
                case Constant.BUY_COINS_1:
                    txtNumBuy.setText("10");

                    break;
                case Constant.BUY_COINS_2:
                    txtNumBuy.setText("60");
                    break;
                case Constant.BUY_COINS_3:
                    txtNumBuy.setText("130");
                    break;

                default:
                    break;
            }
        }
        btnPrice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (product.productId)
                {
                    case Constant.BUY_COINS_1:
                        mainActivity.buy10Coins();
                        break;
                    case Constant.BUY_COINS_2:
                        mainActivity.buy60Coins();
                        break;
                    case Constant.BUY_COINS_3:
                        mainActivity.buy130Coins();
                        break;
                    case Constant.GOLD_24H:
                        mainActivity.buyGOLD24h();
                        break;
                    case Constant.GOLD_1_MONTH:
                        mainActivity.buyGOLD1MonthSubscription();
                        break;
                    case Constant.GOLD_3_MONTHS:
                        mainActivity.buyGOLD3MonthSubscription();
                        break;
                    default:
                        break;
                }
            }
        });



        return rowView;
    }
}