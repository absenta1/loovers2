package com.android.slidingmenu.activities;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Looper;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.slidingmenu.MainActivity;
import com.android.slidingmenu.interfaces.Iphotolistener;
import com.android.slidingmenu.util.Common;
import com.android.slidingmenu.util.ConnectionDetector;
import com.android.slidingmenu.util.GlobalData;
import com.android.slidingmenu.util.ReceiveUriScaledBitmapTask;
import com.android.slidingmenu.util.Visibility;
import com.android.slidingmenu.webservices.Services;
import com.appdupe.flamer.pojo.CamaraData;
import com.appdupe.flamer.pojo.MyProfileData;
import com.appdupe.flamer.utility.CircleTransform;
import com.appdupe.flamer.utility.Constant;
import com.google.gson.Gson;
import com.soundcloud.android.crop.Crop;
import com.squareup.picasso.Picasso;
import com.loovers.app.R;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Date;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseView;


public class UploadPhotoProfileActivity extends Activity implements Iphotolistener,ReceiveUriScaledBitmapTask.ReceiveUriScaledBitmapListener{

    public static final String TAG = "UploadPhotoProfileActivity";
    public ImageView imageProfile,camera,layerimage,rotateImage;
    public static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 7896;
    public static final String TEMP_PHOTO_FILE = "temporary_holder.jpg";
    public static final int CAPTURE_GALLERY = 7890;
    private static final int TYPE_PROFILE = 1;
    public Uri imageUri                      = null;
    public TextView txtTitle, txtBody, txtDescription,txtCancel;
    public static final int SIZE_IMAGE  = 640;//1280
    public Button btnOk;
    private int type;
    private String picturePath = "";
    private String token;
    public static final int  MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 22;
    public int temporal = 0;
    public Bitmap resizedBitmap;
    public ProgressBar progressBarRotate;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.fragment_upload_photo_profile);
        initComponents();
        Bundle extras = getIntent().getExtras();

        if (extras != null)
            type = extras.getInt("TYPE");

    }

    /**
     * Inicializo componentes de la pantallas
     */
    public void initComponents()
    {
        imageProfile        = (ImageView)findViewById(R.id.imageProfile);
        rotateImage         = (ImageView)findViewById(R.id.rotateImage);
        camera              = (ImageView)findViewById(R.id.camera);
        layerimage          = (ImageView)findViewById(R.id.layerimage);
        txtTitle            = (TextView)findViewById(R.id.title);
        txtBody             = (TextView)findViewById(R.id.body);
        txtDescription      = (TextView)findViewById(R.id.description);
        txtCancel           = (TextView)findViewById(R.id.txtCancel);
        btnOk               = (Button)findViewById(R.id.btnOk);
        progressBarRotate   = (ProgressBar)findViewById(R.id.progressBarRotate);


        token = GlobalData.get(Constant.SESSION_TOKEN, UploadPhotoProfileActivity.this);//Settings.getToken(this);
        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openDialogSelectImage();
            }
        });

        imageProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ActivityCompat.checkSelfPermission(UploadPhotoProfileActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED&& ActivityCompat.checkSelfPermission(UploadPhotoProfileActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ) {
                    ActivityCompat.requestPermissions( UploadPhotoProfileActivity.this, new String[] {  android.Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA  },MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
                }
                else {
                    openDialogSelectImage();
                }


            }
        });
        rotateImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(resizedBitmap != null)
                    new AsynckTaskRotateImage().execute();
            }
        });

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(ConnectionDetector.isConnectingToInternet(UploadPhotoProfileActivity.this)) {


                    if (!picturePath.equals("")) {
                        BackGroundTaskForUploadPicture uploadService;

                        if (type == TYPE_PROFILE) {
                            uploadService = new BackGroundTaskForUploadPicture(picturePath, token, true);
                        } else {
                            uploadService = new BackGroundTaskForUploadPicture(picturePath, token, false);
                        }
                        uploadService.setListener(UploadPhotoProfileActivity.this);
                        uploadService.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    }
                }
                else
                    Common.errorMessage(UploadPhotoProfileActivity.this,"",getResources().getString(R.string.check_internet));
            }
        });
    }
    public class AsynckTaskRotateImage extends AsyncTask<String, String, String>
    {
        File dest;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Visibility.visible(progressBarRotate);
        }

        @Override
        protected String doInBackground(String... params) {

            resizedBitmap = Common.rotateImage(resizedBitmap,90);
            dest          = new File(picturePath);
            FileOutputStream out = null;
            try {
                out = new FileOutputStream(dest);
                resizedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
                out.flush();
                out.close();

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Visibility.visible(imageProfile);
            Bitmap bmp = resizedBitmap;
            imageProfile.setImageBitmap(Common.getCroppedBitmap(bmp));
            Visibility.gone(progressBarRotate);
        }
    }
    /**
     * Abrir modal para opciones de foto
     */
    public void openDialogSelectImage()
    {
        // custom dialog
        final Dialog dialog = new Dialog(UploadPhotoProfileActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.modal_select_image);

        Button gallery = (Button) dialog.findViewById(R.id.galeria);
        Button camera = (Button) dialog.findViewById(R.id.camara);
        Button cancel = (Button) dialog.findViewById(R.id.cancelar);
        gallery.setTransformationMethod(null);
        camera.setTransformationMethod(null);
        cancel.setTransformationMethod(null);
        // if button is clicked, close the custom dialog
        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                openGallery();
            }
        });
        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                takePhoto();

            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });

        dialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Crop.REQUEST_CROP) {
            handleCrop(resultCode, data);
        }
        else if ( requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {

            if ( resultCode == RESULT_OK) {

                Visibility.visible(layerimage);
                Visibility.gone(camera);
                if(imageUri != null) {
                   // new AsyncTaskProcessImage(imageUri,picturePath).execute();
                    temporal = 1;
                    startCropActivity(imageUri);

                }
                else
                    Toast.makeText(UploadPhotoProfileActivity.this, R.string.try_again, Toast.LENGTH_SHORT).show();

            } else if ( resultCode == RESULT_CANCELED) {

                Toast.makeText(this, R.string.canceled_photo, Toast.LENGTH_SHORT).show();
            } else {

                Toast.makeText(this, R.string.canceled_photo, Toast.LENGTH_SHORT).show();
            }
        }
        else if ( requestCode == CAPTURE_GALLERY) {

            if ( resultCode == RESULT_OK) {
                Uri selectedImage = data.getData();

                if(selectedImage != null) {
                    temporal = 0;
                    startCropActivity(selectedImage);
                }
                else
                    Toast.makeText(this, R.string.try_again, Toast.LENGTH_SHORT).show();

            }
            else if(resultCode == RESULT_CANCELED)
                Toast.makeText(this, R.string.canceled_photo, Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * ejecutar crop a la foto
     * @param resultCode
     * @param result
     */
    private void handleCrop(int resultCode, Intent result) {
        if (resultCode == Activity.RESULT_OK) {
            new AsyncTaskProcessImage(imageUri, picturePath,temporal).execute();
        }
        else if(resultCode == Activity.RESULT_CANCELED)
        {
            Visibility.gone(layerimage);
            Visibility.visible(camera);
            Toast.makeText(this, R.string.canceled_photo, Toast.LENGTH_SHORT).show();

        } else if (resultCode == Crop.RESULT_ERROR) {
            Toast.makeText(this, Crop.getError(result).getMessage(),Toast.LENGTH_SHORT).show();
        }
    }


    /**
     * Cancela subida de foto de perfil
      * @param view
     */
    public void cancelPhoto(View view)
    {
        Intent intent = new Intent(UploadPhotoProfileActivity.this, MainActivity.class);
        intent.putExtra("finish_tutorial", 2);
        startActivity(intent);
        overridePendingTransition(R.anim.fade_in_slow, R.anim.fade_out);
        finish();
    }

    /**
     * Tomar una foto
     */
    public void takePhoto()
    {
        /*************************** Camera Intent Start ************************/

        // Define the file-name to save photo taken by Camera activity

        String fileName = "Camera_Example.jpg";

        // Create parameters for Intent with filename

        ContentValues values = new ContentValues();

        values.put(MediaStore.Images.Media.TITLE, fileName);

        values.put(MediaStore.Images.Media.DESCRIPTION, "Image capture by camera");

        // imageUri is the current activity attribute, define and save it for later usage

        imageUri = getContentResolver().insert(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

        /**** EXTERNAL_CONTENT_URI : style URI for the "primary" external storage volume. ****/


        // Standard Intent action that can be sent to have the camera
        // application capture an image and return it.

        Intent intent = new Intent( MediaStore.ACTION_IMAGE_CAPTURE );

        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);

        //intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);

        intent.putExtra("return-data", true);

        startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);

        /*************************** Camera Intent End ************************/
    }

    /**
     * Abrir galeria
     */
    public void openGallery()
    {
        try
        {
            Intent intent = new Intent( Intent.ACTION_GET_CONTENT );
            intent.setType( "image/*" );
            startActivityForResult( intent, CAPTURE_GALLERY );
        }
        catch (ActivityNotFoundException e )
        {
            e.printStackTrace();
        }
    }

    /**
     * Foto ok
     */
    public void photoOk()
    {
        Visibility.gone(txtCancel);
        Visibility.gone(txtDescription);
        Visibility.visible(btnOk);
        txtTitle.setText(getResources().getString(R.string.title_confirm_photo));
        txtBody.setText(getResources().getString(R.string.msg_confirm_photo));
    }

    @Override
    public void OnPhotoMadeOK() {}

    @Override
    public void OnPhotoMadeFail() {}



    @Override
    public void onUriScaledBitmapReceived(Uri uri) {
        startCropActivity(uri);
    }
    private void startCropActivity(Uri originalUri) {
        imageUri = Uri.fromFile(getTempFile());
        new Crop(originalUri).output(imageUri).asSquare()
                .start(this);
    }
    public static File getTempFile() {

        if (Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {

            File file = new File(Environment.getExternalStorageDirectory(),
                    TEMP_PHOTO_FILE);
            try {
                file.createNewFile();
            } catch (IOException e) {
            }

            return file;
        } else {

            return null;
        }
    }

    /**
     * Tarea asincrona que nos permite subir una foto al servidor
     */
    private class BackGroundTaskForUploadPicture extends
            AsyncTask<String, Void, Void> {

        private String imageFile;
        private String token;
        private boolean isProfile;
        private Iphotolistener listener;
        public CamaraData camaraData;
        public ProgressDialog progressDialog;
        public boolean status = true;


        public BackGroundTaskForUploadPicture(String imageFile,String token, boolean isProfile) {
            this.imageFile = imageFile;
            this.token = token;
            this.isProfile = isProfile;
        }

        public void setListener(Iphotolistener listener) {
            this.listener = listener;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(UploadPhotoProfileActivity.this,R.style.CustomDialogTheme);
            progressDialog.setCancelable(false);
            progressDialog.show();
            progressDialog.setContentView(R.layout.custom_progress_dialog);
            TextView txtView = (TextView) progressDialog.findViewById(R.id.txtProgressDialog);
            txtView.setText(""+getResources().getString(R.string.updating_photo));
        }

        @Override
        protected Void doInBackground(String... params) {

            try {

                Bitmap bm = BitmapFactory.decodeFile(imageFile);
                Bitmap scaledBitmap = Common.scaleDown(bm, SIZE_IMAGE, true);
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bos);
                byte[] data = bos.toByteArray();

                HttpClient httpClient = new DefaultHttpClient();
                HttpPost postRequest = new HttpPost(Services.UPLOAD_PICTURE_SERVICE);
                ByteArrayBody bab = new ByteArrayBody(data, "profile.jpg");

                MultipartEntity reqEntity = new MultipartEntity(
                        HttpMultipartMode.BROWSER_COMPATIBLE);

                if (isProfile)
                    reqEntity.addPart("isProfile", new StringBody("1"));
                else
                    reqEntity.addPart("isProfile", new StringBody("2"));

                reqEntity.addPart("imageUrl", bab);
                reqEntity.addPart("sessionToken", new StringBody(this.token));
                reqEntity.addPart("sdcardPath", new StringBody(this.imageFile));
                reqEntity.addPart("isFacebook",new StringBody("2"));
                //datos del dispositivo
                reqEntity.addPart("so", new StringBody("1"));
                reqEntity.addPart("isPrivate",new StringBody(Constant.PUBLIC_PHOTO));
                reqEntity.addPart("deviceVersion", new StringBody(""+Common.getVersionDevice(UploadPhotoProfileActivity.this)));
                reqEntity.addPart("appVersion",new StringBody(UploadPhotoProfileActivity.this.getResources().getString(R.string.app_version)));
                reqEntity.addPart("model", new StringBody(""+ Build.MODEL));
                reqEntity.addPart("brand", new StringBody(""+Build.BRAND));

                postRequest.setEntity(reqEntity);
                HttpResponse response = httpClient.execute(postRequest);
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        response.getEntity().getContent(), "UTF-8"));
                String sResponse;
                StringBuilder s = new StringBuilder();

                while ((sResponse = reader.readLine()) != null)
                    s = s.append(sResponse);

                Gson gson = new Gson();
                camaraData = gson.fromJson(s.toString(), CamaraData.class);

            } catch (Exception e) {
                status = false;
                e.printStackTrace();
                Log.e(e.getClass().getName(), e.getMessage());
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            if(status) {

                if (!camaraData.success) {
                    progressDialog.dismiss();
                    Common.errorMessage(UploadPhotoProfileActivity.this, "", getResources().getString(R.string.try_again));
                } else {

                    //peticion con exito
                    if (camaraData.errNum == 0) {
                        if (listener != null)
                            listener.OnPhotoMadeOK();

                    } else {
                        if (listener != null)
                            listener.OnPhotoMadeFail();

                    }

                    getMyUserProfile(imageFile, progressDialog);

                }
            }
            else {
                progressDialog.dismiss();
                Common.errorMessage(UploadPhotoProfileActivity.this, "", getResources().getString(R.string.check_internet));
            }

        }

    }


    /**
     * Metodo encargado de consultar el perfil del usuario
     * @param filepath
     */
    public void getMyUserProfile(final String filepath, final ProgressDialog progressDialog)
    {

        OkHttpClient client = new OkHttpClient();

        RequestBody formBody = new FormBody.Builder()
                .add("sessionToken", "" + GlobalData.get(Constant.SESSION_TOKEN, UploadPhotoProfileActivity.this))
                .add("ip", Common.getLocalIpAddress(UploadPhotoProfileActivity.this))
                .add("version", Common.getVersionDevice(UploadPhotoProfileActivity.this))
                .build();

        Request request = new Request.Builder()
                .url(Services.GET_PROFILE)
                .post(formBody)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                progressDialog.dismiss();
                Looper.prepare();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Common.errorMessage(UploadPhotoProfileActivity.this,"",getResources().getString(R.string.check_internet));
                    }
                });
                Looper.loop();
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {

                String result = response.body().string();
                Log.d("app2you", "respuesta: " + result);
                progressDialog.dismiss();
                Looper.prepare();
                try {
                    Gson gson = new Gson();
                    MyProfileData profileData = gson.fromJson(result,MyProfileData.class);
                    if (profileData.success)
                    {
                        try {
                            GlobalData.set(Constant.USER_STATUS,""+profileData.accountTypeID,UploadPhotoProfileActivity.this);
                            GlobalData.set(Constant.AVATAR_USER, "" + profileData.pimgs.get(0).avatarUrl, UploadPhotoProfileActivity.this);

                            Intent intent = new Intent(UploadPhotoProfileActivity.this, MainActivity.class);
                            intent.putExtra("finish_tutorial",1);
                            startActivity(intent);
                            overridePendingTransition(R.anim.fade_in_slow,R.anim.fade_out);
                            finish();


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        finally {
                            response.body().close();
                        }
                    } else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(UploadPhotoProfileActivity.this, R.string.check_internet, Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(UploadPhotoProfileActivity.this, R.string.error_reponse_server, Toast.LENGTH_SHORT).show();
                        }
                    });
                }
                Looper.loop();
            }
        });
    }

    /**
     * Clase encargada de procesar una imagen, convertirla en bitmap y pintarla sobre un imageView
     */
    class AsyncTaskProcessImage extends AsyncTask<String, String, String>
    {
        public Uri imageUri;
        public String path;

        public ProgressDialog progressDialog;
        public File dest;
        public boolean status = true;
        public int from = 0;

        public AsyncTaskProcessImage(Uri imageUri, String path)
        {
            this.imageUri  = imageUri;
            this.path = path;
        }
        public AsyncTaskProcessImage(Uri imageUri, String path, int from)
        {
            this.imageUri  = imageUri;
            this.path = path;
            this.from = from;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(UploadPhotoProfileActivity.this);
            progressDialog.setCancelable(false);
            progressDialog.setMessage(getResources().getString(R.string.proccessing_image));
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {

            try {
                //Uri selectedImage = data.getData();
                InputStream is = getContentResolver().openInputStream(imageUri);
                Bitmap bitmap = BitmapFactory.decodeStream(is);
                is.close();

                Date d = new Date();
                String filename = d.getTime() + ".jpg";
                File sd = Environment.getExternalStorageDirectory();
                dest = new File(sd, filename);

                picturePath = dest.getPath();

                FileOutputStream out = new FileOutputStream(dest);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 95, out);

                int height = bitmap.getHeight();
                int width = bitmap.getWidth();

                float scaleWidth = ((float) 1024) / width;
                float newHeight = height*scaleWidth;

                float scaleHeight = ((float) newHeight) / height;
                // create a matrix for the manipulation
                Matrix matrix = new Matrix();
                // resize the bit map
                matrix.postScale(scaleWidth, scaleHeight);
                // recreate the new Bitmap
                resizedBitmap = Common.decodeFile(picturePath,from);//Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, false);

                out.flush();
                out.close();


            } catch (IOException e) {
                e.printStackTrace();
                status = false;
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
            if(status) {
                Visibility.visible(imageProfile);
                Visibility.visible(layerimage);
                Visibility.gone(camera);
                photoOk();
                if (resizedBitmap != null) {
                    Visibility.visible(rotateImage);
                    showCase(rotateImage);
                    Visibility.visible(imageProfile);
                    imageProfile.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    Picasso.with(UploadPhotoProfileActivity.this).load(new File(picturePath)).transform(new CircleTransform()).into(imageProfile);
                }
            }
            else
                Toast.makeText(UploadPhotoProfileActivity.this, R.string.error_reponse_server, Toast.LENGTH_SHORT).show();

        }
    }

    @Override
    public void onBackPressed() {
        //disable button back
    }

    public void showCase(View view)
    {
        // single example
        new MaterialShowcaseView.Builder(this)
                .setTarget(view)
                .setDismissText("ENTENDIDO")
                .setDismissTextColor(Color.parseColor("#43b5b6"))
                .setContentText("Este botón, sirve para rotar la foto.")
                .setDelay(0) // optional but starting animations immediately in onCreate can make them choppy
                .singleUse("300") // provide a unique ID used to ensure it is only shown once
                .show();

    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        finish();
    }
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    openDialogSelectImage();
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
}
