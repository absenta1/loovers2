package com.appdupe.payment;


public interface ISetupPaymentListener{
	public void successSetup();
	public void errorInSetup();
}