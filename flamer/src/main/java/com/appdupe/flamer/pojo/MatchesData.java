package com.appdupe.flamer.pojo;

import android.graphics.Bitmap;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.SerializedName;
import com.google.maps.android.clustering.ClusterItem;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MatchesData implements Serializable, ClusterItem, Comparable<MatchesData>{

    @SerializedName("numGallery")
    public int numImgGallery;

    @SerializedName("firstName")
    public String firstName;

    @SerializedName("ladt")
    public String ladt;

    @SerializedName("age")
    public int age;

    @SerializedName("latitude")
    public String latitud;

    @SerializedName("longitude")
    public String longitud;

    @SerializedName("userId")
    public String userId;

    @SerializedName("sexId")
    public String sexId;

    @SerializedName("img")
    public String img;

    @SerializedName("avatar")
    public String avatarUrl;

    @SerializedName("distance")
    public String distance;

    @SerializedName("numLiked")
    public String numLiked;

    @SerializedName("city")
    public String city;

    @SerializedName("message")
    public String message;

    @SerializedName("isFavorite")
    public int isFavorite;

    @SerializedName("likeToMe")
    public int likeToMe;

    @SerializedName("isMatch")
    public int isMatch;

    @SerializedName("personalStatus")
    public int personalStatus;

    @SerializedName("isOnline")
    public int isOnline;

    @SerializedName("bitmap")
    public Bitmap bitmap;

    @SerializedName("chatStatusId")
    public int chatStatusId;

    @SerializedName("requestToMe")
    public boolean requestToMe;

    @SerializedName("meRejected")
    public boolean meRejected;


    //atributos chat pruebas

    @SerializedName("otherUserId")
    public String id_user;

    @SerializedName("name")
    public String name;

    @SerializedName("avatarUrl")
    public String picture;

    @SerializedName("lastestMessage")
    public String lastestMessage;

    @SerializedName("date")
    public String date;

    @SerializedName("numberMessageNotRead")
    public String count_messages;

    @SerializedName("messageTypeId")
    public String messageTypeId;

    @SerializedName("chatTypeId")
    public int chatTypeId;
    
    @SerializedName("senderIdLastestMessage")
    public int senderIdLastestMessage;

    public int featureId;

    public int imageResource;



    public Date StringToDate(String dt)
    {

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        try {
            date = format.parse(dt);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return date;
    }
    @Override
    public int compareTo(MatchesData matchesData) {
        Date date1 = StringToDate(date);
        Date date2 = StringToDate(matchesData.date);
        if (date1 == null || date2 == null)
            return 0;
        return date1.compareTo(date2);
    }

    @Override
    public String toString() {
        return "nombre: "+firstName + " userId: " + userId + " num images: " + numImgGallery + " numLikes: " + numLiked + " avatar " + avatarUrl;
    }

    @Override
    public LatLng getPosition() {
        if(latitud != null && latitud.equals("null"))
            return new LatLng(Double.parseDouble(latitud),Double.parseDouble(longitud));
        else
            return new LatLng(0,0);
    }
}
