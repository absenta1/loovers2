package com.android.slidingmenu.fragments;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.slidingmenu.Analitycs.MyApplication;
import com.android.slidingmenu.MainActivity;
import com.android.slidingmenu.store.DialogStore;
import com.android.slidingmenu.swype.SwipeFlingAdapterView;
import com.android.slidingmenu.util.AppAnimation;
import com.android.slidingmenu.util.Common;
import com.android.slidingmenu.util.GPSTracker;
import com.android.slidingmenu.util.GlobalData;
import com.android.slidingmenu.util.TypesLetter;
import com.android.slidingmenu.util.Visibility;
import com.android.slidingmenu.webservices.Services;
import com.appdupe.MatchFromFlechazosDialogFragment;
import com.appdupe.flamer.pojo.FindMatchData;
import com.appdupe.flamer.pojo.MatchesData;
import com.appdupe.flamer.pojo.UserLikes;
import com.appdupe.flamer.utility.ConnectionDetector;
import com.appdupe.flamer.utility.Constant;
import com.gitonway.lee.niftymodaldialogeffects.lib.Effectstype;
import com.gitonway.lee.niftymodaldialogeffects.lib.NiftyDialogBuilder;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.gson.Gson;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.squareup.picasso.Picasso;
import com.loovers.app.R;
import com.wang.avi.AVLoadingIndicatorView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class FragmentGame extends Fragment implements OnClickListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {


    private RelativeLayout swipeviewlayout;
    private ArrayList<MatchesData> MachedataList;
    public AlertDialog alertDialog;

    private View view;
    public AVLoadingIndicatorView avloadingIndicatorView;
    private FragmentManager mFragmentManager;
    public RelativeLayout containerGame,containerTutorial, containerNoFoundMatches;

    //Constantes ME GUSTA , NO ME GUSTA
    public static final String LIKE = "1";
    public static final String DONT_LIKE = "2";

    //constantes favorite
    public static final int FAVORITE = 1;
    public static final int REMOVE_FAVORITE = 2;

    public String mLatitude;
    public String mLongitude;
    //public String cityUser;
    public RelativeLayout containerImageTutorial;
    public MainActivity mainActivity;
    public ImageView profileImageShowMain,imgTutorial,imgRadarWallamatch ;
    public Button btnNextPage;

    public TextView txtSearchPeople;

    public ProgressBar progressBar;

    //animacion tarjetas
    public MyAppAdapterCards myAppAdapter;
    private SwipeFlingAdapterView flingContainer;
    public FrameLayout containerFrame;
    public int tutorial = 0;
    public int numberPage = 0;

    public int showDialogFragmentGame = 0;
    public final static int MY_PERMISSIONS_REQUEST_READ_GPS = 7851;
    public final static int MY_PERMISSIONS_REQUEST_GET_LOCATION = 8695;

    // Google client to interact with Google API
    private GoogleApiClient mGoogleApiClient;

    private LocationRequest mLocationRequest;
    //public AnimationDrawable frameAnimation;
    public GPSTracker gps;

    public Dialog dialogReloadWaitTime;

    public FragmentGame() {

    }

    public FragmentGame(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    public FragmentGame(MainActivity mainActivity, int showDialogFragmentGame)
    {
        this.mainActivity = mainActivity;
        this.showDialogFragmentGame = showDialogFragmentGame;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_layout1, null);
        if(mainActivity == null)
            mainActivity = (MainActivity)getActivity();

        mainActivity.tvTitle.setText(mainActivity.getResources().getString(R.string.app_name));
        Visibility.visible(mainActivity.tvTitle);

        avloadingIndicatorView      = (AVLoadingIndicatorView)view.findViewById(R.id.avloadingIndicatorView);
        containerFrame              = (FrameLayout)view.findViewById(R.id.containerFrame);
        flingContainer              = (SwipeFlingAdapterView) view.findViewById(R.id.frame);
        containerTutorial           = (RelativeLayout)view.findViewById(R.id.containerTutorial);
        containerImageTutorial      = (RelativeLayout)view.findViewById(R.id.containerImageTutorial);
        containerNoFoundMatches     = (RelativeLayout)view.findViewById(R.id.containerNoFoundMatches);
        imgTutorial                 = (ImageView)view.findViewById(R.id.imgTutorial);
        btnNextPage                 = (Button)view.findViewById(R.id.btnNextPage);
        btnNextPage.setTransformationMethod(null);

        mLatitude                   = ""+GlobalData.get(Constant.LATITUD_USER,mainActivity);
        mLongitude                  = ""+GlobalData.get(Constant.LONGITUD_USER,mainActivity);

        containerGame               = (RelativeLayout) view.findViewById(R.id.containerGame);
        mFragmentManager            = ((FragmentActivity) inflater.getContext()).getSupportFragmentManager();

        profileImageShowMain        = (ImageView) view.findViewById(R.id.image);
        imgRadarWallamatch          = (ImageView) view.findViewById(R.id.imgRadarWallamatch);

        progressBar                 = (ProgressBar) view.findViewById(R.id.progressBar);
        swipeviewlayout             = (RelativeLayout) view.findViewById(R.id.swipeviewlayout);
        swipeviewlayout.setVisibility(View.VISIBLE);

        mainActivity.likeButton     = (ImageView) view.findViewById(R.id.likeButton);
        mainActivity.dislikeButton  = (ImageView) view.findViewById(R.id.dislikeButton);


        txtSearchPeople             = (TextView)view.findViewById(R.id.txtSearchPeople);

        String numLikes = GlobalData.get(Constant.CURRENT_LIKES,mainActivity);
        mainActivity.validateNumCredits(numLikes);

        mainActivity.likeButton.setOnClickListener(this);
        mainActivity.dislikeButton.setOnClickListener(this);
        btnNextPage.setOnClickListener(this);

        imgRadarWallamatch.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                AppAnimation.scale(mainActivity,imgRadarWallamatch);
                //findMatch();
                mLocationRequest = LocationRequest.create()
                        .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                        .setInterval(5000)        // 5 seconds, in milliseconds
                        .setFastestInterval(1000); // 1 second, in milliseconds
                connectGoogle();
            }
        });

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //Metodo para encontrar perfiles
        //findMatch();
       /* mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(5000)        // 5 seconds, in milliseconds
                .setFastestInterval(1000); // 1 second, in milliseconds
        connectGoogle();*/
    }
    /**
     * Metodo encargado de ocultar o mostrar los elementos para la vista de jugar
     *
     * @param enable
     */
    private void visibilityElementsGame(boolean enable) {

        mainActivity.likeButton.setVisibility((enable) ? View.VISIBLE : View.INVISIBLE);
        mainActivity.dislikeButton.setVisibility((enable) ? View.VISIBLE : View.INVISIBLE);
        swipeviewlayout.setVisibility((enable) ? View.VISIBLE : View.INVISIBLE);

    }

    /**
     * Metodo encargado de ejecutar tarea asyncrona para encontrar perfiles
     */
    private void findMatch() {
        if(ConnectionDetector.isConnectingToInternet(mainActivity)) {

            if(GlobalData.isKey(Constant.CURRENT_LIKES,mainActivity) && GlobalData.get(Constant.CURRENT_LIKES,mainActivity).equals("0"))
                getCurrentLikesUser();
            else {
                getProfilesGame();

            }
        }
        else
            Common.errorMessage(mainActivity,"",mainActivity.getResources().getString(R.string.check_internet));
    }
    /**
     * Metodo encargado de recargar likes cuando se termina el tiempo de espera
     */
    public void getCurrentLikesUser()
    {
        OkHttpClient client = new OkHttpClient();

        RequestBody formBody = new FormBody.Builder()
                .add("sessionToken", "" + GlobalData.get(Constant.SESSION_TOKEN, mainActivity))
                .build();

        Request request = new Request.Builder()
                .url(Services.GET_CURRENT_LIKES)
                .post(formBody)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

                Looper.prepare();
                mainActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Common.errorMessage(mainActivity,"",mainActivity.getResources().getString(R.string.check_internet));
                    }
                });
                Looper.loop();
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {

                final String result = response.body().string();
                Log.d("app2you", "respuesta  getCurrentLikes user: " + result);

                Looper.prepare();

                mainActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //apago el dialogo
                        try {
                            JSONObject json = new JSONObject(result);
                            String success = json.getString("success");
                            if (success.equals("true")) {
                                String currentLikes = json.getString("currentLikes");
                                GlobalData.set(Constant.CURRENT_LIKES, "" + currentLikes, mainActivity);
                                Log.d("app2you","wallas disponibles getCurrentLikes: " +currentLikes);
                                String numWallas = currentLikes;

                                if (numWallas.equals("0")) {


                                    if(GlobalData.isKey(Constant.CARDS_GAME,mainActivity))
                                    {
                                        /*if(frameAnimation != null)
                                            frameAnimation.stop();*/
                                        String resultCards = GlobalData.get(Constant.CARDS_GAME,mainActivity);
                                        Gson gson = new Gson();
                                        FindMatchData mFindMatchDataTmp = gson.fromJson(resultCards,FindMatchData.class);
                                        MachedataList = mFindMatchDataTmp.matches;

                                        if(MachedataList.size() > 0) {
                                            Visibility.gone(containerNoFoundMatches);
                                            addCards(MachedataList);
                                        }
                                        else {
                                            getProfilesGame();
                                            //new BackGroundTaskForFindMatch(GlobalData.get(Constant.SESSION_TOKEN, mainActivity)).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                                        }
                                    }
                                    else {
                                        getProfilesGame();
                                        //new BackGroundTaskForFindMatch(GlobalData.get(Constant.SESSION_TOKEN, mainActivity)).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                                    }
                                } else {
                                    mainActivity.updateCredits(currentLikes);
                                    getProfilesGame();
                                    //new BackGroundTaskForFindMatch(GlobalData.get(Constant.SESSION_TOKEN, mainActivity)).execute();
                                }

                            }
                            else {
                                int error = json.getInt("errorNum");
                                if(error == Constant.ERROR_OPEN_SESSION_OTHER_DEVICE)
                                    otherSessionOpen();
                                else
                                    Common.errorMessage(mainActivity,"",mainActivity.getResources().getString(R.string.check_internet));
                            }


                        } catch (JSONException ex) {
                            ex.printStackTrace();
                            Toast.makeText(mainActivity,R.string.error_reponse_server, Toast.LENGTH_SHORT).show();
                        }
                        finally {
                            response.body().close();
                        }
                    }
                });


                Looper.loop();
            }
        });
    }
    /**
     * Método hacer like con okHttp
     */
    public void sendLike(final MatchesData data, final String likeType )
    {
        mainActivity.likeButton.setEnabled(false);
        mainActivity.dislikeButton.setEnabled(false);

        OkHttpClient client = new OkHttpClient();
        RequestBody formBody = new FormBody.Builder()
                .add("sessionToken", "" + GlobalData.get(Constant.SESSION_TOKEN, mainActivity))
                .add("likeToId", data.userId)
                .add("likeType",likeType )
                .build();

        Request request = new Request.Builder()
                .url(Services.LIKES_SERVICE)
                .post(formBody)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

                Looper.prepare();
                mainActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Common.errorMessage(mainActivity,"",mainActivity.getResources().getString(R.string.check_internet));
                        mainActivity.likeButton.setEnabled(true);
                        mainActivity.dislikeButton.setEnabled(true);
                    }
                });
                Looper.loop();
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {

                final String result = response.body().string();
                Log.d("app2you", "respuesta perfiles restantes: " +  MachedataList.size());
                Looper.prepare();
                if(response.isSuccessful()) {

                    mainActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Gson gson = new Gson();
                                final UserLikes likes = gson.fromJson(result, UserLikes.class);

                                if (likes.success) {

                                    if (likes.currentLikes.equals("0")) {

                                        GlobalData.set(Constant.CURRENT_LIKES, "" + likes.currentLikes, mainActivity);
                                        AppAnimation.scale(mainActivity, mainActivity.txtCredits);
                                        mainActivity.txtCredits.setText("0");
                                        mainActivity.likeButton.setEnabled(true);
                                        mainActivity.dislikeButton.setEnabled(true);

                                        String status = GlobalData.get(Constant.USER_STATUS, mainActivity);
                                        if (status.equals(Constant.NOT_GOLD_BEFORE_TRIAL_PICTURE_PENDING)) {
                                            serviceGetWaitTime();
                                            //Common.errorMessage(mainActivity, "", mainActivity.getResources().getString(R.string.limit_wallas_picture_pending));
                                        } else {
                                            if (status.equals(Constant.NOT_GOLD_BEFORE_TRIAL_NO_PICTURE)) {
                                                final NiftyDialogBuilder dialog = NiftyDialogBuilder.getInstance(mainActivity);
                                                dialog.withTitle(null);
                                                dialog.withMessage(null);
                                                dialog.isCancelableOnTouchOutside(false);
                                                dialog.withDialogColor(Color.TRANSPARENT);
                                                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                                dialog.withDuration(Constant.DURATION_ANIMATION);
                                                dialog.withEffect(Effectstype.Shake);
                                                dialog.setCustomView(R.layout.modal_general, mainActivity);

                                                TextView title = (TextView) dialog.findViewById(R.id.title);
                                                TextView text = (TextView) dialog.findViewById(R.id.text);
                                                title.setText(Common.getNickName(mainActivity));
                                                text.setText(mainActivity.getResources().getString(R.string.limit_likes_no_picture));

                                                Button exit = (Button) dialog.findViewById(R.id.exit);
                                                exit.setText(mainActivity.getResources().getString(R.string.exit));
                                                Button accept = (Button) dialog.findViewById(R.id.accept);
                                                // if button is clicked, close the custom dialog
                                                accept.setText(mainActivity.getResources().getString(R.string.update_photo));
                                                exit.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {
                                                        dialog.dismiss();
                                                    }
                                                });
                                                accept.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {
                                                        dialog.dismiss();
                                                        mainActivity.replaceFragment(new FragmentProfile(mainActivity));


                                                    }
                                                });

                                                dialog.show();
                                            } else {
                                                //ejecutar servicio para pedir tiempo de espera
                                                serviceGetWaitTime();
                                            }
                                        }

                                        // popuppanelNomore();
                                    } else {
                                        //actualiza los wallas

                                        AppAnimation.scale(mainActivity, mainActivity.txtCredits);
                                        mainActivity.txtCredits.setText("" + likes.currentLikes);
                                        GlobalData.set(Constant.CURRENT_LIKES, "" + likes.currentLikes, mainActivity);
                                        //habilita de nuevo los botones
                                        mainActivity.likeButton.setEnabled(true);
                                        mainActivity.dislikeButton.setEnabled(true);

                                    }
                                    mainActivity.validateNumCredits(likes.currentLikes);
                                    // Log.d("loovers", "cantidad de matches: " + likes.newMatch.size());
                                    if (likes.newMatch != null && likes.newMatch.size() > 0) {

                                        if (!GlobalData.isKey(Constant.FIRST_MATCH, mainActivity))
                                            GlobalData.set(Constant.FIRST_MATCH, "Y", mainActivity);

                                        MatchFromFlechazosDialogFragment dialog = new MatchFromFlechazosDialogFragment(mainActivity, likes.newMatch.get(0));
                                        dialog.show(mFragmentManager, "CapturePhotoDialogFragment.TAG");
                                    }
                                    //No hay mas cartas para mostrar

                                } else {
                                    mainActivity.likeButton.setEnabled(true);
                                    mainActivity.dislikeButton.setEnabled(true);

                                }

                                if (MachedataList != null && MachedataList.size() == 0)
                                    findMatch();

                            } catch (Exception ex) {
                                ex.printStackTrace();
                                Toast.makeText(mainActivity, R.string.error_reponse_server, Toast.LENGTH_SHORT).show();
                                mainActivity.likeButton.setEnabled(true);
                                mainActivity.dislikeButton.setEnabled(true);
                            }
                            finally {
                                response.body().close();
                            }
                        }
                    });


                }
                else
                    Toast.makeText(mainActivity, R.string.check_internet, Toast.LENGTH_SHORT).show();

                Looper.loop();
            }
        });
    }

    /**
     * Metodo encarga de dar un like a un usuario 1 true , 0 false
     *
     * @param likeToId
     * @param likeType
     */
    private void setUserLike(MatchesData likeToId, String likeType) {
        sendLike(likeToId, likeType);
    }

    /**
     * Metodo encargado de pedir al servidor los perfiles del juego
     */
    public void getProfilesGame()
    {
        if(MachedataList != null && MachedataList.size() > 0)
        {

        }
        else {
            Visibility.visible(containerNoFoundMatches);
            Visibility.invisible(imgRadarWallamatch);
            Visibility.visible(avloadingIndicatorView);
            txtSearchPeople.setText(mainActivity.getResources().getString(R.string.find_new_people));
            txtSearchPeople.setTextColor(Color.parseColor("#c3c3c3"));

        }
        okhttp3.OkHttpClient client = new okhttp3.OkHttpClient();

        okhttp3.RequestBody formBody = new FormBody.Builder()
                .add("sessionToken", "" + GlobalData.get(Constant.SESSION_TOKEN, mainActivity))
                .add("latitud",""+mLatitude)
                .add("longitud",""+mLongitude)
                .add("city",GlobalData.isKey(Constant.CITY_USER,mainActivity)?GlobalData.get(Constant.CITY_USER,mainActivity):"")
                .add("province",GlobalData.isKey(Constant.PROVINCE,mainActivity)?GlobalData.get(Constant.PROVINCE,mainActivity):"")
                .add("typeId",""+Constant.GAME_WALLAMATCH)
                .add("ip",""+Common.getLocalIpAddress(mainActivity))
                .add("version",""+ Common.getVersionDevice(mainActivity))
                .build();


        okhttp3.Request request = new okhttp3.Request.Builder()
                .url(Services.FIND_MATCHES_SERVICE)
                .post(formBody)
                .build();


        client.newCall(request).enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Looper.prepare();

                mainActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Visibility.visible(containerNoFoundMatches);
                        Visibility.visible(imgRadarWallamatch);
                        Visibility.gone(avloadingIndicatorView);

                        txtSearchPeople.setText(mainActivity.getResources().getString(R.string.not_found_persons));
                        Toast.makeText(mainActivity, R.string.check_internet, Toast.LENGTH_SHORT).show();
                    }
                });

                Looper.loop();
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                final String result = response.body().string();
                Log.d("app2you", "respuesta profiles game: " + result);
                Looper.prepare();
                mainActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (response.isSuccessful()) {

                                Gson gson = new Gson();
                                FindMatchData mFindMatchData = gson.fromJson(result, FindMatchData.class);
                                if (mFindMatchData.success) {

                                    GlobalData.set(Constant.USER_STATUS, "" + mFindMatchData.accountTypeID, mainActivity);
                                    // txt_num_wallas.setText("" + mFindMatchData.currentLikes);
                                    GlobalData.set(Constant.CURRENT_LIKES, "" + mFindMatchData.currentLikes, mainActivity);
                                    Log.d("app2you", "wallas disponibles: " + mFindMatchData.currentLikes);
                                    String numWallas = mFindMatchData.currentLikes;
                                    if (numWallas.equals("0")) {
                                        AppAnimation.scale(mainActivity, mainActivity.txtCredits);
                                        mainActivity.txtCredits.setText("0");
                                        GlobalData.set(Constant.CARDS_GAME, new Gson().toJson(mFindMatchData), mainActivity);
                                    } else {
                                        //Visibility.visible(txt_num_wallas);
                                        AppAnimation.scale(mainActivity, mainActivity.txtCredits);
                                        mainActivity.txtCredits.setText("" + numWallas);
                                    }

                                    MachedataList = mFindMatchData.matches;

                                    if (MachedataList != null && MachedataList.size() > 0) {

                                        Visibility.gone(containerNoFoundMatches);
                                        Visibility.gone(avloadingIndicatorView);

                                        if (mainActivity.container_finish_register.getVisibility() == View.VISIBLE || mainActivity.container_personal_status.getVisibility() == View.VISIBLE
                                                || mainActivity.container_questions.getVisibility() == View.VISIBLE || mainActivity.containerBirthdayUser.getVisibility() == View.VISIBLE) {
                                            Visibility.enableDisableView(containerGame, false);
                                        } else {
                                            Visibility.enableDisableView(containerGame, true);

                                        }
                                        visibilityElementsGame(true);

                                        addCards(MachedataList);

                                        if (!GlobalData.isKey(Constant.TUTORIAL_GAME, mainActivity)) {
                                            numberPage = 0;
                                            Visibility.enableDisableView(containerGame, false);
                                            Visibility.enableDisableView(containerTutorial, true);
                                            Visibility.visible(containerTutorial);
                                        }


                                    } else {
                                        Visibility.visible(containerNoFoundMatches);
                                        new Handler().postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                AppAnimation.fadeOut(avloadingIndicatorView, 300);
                                                Visibility.visible(imgRadarWallamatch);
                                                AppAnimation.fadeIn(imgRadarWallamatch, 300);

                                                txtSearchPeople.setText(mainActivity.getResources().getString(R.string.not_found_persons));
                                                txtSearchPeople.setTextColor(Color.parseColor("#e2156c"));
                                                AppAnimation.fadeIn(txtSearchPeople, 400);
                                            }
                                        }, 2000);


                                    }

                                    //}
                                } else {
                                    Visibility.visible(containerNoFoundMatches);
                                    if (mFindMatchData.errNum == Constant.ERROR_OPEN_SESSION_OTHER_DEVICE) {
                                        otherSessionOpen();
                                    }
                                }

                            } else {
                               // Toast.makeText(mainActivity, R.string.check_internet, Toast.LENGTH_SHORT).show();
                                Visibility.visible(containerNoFoundMatches);
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        AppAnimation.fadeOut(avloadingIndicatorView, 300);
                                        Visibility.visible(imgRadarWallamatch);
                                        AppAnimation.fadeIn(imgRadarWallamatch, 300);

                                        txtSearchPeople.setText(mainActivity.getResources().getString(R.string.not_found_persons));
                                        txtSearchPeople.setTextColor(Color.parseColor("#e2156c"));
                                        AppAnimation.fadeIn(txtSearchPeople, 400);
                                    }
                                }, 2000);
                            }
                        } catch (Exception ex) {
                            Visibility.visible(containerNoFoundMatches);
                            ex.printStackTrace();
                            Toast.makeText(mainActivity, R.string.error_reponse_server, Toast.LENGTH_SHORT).show();
                        } finally {
                            response.body().close();
                        }
                    }
                    });


                Looper.loop();
            }
        });
    }


    /**
     * Cerrar session actual porque la han abierto en otro dispositivo
     */
    private void otherSessionOpen() {
        final NiftyDialogBuilder dialog =  NiftyDialogBuilder.getInstance(mainActivity);
        dialog.withTitle(null);
        dialog.withMessage(null);
        dialog.isCancelableOnTouchOutside(false);
        dialog.withDialogColor(Color.TRANSPARENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.withDuration(Constant.DURATION_ANIMATION);
        dialog.withEffect(Effectstype.Slidetop);
        dialog.setCustomView(R.layout.modal_general,mainActivity);

        TextView text = (TextView) dialog.findViewById(R.id.text);
        text.setText(""+mainActivity.getResources().getString(R.string.error_session));


        Button exit = (Button) dialog.findViewById(R.id.exit);
        Visibility.gone(exit);
        Button accept = (Button) dialog.findViewById(R.id.accept);
        // if button is clicked, close the custom dialog
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Common.closeSessionWallamatch(mainActivity);


            }
        });

        dialog.show();
    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.likeButton) {
            //like
            try {
                mainActivity.likeButton.clearAnimation();
                flingContainer.getTopCardListener().selectRight();
                AppAnimation.scale(mainActivity, mainActivity.likeButton); //animacion boton al dar click
            }
            catch(Exception ex)
            {
                ex.printStackTrace();
                Visibility.visible(containerNoFoundMatches);
            }
        }

        else if (v.getId() == R.id.dislikeButton) {
            //no like
           try{
                mainActivity.likeButton.clearAnimation();
                flingContainer.getTopCardListener().selectLeft();
                AppAnimation.scale(mainActivity, mainActivity.dislikeButton);//animacion boton al dar click
            }
            catch(Exception ex) {
                ex.printStackTrace();
                Visibility.visible(containerNoFoundMatches);
            }
        }
        else if(v.getId() == R.id.btnNextPage)
        {
            //Todo controlar los estados del tutorial
            switch (numberPage)
            {
                case 0:
                    ++numberPage ;
                    imgTutorial.setImageResource(R.drawable.tutorial_02);

                    break;
                case 1:
                    ++numberPage ;
                    imgTutorial.setImageResource(R.drawable.tutorial_03);

                    break;
                case 2:
                    containerImageTutorial.setGravity(Gravity.TOP
                    );
                    ++numberPage ;
                    imgTutorial.setImageResource(R.drawable.tutorial_04);
                    btnNextPage.setText(mainActivity.getResources().getString(R.string.finish));
                    break;
                case 3:
                    Visibility.gone(containerTutorial);
                    Visibility.enableDisableView(containerGame, true);
                    GlobalData.set(Constant.TUTORIAL_GAME, "Y", mainActivity);
                    numberPage = 0;

                    if(flingContainer != null) {
                        //flingContainer.setAdapter(myAppAdapter);
                        //myAppAdapter.notifyDataSetChanged();
                        flingContainer.setOnItemClickListener(new SwipeFlingAdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClicked(int itemPosition, Object dataObject) {
                                // Log.d("loovers", "click");
                                try {
                                    String status = GlobalData.get(Constant.USER_STATUS, mainActivity);
                                    if (!status.equals(Constant.NOT_GOLD_BEFORE_TRIAL_NO_PICTURE)) {
                                        View view = flingContainer.getSelectedView();
                                        view.findViewById(R.id.backgroundFrame).setAlpha(0);

                                        myAppAdapter.notifyDataSetChanged();
                                        MatchesData data = (MatchesData) dataObject;//myAppAdapter.listProfile.get(itemPosition);
                                        //Toast.makeText(mainActivity, "nombre: "+ data.firstName, Toast.LENGTH_SHORT).show();
                                        openFragmentDetail(data, Constant.GAME);
                                    } else {
                                        validatePhotoProfile();
                                    }
                                }
                                catch(Exception ex)
                                {
                                    ex.printStackTrace();
                                    Toast.makeText(mainActivity, R.string.error_reponse_server, Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                    }
                    break;
                default:
                    containerImageTutorial.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
                    break;
            }
        }
    }

    /**
     * Metodo encargado de mostrar dialogo  si el usuario no tiene foto de perfil validada
     */
    public void validatePhotoProfile()
    {
        final NiftyDialogBuilder dialog =  NiftyDialogBuilder.getInstance(mainActivity);
        dialog.withTitle(null);
        dialog.withMessage(null);
        dialog.isCancelableOnTouchOutside(false);
        dialog.withDialogColor(Color.TRANSPARENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.withDuration(Constant.DURATION_ANIMATION);
        dialog.withEffect(Effectstype.Shake);
        dialog.setCustomView(R.layout.modal_general,mainActivity);

        TextView title = (TextView) dialog.findViewById(R.id.title);
        TextView text = (TextView) dialog.findViewById(R.id.text);
        title.setText(""+ Common.getNickName(mainActivity));
        text.setText(""+mainActivity.getResources().getString(R.string.can_not_profile));

        Button exit = (Button) dialog.findViewById(R.id.exit);
        exit.setText(mainActivity.getResources().getString(R.string.exit));
        Button accept = (Button) dialog.findViewById(R.id.accept);
        // if button is clicked, close the custom dialog
        accept.setText(mainActivity.getResources().getString(R.string.update_photo));
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                mainActivity.replaceFragment(new FragmentProfile(mainActivity));


            }
        });

        dialog.show();
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d("app2you","on resumen game");
        MyApplication.getInstance().trackScreenView("fragment game");
        if(mainActivity.container_finish_register.getVisibility() == View.VISIBLE || mainActivity.container_personal_status.getVisibility() == View.VISIBLE
                || mainActivity.container_questions.getVisibility()== View.VISIBLE || mainActivity.containerBirthdayUser.getVisibility() == View.VISIBLE || tutorial == 1)
        {
            Visibility.enableDisableView(containerGame,false);
        }
        else
            Visibility.enableDisableView(containerGame,true);


        if(GlobalData.isKey(Constant.TUTORIAL_GAME,mainActivity)) {
            if (GlobalData.isKey(Constant.CARDS, mainActivity)) {
                String resultCards = GlobalData.get(Constant.CARDS, mainActivity);
                Log.d("app2you", "perfiles resume: " + resultCards);
                Gson gson = new Gson();
                MatchesData[] datas = gson.fromJson(resultCards, MatchesData[].class);

                MachedataList = new ArrayList<MatchesData>(Arrays.asList(datas));

                if (MachedataList != null && MachedataList.size() > 0) {
                    Log.d("app2you", "hay cartas en memoria");
                    Visibility.gone(containerNoFoundMatches);
                    Visibility.gone(avloadingIndicatorView);
                    addCards(MachedataList);
                } else {
                    Log.d("app2you", "No hay cartas en memoria");
                    mLocationRequest = LocationRequest.create()
                            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                            .setInterval(5000)        // 5 seconds, in milliseconds
                            .setFastestInterval(1000); // 1 second, in milliseconds
                    connectGoogle();
                }
            } else {
                Log.d("app2you", "resumen ejecuta connectGoogle");
                mLocationRequest = LocationRequest.create()
                        .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                        .setInterval(5000)        // 5 seconds, in milliseconds
                        .setFastestInterval(1000); // 1 second, in milliseconds
                connectGoogle();
            }
        }
        else
        {
            Log.d("app2you", "No hay cartas en memoria");
            mLocationRequest = LocationRequest.create()
                    .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                    .setInterval(5000)        // 5 seconds, in milliseconds
                    .setFastestInterval(1000); // 1 second, in milliseconds
            connectGoogle();
        }

    }

    /**
     * Método encargado de cargar las cartas para jugar en el contenedor
     * @param MachedataList
     */
    public void addCards(final ArrayList<MatchesData> MachedataList) {

        myAppAdapter = new MyAppAdapterCards(MachedataList, mainActivity);
        flingContainer.setAdapter(myAppAdapter);
        myAppAdapter.notifyDataSetChanged();

            flingContainer.setFlingListener(new SwipeFlingAdapterView.onFlingListener() {
                @Override
                public void removeFirstObjectInAdapter() {


                }

                @Override
                public void onLeftCardExit(Object dataObject) {
                    mainActivity.likeButton.clearAnimation();
                    if(GlobalData.isKey(Constant.TUTORIAL_GAME,mainActivity)) {

                        int currentLikes = Integer.parseInt(GlobalData.get(Constant.CURRENT_LIKES, mainActivity));

                        if (MachedataList != null && MachedataList.size() > 0) {
                            if (ConnectionDetector.isConnectingToInternet(mainActivity)) {
                                if (currentLikes != 0) {

                                    setUserLike(MachedataList.get(0), DONT_LIKE);
                                    MachedataList.remove(0);
                                    myAppAdapter.notifyDataSetChanged();
                                } else
                                {

                                    String status = GlobalData.get(Constant.USER_STATUS,mainActivity);
                                    if(status.equals(Constant.NOT_GOLD_BEFORE_TRIAL_PICTURE_PENDING)) {
                                        myAppAdapter.notifyDataSetChanged();
                                        if(dialogReloadWaitTime == null)
                                            serviceGetWaitTime();
                                        else if(!dialogReloadWaitTime.isShowing())
                                            serviceGetWaitTime();
                                       // Common.errorMessage(mainActivity, "", mainActivity.getResources().getString(R.string.limit_wallas_picture_pending));
                                    }
                                    else {
                                        myAppAdapter.notifyDataSetChanged();
                                        if (status.equals(Constant.NOT_GOLD_BEFORE_TRIAL_NO_PICTURE)) {

                                            final NiftyDialogBuilder dialog =  NiftyDialogBuilder.getInstance(mainActivity);
                                            dialog.withTitle(null);
                                            dialog.withMessage(null);
                                            dialog.isCancelableOnTouchOutside(false);
                                            dialog.withDialogColor(Color.TRANSPARENT);
                                            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                            dialog.withDuration(Constant.DURATION_ANIMATION);
                                            dialog.withEffect(Effectstype.Shake);
                                            dialog.setCustomView(R.layout.modal_general,mainActivity);

                                            TextView title = (TextView) dialog.findViewById(R.id.title);
                                            TextView text = (TextView) dialog.findViewById(R.id.text);
                                            title.setText(""+ Common.getNickName(mainActivity));
                                            text.setText(""+mainActivity.getResources().getString(R.string.limit_likes_no_picture));

                                            Button exit = (Button) dialog.findViewById(R.id.exit);
                                            exit.setText(mainActivity.getResources().getString(R.string.exit));
                                            Button accept = (Button) dialog.findViewById(R.id.accept);
                                            // if button is clicked, close the custom dialog
                                            accept.setText(mainActivity.getResources().getString(R.string.update_photo));
                                            exit.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    dialog.dismiss();
                                                }
                                            });
                                            accept.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    dialog.dismiss();
                                                    mainActivity.replaceFragment(new FragmentProfile(mainActivity));


                                                }
                                            });

                                            dialog.show();
                                        } else {
                                            //ejecutar servicio para pedri tiempo restante de espera

                                            if(dialogReloadWaitTime == null)
                                                serviceGetWaitTime();
                                            else if(!dialogReloadWaitTime.isShowing())
                                                serviceGetWaitTime();
                                           // Common.errorMessage(mainActivity, "", mainActivity.getResources().getString(R.string.limit_wallas));
                                        }
                                    }


                                }
                            } else
                                Toast.makeText(mainActivity, R.string.check_internet, Toast.LENGTH_SHORT).show();
                        } else {
                            //visibilityElementsGame(false);
                            //Toast.makeText(mainActivity, "No hay más perfiles para mostrar like", Toast.LENGTH_SHORT).show();
                            findMatch();
                        }
                    }
                    else
                        myAppAdapter.notifyDataSetChanged();//vuelvo la foto a su lugar
                }

                @Override
                public void onRightCardExit(Object dataObject) {
                    mainActivity.likeButton.clearAnimation();
                    if(GlobalData.isKey(Constant.TUTORIAL_GAME,mainActivity)) {

                        String userStatus = GlobalData.get(Constant.USER_STATUS, mainActivity);
                        int currentLikes = Integer.parseInt(GlobalData.get(Constant.CURRENT_LIKES, mainActivity));
                        if (MachedataList != null && MachedataList.size() > 0) {
                            if (ConnectionDetector.isConnectingToInternet(mainActivity)) {
                                if (userStatus.equals(Constant.NOT_GOLD_BEFORE_TRIAL_NO_PICTURE)) {
                                    mainActivity.likeButton.setEnabled(true);
                                    mainActivity.dislikeButton.setEnabled(true);
                                    myAppAdapter.notifyDataSetChanged();
                                    //Common.errorMessage(mainActivity, "Estado usuario: " + userStatus, "Para enviar, recibir Wallas y jugar a Wallamatch, es imprescindible subir tu foto de perfil");
                                    final NiftyDialogBuilder dialog =  NiftyDialogBuilder.getInstance(mainActivity);
                                    dialog.withTitle(null);
                                    dialog.withMessage(null);
                                    dialog.isCancelableOnTouchOutside(false);
                                    dialog.withDialogColor(Color.TRANSPARENT);
                                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                    dialog.withDuration(Constant.DURATION_ANIMATION);
                                    dialog.withEffect(Effectstype.Shake);
                                    dialog.setCustomView(R.layout.modal_general,mainActivity);

                                    TextView text = (TextView) dialog.findViewById(R.id.text);
                                    text.setText(""+mainActivity.getResources().getString(R.string.first_like_without_picture));


                                    Button exit = (Button) dialog.findViewById(R.id.exit);
                                    exit.setText(mainActivity.getResources().getString(R.string.exit));
                                    Button accept = (Button) dialog.findViewById(R.id.accept);
                                    // if button is clicked, close the custom dialog
                                    accept.setText(mainActivity.getResources().getString(R.string.update_photo));
                                    exit.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            dialog.dismiss();
                                        }
                                    });
                                    accept.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            dialog.dismiss();
                                            mainActivity.replaceFragment(new FragmentProfile(mainActivity));


                                                                     }
                                    });

                                    dialog.show();


                                } else {
                                    if (currentLikes != 0) {
                                        setUserLike(MachedataList.get(0), LIKE);
                                        MachedataList.remove(0);
                                        myAppAdapter.notifyDataSetChanged();
                                    } else {
                                        String status = GlobalData.get(Constant.USER_STATUS,mainActivity);
                                        if(status.equals(Constant.NOT_GOLD_BEFORE_TRIAL_PICTURE_PENDING)) {
                                            myAppAdapter.notifyDataSetChanged();
                                            //Common.errorMessage(mainActivity, "", mainActivity.getResources().getString(R.string.limit_wallas_picture_pending));
                                            if(dialogReloadWaitTime == null)
                                                serviceGetWaitTime();
                                            else if(!dialogReloadWaitTime.isShowing())
                                                serviceGetWaitTime();
                                        }
                                        else {
                                            myAppAdapter.notifyDataSetChanged();
                                            if (status.equals(Constant.NOT_GOLD_BEFORE_TRIAL_NO_PICTURE)) {
                                                final NiftyDialogBuilder dialog =  NiftyDialogBuilder.getInstance(mainActivity);
                                                dialog.withTitle(null);
                                                dialog.withMessage(null);
                                                dialog.isCancelableOnTouchOutside(false);
                                                dialog.withDialogColor(Color.TRANSPARENT);
                                                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                                dialog.withDuration(Constant.DURATION_ANIMATION);
                                                dialog.withEffect(Effectstype.Shake);
                                                dialog.setCustomView(R.layout.modal_general,mainActivity);

                                                TextView title = (TextView) dialog.findViewById(R.id.title);
                                                TextView text = (TextView) dialog.findViewById(R.id.text);
                                                title.setText(""+ Common.getNickName(mainActivity));
                                                text.setText(mainActivity.getResources().getString(R.string.limit_likes_no_picture));

                                                Button exit = (Button) dialog.findViewById(R.id.exit);
                                                exit.setText(mainActivity.getResources().getString(R.string.exit));
                                                Button accept = (Button) dialog.findViewById(R.id.accept);
                                                // if button is clicked, close the custom dialog
                                                accept.setText(mainActivity.getResources().getString(R.string.update_photo));
                                                exit.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {
                                                        dialog.dismiss();
                                                    }
                                                });
                                                accept.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {
                                                        dialog.dismiss();
                                                        mainActivity.replaceFragment(new FragmentProfile(mainActivity));


                                                    }
                                                });

                                                dialog.show();
                                            } else {
                                                //ejecutar servicio para pedri tiempo restante de espera

                                                if(dialogReloadWaitTime == null)
                                                    serviceGetWaitTime();
                                                else if(!dialogReloadWaitTime.isShowing())
                                                    serviceGetWaitTime();
                                               // Common.errorMessage(mainActivity, "", mainActivity.getResources().getString(R.string.limit_wallas));
                                            }
                                        }
                                    }
                                }

                            } else
                                Toast.makeText(mainActivity, R.string.check_internet, Toast.LENGTH_SHORT).show();
                        } else {
                            findMatch();
                        }
                    }
                    else
                        myAppAdapter.notifyDataSetChanged();//vuelvo la foto a su lugar
                }

                @Override
                public void onAdapterAboutToEmpty(int itemsInAdapter) {

                }

                @Override
                public void onScroll(float scrollProgressPercent) {

                    View view = flingContainer.getSelectedView();
                    if(view != null) {
                        view.findViewById(R.id.backgroundFrame).setAlpha(0);
                        view.findViewById(R.id.item_swipe_right_indicator).setAlpha(scrollProgressPercent < 0 ? -scrollProgressPercent : 0);
                        view.findViewById(R.id.item_swipe_left_indicator).setAlpha(scrollProgressPercent > 0 ? scrollProgressPercent : 0);
                    }
                }
            });

           if(GlobalData.isKey(Constant.TUTORIAL_GAME,mainActivity)) {
                flingContainer.setOnItemClickListener(new SwipeFlingAdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClicked(int itemPosition, Object dataObject) {
                        // Log.d("loovers", "click");
                        try {
                            mainActivity.likeButton.clearAnimation();

                            String status = GlobalData.get(Constant.USER_STATUS, mainActivity);
                            if (!status.equals(Constant.NOT_GOLD_BEFORE_TRIAL_NO_PICTURE)) {
                                View view = flingContainer.getSelectedView();
                                view.findViewById(R.id.backgroundFrame).setAlpha(0);
                                myAppAdapter.notifyDataSetChanged();
                                MatchesData data = (MatchesData) dataObject;//myAppAdapter.listProfile.get(itemPosition)
                                openFragmentDetail(data, Constant.GAME);
                            } else
                                validatePhotoProfile();
                        }
                        catch (Exception ex)
                        {
                            ex.printStackTrace();
                            Toast.makeText(mainActivity, R.string.error_reponse_server, Toast.LENGTH_SHORT).show();
                        }


                    }
                });
           }

    }

    /**
     * Metodo encargado de abrir el fragmento con los detalles del usuario
     * @param data
     */
    public void openFragmentDetail(MatchesData data, int type)
    {
        DetailProfileFragment dialog = new DetailProfileFragment(mainActivity, data,type);
        dialog.show(mFragmentManager, "DetailProfileFragment.TAG");
    }



    /**
     * Clase encargada del manejo de las cartas del juego
     */
    public class MyAppAdapterCards  extends ArrayAdapter<MatchesData> {

        public List<MatchesData> listProfile;
        public MainActivity mContext;
        public LayoutInflater mInflater;
        //public String option = "1";


        public MyAppAdapterCards(List<MatchesData> apps, MainActivity context) {
            super(context, R.layout.item, apps);
            this.listProfile = apps;
            this.mContext = context;
            this.mInflater = LayoutInflater.from(mContext);
            GlobalData.set("isFavorite", "100", mainActivity);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            final ViewHolder viewHolder;
            if (convertView == null) {


                convertView = mInflater.inflate(R.layout.item, parent, false);
                // configure view holder
                viewHolder = new ViewHolder();
                viewHolder.cardImage        = (ImageView) convertView.findViewById(R.id.cardImage);
                viewHolder.txtNameUser      = (TextView) convertView.findViewById(R.id.txtNameUser);
                viewHolder.progressBarCard  = (ProgressWheel) convertView.findViewById(R.id.progressBarCard);
                viewHolder.txtGimgs         = (TextView) convertView.findViewById(R.id.txtGimgs);
                viewHolder.imgGims          = (ImageView)convertView.findViewById(R.id.imgGims);

                viewHolder.txtNameUser.setTypeface(TypesLetter.getSansRegular(mContext));
                convertView.setTag(viewHolder);

            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            MatchesData data = getItem(position);


            if (data != null) {

                Visibility.visible(viewHolder.progressBarCard);
                Picasso.with(mContext).load(data.img).into(viewHolder.cardImage, new com.squareup.picasso.Callback() {
                    @Override
                    public void onSuccess() {
                        Visibility.gone(viewHolder.progressBarCard);
                    }

                    @Override
                    public void onError() {
                        Visibility.gone(viewHolder.progressBarCard);
                    }
                });

                viewHolder.txtNameUser.setText("" + data.firstName + ", " + data.age);
                if(data.numImgGallery != 0)
                {
                    viewHolder.txtGimgs.setText(""+data.numImgGallery);
                    Visibility.visible(viewHolder.imgGims);
                    Visibility.visible(viewHolder.txtGimgs);
                }
                else
                {
                    Visibility.invisible(viewHolder.imgGims);
                    Visibility.invisible(viewHolder.txtGimgs);
                }

            }

            return convertView;
        }
        private class ViewHolder
        {
            public TextView txtNameUser,txtGimgs;
            public ImageView cardImage, imgGims;
            public ProgressWheel progressBarCard;
        }

    }
    /**
     * Metod encargado de abrir dialogo para visualizar tiempo para recargar wallas y opciones de compra
     */
    public void openDialogReloadWallas(int millis)
    {

        dialogReloadWaitTime = new Dialog(mainActivity/*, R.style.PauseDialog*/);
        dialogReloadWaitTime.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
        //dialogReloadWaitTime.setCancelable(false);
        dialogReloadWaitTime.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialogReloadWaitTime.setContentView(R.layout.reload_likes);

        TextView txtWallasExhausted = (TextView)dialogReloadWaitTime.findViewById(R.id.txtWallasExhausted);
        TextView txtTime = (TextView) dialogReloadWaitTime .findViewById(R.id.txtDate);
        TextView txtPassToGold = (TextView)dialogReloadWaitTime.findViewById(R.id.txtPassToGold);

        Button btnWait   = (Button)dialogReloadWaitTime.findViewById(R.id.btnWait);
        Button btnPassToGold = (Button)dialogReloadWaitTime.findViewById(R.id.btnPassToGold);
        Button btnReloadWallas = (Button)dialogReloadWaitTime.findViewById(R.id.btnReloadWallas);
        String status = GlobalData.get(Constant.USER_STATUS, mainActivity);
        String gender = GlobalData.get(Constant.GENERE,mainActivity);
        btnPassToGold.setVisibility(gender.equals(Constant.MAN) && !status.equals(Constant.GOLD) ?View.VISIBLE:View.GONE);

        //if(status.equals(Constant.NOT_GOLD_AFTER_TRIAL)  && genere.equals(Constant.MAN))
        if(gender.equals(Constant.WOMAN) || status.equals(Constant.GOLD))
            txtPassToGold.setText("No pierdas ni una cita!\nRecarga tu cuenta de Likes");
        else
            txtPassToGold.setText(""+mainActivity.getResources().getString(R.string.txt_reload_likes_after_trial));
        //else
          //  txtPassToGold.setText(""+mainActivity.getResources().getString(R.string.txt_reload_wallas));

        txtTime.setTypeface(TypesLetter.getSansRegular(mainActivity));
        txtPassToGold.setTypeface(TypesLetter.getSansRegular(mainActivity));
        txtWallasExhausted.setTypeface(TypesLetter.getSansBold(mainActivity));

        String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis),
                TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
        txtTime.setText(""+hms);

        final CounterClass timer = new CounterClass(millis,1000,txtTime, dialogReloadWaitTime);
        timer.start();

        btnWait.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogReloadWaitTime.dismiss();
                timer.cancel();
            }
        });
        btnPassToGold.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogReloadWaitTime.dismiss();
                timer.cancel();
                DialogStore dialogStore = new DialogStore(mainActivity,1);
                dialogStore.show(mainActivity.mFragmentManager,"DialogStore.TAG");
                //mainActivity.buyMonth();
            }
        });
        btnReloadWallas.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogReloadWaitTime.dismiss();
                timer.cancel();
                DialogStore dialogStore = new DialogStore(mainActivity,0);
                dialogStore.show(mainActivity.mFragmentManager,"DialogStore.TAG");
                //mainActivity.buyReloadWallas();

            }
        });

        dialogReloadWaitTime.show();
    }

    public class CounterClass extends CountDownTimer
    {
        public TextView txtTime;
        public Dialog dialog;
        public CounterClass(long millisInFuture, long countDownInterval,TextView txtTime,Dialog dialog)
        {
            super(millisInFuture, countDownInterval);
            this.txtTime = txtTime;
            this.dialog  = dialog;
        }
        @Override public void onFinish()
        {
            txtTime.setText("00:00:00");
            if(dialog != null)
                dialog.dismiss();

            mainActivity.setCredits("",Constant.RELOAD_CREDITS_TIME);
           // getCurrentLikes(dialog);
        }
        @Override public void onTick(long millisUntilFinished)
        {
            long millis = millisUntilFinished;
            String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis),
                    TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                    TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
            //System.out.println(hms);
            txtTime.setText(hms);
        }
    }

    /**
     * Metodo encargado de obtener el tiempo de espera en mili segundos para recargar wallas
     */
    public void serviceGetWaitTime()
    {

            enableButtonsGame(false);

            OkHttpClient client = new OkHttpClient();

            RequestBody formBody = new FormBody.Builder()
                    .add("sessionToken", "" + GlobalData.get(Constant.SESSION_TOKEN, mainActivity))
                    .add("serviceId", Constant.TIME_RELOAD_CREDITS)
                    .build();

            Request request = new Request.Builder()
                    .url(Services.GET_WAIT_TIME)
                    .post(formBody)
                    .build();

            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {

                    Looper.prepare();
                    //Toast.makeText(mainActivity, "Error favorito", Toast.LENGTH_SHORT).show();
                    mainActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            enableButtonsGame(true);
                            Common.errorMessage(mainActivity, "", mainActivity.getResources().getString(R.string.check_internet));
                        }
                    });
                    Looper.loop();
                }

                @Override
                public void onResponse(Call call, final Response response) throws IOException {

                    final String result = response.body().string();
                    // Log.d("app2you", "respuesta  getTime: " + result);
                    Looper.prepare();
                    if (response.isSuccessful()) {


                        mainActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                //apago el dialogo
                                try {
                                    enableButtonsGame(true);
                                    JSONObject json = new JSONObject(result);
                                    String success = json.getString("success");
                                    if (success.equals("true")) {
                                        int timeMillis = json.getInt("time");
                                        openDialogReloadWallas(timeMillis);
                                    } else {
                                        Common.errorMessage(mainActivity, "", mainActivity.getResources().getString(R.string.check_internet));
                                    }

                                } catch (JSONException ex) {
                                    ex.printStackTrace();
                                    Toast.makeText(mainActivity, R.string.error_reponse_server, Toast.LENGTH_SHORT).show();
                                } finally {
                                    response.body().close();
                                }
                            }
                        });


                    } else
                        Toast.makeText(mainActivity, R.string.check_internet, Toast.LENGTH_SHORT).show();

                    Looper.loop();
                }
            });

    }


    /**
     * Método encargado de habilitar o deshabilitar los botones del juego
     * @param enable
     */
    public void enableButtonsGame(boolean enable)
    {
        mainActivity.likeButton.setEnabled(enable);
        mainActivity.dislikeButton.setEnabled(enable);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
            logicGoogleGps();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    /**
     * Conectar con el servicio de localización Google
     */
    public void connectGoogle() {

        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.M){
            // Do something for M and above versions
            if (ActivityCompat.checkSelfPermission(mainActivity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mainActivity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions( mainActivity, new String[] {  android.Manifest.permission.ACCESS_COARSE_LOCATION,android.Manifest.permission.ACCESS_FINE_LOCATION  },MY_PERMISSIONS_REQUEST_READ_GPS);
            }
            else
            {
                mGoogleApiClient = new GoogleApiClient.Builder(mainActivity)
                        .addApi(LocationServices.API)
                        .addConnectionCallbacks(this)
                        .addOnConnectionFailedListener(this)
                        .build();

                mGoogleApiClient.connect();

                if(mGoogleApiClient != null)
                    LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            }
        } else{
            mGoogleApiClient = new GoogleApiClient.Builder(mainActivity)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .build();

            mGoogleApiClient.connect();

            if(mGoogleApiClient != null)
                LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        }
    }

    /**
     * Método encargado de obtener los coordenadas del dispositivo
     */
    public void logicGoogleGps()
    {
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.M){
            if (ActivityCompat.checkSelfPermission(mainActivity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mainActivity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions( mainActivity, new String[] {  android.Manifest.permission.ACCESS_COARSE_LOCATION,android.Manifest.permission.ACCESS_FINE_LOCATION  },MY_PERMISSIONS_REQUEST_GET_LOCATION);
            }
            else
            {
                Location location = null;
                if(mGoogleApiClient != null)
                    location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

                if (location == null) {
                    Log.d("app2you", "location null");
                    gps = new GPSTracker(mainActivity);
                    if (!gps.canGetLocation()) {
                        noFoundProfiles();
                        //gps.showSettingsAlert();
                    }
                    else {
                        noProfiles();
                    }

                    if(mGoogleApiClient != null && mGoogleApiClient.isConnected())
                        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);

                } else {

                    getPositionUser(location.getLatitude(),location.getLongitude());
                    GlobalData.set(Constant.LATITUD_USER,""+location.getLatitude(),mainActivity);
                    GlobalData.set(Constant.LONGITUD_USER,""+location.getLongitude(),mainActivity);

                }
            }
        }
        else {
            Location location = null;

            if(mGoogleApiClient != null && mGoogleApiClient.isConnected())
                location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

            if (location == null) {
                Log.d("app2you", "location null");
                gps = new GPSTracker(mainActivity);
                if (!gps.canGetLocation()) {
                   noFoundProfiles();
                    //gps.showSettingsAlert();
                }
                else
                {
                    noProfiles();

                }

                if(mGoogleApiClient != null && mGoogleApiClient.isConnected())
                    LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);

            } else {

                getPositionUser(location.getLatitude(),location.getLongitude());
                GlobalData.set(Constant.LATITUD_USER,""+location.getLatitude(),mainActivity);
                GlobalData.set(Constant.LONGITUD_USER,""+location.getLongitude(),mainActivity);
                //findMatch();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        try {
            switch (requestCode) {

                case MY_PERMISSIONS_REQUEST_READ_GPS: {
                    // If request is cancelled, the result arrays are empty.
                    if (grantResults.length > 0
                            && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        connectGoogle();
                        // permission was granted, yay! Do the
                        // contacts-related task you need to do.

                    } else {

                        // permission denied, boo! Disable the
                        // functionality that depends on this permission.
                        Common.errorMessage(mainActivity,"Hola "+Common.getNickName(mainActivity),"Para encontrar personas cerca de tí, es necesario tener los permisos de ubicación");
                    }
                    return;
                }
                case MY_PERMISSIONS_REQUEST_GET_LOCATION: {
                    // If request is cancelled, the result arrays are empty.
                    if (grantResults.length > 0
                            && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                        logicGoogleGps();

                    } else {

                        // permission denied, boo! Disable the
                        // functionality that depends on this permission.
                        Common.errorMessage(mainActivity,"Hola "+Common.getNickName(mainActivity),"Para encontrar personas cerca de tí, es necesario tener los permisos de ubicación");
                    }
                    return;
                }


                // other 'case' lines to check for other
                // permissions this app might request
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            Toast.makeText(mainActivity, R.string.try_again, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mGoogleApiClient != null) {

            if(mGoogleApiClient.isConnected()) {
                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
                mGoogleApiClient.disconnect();
            }
        }
        if(alertDialog != null && alertDialog.isShowing())
            alertDialog.dismiss();

        if(dialogReloadWaitTime != null && dialogReloadWaitTime.isShowing())
            dialogReloadWaitTime.dismiss();

        if(MachedataList != null && MachedataList.size() > 0)
            GlobalData.set(Constant.CARDS, new Gson().toJson(MachedataList), mainActivity);
    }

    /**
     * Método que indica que no se encontraron perfiles para jugar
     */
    public void noFoundProfiles()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(mainActivity);

        // Setting Dialog Title
        builder.setTitle(""+mainActivity.getResources().getString(R.string.title_gps));

        // Setting Dialog Message
        builder.setMessage(""+mainActivity.getResources().getString(R.string.gps_no_found));

        // On pressing Settings button
        builder.setPositiveButton(mainActivity.getResources().getString(R.string.go_configuration_gps), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
                dialog.cancel();
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                mainActivity.startActivity(intent);

            }
        });

        // on pressing cancel button
        builder.setNegativeButton(mainActivity.getResources().getString(R.string.cancel_normal), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                Visibility.visible(containerNoFoundMatches);

                if(!GlobalData.isKey(Constant.LATITUD_USER,mainActivity)) {
                    AppAnimation.fadeOut(avloadingIndicatorView, 300);
                    Visibility.visible(imgRadarWallamatch);
                    AppAnimation.fadeIn(imgRadarWallamatch, 300);
                    txtSearchPeople.setText("Debes encender el GPS para poder encontrar perfiles, cuando lo enciendas toca sobre el pin.");
                    txtSearchPeople.setTextColor(Color.parseColor("#e2156c"));
                    AppAnimation.fadeIn(txtSearchPeople, 400);
                }
                else
                    findMatch();

            }
        });

        // Showing Alert Message
        alertDialog   = builder.create();
        alertDialog.show();

    }

    /**
     * Método encargado de intentar conectarse y obtener coordenadas para encontrar nuevos perfiles
     */
    public void noProfiles()
    {
        txtSearchPeople.setText(mainActivity.getResources().getString(R.string.find_new_people));
        txtSearchPeople.setTextColor(Color.parseColor("#c3c3c3"));
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mLocationRequest = LocationRequest.create()
                        .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                        .setInterval(5000)        // 5 seconds, in milliseconds
                        .setFastestInterval(1000); // 1 second, in milliseconds
                connectGoogle();
            }
        },1000);

    }

    /**
     * Obtener info de de la posicion de la persona
     * @param latAsync
     * @param lonAsync
     */
    public void getPositionUser(double latAsync, double lonAsync) {


            Log.d("app2you", "values gps: " + latAsync + " lon: " + lonAsync);
            if (latAsync != 0.0) {
                double latOld = GlobalData.isKey(Constant.LATITUD_USER,mainActivity)?Double.parseDouble(GlobalData.get(Constant.LATITUD_USER,mainActivity)):0.0;
                double lonOld = GlobalData.isKey(Constant.LONGITUD_USER,mainActivity)?Double.parseDouble(GlobalData.get(Constant.LONGITUD_USER,mainActivity)):0.0;

                Location locationOld = new Location("One");
                locationOld.setLatitude(latOld);
                locationOld.setLongitude(lonOld);

                Location locationNew = new Location("Two");
                locationNew.setLatitude(latAsync);
                locationNew.setLongitude(lonAsync);

                float distance = locationOld.distanceTo(locationNew);

                if (distance >= 5000) {

                    GlobalData.set(Constant.LATITUD_USER, "" + latAsync ,mainActivity);
                    GlobalData.set(Constant.LONGITUD_USER, "" + lonAsync, mainActivity);
                    getDataLocationPerson(latAsync,lonAsync);

                }
                else
                {
                    Log.d("app2you", "no ha pasado los 5000");
                    if(GlobalData.isKey(Constant.CITY_USER,mainActivity))
                        findMatch();
                    else
                        getDataLocationPerson(latAsync,lonAsync);
                }
            }


    }

    /**
     * Método que ejecuta la tarea en background, para recolectar la información de ubicación del dispositivo.
     * @param latAsync
     * @param lonAsync
     */
    private void getDataLocationPerson(double latAsync, double lonAsync) {

        new AsyncTaskLocationData(latAsync, lonAsync).execute();
    }

    /**
     * Método encargado de traer los datos de posición desde el servicio web
     * @param lat
     * @param lng
     * @return
     */
    private JSONObject getLocationInfo(double lat, double lng) {

        HttpGet httpGet = new HttpGet(
                "https://maps.googleapis.com/maps/api/geocode/json?key="+mainActivity.getString(R.string.key_geocoding)+"&latlng="
                        + lat + "," + lng + "&sensor=false");

        final HttpParams httpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParams, 12000);
        HttpClient client = new DefaultHttpClient(httpParams);

        HttpResponse response;
        StringBuilder stringBuilder = new StringBuilder();

        try {
            response = client.execute(httpGet);
            HttpEntity entity = response.getEntity();
            InputStream stream = entity.getContent();
            /*int b;
            while ((b = stream.read()) != -1)
                stringBuilder.append((char) b);*/
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(stream, "UTF-8"), 8);
            //StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line);
            }

        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject = new JSONObject(stringBuilder.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject;
    }

    /**
     * Clase para obtener localización, país, ciudad, localidad
     */
    public class AsyncTaskLocationData extends AsyncTask<String, String, String>
    {
        double latAsync;
        double lonAsync;
        public AsyncTaskLocationData(double latAsync, double lonAsync)
        {
            this.latAsync = latAsync;
            this.lonAsync = lonAsync;
        }
        @Override
        protected String doInBackground(String... params) {
            String jsonStr = getLocationInfo(latAsync, lonAsync).toString();
            return jsonStr;
        }

        @Override
        protected void onPostExecute(String jsonStr) {
            super.onPostExecute(jsonStr);

            if (jsonStr != null) {

                JSONObject jsonObj;
                try {
                    jsonObj = new JSONObject(jsonStr);
                    Log.d("app2you","json: " + jsonStr);
                    String Status = jsonObj.getString("status");
                    if (Status.equalsIgnoreCase("OK")) {
                        JSONArray Results = jsonObj.getJSONArray("results");
                        JSONObject zero = Results.getJSONObject(0);
                        JSONArray address_components = zero
                                .getJSONArray("address_components");

                        for (int i = 0, len = address_components.length(); i < len; ++i) {
                            JSONObject zero2 = address_components
                                    .getJSONObject(i);
                            String long_name = zero2.getString("long_name");
                            String short_name = zero2.getString("short_name");
                            JSONArray mtypes = zero2.getJSONArray("types");
                            String type = mtypes.getString(0);

                            if (type.equalsIgnoreCase("locality")) {
                                // Address2 = Address2 + long_name + ", ";
                                String city = long_name;

                                GlobalData.set(Constant.CITY_USER, "" + city, mainActivity);
                                //Log.d("app2you", "city async " + city );
                            }

                            if (type.equalsIgnoreCase("country")) {
                                // Address2 = Address2 + long_name + ", ";
                                String codeCountry = short_name;
                                GlobalData.set(Constant.COUNTRY_USER, "" + codeCountry, mainActivity);
                                // Log.d("app2you", "pais async " + codeCountry );
                            }
                            /*if(type.equalsIgnoreCase("postal_code"))
                            {
                                String postal_code = long_name;
                                GlobalData.set(Constant.POSTAL_CODE, "" + postal_code, IntroActivity.this);
                                Log.d("app2you", "postal code async " + postal_code );
                            }*/
                            if(type.equalsIgnoreCase("administrative_area_level_2"))
                            {
                                String province = long_name;

                                GlobalData.set(Constant.PROVINCE, "" + province, mainActivity);
                                //  Log.d("app2you", "province async " + province );
                            }

                        }

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            findMatch();

        }
    }

}
