package com.android.slidingmenu.fragments;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.slidingmenu.Analitycs.MyApplication;
import com.android.slidingmenu.MainActivity;
import com.android.slidingmenu.activities.ViewContent;
import com.android.slidingmenu.adapter.GeneralAdapter;
import com.android.slidingmenu.adapter.ReportAdapter;
import com.android.slidingmenu.objects.UserDetail;
import com.android.slidingmenu.reports.Report;
import com.android.slidingmenu.util.AppAnimation;
import com.android.slidingmenu.util.Common;
import com.android.slidingmenu.util.GlobalData;
import com.android.slidingmenu.util.TouchImageView;
import com.android.slidingmenu.util.TypesLetter;
import com.android.slidingmenu.util.Visibility;
import com.android.slidingmenu.webservices.Services;
import com.appdupe.androidpushnotifications.ChatActivity;
import com.appdupe.flamer.pojo.MatchesData;
import com.appdupe.flamer.pojo.PictureData;
import com.appdupe.flamer.utility.Constant;
import com.appyvet.rangebar.RangeBar;
import com.google.gson.Gson;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.squareup.picasso.Picasso;
import com.viewpagerindicator.CirclePageIndicator;
import com.loovers.app.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;



/**
 * Created by Juan Martin Bernal on 29/12/15.
 */
public class DetailProfileFragment extends DialogFragment
{
    public MainActivity mainActivity;
    public MatchesData data;
    public View viewDetail;
    public ImageView fire_detail_match, imgFavorite,imgReport,btn_chat;
    public TextView name, city,personalStatus, txtNumberDulce, txtNumberApasionada,txtNumberAtrevida, txtNumberTraviesa, txtDulce, txtApasionada, txtAtrevida, txtTraviesa;
    public Button btnBack;
    public RelativeLayout containerButtonBack,containerActionBarTop,containerGalleryPhotos;
    public RangeBar rangeDulce, rangeApasionada,rangeAtrevida, rangeTraviesa;
    public ProgressBar progressBarLoadImageDetail;
    public LinearLayout containerDescription;
    public int type = 0;
    public static final int FAVORITE = 1;
    public static final int NO_FAVORITE = 2;
    public GeneralAdapter generalAdapter;
    public int option;
    public ArrayList<PictureData> gimgs = new ArrayList<>();
    public ArrayList<PictureData> pimgsDetail = new ArrayList<>();
    public ArrayList<PictureData> all_photos = new ArrayList<>();
    public ProgressWheel progressBarLoadImage;

    public boolean statusMatch = false;
    //public boolean statusWallas = false;
    public int chatStatusId = -1;
    public ReportAdapter reportAdapter;
    public ViewPager viewpagerUserProfile;


    //Constructor
    public DetailProfileFragment()
    {}

    /**
     *
     * @param mainActivity
     */
    public DetailProfileFragment(MainActivity mainActivity, MatchesData data)
    {
        this.mainActivity = mainActivity;
        this.data       = data;
    }

    /**
     *
     * @param mainActivity
     */
    public DetailProfileFragment(MainActivity mainActivity, MatchesData data,int type)
    {
        this.mainActivity = mainActivity;
        this.data       = data;
        this.type       = type;
    }
    /**
     *
     * @param mainActivity
     */
    public DetailProfileFragment(MainActivity mainActivity, MatchesData data,int type,GeneralAdapter generalAdapter)
    {
        this.mainActivity   = mainActivity;
        this.data           = data;
        this.type           = type;
        this.generalAdapter = generalAdapter;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       setStyle(DialogFragment.STYLE_NO_FRAME, android.R.style.Theme_Holo_Light_DarkActionBar);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        viewDetail = inflater.inflate(R.layout.detail_profile_fragment,container,false);
        if(mainActivity == null)
            mainActivity = (MainActivity)getActivity();

        initComponents();


        return viewDetail;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if(data != null)
             getDataProfile(data.userId);

        switch (type)
        {

            case Constant.WALLAS:
                Visibility.gone(fire_detail_match);
               // btn_chat.setAlpha(0.5f);
                //btn_chat.setEnabled(false);
                setDataFavorite();
                break;
            case Constant.FAVORITE:
                Visibility.gone(fire_detail_match);
                setPropertiesFavorite();
              //  btn_chat.setAlpha(0.5f);
                //btn_chat.setEnabled(false);
                containerActionBarTop.setBackgroundResource(R.drawable.cabecera_roja);
                imgFavorite.setImageResource(R.drawable.corazon_on);
                option = NO_FAVORITE;
                break;
            case Constant.MATCH:
                Visibility.visible(fire_detail_match);

                btn_chat.setEnabled(true);

               // btn_walla.setAlpha(0.5f);
                //Visibility.invisible(btn_walla);
                setDataFavorite();
                break;
            case Constant.GAME:
                Visibility.gone(fire_detail_match);
               // btn_chat.setAlpha(0.5f);
                setDataFavorite();
                break;
            case Constant.LIST_PERSONS:
                Visibility.gone(fire_detail_match);
                // btn_chat.setAlpha(0.5f);
                setDataFavorite();
                break;
            default:
                break;

        }

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validateFavoriteType();
            }
        });
        containerButtonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validateFavoriteType();
            }
        });



        btn_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if(type == Constant.MATCH || statusMatch) {
                    if(data != null) {
                        ChatActivity dialog = new ChatActivity(mainActivity, data,Constant.MATCH,DetailProfileFragment.this);
                        dialog.show(mainActivity.mFragmentManager, "ChatActivity.TAG");
                    }
                    else
                        Toast.makeText(mainActivity, R.string.try_again, Toast.LENGTH_SHORT).show();

                }
                else
                {
                    //customDialogWallas();
                        String gender = GlobalData.get(Constant.GENERE,mainActivity);
                        if(data.requestToMe)
                        {
                            //ME HICIERÓN LA PETICIÓN
                            switch (chatStatusId) {
                                case Constant.NOT_REQUEST_CHAT:
                                    openDirectChat();
                                    break;
                                case Constant.REQUEST_CHAT_PENDING:
                                    openDirectChat();
                                    break;
                                case Constant.ACCEPT_CHAT_DIRECT:
                                    openDirectChat();
                                    break;
                                case Constant.REJECT_CHAT_DIRECT:
                                    Common.errorMessage(mainActivity, "", (data.meRejected)?""+mainActivity.getString(R.string.you_rejected_chat):""+mainActivity.getString(R.string.request_rejected));
                                    break;
                                default:
                                    customDialogWallas();
                                    break;
                            }

                        }
                        else {
                            switch (chatStatusId) {
                                case Constant.NOT_REQUEST_CHAT:
                                    openDirectChat();
                                    break;
                                case Constant.REQUEST_CHAT_PENDING:

                                    String messageRequest = gender.equals(Constant.MAN) ? "Tu mensaje fue enviado a " + data.firstName + ", debes esperar a que responda, para poder hablar con ella." : "Tu mensaje fue enviado a " + data.firstName + ", debes esperar a que responda, para poder hablar con él.";
                                    Common.errorMessage(mainActivity, "", messageRequest);
                                    break;
                                case Constant.ACCEPT_CHAT_DIRECT:
                                    openDirectChat();
                                    break;
                                case Constant.REJECT_CHAT_DIRECT:
                                    Common.errorMessage(mainActivity, "", (data.meRejected)?""+mainActivity.getString(R.string.you_rejected_chat):""+mainActivity.getString(R.string.request_rejected));
                                    break;
                                default:
                                    customDialogWallas();
                                    break;
                            }
                        }
                }
                /*else if(type == Constant.WALLAS) {
                   // Common.errorMessage(mainActivity, "", mainActivity.getResources().getString(R.string.message_can_not_chat) + " " + data.firstName + " " + mainActivity.getResources().getString(R.string.message_can_not_chat2));
                    customDialogWallas();
                }
                else if(statusWallas)
                    Common.errorMessage(mainActivity,"",mainActivity.getResources().getString(R.string.message_can_not_chat)+" "+data.firstName + " "+ mainActivity.getResources().getString(R.string.message_can_not_chat2));
                else if(type == Constant.GAME || type == Constant.FAVORITE)
                    Common.errorMessage(mainActivity,"",mainActivity.getResources().getString(R.string.message_can_not_chat)+" "+data.firstName + " "+ mainActivity.getResources().getString(R.string.message_can_not_chat_from_game));
*/
            }
        });

        imgFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Todo: hacer favorite
                imgFavorite.setEnabled(false);
                if (option == FAVORITE) {
                    if(!GlobalData.isKey(Constant.FIRST_FAVORITE,mainActivity)) {
                        GlobalData.set(Constant.FIRST_FAVORITE,"Y", mainActivity);
                        Common.errorMessage(mainActivity,"",mainActivity.getResources().getString(R.string.message_add_favorite_first_time) +" " + data.firstName+ " "+mainActivity.getResources().getString(R.string.message_add_favorite_first_time2));
                        setPropertiesFavorite();
                    }
                    else
                        setPropertiesFavorite();
                }
                else {
                    if(!GlobalData.isKey(Constant.DELETE_FIRST_FAVORITE,mainActivity)) {
                        GlobalData.set(Constant.DELETE_FIRST_FAVORITE,"Y", mainActivity);
                        Common.errorMessage(mainActivity,"",mainActivity.getResources().getString(R.string.message_delete_favorite_first_time) + " " + data.firstName +"!"+ "\n"+mainActivity.getResources().getString(R.string.message_delete_favorite_first_time2));
                        setPropertiesNoFavorite();
                    } else
                        setPropertiesNoFavorite();
                }

                if (data != null) {
                    //Log.d("app2you","converProfileFavorite: "+option);
                    converProfileFavorite(data.userId, "" + option, imgFavorite);
                }
            }
        });

        imgReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //abrir dialog de denuncia
                openDialogReport(data.userId);
            }
        });

    }

    /**
     * Método encargado de abrir chat directo
     */
    public void openDirectChat()
    {
        /*Match match         = new Match();
        match.username      = data.firstName;
        match.idUser        = data.userId;
        match.picture       = data.avatarUrl;
        match.chatStatusId  = chatStatusId;*/
        ChatActivity dialog = new ChatActivity(mainActivity, data,Constant.DIRECT_CHAT);
        dialog.show(mainActivity.mFragmentManager, "ChatActivity.TAG");
    }
    /**
     * Dialogo que indica al usuario que debe dar un Walla
     */
    private void customDialogWallas() {

        final Dialog dgl =  new Dialog(mainActivity,R.style.PauseDialog);
        dgl.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
        dgl.setCancelable(false);
        dgl.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        dgl.setContentView(R.layout.modal_general);


        TextView title = (TextView)dgl.findViewById(R.id.title);
        title.setText("Enviar Mensaje");
        TextView text = (TextView) dgl.findViewById(R.id.text);
        text.setText("Para enviarle un mensaje a " + data.firstName+" envíale un Walla y espera a ver si te quiere conocer.");


        Button exit = (Button) dgl.findViewById(R.id.exit);
        exit.setText(""+mainActivity.getResources().getString(R.string.cancel));
        Button accept = (Button) dgl.findViewById(R.id.accept);
        accept.setText("ENVIAR WALLA");
        // if button is clicked, close the custom dialog

        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dgl.dismiss();
            }
        });
        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dgl.dismiss();
                dismiss();
                Animation anim = AnimationUtils.loadAnimation(mainActivity,R.anim.scale_cake);
                mainActivity.likeButton.startAnimation(anim);


            }
        });

        dgl.show();
    }

    /**
     * Abrir cuadro de dialogo con las opciones para reportar usuario
     * @param userId
     */
    private void openDialogReport(final String userId) {

        final Dialog dialog = new Dialog(mainActivity,R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_report_user);

        TextView txtTitleReport = (TextView) dialog.findViewById(R.id.txtTitleReport);
        final ListView listReport     = (ListView)dialog.findViewById(R.id.listReport);
        txtTitleReport.setTypeface(TypesLetter.getSansBold(mainActivity));

        if(reportAdapter != null)
            listReport.setAdapter(reportAdapter);

        listReport.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final ProgressBar progressBar = (ProgressBar)view.findViewById(R.id.progressBarReportUser);
                Visibility.visible(progressBar);

                final Report report = (Report)listReport.getItemAtPosition(position);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        serviceReportUser(userId, report.idCategory,progressBar,dialog);
                    }
                },200);

            }
        });

        dialog.show();
    }

    /**
     * Metodo encargado de abrir el dialogo de confirmación despues de enviar reporte
     */
    public void openDialogConfirmReport(String message)
    {
        final Dialog dialog = new Dialog(mainActivity, R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_confirm_report);

        TextView txtTitleReport = (TextView)dialog.findViewById(R.id.txtTitleReport);
        txtTitleReport.setTypeface(TypesLetter.getSansRegular(mainActivity));
        txtTitleReport.setText(""+message);
        String status = ""+GlobalData.get(Constant.GENERE,mainActivity);
        Button btnClose = (Button)dialog.findViewById(R.id.btnClose);
        ImageView  imgReportPerson = (ImageView)dialog.findViewById(R.id.imgReportPerson);
        imgReportPerson.setImageResource(status.equals(Constant.MAN)?R.drawable.denunciado_chica:R.drawable.denunciado_chico);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }


    /**
     * Llamar al servidor para reportar el usuario
     * @param userId
     * @param categoryId
     * @param progressBar
     */
    public void serviceReportUser(String userId, int categoryId,final ProgressBar progressBar, final Dialog dialog)
    {
        OkHttpClient client = new OkHttpClient();

        RequestBody formBody = new FormBody.Builder()
                .add("sessionToken", "" + GlobalData.get(Constant.SESSION_TOKEN, mainActivity))
                .add("otherUserId", "" + userId)
                .add("categoryId",""+categoryId)
                .build();

        Request request = new Request.Builder()
                .url(Services.REPORT_USER)
                .post(formBody)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

                Looper.prepare();
                mainActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Visibility.gone(progressBar);
                        Common.errorMessage(mainActivity,"",mainActivity.getResources().getString(R.string.check_internet));
                    }
                });
                Looper.loop();
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {

                final String result = response.body().string();
                Log.d("app2you", "respuesta  report user: " + result);
                Looper.prepare();
                if (response.isSuccessful()) {


                    mainActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //apago el dialogo
                            try {
                                dialog.dismiss();
                                Visibility.gone(progressBar);

                                JSONObject json = new JSONObject(result);
                                String success = json.getString("success");
                                if (success.equals("true")) {
                                    openDialogConfirmReport("" + mainActivity.getResources().getString(R.string.report_receiver));
                                } else {
                                    openDialogConfirmReport("Ya ha sido reportado.");
                                }

                            } catch (JSONException ex) {
                                ex.printStackTrace();

                                openDialogConfirmReport("" + mainActivity.getResources().getString(R.string.report_receiver));
                                //Toast.makeText(mainActivity, R.string.try_again, Toast.LENGTH_SHORT).show();

                            } finally {
                                response.body().close();
                            }
                        }
                    });

                }
                else
                    Toast.makeText(mainActivity, R.string.check_internet, Toast.LENGTH_SHORT).show();

                Looper.loop();
            }
        });
    }

    private void setDataFavorite() {
        if(data.isFavorite == 1) {
            setPropertiesFavorite();
            option = NO_FAVORITE;
        }
        else {
            setPropertiesNoFavorite();
            option = FAVORITE;
        }
    }

    /**
     * Metodo encargado de validar si al abrir detalles se hizo desde favoritos
     */
    public void validateFavoriteType()
    {
        dismiss();
    }

    /**
     * propiedades no favoritos
     */
    public void setPropertiesNoFavorite()
    {
        containerActionBarTop.setBackgroundResource(R.drawable.cabecera_blanca);
        btnBack.setBackgroundResource(R.drawable.arrow_back_gray);
        name.setTextColor(Color.parseColor("#e2156c"));
        imgFavorite.setImageResource(R.drawable.corazon_off);



    }

    /**
     * propiedades favoritos
     */
    public void setPropertiesFavorite()
    {
        containerActionBarTop.setBackgroundResource(R.drawable.cabecera_roja);
        btnBack.setBackgroundResource(R.drawable.arrow_back);
        name.setTextColor(Color.WHITE);
        city.setTextColor(Color.parseColor("#c3c3c3"));
        imgFavorite.setImageResource(R.drawable.corazon_on);

    }

    /**
     * Inicializo componentes de la GUI
     */
    public void initComponents()
    {

        fire_detail_match               = (ImageView)viewDetail.findViewById(R.id.fire_detail_match);
        imgFavorite                     = (ImageView)viewDetail.findViewById(R.id.imgFavorite);
        imgReport                       = (ImageView)viewDetail.findViewById(R.id.imgReport);
        Visibility.gone(imgReport);
        imgFavorite.setEnabled(false);

        name                            = (TextView)viewDetail.findViewById(R.id.name_user);
        city                            = (TextView)viewDetail.findViewById(R.id.city);
        personalStatus                  = (TextView)viewDetail.findViewById(R.id.personalStatus);
        txtDulce                        = (TextView)viewDetail.findViewById(R.id.txtDulce);
        txtApasionada                   = (TextView)viewDetail.findViewById(R.id.txtApasionada);
        txtAtrevida                     = (TextView)viewDetail.findViewById(R.id.txtAtrevida);
        txtTraviesa                     = (TextView)viewDetail.findViewById(R.id.txtTraviesa);
        btnBack                         = (Button)viewDetail.findViewById(R.id.btn_back);
        btn_chat                        = (ImageView) viewDetail.findViewById(R.id.btn_chat);
        containerActionBarTop           = (RelativeLayout)viewDetail.findViewById(R.id.containerActionBarTop);
        containerGalleryPhotos          = (RelativeLayout)viewDetail.findViewById(R.id.containerGalleryPhotos);

        viewpagerUserProfile            = (ViewPager) viewDetail.findViewById(R.id.viewpagerUserProfile);
        progressBarLoadImage            = (ProgressWheel) viewDetail.findViewById(R.id.progressBarLoadImage);

        name.setTypeface(TypesLetter.getSansRegular(mainActivity));
        city.setTypeface(TypesLetter.getSansRegular(mainActivity));
        personalStatus.setTypeface(TypesLetter.getSansRegular(mainActivity));

        txtDulce.setTypeface(TypesLetter.getSansRegular(mainActivity));
        txtApasionada.setTypeface(TypesLetter.getSansRegular(mainActivity));
        txtAtrevida.setTypeface(TypesLetter.getSansRegular(mainActivity));
        txtTraviesa.setTypeface(TypesLetter.getSansRegular(mainActivity));

        containerButtonBack         = (RelativeLayout)viewDetail.findViewById(R.id.containerButtonBack);
        rangeDulce                  = (RangeBar)viewDetail.findViewById(R.id.rangebar_dulce);
        rangeApasionada             = (RangeBar)viewDetail.findViewById(R.id.rangebar_apasionada);
        rangeAtrevida               = (RangeBar)viewDetail.findViewById(R.id.rangebar_atrevida);
        rangeTraviesa               = (RangeBar)viewDetail.findViewById(R.id.rangebar_traviesa);
        containerDescription        = (LinearLayout)viewDetail.findViewById(R.id.containerDescription);
        progressBarLoadImageDetail  = (ProgressBar)viewDetail.findViewById(R.id.progressBarLoadImageDetail);
        txtNumberDulce              = (TextView)viewDetail.findViewById(R.id.txtNumberDulce);
        txtNumberApasionada         = (TextView)viewDetail.findViewById(R.id.txtNumberApasionada);
        txtNumberAtrevida           = (TextView)viewDetail.findViewById(R.id.txtNumberAtrevida);
        txtNumberTraviesa           = (TextView)viewDetail.findViewById(R.id.txtNumberTraviesa);

        rangeDulce.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
        rangeApasionada.setOnTouchListener(new View.OnTouchListener(){
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
        rangeAtrevida.setOnTouchListener(new View.OnTouchListener(){
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
        rangeTraviesa.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
    }

    /**
     * Metodo encargado de pedir los datos del usuario al servidor
     * @param idUser
     */
    public void getDataProfile(String idUser)
    {
        Visibility.visible(progressBarLoadImage);
        OkHttpClient client = new OkHttpClient();

        RequestBody formBody = new FormBody.Builder()
                .add("sessionToken", "" + GlobalData.get(Constant.SESSION_TOKEN, mainActivity))
                .add("idOtherUser", "" + idUser)
                .build();

        Request request = new Request.Builder()
                .url(Services.GET_PROFILE)
                .post(formBody)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

                Looper.prepare();
                mainActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Common.errorMessage(mainActivity,"",mainActivity.getResources().getString(R.string.check_internet));
                    }
                });
                Looper.loop();
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {

                final String result = response.body().string();
                Log.d("app2you", "data profile: " + result);
                Looper.prepare();
                if(response.isSuccessful()) {

                    mainActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            try {
                                Visibility.gone(progressBarLoadImage);
                                Gson gson = new Gson();
                                UserDetail userDetail = gson.fromJson(result, UserDetail.class);

                                if (userDetail.success) {

                                    chatStatusId = userDetail.chatStatusId;
                                    //se actualiza el estado de peticion de chat de un usuario
                                    data.age = Integer.parseInt(userDetail.age);
                                    data.chatStatusId = chatStatusId;
                                    if (generalAdapter != null)
                                        generalAdapter.notifyDataSetChanged();

                                    Visibility.visible(btn_chat);
                                    AppAnimation.fadeIn(btn_chat, 200);

                                    gimgs.clear();
                                    gimgs = userDetail.gimgs;
                                    pimgsDetail.clear();
                                    pimgsDetail = userDetail.pimgs;

                                    if (gimgs != null && gimgs.size() > 0) {

                                        if (pimgsDetail.get(0) != null) {
                                            all_photos.clear();
                                            all_photos.add(0, pimgsDetail.get(0));
                                            all_photos.addAll(gimgs);
                                        }

                                        CustomPagerAdapter mCustomPagerAdapter2 = new CustomPagerAdapter(mainActivity, all_photos);
                                        viewpagerUserProfile.setAdapter(mCustomPagerAdapter2);
                                        CirclePageIndicator titleIndicator2 = (CirclePageIndicator) viewDetail.findViewById(R.id.titlesUserProfile);
                                        titleIndicator2.setViewPager(viewpagerUserProfile);
                                    } else {
                                        CustomPagerAdapter mCustomPagerAdapter2 = new CustomPagerAdapter(mainActivity, pimgsDetail);
                                        viewpagerUserProfile.setAdapter(mCustomPagerAdapter2);
                                        CirclePageIndicator titleIndicator2 = (CirclePageIndicator) viewDetail.findViewById(R.id.titlesUserProfile);
                                        titleIndicator2.setViewPager(viewpagerUserProfile);
                                    }

                                    Visibility.visible(imgReport);
                                    reportAdapter = new ReportAdapter(mainActivity, Common.loadCategoriesReport(mainActivity));

                                    if (userDetail.isMatch == 1) {
                                        Visibility.visible(fire_detail_match);
                                        statusMatch = true;
                                    } else {
                                        statusMatch = false;
                                        Visibility.gone(fire_detail_match);
                                    }
                                    //statusWallas = (userDetail.likeToMe == 1) ? true : false;


                                    //termino de traer datos, habilito las imagenes
                                    // imageUser.setEnabled(true);
                                    imgFavorite.setEnabled(true);
                                    // String fakeTmp = (data.fake == 1)?"- Fake":" - Normal";
                                    name.setText("" + userDetail.name + ", " + userDetail.age + " " + mainActivity.getResources().getString(R.string.age) + " ");
                                    city.setText(""+userDetail.city);
                                    city.setVisibility((userDetail.city != null && userDetail.city.length() > 0) ?View.VISIBLE:View.GONE);
                                    if (userDetail.isFavorite == 1)
                                        setPropertiesFavorite();
                                    else
                                        setPropertiesNoFavorite();

                                    if (!userDetail.personalStatus.isEmpty())
                                        setPersonalStatus(userDetail.personalStatus);


                                    if (userDetail.descriptionOptions != null) {
                                        JSONArray json = new JSONArray(userDetail.descriptionOptions);
                                        if (json != null) {
                                            //Log.d("app2you", "descriptionOptions:" + json.toString());

                                            int op1 = Integer.parseInt(json.getJSONObject(0).getString("value"));
                                            int op2 = Integer.parseInt(json.getJSONObject(1).getString("value"));
                                            int op3 = Integer.parseInt(json.getJSONObject(2).getString("value"));
                                            int op4 = Integer.parseInt(json.getJSONObject(3).getString("value"));
                                            Common.setPropertiesRange(rangeDulce, op1, false, true);
                                            Common.setPropertiesRange(rangeApasionada, op2, false, true);
                                            Common.setPropertiesRange(rangeAtrevida, op3, false, true);
                                            Common.setPropertiesRange(rangeTraviesa, op4, false, true);
                                            txtNumberDulce.setText("" + op1);
                                            txtNumberApasionada.setText("" + op2);
                                            txtNumberAtrevida.setText("" + op3);
                                            txtNumberTraviesa.setText("" + op4);


                                        } else {
                                            Common.setPropertiesRange(rangeDulce, 5, false, true);
                                            Common.setPropertiesRange(rangeApasionada, 6, false, true);
                                            Common.setPropertiesRange(rangeAtrevida, 4, false, true);
                                            Common.setPropertiesRange(rangeTraviesa, 3, false, true);
                                            txtNumberDulce.setText("" + 5);
                                            txtNumberApasionada.setText("" + 6);
                                            txtNumberAtrevida.setText("" + 4);
                                            txtNumberTraviesa.setText("" + 3);

                                        }
                                    } else {
                                        Common.setPropertiesRange(rangeDulce, 5, false, true);
                                        Common.setPropertiesRange(rangeApasionada, 6, false, true);
                                        Common.setPropertiesRange(rangeAtrevida, 4, false, true);
                                        Common.setPropertiesRange(rangeTraviesa, 3, false, true);
                                        txtNumberDulce.setText("" + 5);
                                        txtNumberApasionada.setText("" + 6);
                                        txtNumberAtrevida.setText("" + 4);
                                        txtNumberTraviesa.setText("" + 3);

                                    }


                                    //hacer visible el contenedor de descriptionsOptions
                                    Visibility.visible(containerDescription);

                                } else {
                                    profileNoFound(data.firstName);
                                }

                            } catch (Exception ex) {
                                ex.printStackTrace();
                                profileNoFound(data.firstName);
                                //Toast.makeText(mainActivity, R.string.error_reponse_server, Toast.LENGTH_SHORT).show();
                            }
                            finally {
                                response.body().close();
                            }
                        }
                    });


                }
                else
                    Toast.makeText(mainActivity, R.string.check_internet, Toast.LENGTH_SHORT).show();

                Looper.loop();
            }
        });
    }

    /**
     * Dialogo para informar que el perfil no fue encontrado
     * @param name
     */
    public void profileNoFound(String name)
    {
        final Dialog dialog =  new Dialog(mainActivity,R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        dialog.setContentView(R.layout.modal_general);

        TextView text = (TextView) dialog.findViewById(R.id.text);
        text.setText("Upps.. no hemos podido encontrar el perfil de " + name + ", intentalo más tarde.");


        Button exit = (Button) dialog.findViewById(R.id.exit);
        Visibility.gone(exit);
        Button accept = (Button) dialog.findViewById(R.id.accept);
        // if button is clicked, close the custom dialog
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                dismiss();


            }
        });

        dialog.show();
    }

    /**
     * Setear estado personal del usuario
     * @param status
     */
    public void setPersonalStatus(String status)
    {
        int number = 0;
        number = Integer.parseInt(status);
        String gender = GlobalData.isKey(Constant.GENERE,mainActivity)?GlobalData.get(Constant.GENERE,mainActivity):Constant.MAN;

        switch (number) {
            case Constant.CHATEAR_UN_RATO:

                personalStatus.setText(mainActivity.getResources().getString(R.string.chat));
                break;
            case Constant.INVITARME_A_SALIR:
                personalStatus.setText((gender.equals(Constant.WOMAN))?mainActivity.getResources().getString(R.string.invite_to_exit):mainActivity.getResources().getString(R.string.invite_to_dinner));
                break;
            case Constant.SALTARME_LOS_PRELIMINARES:
                personalStatus.setText(mainActivity.getResources().getString(R.string.skip_preliminaries));
                break;
            default:
                personalStatus.setText((gender.equals(Constant.WOMAN))?mainActivity.getResources().getString(R.string.invite_to_exit):mainActivity.getResources().getString(R.string.invite_to_dinner));
                break;
        }
        txtDulce.setText((gender.equals(Constant.WOMAN))?mainActivity.getResources().getString(R.string.romantic):mainActivity.getResources().getString(R.string.sweet));
        txtApasionada.setText((gender.equals(Constant.WOMAN))?mainActivity.getResources().getString(R.string.retailer):mainActivity.getResources().getString(R.string.passionate));
        txtAtrevida.setText((gender.equals(Constant.WOMAN)) ? mainActivity.getResources().getString(R.string.daring) : mainActivity.getResources().getString(R.string.bold));
        txtTraviesa.setText((gender.equals(Constant.WOMAN))?mainActivity.getResources().getString(R.string.seductive):mainActivity.getResources().getString(R.string.naughty));

        AppAnimation.fadeIn(personalStatus, 200);
    }

    /**
     * Metodo encargado de volver un perfil como favorito
     */
    public void converProfileFavorite(String userId, final String favorite, final ImageView imageHeart)
    {
        OkHttpClient client = new OkHttpClient();

        RequestBody formBody = new FormBody.Builder()
                .add("sessionToken", "" + GlobalData.get(Constant.SESSION_TOKEN, mainActivity))
                .add("otherUserId", "" + userId)
                .add("isFavorite",""+favorite)
                .build();

        Request request = new Request.Builder()
                .url(Services.SET_USER_FAVORITE)
                .post(formBody)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

                Looper.prepare();
                mainActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Common.errorMessage(mainActivity,"",mainActivity.getResources().getString(R.string.check_internet));
                    }
                });
                Looper.loop();
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {

                final String result = response.body().string();
                //Log.d("app2you", "respuesta  favorite: " + result);
                Looper.prepare();
                if(response.isSuccessful()) {


                    mainActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //apago el dialogo
                            try {
                                JSONObject json = new JSONObject(result);
                                String success = json.getString("success");
                                if (success.equals("true")) {
                                    GlobalData.delete(Constant.REQUEST_FAVORITES, mainActivity);

                                    if (favorite.equals("1"))
                                        option = NO_FAVORITE;
                                    else
                                        option = FAVORITE;
                                } else {
                                    if (favorite.equals("" + FAVORITE))
                                        setPropertiesNoFavorite();
                                    else
                                        setPropertiesFavorite();
                                    //Toast.makeText(mainActivity, "Respuesta false favorito", Toast.LENGTH_SHORT).show();
                                }
                                imageHeart.setEnabled(true);

                            } catch (Exception ex) {
                                ex.printStackTrace();
                                Toast.makeText(mainActivity, R.string.try_again, Toast.LENGTH_SHORT).show();

                            } finally {
                                response.body().close();
                            }
                        }
                    });


                }
                else
                    Toast.makeText(mainActivity, R.string.try_again, Toast.LENGTH_SHORT).show();

                Looper.loop();
            }
        });
    }

    /**
     * Clase que permite visualizar las imagenes en un carrusel
     */
    class CustomPagerAdapter extends PagerAdapter {

        Context mContext;
        LayoutInflater mLayoutInflater;
        ArrayList<PictureData> mLista = new ArrayList<PictureData>();

        public CustomPagerAdapter(Context context, ArrayList<PictureData> lista1) {

            this.mLista = lista1;

            mContext = context;
            mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return mLista.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((FrameLayout) object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, final int position) {
            View itemView = mLayoutInflater.inflate(R.layout.pic_viewpager_page, container, false);

            TouchImageView imageView = (TouchImageView) itemView.findViewById(R.id.user_gallery_detail_viewflow_item_imagen);
            imageView.setMaxZoom(4f);
            final ProgressBar progressBarLoadGallery = (ProgressBar)itemView.findViewById(R.id.progressBarLoadGallery);
            final RelativeLayout containerLock = (RelativeLayout)itemView.findViewById(R.id.containerLock);
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            Visibility.gone(containerLock);
            final PictureData pictureData = mLista.get(position);

            if(pictureData.isPrivate != null && pictureData.isPrivate.equals(Constant.PRIVATE_PHOTO))
                Visibility.visible(containerLock);

            Picasso.with(mContext).load(pictureData.imageUrl).error(R.drawable.no_foto).into(imageView, new com.squareup.picasso.Callback() {
                @Override
                public void onSuccess() {
                    Visibility.gone(progressBarLoadGallery);
                }

                @Override
                public void onError() {
                    Visibility.gone(progressBarLoadGallery);
                }
            });

            container.addView(itemView);
            //pasarlo a dialogFragment
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(pictureData.isPrivate != null && pictureData.isPrivate.equals(Constant.PRIVATE_PHOTO))
                        Common.errorMessage(mainActivity,"",mainActivity.getResources().getString(R.string.this_private_picture)+" " + data.firstName);
                    else {
                        ViewContent viewContent = new ViewContent(mainActivity, mLista, position,1);
                        viewContent.show(mainActivity.mFragmentManager, "ViewContent.TAG");
                    }
                }
            });
            return itemView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((FrameLayout) object);
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        MyApplication.getInstance().trackScreenView("Detail fragment");
    }
}
