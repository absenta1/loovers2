package com.android.slidingmenu.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.android.slidingmenu.Analitycs.MyApplication;
import com.android.slidingmenu.MainActivity;
import com.android.slidingmenu.util.Visibility;
import com.loovers.app.R;
import com.wang.avi.AVLoadingIndicatorView;


/**
 * Created by Juan Martin Bernal on 12/2/16.
 */
public class FragmentPrivacity extends DialogFragment implements View.OnClickListener{

    public View view;

    public MainActivity mainActivity;

    public LinearLayout containerOptionsBackChat;
    public ImageView imgBack;
    public AVLoadingIndicatorView progressBarWebview;
    WebView wv;

    public FragmentPrivacity(){}

    public FragmentPrivacity(MainActivity mainActivity)
    {
        this.mainActivity     = mainActivity;

    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.layout_privacity,container,false);
        if(mainActivity == null)
            mainActivity = (MainActivity)getActivity();

        containerOptionsBackChat    = (LinearLayout) view.findViewById(R.id.containerOptionsBackChat);
        imgBack                     = (ImageView) view.findViewById(R.id.imgBack);
        progressBarWebview          = (AVLoadingIndicatorView)view.findViewById(R.id.progressBarWebview) ;
         wv = (WebView) view.findViewById(R.id.webView);
        //Scroll bars should not be hidden
        wv.setScrollbarFadingEnabled(false);
        //Disable the horizontal scroll bar

        //Enable JavaScript
        wv.getSettings().setJavaScriptEnabled(true);

        wv.setWebViewClient(new WebViewClient() {

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);

                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                Visibility.gone(progressBarWebview);
            }
        });

        wv.loadUrl(mainActivity.getResources().getString(R.string.url_privacity));

        containerOptionsBackChat.setOnClickListener(this);
        imgBack.setOnClickListener(this);
        wv.setOnKeyListener(keyListener);

        return view;
    }

    View.OnKeyListener keyListener = new View.OnKeyListener() {
        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {
            if( keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN ){
                if( wv.canGoBack() ){
                    wv.goBack();
                    return true;
                }
            }

            return false;
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_FRAME, android.R.style.Theme_Holo_Light_DarkActionBar);
    }

    @Override
    public void onClick(View view) {

        int id = view.getId();
        switch (id)
        {
            case R.id.containerOptionsBackChat:
                dismiss();
                break;
            case R.id.imgBack:
                dismiss();
                break;
            default:
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        MyApplication.getInstance().trackScreenView("fragment privacity");
    }
}
