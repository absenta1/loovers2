package com.appdupe.flamer;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.android.slidingmenu.MainActivity;
import com.appdupe.flamer.utility.SessionManager;
import com.loovers.app.R;

public class SplashActivity extends Activity {

	protected boolean _active = true;

	/** The _splash time. */
	protected int _splashTime = 1000;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splashactivity);


		Thread splashTread = new Thread() {
			@Override
			public void run() {
				try {
					int waited = 0;
					while (_active && (waited < _splashTime)) {
						sleep(100);
						if (_active) {
							waited += 100;
						}
					}
				} catch (InterruptedException e) {
					// do nothing
				} finally {
					try {

						SessionManager mSessionManager = new SessionManager(
								SplashActivity.this);
						System.out.println("onCreate mSessionManager " + mSessionManager);
						if (mSessionManager.isLoggedIn()) {
							System.out.println("onCreate user is logedind  "
									+ mSessionManager);

							Intent mIntent = new Intent(SplashActivity.this,
									MainActivity.class);
							// mIntent.putExtras(dataBundle);
							startActivity(mIntent);
							overridePendingTransition(R.anim.fade_in_slow, R.anim.fade_out);
							finish();
							// }

						} else {
							System.out.println("onCreate  user not loged in ");
							// FlurryAgent.logEvent("SplahScreenCompleted");
							Intent mIntent = new Intent(SplashActivity.this,
                                    RegisterOrSessionActivityActivity.class);
							// Intent mIntent=new Intent(SplashActivity.this,
							// MainActivity.class);
							startActivity(mIntent);
							overridePendingTransition(R.anim.fade_in_slow,R.anim.fade_out);
							finish();
						}

					} catch (Exception e2) {
						// TODO: handle exception
						e2.printStackTrace();
						System.out.println("onCreate  Exception " + e2);

					}
				}
			}
		};
		splashTread.start();

	}
}
