package com.android.slidingmenu.nearyou;


import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.GridView;
import android.widget.Toast;

import com.android.slidingmenu.Analitycs.MyApplication;
import com.android.slidingmenu.MainActivity;
import com.android.slidingmenu.adapter.GeneralAdapter;
import com.android.slidingmenu.util.Common;
import com.android.slidingmenu.util.GlobalData;
import com.android.slidingmenu.util.Visibility;
import com.android.slidingmenu.webservices.Services;
import com.appdupe.flamer.pojo.FindMatchData;
import com.appdupe.flamer.pojo.MatchesData;
import com.appdupe.flamer.utility.Constant;
import com.google.gson.Gson;
import com.loovers.app.R;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import jp.co.recruit_lifestyle.android.widget.WaveSwipeRefreshLayout;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Clase encargada de mostrar las personas nuevas en la aplicación
 * @author Juan Martín Bernal
 * @date 16/02/2016
 */
public class ListNewsPersonsFragment extends Fragment implements
        WaveSwipeRefreshLayout.OnRefreshListener{

    public MainActivity mainActivity;
    public AVLoadingIndicatorView progressBar;
    public ArrayList<String> oldUsers = new ArrayList<>();
    public ArrayList<MatchesData> matchesDatas;
    public GeneralAdapter adapter;
    public FindMatchData mFindMatchData;
    public WaveSwipeRefreshLayout mWaveSwipeRefreshLayout;

    public ListNewsPersonsFragment() {
        // Required empty public constructor
    }
    public ListNewsPersonsFragment(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_list_nearby_person, null);

        if (mainActivity == null)
            mainActivity = (MainActivity) getActivity();

        mWaveSwipeRefreshLayout = (WaveSwipeRefreshLayout)view.findViewById(R.id.main_swipe);
        mWaveSwipeRefreshLayout.setColorSchemeColors(Color.WHITE, Color.WHITE);
        mWaveSwipeRefreshLayout.setWaveColor(Color.parseColor("#e2156c"));
        mWaveSwipeRefreshLayout.setOnRefreshListener(this);

        mainActivity.gridview   = (GridView)view.findViewById(R.id.gridview);
        matchesDatas            = new ArrayList<>();
        progressBar             = (AVLoadingIndicatorView)view.findViewById(R.id.progressBarFavoritesWallas);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mainActivity.gridview.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                int threshold = 1;
                int count = mainActivity.gridview.getCount();

                if (scrollState == SCROLL_STATE_IDLE) {
                    if (mainActivity.gridview.getLastVisiblePosition() >= count - threshold) {
                        Log.d("app2you", "loading more data");
                        if(matchesDatas != null && matchesDatas.size() <= 100)
                            findNewPeople(1);
                    }
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

            }
        });

    }

    /**
     * Metodo encargado de obtener las personas que están a sua alrededor
     */
    public void findNewPeople(final int from) {
        if(from != 2)
            Visibility.visible(progressBar);

        JSONObject jsonObj = new JSONObject();

        for (int i = 0, len = oldUsers.size(); i < len; ++i) {
            try {
                jsonObj.put(""+(i+1), ""+oldUsers.get(i));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        OkHttpClient client = new OkHttpClient();
        RequestBody formBody = new FormBody.Builder()
                .add("sessionToken", "" + GlobalData.get(Constant.SESSION_TOKEN, mainActivity))
                .add("latitud", "" + GlobalData.get(Constant.LATITUD_USER, mainActivity))
                .add("longitud", "" + GlobalData.get(Constant.LONGITUD_USER, mainActivity))
                .add("typeId", ""+Constant.NEW_PEOPLE)
                .add("oldUsers",""+jsonObj.toString())
                .build();

        Request request = new Request.Builder()
                .url(Services.FIND_MATCHES_SERVICE)
                .post(formBody)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

                Looper.prepare();

                mainActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Visibility.gone(progressBar);
                        GlobalData.delete("service", mainActivity);
                        Common.errorMessage(mainActivity, "", mainActivity.getResources().getString(R.string.check_internet));
                        mWaveSwipeRefreshLayout.setRefreshing(false);
                    }
                });

                Looper.loop();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                final String result = response.body().string();
                Log.d("app2you", "respuesta findNewPeople news persons: " + result);

                Looper.prepare();

                mainActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {

                            Visibility.gone(progressBar);
                            GlobalData.delete("service", mainActivity);
                            mWaveSwipeRefreshLayout.setRefreshing(false);
                            Gson gson = new Gson();
                            mFindMatchData = gson.fromJson(result, FindMatchData.class);

                            if (mFindMatchData.success) {

                                for (MatchesData data: mFindMatchData.matches) {
                                    if (!oldUsers.contains(data.userId)) {
                                        oldUsers.add(data.userId);
                                        matchesDatas.add(data);
                                    }
                                }
                                Log.d("app2you", " cantidad personas nuevas: " + matchesDatas.size() + " from: "+ from);
                                if(from == 0)
                                {
                                    if (matchesDatas != null && matchesDatas.size() > 0) {
                                        adapter = new GeneralAdapter(mainActivity, matchesDatas, Constant.LIST_PERSONS);
                                        mainActivity.gridview.setAdapter(adapter);
                                    }
                                }
                                else {
                                    if(adapter != null)
                                        adapter.notifyDataSetChanged();
                                }

                            } else {
                          //      Toast.makeText(mainActivity, "false response", Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(mainActivity, R.string.error_reponse_server, Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                Looper.loop();
            }
        });
    }
    @Override
    public void onResume() {
        super.onResume();

        if(GlobalData.isKey("listnewpersons",mainActivity))
        {
            String resultCards = GlobalData.get("listnewpersons",mainActivity);
            Log.d("app2you","maches resume: " + resultCards);
            Gson gson = new Gson();
            MatchesData []datas = gson.fromJson(resultCards, MatchesData[].class);
            oldUsers.clear();
            for (MatchesData data : datas)
                if (!oldUsers.contains(data.userId))
                    oldUsers.add(data.userId);

            matchesDatas = new ArrayList<MatchesData>(Arrays.asList(datas));

            if (matchesDatas != null && matchesDatas.size() > 0) {
                adapter = new GeneralAdapter(mainActivity, matchesDatas, Constant.LIST_PERSONS);
                mainActivity.gridview.setAdapter(adapter);
            }

        }
        else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    findNewPeople(0);
                }
            }, Constant.TIME_EXCUTE_FRAGMENT);
        }

        MyApplication.getInstance().trackScreenView("fragment new persons");

    }

    @Override
    public void onPause() {
        super.onPause();

        if(matchesDatas != null && matchesDatas.size() > 0)
            GlobalData.set("listnewpersons",new Gson().toJson(matchesDatas),mainActivity);
    }


    @Override
    public void onRefresh() {
        if(!GlobalData.isKey("service",mainActivity)) {
            GlobalData.set("service", "Y", mainActivity);
            oldUsers.clear();
            matchesDatas.clear();
            findNewPeople(2);
        }

    }
}
