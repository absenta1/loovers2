package com.android.slidingmenu.util;

import android.app.Activity;
import android.app.Dialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.android.slidingmenu.activities.LoginActivity;
import com.android.slidingmenu.objects.ItemOptionChat;
import com.android.slidingmenu.objects.Store;
import com.android.slidingmenu.products.Product;
import com.android.slidingmenu.reports.Report;
import com.appdupe.flamer.RegisterOrSessionActivityActivity;
import com.appdupe.flamer.utility.Constant;
import com.appyvet.rangebar.RangeBar;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.loovers.app.R;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.NetworkInterface;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Locale;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Juan Martin Bernal on 20/11/15.
 */
public class Common {

    static PendingIntent pending;


    /**
     * Método encargado de obtner hash key dado el paquete de la app
     * @param cxt
     */
    public static void getHasKey(Context cxt)
    {
        // Add code to print out the key hash
        try {
            //3Dqrkmeqo0G14LupRzWbRn9A6Zo= //key release
            PackageInfo info = cxt.getPackageManager().getPackageInfo(
                    "com.loovers.app",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }
    /**
     * Obtiene el numero unico de identificacion del dispositivo
     * @param cxt
     * @return
     */
    public static String getIMEI(Context cxt) {
        TelephonyManager telephonyManager = (TelephonyManager) cxt.getSystemService(Context.TELEPHONY_SERVICE);
        return telephonyManager.getDeviceId();
    }

    /**
     * Obtiene la ip de una conexion wifi
     * @param cxt
     * @return
     */
    public static String getLocalIpAddress(Context cxt){

        if(ConnectionDetector.isConnectedMobile(cxt))
            return getIPMobile();
        else
            return getIPWifi(cxt);
    }

    /**
     * Obtiene la ip de una conexion movil
     * @return
     */
    public static String getIPMobile()
    {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces();
                 en.hasMoreElements();) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements();) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()) {
                        return inetAddress.getHostAddress().toString();
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
           // Log.e("IP Address", ex.toString());
        }
        return null;
    }
    /**
     * Get the IP of current Wi-Fi connection
     * @return IP as string
     */
    public static String getIPWifi(Context cxt) {
        try {
            WifiManager wifiManager = (WifiManager) cxt.getSystemService(Context.WIFI_SERVICE);
            WifiInfo wifiInfo = wifiManager.getConnectionInfo();
            int ipAddress = wifiInfo.getIpAddress();
            return String.format(Locale.getDefault(), "%d.%d.%d.%d",
                    (ipAddress & 0xff), (ipAddress >> 8 & 0xff),
                    (ipAddress >> 16 & 0xff), (ipAddress >> 24 & 0xff));
        } catch (Exception ex) {
            ex.printStackTrace();
           // Log.e("lovinghood", ex.getMessage());
            return null;
        }
    }

    /**
     * Obtiene la versión del android que tiene el dispositivo
     * @param cxt
     * @return
     */
    public static String getVersionDevice(Context cxt)
    {
        return (GlobalData.isKey(Constant.VERSION_SO, cxt))?GlobalData.get(Constant.VERSION_SO, cxt):""+Build.VERSION.RELEASE;
    }

    /**
     * convert url a bitmap
     * @param src
     * @return
     */
    public static Bitmap getBitmapFromURL(String src) {
        try {
            /*URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);*/
            URL url = new URL(src);
            InputStream in = url.openConnection().getInputStream();
            BufferedInputStream bis = new BufferedInputStream(in,1024*8);
            ByteArrayOutputStream out = new ByteArrayOutputStream();

            int len=0;
            byte[] buffer = new byte[1024];
            while((len = bis.read(buffer)) != -1){
                out.write(buffer, 0, len);
            }
            out.close();
            bis.close();

            byte[] data = out.toByteArray();
            Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
            return bitmap;
        } catch (IOException e) {
            // Log exception
            return null;
        }
    }

    /**
     * Método encargado de escalar un bitmap
     * @param realImage
     * @param maxImageSize
     * @param filter
     * @return
     */
    public  static Bitmap scaleDown(Bitmap realImage, float maxImageSize,
                             boolean filter) {
        float ratio = Math.min(
                (float) maxImageSize / realImage.getWidth(),
                (float) maxImageSize / realImage.getHeight());
        int width = Math.round((float) ratio * realImage.getWidth());
        int height = Math.round((float) ratio * realImage.getHeight());

        Bitmap newBitmap = Bitmap.createScaledBitmap(realImage, width,
                height, filter);
        return newBitmap;
    }

    /**
     * Abre mensaje estandar para informar sobre algun error
     * @param cxt
     * @param title
     * @param message
     */
    public static void errorMessage(Context cxt, String title, String message) {

        final Dialog dialog =  new Dialog(cxt,R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.modal_general);

        TextView text = (TextView) dialog.findViewById(R.id.text);
        text.setText(""+message);
        Button exit = (Button) dialog.findViewById(R.id.exit);
        Visibility.gone(exit);
        Button accept = (Button) dialog.findViewById(R.id.accept);
        // if button is clicked, close the custom dialog
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    /**
     * Abre mensaje estandar para informar sobre algun error
     * @param cxt

     */
    public static void dialogLoginDaily(Context cxt, String message) {


        final Dialog dialog =  new Dialog(cxt,R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.layout_add_credits_free);

        TextView txtMessageReloadCredits = (TextView)dialog.findViewById(R.id.txtMessageReloadCredits);
        txtMessageReloadCredits.setText(""+message);
        txtMessageReloadCredits.setTypeface(TypesLetter.getSansRegular(cxt));

        Button accept = (Button) dialog.findViewById(R.id.btnAcceptReloadCreditsLogin);
        accept.setTypeface(TypesLetter.getSansRegular(cxt));
        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();


            }
        });

        dialog.show();
    }


    /**
     * Genera un numero aleatorio en un rango de numeros
     * @param min
     * @param max
     * @return
     */
    public static int generateNumber(int min,int max)
    {
       // int min = 65;
        //int max = 80;
        Random r = new Random();
        int number = r.nextInt(max - min + 1) + min;
        return number;
    }
    /**
     * setea propriedades iniciales a la barra de rango
     * @param range_bar
     */
    public static void setPropertiesRange(RangeBar range_bar, int init,boolean enableRange, boolean enablePins)
    {
        int colorRange = Color.parseColor("#e2156c");
        range_bar.setBarColor(Color.parseColor("#552648"));
        range_bar.setPinColor(colorRange);
        range_bar.setConnectingLineColor(colorRange);
        range_bar.setSelectorColor(colorRange);
        range_bar.setBarWeight(8);
        range_bar.setSeekPinByIndex(init);
        range_bar.setConnectingLineWeight(6);
        range_bar.setTickHeight(0);
        range_bar.setTemporaryPins(enablePins);
        range_bar.setFocusableInTouchMode(enableRange);
        range_bar.setFocusable(enableRange);
        range_bar.setClickable(enableRange);
    }

    /**
     * Cerrar session loovers
     * @param cxt
     */
    public static void closeSessionWallamatch(Activity cxt)
    {
        FacebookSdk.sdkInitialize(cxt);
        deleteGlobalData(cxt);
        LoginManager.getInstance().logOut();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(cxt);
        try {
            gcm.unregister();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //eliminar notificaciones
        NotificationManager notificationManager = (NotificationManager)cxt.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(0);//id de la notificacion

        Intent intent = new Intent(cxt, RegisterOrSessionActivityActivity.class);
        cxt.startActivity(intent);
        cxt.overridePendingTransition(R.anim.fade_in_slow, R.anim.fade_out);
        cxt.finish();
    }
    /**
     * Cerrar session loovers
     * @param cxt
     */
    public static void closeSessionWallamatch(Activity cxt, ProgressDialog progressDialog)
    {
        FacebookSdk.sdkInitialize(cxt);
        deleteGlobalData(cxt);
        LoginManager.getInstance().logOut();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(cxt);
        try {
            gcm.unregister();
        } catch (IOException e) {
            e.printStackTrace();
        }
        // DatabaseHandler databaseHandler = new DatabaseHandler(cxt);
        //databaseHandler.deleteChats();
        /*SharedPreferences prefs = cxt.getSharedPreferences(
                MainActivity.class.getSimpleName(),
                Context.MODE_PRIVATE);
        prefs.edit().clear().commit();*/
        //eliminar notificaciones
        NotificationManager notificationManager = (NotificationManager)cxt.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(0);//id de la notificacion

        if(progressDialog != null)
            progressDialog.dismiss();

        Intent intent = new Intent(cxt, RegisterOrSessionActivityActivity.class);
        cxt.startActivity(intent);
        cxt.overridePendingTransition(R.anim.fade_in_slow, R.anim.fade_out);
        cxt.finish();
    }

    /* probar logout facebook
    public void disconnectFromFacebook() {

    if (AccessToken.getCurrentAccessToken() == null) {
        return; // already logged out
    }

    new GraphRequest(AccessToken.getCurrentAccessToken(), "/me/permissions/", null, HttpMethod.DELETE, new GraphRequest
            .Callback() {
        @Override
        public void onCompleted(GraphResponse graphResponse) {

            LoginManager.getInstance().logOut();

        }
    }).executeAsync();
}
     */
    public static byte[] getBytes(InputStream input) {
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();

        int nRead;
        byte[] data = new byte[2048];

        try {
            while ((nRead = input.read(data, 0, data.length)) != -1) {
                buffer.write(data, 0, nRead);
            }

            buffer.flush();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return buffer.toByteArray();
    }
    public static byte[] getBytesURL(String url)
    {
        byte[] tmp = null;
        try {
            InputStream input = new java.net.URL(url).openStream();
            tmp               = getBytes(input);
        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return tmp;
    }
    public static String getNickName(Context cxt)
    {
        String nick = (GlobalData.isKey(Constant.NICKNAME,cxt))?GlobalData.get(Constant.NICKNAME,cxt):"Wallamatch";
        StringBuffer res = new StringBuffer();

        String[] strArr = nick.split(" ");
        for (String str : strArr) {
            char[] stringArray = str.trim().toCharArray();
            stringArray[0] = Character.toUpperCase(stringArray[0]);
            str = new String(stringArray);
            res.append(str).append(" ");
        }


        return res.toString().trim();
    }

    /**
     * Obtener el los pixels de una pantalla 1 width, 2 height
     * @param cxt
     * @param typeScreen
     * @return
     */
    public static int getScreen(Context cxt, int typeScreen)
    {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager wm = (WindowManager) cxt.getSystemService(Context.WINDOW_SERVICE); // the results will be higher than using the activity context object or the getWindowManager() shortcut
        wm.getDefaultDisplay().getMetrics(displayMetrics);
        int screen = 0;

        int screenWidth = displayMetrics.widthPixels;
        int screenHeight = displayMetrics.heightPixels;
        switch (typeScreen)
        {
            case 1:
                screen = screenWidth;
                break;
            case 2:
                screen = screenHeight;
                break;
            default:
                break;
        }

        return screen;
    }

    public static void closeSessionWallamatchSecurity(Activity cxt)
    {
        FacebookSdk.sdkInitialize(cxt);
        deleteGlobalData(cxt);
        LoginManager.getInstance().logOut();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(cxt);
        try {
            gcm.unregister();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //eliminar notificaciones
        NotificationManager notificationManager = (NotificationManager)cxt.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(0);//id de la notificacion

        Intent intent = new Intent(cxt, LoginActivity.class);
        cxt.startActivity(intent);
        cxt.overridePendingTransition(R.anim.fade_in_slow, R.anim.fade_out);
        cxt.finish();
    }

    /**
     * Carga los productos disponibles que tiene loovers
     * @param cxt
     * @return
     */
    public static ArrayList<Product> loadProductsWallamatch(Context cxt)
    {
        ArrayList<Product> products = new ArrayList<>();

        products.add(new Product(Constant.BUY_COINS_1,R.drawable.credito,"10 Likes","","","0,86€"));
        products.add(new Product(Constant.BUY_COINS_2,R.drawable.moneda_60,"60 Likes","AHÓRRATE 0,98€","","4,32€"));
        products.add(new Product(Constant.BUY_COINS_3,R.drawable.moneda_130,"130 Likes","AHÓRRATE 2,94€","","8,05€"));


        return products;
    }
    /**
     * Carga los productos GOLD disponibles que tiene loovers
     * @param cxt
     * @return
     */
    public static ArrayList<Product> loadProductsGOLDWallamatch(Context cxt)
    {
        ArrayList<Product> products = new ArrayList<>();
       // products.add(new Product(Constant.GOLD_24H,R.drawable.diamante_pasategold,"GOLD 24h","","","5,98€"));
        products.add(new Product(Constant.GOLD_1_MONTH,R.drawable.diamante_pasategold,"GOLD\n1 mes","","-21%","16,00€"));
        products.add(new Product(Constant.GOLD_3_MONTHS,R.drawable.diamante_pasategold,"GOLD\n3 meses","","-50%","29,60€"));


        return products;
    }

    /**
     * Carga las opciones del chat
     * @param cxt
     * @return
     */
    public static ArrayList<ItemOptionChat> loadOptionsChat(Context cxt)
    {
        ArrayList<ItemOptionChat> options = new ArrayList<>();
        options.add(new ItemOptionChat(R.drawable.ic_photo_album_black_24dp,"Solicitar fotos"));
        //options.add(new ItemOptionChat(R.drawable.ic_record_voice_over_black_24dp,cxt.getResources().getString(R.string.report_user)));

        return options;
    }

    public static ArrayList<Report> loadReasonsDeleteAccount(Context cxt)
    {
        ArrayList<Report> reasons = new ArrayList<>();
        reasons.add(new Report(Constant.DONT_LIKE_APP,R.drawable.ic_thumb_down_black_24dp,""+cxt.getResources().getString(R.string.not_expected)));
        reasons.add(new Report(Constant.NOT_FOUND_PEOPLE,R.drawable.ic_wc_black_24dp,""+cxt.getResources().getString(R.string.not_fond_people_in_my_zone)));
        reasons.add(new Report(Constant.NOT_RESPONSE_CHAT,R.drawable.ic_call_missed_outgoing_black_24dp,""+cxt.getResources().getString(R.string.not_response_chats)));
        reasons.add(new Report(Constant.APP_DOES_NOT_WORK,R.drawable.ic_bug_report_black_24dp,""+cxt.getResources().getString(R.string.crashed_app)));
        reasons.add(new Report(Constant.OTHER_REASON_DELETE_APP,R.drawable.ic_warning_black_24dp,""+cxt.getResources().getString(R.string.other_reason_delete_account)));
        return reasons;
    }


    /**
     * Carga las categorias del reportar usuario
     * @return
     */
    public static ArrayList<Report> loadCategoriesReport(Context mainActivity)
    {
        ArrayList<Report> reports = new ArrayList<>();
        reports.add(new Report(Constant.PROFILE_FAKE,R.drawable.ic_thumb_down_black_24dp,mainActivity.getResources().getString(R.string.fake_profile)));
        reports.add(new Report(Constant.PERSON_RUDE,R.drawable.ic_record_voice_over_black_24dp,mainActivity.getResources().getString(R.string.impolite_person)));
        reports.add(new Report(Constant.YOUNGER,R.drawable.ic_face_black_24dp,mainActivity.getResources().getString(R.string.younger_person)));
        reports.add(new Report(Constant.SEXUAL_PHOTOS,R.drawable.ic_remove_circle_outline_black_24dp,mainActivity.getResources().getString(R.string.sexual_content_photos)));
        reports.add(new Report(Constant.FRAUD_PRIVATE_PRIVATE_PHOTO,R.drawable.ic_bug_report_black_24dp,mainActivity.getResources().getString(R.string.fraud_private_photo)));
        reports.add(new Report(Constant.OTHER_REASON,R.drawable.ic_warning_black_24dp,mainActivity.getResources().getString(R.string.other_reasons)));

        return reports;
    }

    /**
     * Carga las ventajas de ser usuario GOLD
     * @return
     */
    public static ArrayList<Store> loadPromotions(Context cxt)
    {
        String gender   = GlobalData.get(Constant.GENERE,cxt);
        String status   = GlobalData.get(Constant.USER_STATUS,cxt);
        String url      = GlobalData.isKey(Constant.AVATAR_USER,cxt)?GlobalData.get(Constant.AVATAR_USER,cxt):"";

        ArrayList<Store> stores = new ArrayList<>();
        if(!GlobalData.isKey(Constant.SHARE_FACEBOOK,cxt))
            stores.add(new Store(Constant.FEATURE_1,"",R.drawable.credito,"Consigue Likes gratis!"));

        stores.add(new Store(Constant.FEATURE_2,"",R.drawable.logo_gold,(gender.equals(Constant.MAN)?"Chicas nuevas en tu ciudad!":"Chicos nuevos en tu ciudad")));

        if(gender.equals(Constant.WOMAN) || status.equals(Constant.GOLD)){}
        else {
            if(url.length() > 0)
                stores.add(new Store(Constant.FEATURE_3, (url.length() > 0) ? url : "", 0, "Destaca tu foto sobre las demás"));

            stores.add(new Store(Constant.FEATURE_7,"",R.drawable.populares,"Chatea con las más populares"));
        }
        stores.add(new Store(Constant.FEATURE_5,"",R.drawable.privadas,"Publica fotos privadas"));
        stores.add(new Store(Constant.FEATURE_6,"",R.drawable.like_app_facebook,"Tu like nos anima a seguir"));

        if(!GlobalData.isKey(Constant.RATE_APP, cxt))
            stores.add(new Store(Constant.FEATURE_8,"",R.drawable.puntua,"Valoranos si te gusta la app"));

        return stores;
    }

    /**
     * Método para borrar la variables globales de la app
     * @param cxt
     */
    public static void deleteGlobalData(Context cxt)
    {
        GlobalData.delete(Constant.LOGIN_APP, cxt);
        GlobalData.delete("matchesData", cxt);
        GlobalData.delete(Constant.GENERE, cxt);
        GlobalData.delete(Constant.SESSION_TOKEN,cxt);
        GlobalData.delete(Constant.AVATAR_USER,cxt);
        GlobalData.delete(Constant.USER_STATUS,cxt);
        GlobalData.delete(Constant.FIRST_REGISTER,cxt);
        GlobalData.delete(Constant.CITY_USER,cxt);
        GlobalData.delete(Constant.PROVINCE,cxt);

        GlobalData.delete(Constant.NOTIFICATION_WALLAS, cxt);
        GlobalData.delete(Constant.NOTIFICATION_FAVORITES, cxt);
        GlobalData.delete(Constant.NOTIFICATION_MATCHES, cxt);
        GlobalData.delete(Constant.NOTIFICATION_MESSAGES,cxt);

        GlobalData.delete(Constant.COUNTER_WALLAS, cxt);
        GlobalData.delete(Constant.COUNTER_FAVORITES, cxt);
        GlobalData.delete(Constant.COUNTER_MATCHS, cxt);
        GlobalData.delete(Constant.COUNTER_MESSAGES, cxt);

        GlobalData.delete(Constant.TUTORIAL_GAME, cxt);
        GlobalData.delete(Constant.FIRST_MATCH, cxt);

        GlobalData.delete(Constant.FIRST_FAVORITE, cxt);
        GlobalData.delete(Constant.DELETE_FIRST_FAVORITE, cxt);
        GlobalData.delete(Constant.MESSAGE_PHOTO_PENDING ,cxt);
        GlobalData.delete(Constant.MESSAGE_NOT_PHOTO ,cxt);
        GlobalData.delete(Constant.LOGIN_FACEBOOK,cxt);
        GlobalData.delete(Constant.EMAIL_VERIFIED,cxt);
        GlobalData.delete(Constant.BIRTHDAY_VERIFIED,cxt);
        GlobalData.delete(Constant.PIN_SECURITY,cxt);
        GlobalData.delete(Constant.NUMBER_ATTEMPS,cxt);
        GlobalData.delete(Constant.RATE_APP,cxt);
        GlobalData.delete(Constant.REQUEST_MATCH,cxt);
        GlobalData.delete(Constant.REQUEST_FAVORITES,cxt);
        GlobalData.delete(Constant.REQUEST_WANT_TO_ME,cxt);
        GlobalData.delete(Constant.REGISTRATION_ID_GCM_OLD,cxt);
        GlobalData.delete(Constant.CARDS,cxt);
        GlobalData.delete(Constant.CARDS_GAME,cxt);
        GlobalData.delete(Constant.SHARE_FACEBOOK,cxt);
        GlobalData.delete("from",cxt);
    }

    public  static Bitmap decodeFile(String path, int from) {//you can provide file path here
        int orientation;
        try {


            Bitmap bm = BitmapFactory.decodeFile(path);
            Bitmap bitmap = bm;

            ExifInterface exif = new ExifInterface(path);

            orientation = exif
                    .getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);

          //  Log.e("ExifInteface .........", "rotation ="+orientation);

            Log.d("orientationWallamatch", "" + orientation + " modelo: " +android.os.Build.MODEL);
            Matrix m = new Matrix();

           /* if ((orientation == ExifInterface.ORIENTATION_ROTATE_180)) {
                m.postRotate(180);

                Log.e("in orientation", "" + orientation);
                bitmap = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(),
                        bm.getHeight(), m, true);
                return bitmap;
            } else if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
                m.postRotate(90);
                Log.e("in orientation", "" + orientation);
                bitmap = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(),
                        bm.getHeight(), m, true);
                return bitmap;
            }
            else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
                m.postRotate(270);
                Log.e("in orientation", "" + orientation);
                bitmap = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(),
                        bm.getHeight(), m, true);
                return bitmap;
            }*/
             if(orientation == 0 && from == 1 && (android.os.Build.MODEL.contains("SM") || Build.BRAND.toLowerCase().contains("samsung")))
            {
                m.postRotate(90);
                Log.e("in orientation", "" + orientation);
                bitmap = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(),
                        bm.getHeight(), m, true);
                File dest = new File(path);
                FileOutputStream out = null;
                try {
                    out = new FileOutputStream(dest);
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
                    out.flush();
                    out.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return bitmap;
            }
            return bitmap;
        } catch (Exception e) {
            return null;
        }

    }

    /**
     * Método encargado de girar la imagen dependiendo de los grados
     * @param source
     * @param angle
     * @return
     */
    public static Bitmap rotateImage(Bitmap source, float angle) {
        Bitmap retVal;

        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        retVal = Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);

        return retVal;
    }

    public static Bitmap getCroppedBitmap(Bitmap bitmap) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        // canvas.drawRoundRect(rectF, roundPx, roundPx, paint);
        canvas.drawCircle(bitmap.getWidth() / 2, bitmap.getHeight() / 2,
                bitmap.getWidth() / 2, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        //Bitmap _bmp = Bitmap.createScaledBitmap(output, 60, 60, false);
        //return _bmp;
        return output;
    }

    /**
     * Método encargado de eliminar notificaciones
     * @param cxt
     */
    public static void deleteNotifications(Context cxt)
    {
        //eliminar notificaciones
        NotificationManager notificationManager = (NotificationManager)cxt.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(0);//id de la notificacion
    }

    // Pull all links from the body for easy retrieval
    public static ArrayList<String> pullLinks(String text) {
        ArrayList<String> links = new ArrayList<String>();

        String regex = "\\(?\\b(http://|www[.]|https|Https)[-A-Za-z0-9+&amp;@#/%?=~_()|!:,.;]*[-A-Za-z0-9+&amp;@#/%=~_()|]";
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(text);
        while (m.find()) {
            String urlStr = m.group();
            if (urlStr.startsWith("(") && urlStr.endsWith(")"))
                urlStr = urlStr.substring(1, urlStr.length() - 1);

            if (urlStr.contains("http"))
                links.add(urlStr);
            else
                links.add("http://" + urlStr);
        }
        return links;
    }

    public static void loadLink(String text, Context cxt) {

            if (text != null) {
                ArrayList<String> url = pullLinks(text);
                if (url != null && url.size() > 0) {
                    String link = url.get(0);

                    // carga actividad para pintar el webview
                    if (ConnectionDetector.isConnectingToInternet(cxt)) {
                        Intent i = new Intent(Intent.ACTION_VIEW);
                        i.setData(Uri.parse(link));
                        cxt.startActivity(i);
                    }

                }

        }
    }

}
