package com.android.slidingmenu.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.slidingmenu.Analitycs.MyApplication;
import com.android.slidingmenu.MainActivity;
import com.android.slidingmenu.adapter.GeneralAdapter;
import com.android.slidingmenu.objects.UserData;
import com.android.slidingmenu.util.Common;
import com.android.slidingmenu.util.GlobalData;
import com.android.slidingmenu.util.TypesLetter;
import com.android.slidingmenu.util.Visibility;
import com.android.slidingmenu.webservices.Services;
import com.appdupe.flamer.pojo.MatchesData;
import com.appdupe.flamer.utility.Constant;
import com.google.gson.Gson;
import com.loovers.app.R;
import com.wang.avi.AVLoadingIndicatorView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import jp.co.recruit_lifestyle.android.widget.WaveSwipeRefreshLayout;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class FragmentMatch extends Fragment  implements WaveSwipeRefreshLayout.OnRefreshListener{


    //private FragmentManager mFragmentManager;
    public GeneralAdapter adapter;
    public MainActivity mainActivity;
    //private GridView gridview;
    //public RelativeLayout containerMatchEmpty;
    public ArrayList<MatchesData> matchesDatas = new ArrayList<>();
    public Button btnPLayWalla;
    public TextView txtTitle,txtSubTitle;
    public WaveSwipeRefreshLayout mWaveSwipeRefreshLayout;
    public static final int SHOW_DIALOG = 1;
    public static final int HIDE_DIALOG = 0;
    public AVLoadingIndicatorView progressBarMatch;

    public FragmentMatch()
    {}
    public FragmentMatch(MainActivity mainActivity)
    {
        this.mainActivity = mainActivity;
    }

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.searchactivity, null);


        if(mainActivity == null)
            mainActivity = (MainActivity)getActivity();

        Visibility.visible(mainActivity.tvTitle);
        GlobalData.set(Constant.COUNTER_MATCHS, "0", mainActivity);

        mWaveSwipeRefreshLayout = (WaveSwipeRefreshLayout)view.findViewById(R.id.main_swipe);
        mWaveSwipeRefreshLayout.setColorSchemeColors(Color.WHITE, Color.WHITE);
        mWaveSwipeRefreshLayout.setWaveColor(Color.parseColor("#e2156c"));
        mWaveSwipeRefreshLayout.setOnRefreshListener(this);

        //mFragmentManager= ((FragmentActivity) inflater.getContext()).getSupportFragmentManager();
        mainActivity.gridview = (GridView)view.findViewById(R.id.search_gridview);
        mainActivity.containerMatchEmpty = (RelativeLayout)view.findViewById(R.id.containerMatchEmpty);
        btnPLayWalla        = (Button)view.findViewById(R.id.btnPLayWalla);
        btnPLayWalla.setTransformationMethod(null);
        progressBarMatch    = (AVLoadingIndicatorView)view.findViewById(R.id.progressBarMatch);

        txtTitle            = (TextView)view.findViewById(R.id.txtTitle);
        txtSubTitle         = (TextView)view.findViewById(R.id.txtSubTitle);

        txtTitle.setTypeface(TypesLetter.getSansBold(mainActivity));
        txtSubTitle.setTypeface(TypesLetter.getSansRegular(mainActivity));
        btnPLayWalla.setTypeface(TypesLetter.getSansRegular(mainActivity));

        btnPLayWalla.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mainActivity.replaceFragment(new FragmentGame(mainActivity));
            }
        });


		return view;
	}

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if(GlobalData.isKey(Constant.REQUEST_MATCH,mainActivity))
        {
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    loadMatchs();
                }
            }, 260);

        }
        else {
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    getMatches(SHOW_DIALOG);
                }
            }, Constant.TIME_EXCUTE_FRAGMENT);
        }
    }
    /**
     * Cargar los match que están en memoria
     */
    public void loadMatchs()
    {
        String resultCards = GlobalData.get(Constant.REQUEST_MATCH,mainActivity);
       // Log.d("app2you","maches resume: " + resultCards);
        Gson gson = new Gson();
        MatchesData []datas = gson.fromJson(resultCards, MatchesData[].class);

        matchesDatas = new ArrayList<MatchesData>(Arrays.asList(datas));

        if (matchesDatas != null && matchesDatas.size() > 0) {

            adapter = new GeneralAdapter(mainActivity, matchesDatas, Constant.MATCH);
            mainActivity.gridview.setAdapter(adapter);
            Visibility.visible(mainActivity.gridview);
            Visibility.gone(mainActivity.containerMatchEmpty);

        } else {
            Visibility.gone(mainActivity.gridview);
            Visibility.visible(mainActivity.containerMatchEmpty);
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        if(matchesDatas != null && matchesDatas.size() > 0)
            GlobalData.set(Constant.REQUEST_MATCH,new Gson().toJson(matchesDatas),mainActivity);
    }

    /**
     * Metodo encargado de obtener los matches que tiene un usuario
     */
    public void getMatches(final int option)
    {
        if(option == SHOW_DIALOG)
            Visibility.visible(progressBarMatch);

        OkHttpClient client = new OkHttpClient();
        RequestBody formBody = new FormBody.Builder()
                .add("sessionToken", "" + GlobalData.get(Constant.SESSION_TOKEN, mainActivity))
                .add("serviceId", "" + Constant.MATCH)
                .build();

        Request request = new Request.Builder()
                .url(Services.GET_MY_USERS)
                .post(formBody)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

                Looper.prepare();
                mainActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Common.errorMessage(mainActivity, "", mainActivity.getResources().getString(R.string.check_internet));
                        Visibility.gone(progressBarMatch);
                        GlobalData.set(Constant.SERVICE_EXECUTION, "N", mainActivity);
                        mWaveSwipeRefreshLayout.setRefreshing(false);
                    }
                });

                Looper.loop();
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {


                final String result = response.body().string();
                Log.d("app2you", "respuesta match: " + result);
                Looper.prepare();
                if(response.isSuccessful()) {


                    mainActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Gson gson = new Gson();
                                final UserData userData = gson.fromJson(result, UserData.class);
                                GlobalData.set(Constant.SERVICE_EXECUTION, "N", mainActivity);
                                mWaveSwipeRefreshLayout.setRefreshing(false);
                                if (userData.success) {
                                    //true
                                    matchesDatas = userData.results;
                                    if (matchesDatas != null && matchesDatas.size() > 0) {
                                        if (!GlobalData.isKey(Constant.FIRST_MATCH, mainActivity))
                                            GlobalData.set(Constant.FIRST_MATCH, "Y", mainActivity);

                                        adapter = new GeneralAdapter(mainActivity, matchesDatas, Constant.MATCH);
                                        mainActivity.gridview.setAdapter(adapter);
                                        Visibility.visible(mainActivity.gridview);
                                        Visibility.gone(mainActivity.containerMatchEmpty);

                                    } else {
                                        Visibility.gone(mainActivity.gridview);
                                        Visibility.visible(mainActivity.containerMatchEmpty);
                                    }
                                    Visibility.gone(progressBarMatch);
                                } else {
                                    //false
                                    Toast.makeText(mainActivity, R.string.check_internet, Toast.LENGTH_SHORT).show();
                                }
                                Visibility.gone(progressBarMatch);
                            } catch (Exception ex) {
                                Visibility.gone(progressBarMatch);
                                ex.printStackTrace();
                                Toast.makeText(mainActivity, R.string.error_reponse_server, Toast.LENGTH_SHORT).show();
                            } finally {
                                response.body().close();
                            }
                        }
                    });



                }
                else
                    Toast.makeText(mainActivity, R.string.check_internet, Toast.LENGTH_SHORT).show();

                Looper.loop();
            }
        });
    }

    @Override
    public void onRefresh() {
        if(GlobalData.isKey(Constant.SERVICE_EXECUTION,mainActivity)) {
            if (!GlobalData.get(Constant.SERVICE_EXECUTION, mainActivity).equals("Y"))
                if(mainActivity.containerMatchEmpty.getVisibility() == View.GONE) {
                    GlobalData.set(Constant.SERVICE_EXECUTION, "Y", mainActivity);
                    getMatches(HIDE_DIALOG);
                }
        }
        else {

            if(mainActivity.containerMatchEmpty.getVisibility() == View.GONE) {
                GlobalData.set(Constant.SERVICE_EXECUTION, "Y", mainActivity);
                getMatches(HIDE_DIALOG);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        MyApplication.getInstance().trackScreenView("fragment matches");
    }
}

