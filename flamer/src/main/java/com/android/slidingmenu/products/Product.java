package com.android.slidingmenu.products;

/**
 * Created by Juan Martin Bernal on 9/3/16.
 */
public class Product {


    public int productId;
    public int image;
    public String name;

    public String description;
    public String offer;
    public String price;

    public Product(int productId, int image,String name,String description,String offer, String price) {
        this.productId = productId;
        this.image     = image;
        this.name = name;
        this.description = description;
        this.offer = offer;
        this.price = price;
    }
}
