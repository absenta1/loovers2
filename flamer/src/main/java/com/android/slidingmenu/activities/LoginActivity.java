package com.android.slidingmenu.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.slidingmenu.MainActivity;
import com.android.slidingmenu.util.Common;
import com.android.slidingmenu.util.GlobalData;
import com.android.slidingmenu.util.KeyBoard;
import com.android.slidingmenu.util.Validate;
import com.android.slidingmenu.webservices.Services;
import com.appdupe.flamer.RegisterOrSessionActivityActivity;
import com.appdupe.flamer.pojo.MyProfileData;
import com.appdupe.flamer.utility.ConnectionDetector;
import com.appdupe.flamer.utility.Constant;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.gson.Gson;
import com.loovers.app.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends Activity /*implements LoaderCallbacks<Cursor> required permision READ_CONTACTS*/ {

    // UI references.
    private static final String TAG = "app2you";
    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;
    private Button btn_login;
    public static final String EXTRA_MESSAGE = "message";
    public static final String PROPERTY_REG_ID = "registration_id";
    private static final String PROPERTY_APP_VERSION = "appVersion";
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final String PROPERTY_EXPIRATION_TIME = "onServerExpirationTimeMs";
    private static final String PROPERTY_USER = "user";
    public static final long EXPIRATION_TIME_MS = 1000 * 3600 * 24 * 7;
    public GoogleCloudMessaging gcm;
    private String regid;
    //private String SENDER_ID = "1077626696227";
    //private String SENDER_ID = "705758513230";//WALLAMATCH JUAN MARTIN id de proyecto
    private String SENDER_ID = "246265097728";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        int sdk = android.os.Build.VERSION.SDK_INT;
        if(sdk <= Build.VERSION_CODES.JELLY_BEAN) {
            super.setTheme(R.style.AppTheme);

        }
        super.onCreate(savedInstanceState);
        //Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_login);

        // Set up the login form.
        btn_login = (Button)findViewById(R.id.btn_login);
        btn_login.setTransformationMethod(null);

        mEmailView = (AutoCompleteTextView) findViewById(R.id.email);
      //  populateAutoComplete();

        mPasswordView = (EditText) findViewById(R.id.password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });


        btn_login.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        Bundle bundle = getIntent().getExtras();
        if(bundle!=null)
        {
            if(bundle.getString("success_password").equalsIgnoreCase("OK"))
                Common.errorMessage(LoginActivity.this,"",""+getResources().getString(R.string.password_change_success));
        }

    }

    private void populateAutoComplete() {
       // getLoaderManager().initLoader(0, null, this);
    }


    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {

        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (TextUtils.isEmpty(password)) {
            mPasswordView.setError(getResources().getString(R.string.error_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getResources().getString(R.string.error_mail));
            focusView = mEmailView;
            cancel = true;
        } else if (!Validate.validEmail2(email)) {
            mEmailView.setError(getResources().getString(R.string.error_mail));
            focusView = mEmailView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            //Lanzar servicio de login
            if (ConnectionDetector.isConnectingToInternet(LoginActivity.this)) {
                gcm = GoogleCloudMessaging.getInstance(LoginActivity.this);

                //Obtenemos el Registration ID guardado
                regid = getRegistrationId(LoginActivity.this);

                //Si no disponemos de Registration ID comenzamos el registro
               // if (regid.equals("")) {
                new TareaRegistroGCM(email,password).execute();
                //}
            }
            else
                Toast.makeText(LoginActivity.this, R.string.check_internet, Toast.LENGTH_SHORT).show();

        }
    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();
        GlobalData.delete(Constant.LOGIN_APP, this);
        Intent mIntent = new Intent(LoginActivity.this,
                RegisterOrSessionActivityActivity.class);
        startActivity(mIntent);
        overridePendingTransition(R.anim.fade_in_slow, R.anim.fade_out);
        finish();
    }

    /**
     * Servicio para login un usuario en el sistema
     */
    public void loginUser(final String email, String password, final String regId,final ProgressDialog progressDialog) {
        OkHttpClient client = new OkHttpClient();

        RequestBody formBody = new FormBody.Builder()
                .add("email", "" + email)
                .add("password", "" + password)
                .add("registrationId", "" + regId)
                .add("isFacebook","2")
                .add("ip", "" + Common.getLocalIpAddress(LoginActivity.this))
                .add("version", "" + Common.getVersionDevice(LoginActivity.this))
                .add("registrationIdOld",GlobalData.isKey(Constant.REGISTRATION_ID_GCM_OLD,LoginActivity.this)?GlobalData.get(Constant.REGISTRATION_ID_GCM_OLD,LoginActivity.this):"")
                .build();

        Request request = new Request.Builder()

                .url(Services.LOGIN_SERVICE)
                .post(formBody)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                progressDialog.dismiss();
                Looper.prepare();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Common.errorMessage(LoginActivity.this, "", getResources().getString(R.string.check_internet));
                    }
                });
                //Toast.makeText(LoginActivity.this, "Ocurrio un error en logueo usuario", Toast.LENGTH_SHORT).show();
                Looper.loop();
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                progressDialog.dismiss();


                final String result = response.body().string();
                Log.d("app2you", "respuesta login: " + result);
                Looper.prepare();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {


                        try {
                            JSONObject json = new JSONObject(result);

                            String success = json.getString("success");

                            if (success.equals("true")) {

                                sendPing();
                                String sessionToken = json.getString("sessionToken");
                                String wantedFound = json.getString("preferedSex");
                                String nickname = json.getString("nickname");
                                String accountTypeID = json.getString("accountTypeID");
                                if(!accountTypeID.equals(Constant.NOT_GOLD_BEFORE_TRIAL_NO_PICTURE))
                                    GlobalData.set(Constant.TUTORIAL_GAME, "Y", LoginActivity.this);

                                String isShareFacebook = json.has("shareFacebook")?json.getString("shareFacebook"):"0";
                                if(isShareFacebook.equals("1"))
                                    GlobalData.set(Constant.SHARE_FACEBOOK,"Y",LoginActivity.this);

                                String currentLikes = json.getString("currentLikes");
                                String emailVerified = json.getString("emailVerified");
                                String city = json.getString("city");
                                String genere = json.getString("sexId");
                                String personalStatus = json.getString("personalStatus");
                                String myUserId = json.getString("myUserId");
                                int creditsLoginDaily = json.has("creditsLoginDaily")?json.getInt("creditsLoginDaily"):5;
                                GlobalData.set(Constant.CREDITS_LOGIN_DAY,""+creditsLoginDaily,LoginActivity.this);

                                int creditsShareFacebook    = json.has("creditsShareFacebook")?json.getInt("creditsShareFacebook"):20;
                                GlobalData.set(Constant.CREDITS_SHARE_FACEBOOK,""+creditsShareFacebook,LoginActivity.this);


                                String avatar = "";
                                String lastConnection = "";

                                if(json.has("lastConnection"))
                                {
                                    lastConnection = json.getString("lastConnection");
                                }
                                else
                                {
                                    SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");
                                    Date today = Calendar.getInstance().getTime();
                                    lastConnection = myFormat.format(today);
                                }

                                setRegistrationId(LoginActivity.this,""+nickname,regId);

                                Gson gson = new Gson();
                                MyProfileData myProfileData = gson.fromJson(result, MyProfileData.class);
                                if (myProfileData.pimgs != null  && myProfileData.pimgs.size() > 0) {
                                    avatar = myProfileData.pimgs.get(0).avatarUrl;
                                    GlobalData.set(Constant.AVATAR_USER, "" + avatar, LoginActivity.this);
                                }
                                GlobalData.set(Constant.REGISTRATION_ID_GCM_OLD,""+regId,LoginActivity.this);
                                GlobalData.set(Constant.EMAIL_VERIFIED,""+emailVerified, LoginActivity.this);
                                GlobalData.set(Constant.LOGIN_APP, "Y", LoginActivity.this);
                                GlobalData.set(Constant.FIRST_REGISTER, "N", LoginActivity.this);
                                GlobalData.set(Constant.SESSION_TOKEN, "" + sessionToken, LoginActivity.this);
                                GlobalData.set(Constant.WANTED_FOUND, "" + wantedFound, LoginActivity.this);
                                GlobalData.set(Constant.NICKNAME, "" + nickname, LoginActivity.this);
                                GlobalData.set(Constant.USER_STATUS, "" + accountTypeID, LoginActivity.this);
                                GlobalData.set(Constant.CURRENT_LIKES, "" + currentLikes, LoginActivity.this);
                                GlobalData.set(Constant.MY_USER_ID,""+myUserId,LoginActivity.this);
                                GlobalData.set(Constant.CITY_USER, "" + city, LoginActivity.this);
                                GlobalData.set(Constant.EMAIL_ADDRESS,""+email,LoginActivity.this);
                                GlobalData.set(Constant.GENERE,""+genere,LoginActivity.this);
                                GlobalData.set(Constant.PERSONAL_STATUS,""+personalStatus,LoginActivity.this);
                                GlobalData.set(Constant.BIRTHDAY_VERIFIED,"Y",LoginActivity.this);
                                GlobalData.set(Constant.LAST_CONNECTION,lastConnection,LoginActivity.this);

                                //ejecuta la actividad principal
                                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                startActivity(intent);
                                overridePendingTransition(R.anim.fade_in_slow, R.anim.fade_out);
                                finish();

                            } else {


                                int errorMessage = Integer.parseInt(json.getString("errorNum"));
                                //Toast.makeText(LoginActivity.this, "Algo paso login: " + errorMessage, Toast.LENGTH_SHORT).show();
                                switch (errorMessage) {
                                    case 1:
                                        break;
                                    case 2:
                                        break;
                                    case 3:
                                        Toast.makeText(LoginActivity.this, R.string.invalid_data, Toast.LENGTH_SHORT).show();

                                        break;
                                    default:
                                        Toast.makeText(LoginActivity.this, R.string.invalid_data, Toast.LENGTH_SHORT).show();
                                        break;


                                }

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(LoginActivity.this, R.string.error_reponse_server, Toast.LENGTH_SHORT).show();
                        }
                        finally {
                            response.body().close();
                        }
                    }
                });
                Looper.loop();
            }
        });


    }

    /**
     * Metodo encargado de lanzar actividad para recuperar contrasena
     *
     * @param view
     */
    public void forgot(View view) {
        Intent intent = new Intent(LoginActivity.this,RecoveryPasswordActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.fade_in_slow, R.anim.fade_out);
        finish();

    }

    private static int getAppVersion(Context context)
    {
        try
        {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);

            return packageInfo.versionCode;
        }
        catch (PackageManager.NameNotFoundException e)
        {
            throw new RuntimeException("Error al obtener versión: " + e);
        }
    }




    private String getRegistrationId(Context context)
    {
        SharedPreferences prefs = getSharedPreferences(
                MainActivity.class.getSimpleName(),
                Context.MODE_PRIVATE);

        String registrationId = prefs.getString(PROPERTY_REG_ID, "");

        if (registrationId.length() == 0)
        {
            Log.d(TAG, "Registro GCM no encontrado.");
            return "";
        }

        String registeredUser =
                prefs.getString(PROPERTY_USER, "user");

        int registeredVersion =
                prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);

        long expirationTime =
                prefs.getLong(PROPERTY_EXPIRATION_TIME, -1);

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.getDefault());
        String expirationDate = sdf.format(new Date(expirationTime));

        Log.d(TAG, "Registro GCM encontrado (usuario=" + registeredUser +
                ", version=" + registeredVersion +
                ", expira=" + expirationDate + ")");

        int currentVersion = getAppVersion(context);

        if (registeredVersion != currentVersion)
        {
            Log.d(TAG, "Nueva versión de la aplicación.");
            return "";
        }
        else if (System.currentTimeMillis() > expirationTime)
        {
            Log.d(TAG, "Registro GCM expirado.");
            return "";
        }
        return registrationId;
    }

    private class TareaRegistroGCM extends AsyncTask<String,Integer,String>
    {
        String email;
        String password;

        ProgressDialog progressDialog;
        boolean status = true;

        public TareaRegistroGCM(String email, String password)
        {
            this.email = email;
            this.password = password;

        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(LoginActivity.this,R.style.CustomDialogTheme);
            progressDialog.setCancelable(false);
          //  progressDialog.setMessage(getResources().getString(R.string.init_session));
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            progressDialog.show();
            progressDialog.setContentView(R.layout.custom_progress_dialog);
            TextView txtView = (TextView) progressDialog.findViewById(R.id.txtProgressDialog);
            txtView.setText(""+getResources().getString(R.string.init_session));
        }

        @Override
        protected String doInBackground(String... params)
        {
            String msg = "";

            try
            {
                if (gcm == null)
                    gcm = GoogleCloudMessaging.getInstance(LoginActivity.this);


                //Nos registramos en los servidores de GCM
                regid = gcm.register(SENDER_ID);

                Log.d(TAG, "Registrado en GCM: registration_id=" + regid);

            }
            catch (IOException ex)
            {
                status = false;
                ex.printStackTrace();
                Log.d(TAG, "Error registro en GCM:" + ex.getMessage());

            }

            return msg;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(status) {
                closeKeyBoardGUI();
                loginUser(email, password, regid, progressDialog);
            }
            else {
                progressDialog.dismiss();
                new TareaRegistroGCM(email,password).execute();
                //Toast.makeText(LoginActivity.this, "error GCM ID login", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void setRegistrationId(Context context, String user, String regId)
    {
        SharedPreferences prefs = getSharedPreferences(
                MainActivity.class.getSimpleName(),
                Context.MODE_PRIVATE);

        int appVersion = getAppVersion(context);

        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PROPERTY_USER, user);
        editor.putString(PROPERTY_REG_ID, regId);
        editor.putInt(PROPERTY_APP_VERSION, appVersion);
        editor.putLong(PROPERTY_EXPIRATION_TIME,
                System.currentTimeMillis() + EXPIRATION_TIME_MS);

        editor.commit();
    }

    /**
     * Cerrar teclado en la vista
     */
    public void closeKeyBoardGUI() {
        KeyBoard.hide(mEmailView);
        KeyBoard.hide(mPasswordView);
    }
    //login

    //Enviar ping primera vez en login
    public void sendPing()
    {
        if(!GlobalData.isKey(Constant.SESSION_TOKEN,LoginActivity.this)) {
            if (!GlobalData.isKey(Constant.SERVER_PING_LOGIN_SUCCESS, LoginActivity.this)) {
                String ip = "false";

                int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                if (currentapiVersion >= Build.VERSION_CODES.M) {
                    sendPingSever(ip);
                } else {
                    ip = (Common.getLocalIpAddress(LoginActivity.this).length() > 0) ? Common.getLocalIpAddress(LoginActivity.this) : "false";
                    sendPingSever(ip);
                }

            }
        }
    }

    /**
     * Envia datos de instalacion al servidor, por primera vez
     */
    public void sendPingSever(String ip)
    {

        OkHttpClient client = new OkHttpClient();

        RequestBody formBody = new FormBody.Builder()
                .add("hashSession",GlobalData.isKey(Constant.HASH_SESSION,LoginActivity.this)?GlobalData.get(Constant.HASH_SESSION,LoginActivity.this):"false")
                .add("brand",""+Build.BRAND)
                .add("model",""+Build.MODEL)
                .add("so","1")
                .add("ip",""+ip)
                .add("deviceVersion", Common.getVersionDevice(LoginActivity.this))
                .add("appVersion",""+getResources().getString(R.string.app_version))
                .add("typeId","9")
                .build();

        Request request = new Request.Builder()
                .url(Services.REGISTER_INSTALL)
                .post(formBody)
                .build();

        client.newCall(request).enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(Call call, IOException e) {


            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                final String result = response.body().string();
                //Log.d("app2you", "respuesta  ping  " + result);

                Looper.prepare();

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            JSONObject json = new JSONObject(result);

                            String success = json.getString("success");

                            if (success.equals("true")) {
                                GlobalData.set(Constant.SERVER_PING_LOGIN_SUCCESS,"Y",LoginActivity.this);
                            } else {

                            }
                        } catch (JSONException ex) {
                            ex.printStackTrace();
                        }
                        finally {
                            response.body().close();
                        }

                    }
                });
                Looper.loop();
            }
        });
    }


}

