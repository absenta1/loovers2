package com.appdupe.flamer.pojo;

import com.google.gson.annotations.SerializedName;

public class UpdatePrefrence {

    @SerializedName("success")
    public Boolean success;
    @SerializedName("errorNum")
    public int errNum;
    @SerializedName("errorMsg")
    public String errMsg;

}
