package com.android.slidingmenu.security;


import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.slidingmenu.MainActivity;
import com.android.slidingmenu.util.Common;
import com.android.slidingmenu.util.GlobalData;
import com.android.slidingmenu.util.KeyBoard;
import com.android.slidingmenu.util.TypesLetter;
import com.android.slidingmenu.util.Visibility;
import com.appdupe.flamer.utility.Constant;
import com.loovers.app.R;

/**
 * Created by Juan Martin Bernal on 19/2/16.
 */
public class FragmentSecurity extends DialogFragment implements View.OnClickListener{

    public MainActivity mainActivity;
    private TextView senderName,txtNumberAttemps,titleSecurity;
    private ImageView imgBack;
    public LinearLayout containerOptionsBackChat;
    public Button btnAccept;
    public EditText fieldSecurity, fieldRepeatSecurity;
    public int from = 0;
    public int profile = 0;

    public FragmentSecurity(){}

    public FragmentSecurity(MainActivity mainActivity)
    {
        this.mainActivity = mainActivity;
    }

    public FragmentSecurity(MainActivity mainActivity, int from)
    {
        this.mainActivity = mainActivity;
        this.from         = from;
    }
    public FragmentSecurity(MainActivity mainActivity, int from, int profile)
    {
        this.mainActivity = mainActivity;
        this.from         = from;
        this.profile      = profile;
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_security, null);

        if(mainActivity == null)
            mainActivity = (MainActivity)getActivity();

        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        initComponents(view);

        return view;

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(android.support.v4.app.DialogFragment.STYLE_NO_FRAME, android.R.style.Theme_Holo_Light_DarkActionBar);
    }
    public void initComponents(View view)
    {
        senderName = (TextView)view.findViewById(R.id.senderName);
        titleSecurity = (TextView)view.findViewById(R.id.titleSecurity);
        txtNumberAttemps = (TextView)view.findViewById(R.id.txtNumberAttemps);
        senderName.setText(""+mainActivity.getResources().getString(R.string.title_security));
        senderName.setTypeface(TypesLetter.getAvenir(mainActivity));
        titleSecurity.setTypeface(TypesLetter.getSansRegular(mainActivity));
        containerOptionsBackChat = (LinearLayout)view.findViewById(R.id.containerOptionsBackChat);
        imgBack                  = (ImageView)view.findViewById(R.id.imgBack);

        btnAccept                = (Button)view.findViewById(R.id.btnAccept);
        fieldSecurity            = (EditText)view.findViewById(R.id.fieldSecurity);
        fieldRepeatSecurity      = (EditText)view.findViewById(R.id.fieldRepeatSecurity);
        btnAccept.setTypeface(TypesLetter.getSansRegular(mainActivity));
        fieldSecurity.setTypeface(TypesLetter.getAvenir(mainActivity));
        fieldRepeatSecurity.setTypeface(TypesLetter.getAvenir(mainActivity));

        if(GlobalData.isKey(Constant.PIN_SECURITY,mainActivity)) {
            Visibility.gone(fieldRepeatSecurity);

            if(from == 1)
            {
                fieldSecurity.addTextChangedListener(passwordWatcher);
                titleSecurity.setText(""+mainActivity.getResources().getString(R.string.entry_pin_unlock));
                if(!GlobalData.isKey(Constant.NUMBER_ATTEMPS,mainActivity))
                    GlobalData.set(Constant.NUMBER_ATTEMPS,"1",mainActivity);

                int numberAttemp = Integer.parseInt(GlobalData.get(Constant.NUMBER_ATTEMPS,mainActivity));
                txtNumberAttemps.setText(mainActivity.getResources().getString(R.string.number_attemps)+" "+numberAttemp+"/"+Constant.NUMBER_MAX_ATTEMPS);

                Visibility.visible(txtNumberAttemps);
                Visibility.invisible(imgBack);
                Visibility.invisible(containerOptionsBackChat);
                Visibility.invisible(btnAccept);
            }
            else
            {
                titleSecurity.setText(""+mainActivity.getResources().getString(R.string.delete_pin_security));
            }
        }
        else
        {
            titleSecurity.setText(""+mainActivity.getResources().getString(R.string.protected_app_message));
        }

        imgBack.setOnClickListener(this);
        containerOptionsBackChat.setOnClickListener(this);
        btnAccept.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.containerOptionsBackChat)
        {
            KeyBoard.hide(fieldSecurity);
            KeyBoard.hide(fieldRepeatSecurity);
            if(mainActivity.menu.isMenuShowing())
                mainActivity.setProfilePick(mainActivity.profileimage);
            dismiss();

        }
        else if(v.getId() == R.id.imgBack)
        {
            KeyBoard.hide(fieldSecurity);
            KeyBoard.hide(fieldRepeatSecurity);
            if(mainActivity.menu.isMenuShowing())
                mainActivity.setProfilePick(mainActivity.profileimage);
            dismiss();
        }
        else if(v.getId() == R.id.btnAccept)
        {
            if(from == 1)
            {
                //loovers con código de bloqueo
                int numberAttemp = Integer.parseInt(GlobalData.get(Constant.NUMBER_ATTEMPS,mainActivity));
                String pin = fieldSecurity.getText().toString().trim();
                if(!pin.isEmpty() && pin.length() > 0) {
                    String pin_memory = GlobalData.get(Constant.PIN_SECURITY, mainActivity);

                    if (pin.equalsIgnoreCase(pin_memory)) {
                        GlobalData.set(Constant.NUMBER_ATTEMPS, "1", mainActivity);
                        KeyBoard.hide(fieldSecurity);
                        KeyBoard.hide(fieldRepeatSecurity);
                        dismiss();
                    } else {
                        //Common.errorMessage(mainActivity, "", "Pin invalido");
                        fieldSecurity.setText("");
                        int numberTmp = numberAttemp + 1;
                        txtNumberAttemps.setText(mainActivity.getResources().getString(R.string.number_attemps) + " " + numberTmp + "/" + Constant.NUMBER_MAX_ATTEMPS);
                        GlobalData.set(Constant.NUMBER_ATTEMPS, "" + numberTmp, mainActivity);

                        if (numberTmp == Constant.NUMBER_MAX_ATTEMPS) {
                            GlobalData.set(Constant.NUMBER_ATTEMPS, "0", mainActivity);
                            // dismiss();
                            Common.closeSessionWallamatchSecurity(mainActivity);
                        } else
                            Toast.makeText(mainActivity, R.string.invalid_pin, Toast.LENGTH_SHORT).show();

                    }
                }
                else
                    Toast.makeText(mainActivity, R.string.entry_pin_unlock, Toast.LENGTH_SHORT).show();

            }
            else {
                if (GlobalData.isKey(Constant.PIN_SECURITY, mainActivity)) {
                    String pin = fieldSecurity.getText().toString().trim();
                    String pin_memory = GlobalData.get(Constant.PIN_SECURITY, mainActivity);
                    fieldSecurity.setText("");
                    if (pin.equalsIgnoreCase(pin_memory)) {
                        GlobalData.delete(Constant.PIN_SECURITY, mainActivity);
                        Visibility.visible(fieldRepeatSecurity);
                        titleSecurity.setText(""+mainActivity.getResources().getString(R.string.protected_app_message));
                        Toast.makeText(mainActivity, R.string.pin_unlock, Toast.LENGTH_SHORT).show();
                        KeyBoard.hide(fieldSecurity);
                        KeyBoard.hide(fieldRepeatSecurity);
                        if(mainActivity.menu.isMenuShowing())
                            mainActivity.setProfilePick(mainActivity.profileimage);
                        dismiss();
                    } else {
                        Toast.makeText(mainActivity, R.string.invalid_pin, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    //no existe pin de seguridad
                    String pin = fieldSecurity.getText().toString().trim();
                    String repeat_pin = fieldRepeatSecurity.getText().toString().trim();
                    if (pin.equals(repeat_pin) && pin.length() == 4 && repeat_pin.length() == 4) {
                        GlobalData.set(Constant.PIN_SECURITY, "" + pin, mainActivity);

                        KeyBoard.hide(fieldSecurity);
                        KeyBoard.hide(fieldRepeatSecurity);
                        if(mainActivity.menu.isMenuShowing())
                            mainActivity.setProfilePick(mainActivity.profileimage);
                        dismiss();
                        Toast.makeText(mainActivity, R.string.pin_save_success, Toast.LENGTH_SHORT).show();
                    } else {
                        Common.errorMessage(mainActivity, "", ""+getResources().getString(R.string.error_write_pin));
                    }
                }
            }

        }
    }

    @Override
    public void onPause() {
        super.onPause();
        KeyBoard.hide(fieldSecurity);
        KeyBoard.hide(fieldRepeatSecurity);
        dismiss();

    }

    @Override
    public void onResume() {
        super.onResume();
        if(from != 1)
        {
            getDialog().setOnKeyListener(new DialogInterface.OnKeyListener() {
                @Override
                public boolean onKey(android.content.DialogInterface dialog, int keyCode,
                                     android.view.KeyEvent event) {


                    if ((keyCode == android.view.KeyEvent.KEYCODE_BACK)) {
                        //This is the filter
                        if (event.getAction() != KeyEvent.ACTION_DOWN) {

                            KeyBoard.hide(fieldSecurity);
                            KeyBoard.hide(fieldRepeatSecurity);
                            dismiss();

                            return true;
                        }
                        else {
                            //Hide your keyboard here!!!!!!
                            //preguntar teclado esta abierto

                            InputMethodManager imm = (InputMethodManager) getActivity()
                                    .getSystemService(Context.INPUT_METHOD_SERVICE);

                            if (imm.isAcceptingText()) {
                                //writeToLog("Software Keyboard was shown");
                                KeyBoard.hide(fieldSecurity);
                                KeyBoard.hide(fieldRepeatSecurity);
                            } else {
                                //writeToLog("Software Keyboard was not shown");
                                KeyBoard.hide(fieldSecurity);
                                KeyBoard.hide(fieldRepeatSecurity);
                                dismiss();
                            }


                            return true; // pretend we've processed it
                        }
                    } else
                        return false; // pass on to be processed as normal
                }
            });
        }

    }
    private final TextWatcher passwordWatcher = new TextWatcher() {
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
           ;
        }

        public void afterTextChanged(Editable s) {
            if (s.length() == 4) {
                int numberAttemp = Integer.parseInt(GlobalData.get(Constant.NUMBER_ATTEMPS, mainActivity));
                String pin = fieldSecurity.getText().toString().trim();
                if (!pin.isEmpty() && pin.length() > 0) {
                    String pin_memory = GlobalData.get(Constant.PIN_SECURITY, mainActivity);

                    if (pin.equalsIgnoreCase(pin_memory)) {
                        GlobalData.set(Constant.NUMBER_ATTEMPS, "1", mainActivity);
                        KeyBoard.hide(fieldSecurity);
                        KeyBoard.hide(fieldRepeatSecurity);

                        if(profile == 1)
                        {
                            GlobalData.delete(Constant.PIN_SECURITY, mainActivity);
                            if(mainActivity.menu.isMenuShowing())
                                mainActivity.setProfilePick(mainActivity.profileimage);
                        }
                        dismiss();
                    } else {
                        //Common.errorMessage(mainActivity, "", "Pin invalido");
                        fieldSecurity.setText("");
                        int numberTmp = numberAttemp + 1;
                        txtNumberAttemps.setText(mainActivity.getResources().getString(R.string.number_attemps) + " " + numberTmp + "/" + Constant.NUMBER_MAX_ATTEMPS);
                        GlobalData.set(Constant.NUMBER_ATTEMPS, "" + numberTmp, mainActivity);

                        if (numberTmp == Constant.NUMBER_MAX_ATTEMPS) {
                            GlobalData.set(Constant.NUMBER_ATTEMPS, "0", mainActivity);
                            // dismiss();
                            Common.closeSessionWallamatchSecurity(mainActivity);
                        } else
                            Toast.makeText(mainActivity, R.string.invalid_pin, Toast.LENGTH_SHORT).show();

                    }
                }
                else
                    Toast.makeText(mainActivity, R.string.entry_pin_unlock, Toast.LENGTH_SHORT).show();
            }

        }
    };
}
