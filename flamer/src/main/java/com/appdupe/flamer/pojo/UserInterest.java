package com.appdupe.flamer.pojo;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class UserInterest 
{
	@SerializedName("data")
       private ArrayList<InterestData>data;

	public ArrayList<InterestData> getData() {
		return data;
	}

	public void setData(ArrayList<InterestData> data) {
		this.data = data;
	}
	
}
