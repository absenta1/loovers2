package com.android.slidingmenu.messages;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.slidingmenu.MainActivity;
import com.android.slidingmenu.fragments.DetailProfileFragment;
import com.android.slidingmenu.fragments.FragmentGame;
import com.android.slidingmenu.util.Common;
import com.android.slidingmenu.util.DateEnrutate;
import com.android.slidingmenu.util.GlobalData;
import com.android.slidingmenu.util.TypesLetter;
import com.android.slidingmenu.util.Visibility;
import com.android.slidingmenu.webservices.Services;
import com.appdupe.androidpushnotifications.ChatActivity;
import com.appdupe.flamer.pojo.MatchesData;
import com.appdupe.flamer.utility.CircleTransform;
import com.appdupe.flamer.utility.Constant;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.loovers.app.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;

import okhttp3.Call;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by Juan Martin Bernal on 18/4/16.
 */
public class RequestChatsFragment extends Fragment {

    public View view;
    public ListView listFriends;

    public FriendsAdapter friendsAdapter;
    public TextView txtDescRequestChat, txtTitleRequestChat;
    public MainActivity mainActivity;
    public Button btnPLayWalla;

    public RequestChatsFragment(){}
    public RequestChatsFragment(MainActivity mainActivity)
    {
        this.mainActivity = mainActivity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.request_chat_fragment,container, false);

        if(mainActivity == null)
            mainActivity = (MainActivity)getActivity();

        Visibility.visible(mainActivity.tvTitle);

        listFriends                             = (ListView)view.findViewById(R.id.listView);
        mainActivity.containerNoRequestChat     = (LinearLayout) view.findViewById(R.id.containerNoRequestChat);
        txtDescRequestChat                      = (TextView)view.findViewById(R.id.txtDescRequestChat);
        txtTitleRequestChat                     = (TextView)view.findViewById(R.id.txtTitleRequestChat);
        btnPLayWalla                            = (Button)view.findViewById(R.id.btnPLayWalla);

        btnPLayWalla.setTransformationMethod(null);
        btnPLayWalla.setTypeface(TypesLetter.getSansRegular(mainActivity));
        txtTitleRequestChat.setTypeface(TypesLetter.getSansBold(mainActivity));
        txtDescRequestChat.setTypeface(TypesLetter.getSansRegular(mainActivity));

        return view;
    }



    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        GlobalData.set(Constant.COUNTER_MESSAGES,"0",mainActivity);

        listFriends.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                MatchesData matchesData = (MatchesData) listFriends.getItemAtPosition(position);
                //data.count_messages = "0";
                matchesData.count_messages = "0";
                matchesData.userId         = matchesData.id_user;
                matchesData.firstName      = matchesData.name;
                matchesData.avatarUrl      = matchesData.picture;
                // Visibility.invisible(listFriends);
                /*MatchesData matchesData = new MatchesData();
                matchesData.userId      = chat.id_user;
                matchesData.firstName   = chat.name;
                matchesData.avatarUrl   = chat.picture;*/

                ChatActivity dialog = new ChatActivity(mainActivity, matchesData,Constant.DIRECT_CHAT, RequestChatsFragment.this,position, friendsAdapter);
                dialog.show(mainActivity.mFragmentManager, "ChatActivity.TAG");

            }
        });

        listFriends.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                MatchesData matchesData = (MatchesData) listFriends.getItemAtPosition(position);
                //RelativeLayout container = (RelativeLayout)view.findViewById(R.id.containerRowChat);
                //Common.errorMessage(mainActivity,"","¿Estás seguro de eliminar esta solicitud? "  + chat.name);

                dialogDeleteRelation("¿Estas seguro de eliminar esta solicitud?",Constant.DELETE_DIRECT_CHAT,matchesData.id_user,position);
                return true;
            }
        });

        btnPLayWalla.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainActivity.replaceFragment(new FragmentGame(mainActivity));
            }
        });



    }

    /**
     * Dialogo para eliminar relación
     */
    public void dialogDeleteRelation(String message, final int relationId,final String userId, final int position) {
        // custom dialog
        final Dialog dialog =  new Dialog(mainActivity,R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        dialog.setContentView(R.layout.modal_general);

        TextView text = (TextView) dialog.findViewById(R.id.text);
        text.setText(""+message);


        Button exit = (Button) dialog.findViewById(R.id.exit);
        Button accept = (Button) dialog.findViewById(R.id.accept);
        // if button is clicked, close the custom dialog
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                deleteRelationship(""+relationId,userId,position);

            }
        });

        dialog.show();

    }
    /**
     * Metodo encargado de eliminar la relación de chat Mathc o  chat directo
     * @param userId
     * @param position
     */
    public void deleteRelationship(final String relationId, final String userId, final int position)
    {
        final ProgressDialog progressDialog = new ProgressDialog(mainActivity);
        progressDialog.setMessage(""+mainActivity.getResources().getString(R.string.delete_relation));
        progressDialog.setCancelable(false);
        progressDialog.show();
        OkHttpClient client = new OkHttpClient();
        RequestBody formBody = new FormBody.Builder()
                .add("sessionToken", "" + GlobalData.get(Constant.SESSION_TOKEN, mainActivity))
                .add("serviceId", relationId )
                .add("otherUserId",userId)
                .build();

        Request request = new Request.Builder()
                .url(Services.DELETE_RELATIONSHIP)
                .post(formBody)
                .build();

        client.newCall(request).enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                progressDialog.dismiss();
                Looper.prepare();
                mainActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Common.errorMessage(mainActivity, "", mainActivity.getResources().getString(R.string.check_internet));
                    }
                });

                Looper.loop();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                progressDialog.dismiss();
                final String result = response.body().string();
                Log.d("app2you", "respuesta eliminar relacion: " + result);

                Looper.prepare();


                mainActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        JSONObject json = null;
                        try {
                            json = new JSONObject(result);

                            String success = json.getString("success");
                            if (success.equals("true")) {
                                //al ingresar a cerca de mi se piden nueva mente
                                GlobalData.delete("listPersons",mainActivity);
                                GlobalData.delete("listnewpersons",mainActivity);

                                mainActivity.listRequestChat.remove(position);

                                if(friendsAdapter != null)
                                    friendsAdapter.notifyDataSetChanged();

                                int listSize = mainActivity.listRequestChat.size();
                                if( listSize == 0) {
                                    Visibility.gone(mainActivity.badgeTab);
                                    if(mainActivity.containerNoRequestChat != null)
                                        Visibility.visible(mainActivity.containerNoRequestChat);
                                }
                                else
                                {
                                    if(mainActivity.badgeTab != null)
                                        mainActivity.badgeTab.setText(""+listSize);
                                }



                            } else {
                                Common.errorMessage(mainActivity, "", mainActivity.getResources().getString(R.string.check_internet));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

                Looper.loop();
            }
        });
    }


    public class FriendsAdapter extends ArrayAdapter<MatchesData> {
        public Context mContext;
        public List<MatchesData> chats;

        public FriendsAdapter(Context context, List<MatchesData> chats) {
            super(context,R.layout.row_friends,chats);
            this.mContext = context;
            this.chats = chats;
        }


        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ViewHolder holder;
            if (convertView == null) {
                holder = new ViewHolder();
                convertView = LayoutInflater.from(mContext).inflate(R.layout.row_friends, parent, false);
                holder.txtNameUser          = (TextView) convertView.findViewById(R.id.txtNameUser);
                holder.imgUser              = (ImageView) convertView.findViewById(R.id.imgUser);
                holder.txtMessage           = (TextView) convertView .findViewById(R.id.txtMessage);
                holder.txtDate              = (TextView) convertView .findViewById(R.id.txtDate);
                holder.txtNumMessages       = (TextView)convertView.findViewById(R.id.txtNumMessages);
                holder.containerRowChat     = (RelativeLayout) convertView.findViewById(R.id.containerRowChat);

                holder.txtNameUser.setTypeface(TypesLetter.getSansRegular(mContext));
                holder.txtMessage.setTypeface(TypesLetter.getSansRegular(mContext));
                holder.txtDate.setTypeface(TypesLetter.getSansRegular(mContext));
                convertView.setTag(holder);
            } else
                holder = (ViewHolder) convertView.getTag();

            final MatchesData chat = getItem(position);


            if(chat != null)
            {
                // Log.d("app2you", "usuario " + chat.name + " fecha: " + chat.date);
                if(!(chat.picture.isEmpty()) && chat.picture.length() > 0)
                    Picasso.with(mContext).load(chat.picture).error(R.drawable.no_foto).transform(new CircleTransform()).into(holder.imgUser);
                else
                    Picasso.with(mContext).load(R.drawable.logo_parejita_2).transform(new CircleTransform()).into(holder.imgUser);

                String messageEncode = chat.lastestMessage.trim();
                try {
                    //messageEncode  = messageEncode.replaceAll("%","");
                    messageEncode = URLDecoder.decode(messageEncode, "UTF-8");
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                holder.txtNameUser.setText(""+chat.name);
                if(chat.messageTypeId != null && chat.messageTypeId.equals(Constant.REQUEST_PRIVATE_PHOTOS))
                    holder.txtMessage.setText("Solicitud de foto privada");
                else
                    holder.txtMessage.setText(""+ Html.fromHtml(messageEncode));

                holder.txtDate.setText(""+ DateEnrutate.getUpdate(chat.date));
                //String status = GlobalData.get(Constant.USER_STATUS,mainActivity);
                //holder.txtNumMessages.setBackgroundResource((status.equals(Constant.GOLD) || status.equals(Constant.GOLD_TRIAL))?R.drawable.alerta_gold:R.drawable.alerta);
                if(chat.count_messages != null && !chat.count_messages.equals("0"))
                {
                    Visibility.visible(holder.txtNumMessages);
                    holder.txtNumMessages.setText(""+chat.count_messages);
                }
                else
                    Visibility.gone(holder.txtNumMessages);
            }
            holder.imgUser.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //foto para ver perfil
                    openDialog(chat);
                }
            });

            return convertView;
        }


        private class ViewHolder {
            public TextView txtNameUser, txtMessage,txtDate,txtNumMessages;
            public ImageView imgUser;
            public RelativeLayout containerRowChat;

        }

        @Override
        public long getItemId(int position) {
            // Unimplemented, because we aren't using Sqlite.
            return 0;
        }

        /**
         * Metodo encargado de mostrar los detalles del chat seleccionado
         * @param matchesData
         */
        public void openDialog(final MatchesData matchesData)
        {
            // custom dialog
            final Dialog dialog = new Dialog(mainActivity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.setContentView(R.layout.custom_dialog_friend);

            final ProgressBar progressBarFriend = (ProgressBar)dialog.findViewById(R.id.progressBarFriend);
            TextView title = (TextView) dialog .findViewById(R.id.name_site);
            title.setText("" + matchesData.name);
            title.setTypeface(TypesLetter.getSansBold(mainActivity));


            ImageView image = (ImageView) dialog
                    .findViewById(R.id.image_site);
       /* if (poi.image != null)
            image.setImageBitmap(BitmapFactory.decodeByteArray(poi.image, 0, poi.image.length));*/
            Picasso.with(mainActivity) .load(matchesData.picture).error(R.drawable.no_foto).into(image, new Callback() {
                @Override
                public void onSuccess() {
                    Visibility.gone(progressBarFriend);
                }

                @Override
                public void onError() {
                    Visibility.gone(progressBarFriend);
                }
            });

            ImageView email = (ImageView) dialog.findViewById(R.id.email);
            // if button is clicked, close the custom dialog
            email.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    matchesData.userId         = matchesData.id_user;
                    matchesData.firstName      = matchesData.name;
                    matchesData.avatarUrl      = matchesData.picture;
                    ChatActivity dialog = new ChatActivity(mainActivity, matchesData);
                    dialog.show(mainActivity.mFragmentManager, "ChatActivity.TAG");

                }
            });

            ImageView call = (ImageView) dialog.findViewById(R.id.call);
            // if button is clicked, close the custom dialog
            call.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(matchesData.id_user.equals(Constant.ID_SUPPORT) || matchesData.id_user.equals(Constant.ID_WALLAMATCH))
                    {
                        Common.errorMessage(mainActivity,"",""+mainActivity.getResources().getString(R.string.message_no_profile));
                    }
                    else {
                        dialog.dismiss();
                        matchesData.userId         = matchesData.id_user;
                        matchesData.firstName      = matchesData.name;
                        matchesData.avatarUrl      = matchesData.picture;
                        DetailProfileFragment dialog = new DetailProfileFragment(mainActivity, matchesData, Constant.GAME);
                        dialog.show(mainActivity.mFragmentManager, "DetailProfileFragment.TAG");
                    }
                }
            });

            dialog.show();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if(mainActivity.listRequestChat.size() > 0)
        {
            friendsAdapter = new FriendsAdapter(mainActivity, mainActivity.listRequestChat);
            listFriends.setAdapter(friendsAdapter);
            friendsAdapter.notifyDataSetChanged();
            Visibility.visible(listFriends);
            Visibility.gone(mainActivity.containerNoRequestChat);
        }
        else
            Visibility.visible(mainActivity.containerNoRequestChat);

    }



}
