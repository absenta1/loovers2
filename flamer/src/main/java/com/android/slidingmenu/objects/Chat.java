package com.android.slidingmenu.objects;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Juan Martin Bernal on 14/1/16.
 */
public class Chat implements Serializable, Comparable<Chat> {

    @SerializedName("otherUserId")
    public String id_user;

    @SerializedName("name")
    public String name;

    @SerializedName("avatarUrl")
    public String picture;

    @SerializedName("lastestMessage")
    public String message;

    @SerializedName("date")
    public String date;

    @SerializedName("numberMessageNotRead")
    public String count_messages;

    @SerializedName("messageTypeId")
    public String messageTypeId;

    @SerializedName("chatTypeId")
    public int chatTypeId;

    @SerializedName("isMatch")
    public int  isMatch;

    @Override
    public String toString() {
        return id_user +  " " + name + " " + picture + " " + message;
    }


    public Date StringToDate(String dt)
    {

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        try {
            date = format.parse(dt);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return date;
    }
    @Override
    public int compareTo(Chat chat) {
        Date date1 = StringToDate(date);
        Date date2 = StringToDate(chat.date);
        if (date1 == null || date2 == null)
            return 0;
        return date1.compareTo(date2);
    }
}
