package com.android.slidingmenu.objects;

/**
 * Created by Juan Martin Bernal on 4/4/16.
 */
public class ItemOptionChat {

    public int icon;
    public String name;

    public ItemOptionChat(int icon, String name) {
        this.icon = icon;
        this.name = name;
    }
}
