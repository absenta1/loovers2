package com.android.slidingmenu.adapter;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.slidingmenu.MainActivity;
import com.android.slidingmenu.fragments.DetailProfileFragment;
import com.android.slidingmenu.util.Common;
import com.android.slidingmenu.util.DateEnrutate;
import com.android.slidingmenu.util.GlobalData;
import com.android.slidingmenu.util.TypesLetter;
import com.android.slidingmenu.util.Visibility;
import com.appdupe.androidpushnotifications.ChatActivity;
import com.appdupe.flamer.pojo.MatchesData;
import com.appdupe.flamer.utility.CircleTransform;
import com.appdupe.flamer.utility.Constant;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.loovers.app.R;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Collections;
import java.util.List;

/**
 * Created by Juan Martin Bernal on 26/4/16.
 */
public class FriendsAdapter extends ArrayAdapter<MatchesData> {
    public MainActivity mainActivity;
    public List<MatchesData> chats;
    public String userId = "";

    public FriendsAdapter(MainActivity mainActivity, List<MatchesData> chats) {
        super(mainActivity, R.layout.row_friends, chats);
        this.mainActivity = mainActivity;
        this.chats = chats;
        userId = GlobalData.isKey(Constant.MY_USER_ID,mainActivity)?GlobalData.get(Constant.MY_USER_ID,mainActivity):"";
    }

    @Override
    public void notifyDataSetChanged() {
        Collections.sort(chats, Collections.<MatchesData>reverseOrder());
        super.notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        //formato hora mensaje 2016-04-22 18:27:20
        ViewHolder holder;
        if (convertView == null) {
            holder                      = new ViewHolder();
            convertView                 = LayoutInflater.from(mainActivity).inflate(R.layout.row_friends, parent, false);
            holder.txtNameUser          = (TextView) convertView.findViewById(R.id.txtNameUser);
            holder.imgUser              = (ImageView) convertView.findViewById(R.id.imgUser);
            holder.txtMessage           = (TextView) convertView.findViewById(R.id.txtMessage);
            holder.txtDate              = (TextView) convertView.findViewById(R.id.txtDate);
            holder.txtNumMessages       = (TextView) convertView.findViewById(R.id.txtNumMessages);
            holder.containerRowChat     = (RelativeLayout) convertView.findViewById(R.id.containerRowChat);

            holder.txtNameUser.setTypeface(TypesLetter.getSansRegular(mainActivity));
            holder.txtMessage.setTypeface(TypesLetter.getSansRegular(mainActivity));
            holder.txtDate.setTypeface(TypesLetter.getSansRegular(mainActivity));
            convertView.setTag(holder);
        } else
            holder = (ViewHolder) convertView.getTag();

        final MatchesData chat = chats.get(position);

        Visibility.visible(holder.txtDate);
        Visibility.visible(holder.txtNameUser);
        //holder.txtMessage.setTextSize(16);
        holder.txtMessage.setSingleLine(true);

        if (chat != null) {

            if(chat.userId != null && chat.userId.equals(Constant.PROMOTION_ID)) {
                //#1DA1F2,331660(convence), 37176A
                holder.containerRowChat.setBackgroundColor(Color.parseColor("#27104B"));
                Visibility.gone(holder.txtDate);
                Visibility.gone(holder.txtNameUser);
                holder.txtMessage.setTextSize(18);
                holder.txtMessage.setTextColor(Color.WHITE);
                Visibility.gone(holder.txtNumMessages);
                holder.txtMessage.setTypeface(TypesLetter.getSansRegular(mainActivity), Typeface.BOLD);
                holder.txtMessage.setText(chat.lastestMessage);
                holder.txtMessage.setSingleLine(false);
                if(chat.featureId == Constant.FEATURE_3) {
                    if (chat.picture != null && chat.picture.length() > 0)
                        Picasso.with(mainActivity).load(chat.picture).error(R.drawable.no_foto).placeholder(R.drawable.no_foto).transform(new CircleTransform()).into(holder.imgUser);
                    else
                        Picasso.with(mainActivity).load(R.drawable.no_foto).transform(new CircleTransform()).into(holder.imgUser);
                }
                else
                    holder.imgUser.setImageResource(chat.imageResource);
            }
            else {
                int sdk = android.os.Build.VERSION.SDK_INT;
                if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    holder.containerRowChat.setBackgroundDrawable(null);
                } else {
                    holder.containerRowChat.setBackground(null);
                }

                if (chat.picture != null && chat.picture.length() > 0)
                    Picasso.with(mainActivity).load(chat.picture).error(R.drawable.no_foto).placeholder(R.drawable.no_foto).transform(new CircleTransform()).into(holder.imgUser);
                else
                    Picasso.with(mainActivity).load(R.drawable.logo_parejita_2).transform(new CircleTransform()).into(holder.imgUser);


                String messageEncode = (chat.lastestMessage != null) ? chat.lastestMessage.trim() : "";
                try {
                    if (userId.equalsIgnoreCase("" + chat.senderIdLastestMessage))
                        messageEncode = URLDecoder.decode("Tú: " + messageEncode, "UTF-8");
                    else
                        messageEncode = URLDecoder.decode(messageEncode, "UTF-8");
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                holder.txtNameUser.setText("" + chat.name);
                if (chat.messageTypeId != null && chat.messageTypeId.equals(Constant.REQUEST_PRIVATE_PHOTOS))
                    holder.txtMessage.setText("Solicitud de foto privada");
                else
                    holder.txtMessage.setText("" + Html.fromHtml(messageEncode));

                holder.txtDate.setText((chat.date != null) ? "" + DateEnrutate.getUpdate(chat.date) : "");
                //String status = GlobalData.get(Constant.USER_STATUS,mainActivity);
                //holder.txtNumMessages.setBackgroundResource((status.equals(Constant.GOLD) || status.equals(Constant.GOLD_TRIAL))?R.drawable.alerta_gold:R.drawable.alerta);
                if (chat.count_messages != null && !chat.count_messages.equals("0")) {
                    Visibility.visible(holder.txtNumMessages);
                    holder.txtNumMessages.setText("" + chat.count_messages);
                    holder.txtMessage.setTextColor(Color.WHITE);
                    holder.txtMessage.setTypeface(TypesLetter.getSansRegular(mainActivity), Typeface.BOLD);

                } else {
                    Visibility.gone(holder.txtNumMessages);
                    holder.txtMessage.setTextColor(Color.parseColor("#c3c3c3"));
                }
            }
        }
        holder.imgUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //foto para ver perfil
                if(chat.userId != null && !chat.userId.equals(Constant.PROMOTION_ID))
                    openDialog(chat);
            }
        });

        return convertView;
    }


    private class ViewHolder {
        public TextView txtNameUser, txtMessage, txtDate, txtNumMessages;
        public ImageView imgUser;
        public RelativeLayout containerRowChat;

    }

    /**
     * Metodo encargado de mostrar los detalles del chat seleccionado
     * @param matchesData
     */
    public void openDialog(final MatchesData matchesData)
    {
        // custom dialog
        final Dialog dialog = new Dialog(mainActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_dialog_friend);

        final ProgressBar progressBarFriend = (ProgressBar)dialog.findViewById(R.id.progressBarFriend);
        TextView title = (TextView) dialog .findViewById(R.id.name_site);
        title.setText("" + matchesData.name);
        title.setTypeface(TypesLetter.getSansBold(mainActivity));


        ImageView image = (ImageView) dialog
                .findViewById(R.id.image_site);
       /* if (poi.image != null)
            image.setImageBitmap(BitmapFactory.decodeByteArray(poi.image, 0, poi.image.length));*/
        Picasso.with(mainActivity) .load(matchesData.picture).error(R.drawable.no_foto).into(image, new Callback() {
            @Override
            public void onSuccess() {
                Visibility.gone(progressBarFriend);
            }

            @Override
            public void onError() {
                Visibility.gone(progressBarFriend);
            }
        });

        ImageView email = (ImageView) dialog.findViewById(R.id.email);
        // if button is clicked, close the custom dialog
        email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                matchesData.userId         = matchesData.id_user;
                matchesData.firstName      = matchesData.name;
                matchesData.avatarUrl      = matchesData.picture;
                ChatActivity dialog = new ChatActivity(mainActivity, matchesData);
                dialog.show(mainActivity.mFragmentManager, "ChatActivity.TAG");

            }
        });

        ImageView call = (ImageView) dialog.findViewById(R.id.call);
        // if button is clicked, close the custom dialog
        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(matchesData.id_user.equals(Constant.ID_SUPPORT) || matchesData.id_user.equals(Constant.ID_WALLAMATCH))
                {
                    Common.errorMessage(mainActivity,"",""+mainActivity.getResources().getString(R.string.message_no_profile));
                }
                else {
                    dialog.dismiss();
                    matchesData.userId         = matchesData.id_user;
                    matchesData.firstName      = matchesData.name;
                    matchesData.avatarUrl      = matchesData.picture;
                    DetailProfileFragment dialog = new DetailProfileFragment(mainActivity, matchesData, Constant.GAME);
                    dialog.show(mainActivity.mFragmentManager, "DetailProfileFragment.TAG");
                }
            }
        });

        dialog.show();
    }
}