package com.appdupe.androidpushnotifications;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;

import com.android.slidingmenu.MainActivity;
import com.android.slidingmenu.util.Common;
import com.android.slidingmenu.util.GlobalData;
import com.android.slidingmenu.util.Notificacion;
import com.appdupe.flamer.utility.Constant;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.loovers.app.R;

/**
 * Created by Juan Martin Bernal on 13/1/16.
 */
public class GCMBroadCast extends BroadcastReceiver {
    private static final String WALLA = "walla";
    private static final String MATCHES = "match";
    private static final String FAVORITES = "favorite";
    private static final String MESSAGES = "message";
    private static final String NOTIFICATION = "notification";
    @Override
    public void onReceive(Context context, Intent intent) {


        /*ComponentName comp =
                new ComponentName(context.getPackageName(),
                        GcmAppService.class.getName());

        //startWakefulService(context, (intent.setComponent(comp)));
        Intent intent1 = new Intent(context,GcmAppService.class);
        context.startService(intent1);

        setResultCode(Activity.RESULT_OK);*/
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(context);
        String messageType       = gcm.getMessageType(intent);
        Bundle extras            = intent.getExtras();

        if (!extras.isEmpty())
        {
            if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType))
            {


                String message = (extras.containsKey("payload"))?extras.getString("payload"):"";
                String action = (extras.containsKey("action"))?extras.getString("action"):"";
                String strMessageType = (extras.containsKey("mt"))?extras.getString("mt"):"";

                String strMessageID = (extras.containsKey("mid"))?extras.getString("mid"):"";
                String strSenderName = (extras.containsKey("sname"))?extras.getString("sname"):"";
                String strDateTime = (extras.containsKey("dt"))?extras.getString("dt"):"";
                String myId = (extras.containsKey("sfid"))?extras.getString("sfid"):"";
                String strSenderId = (extras.containsKey("senderId"))?extras.getString("senderId"):"";
                String avatar = (extras.containsKey("image"))?extras.getString("image"):"";
                //String num = extras.getString("num");
                String accountTypeId = (extras.containsKey("accountTypeId"))?extras.getString("accountTypeId"):"";
                String type = (extras.containsKey("type"))?extras.getString("type"):"";
                String priority = (extras.containsKey("priority"))?extras.getString("priority"):"";
                String notification = (extras.containsKey("notification"))?extras.getString("notification"):"";

                Log.d("app2you","type: "+ type + " message: "+ message + " notification: "+ notification + "****** "+" accountId "+accountTypeId + " mi id: " +myId + " id otro usurio: "+ strSenderId);
                //Notificacion.create(this,"notificacion" ,""+priority, "", R.drawable.ic_launcher_wallamatch, null, null, MainActivity.class,1);

                if(type.equalsIgnoreCase(WALLA))
                {
                    //from 2
                    try {
                        if(!accountTypeId.equals("")) {
                            if (Integer.parseInt(accountTypeId) > 2) {

                                if (GlobalData.isKey(Constant.NOTIFICATION_WALLAS, context))
                                    if (GlobalData.get(Constant.NOTIFICATION_WALLAS, context).equals("Y"))
                                        Notificacion.create(context, context.getResources().getString(R.string.app_name), context.getResources().getString(R.string.message_notification_like_text), "", R.drawable.logo_parejita_2, null, null, MainActivity.class, 2);

                            } else
                                GlobalData.set(Constant.COUNTER_WALLAS, "0", context);
                        }

                        //Intent i = new Intent("LOCATION_UPDATED");
                        //i.putExtra("name", "" + strSenderName);
                        //i.putExtra("type","1");
                        //sendBroadcast(i);
                    }
                    catch(Exception ex)
                    {
                        ex.printStackTrace();
                    }

                }

                else if(type.equalsIgnoreCase(MATCHES))
                {
                    //from 3
                    Bitmap bitmap = null;
                    if(avatar != null)
                        bitmap = Common.getBitmapFromURL(avatar);


                    Log.d("app2you","match de "+strSenderName + " "+ strSenderId + " avatar: "+avatar );
                    String tmp = (GlobalData.isKey(Constant.COUNTER_MATCHS,context))?GlobalData.get(Constant.COUNTER_MATCHS,context):null;
                    String walla = (GlobalData.isKey(Constant.COUNTER_WALLAS,context))?GlobalData.get(Constant.COUNTER_WALLAS,context):"0";
                    int numWallas = Integer.parseInt(walla);

                    String countWalla =  String.valueOf((numWallas > 0) ? numWallas - 1 : "0");

                    GlobalData.set(Constant.COUNTER_WALLAS, "" + countWalla, context);

                    String countMatch = String.valueOf((tmp != null) ? Integer.parseInt(tmp) + 1 : "1");
                    GlobalData.set(Constant.COUNTER_MATCHS, "" + countMatch, context);

                    if(GlobalData.isKey(Constant.NOTIFICATION_MATCHES, context))
                        if(GlobalData.get(Constant.NOTIFICATION_MATCHES,context).equals("Y"))
                            Notificacion.create(context, context.getResources().getString(R.string.app_name),context.getResources().getString(R.string.message_notification_match_text), "", R.drawable.logo_parejita_2, bitmap, null, MainActivity.class,3);
                }
                else if(type.equalsIgnoreCase(FAVORITES))
                {
                    //from 4 apagado temporalmente
                    /*Log.d("app2you","favorito de "+strSenderName + " "+ strSenderId );
                    String tmp = (GlobalData.isKey(Constant.COUNTER_FAVORITES,this))?GlobalData.get(Constant.COUNTER_FAVORITES,this):null;
                    String countFavorite = String.valueOf((tmp != null) ? Integer.parseInt(tmp) + 1 : "1");
                    GlobalData.set(Constant.COUNTER_FAVORITES,""+countFavorite,this);

                    Notificacion.create(this, strSenderName +" te hizo favorito", "favorite", "", R.drawable.ic_launcher_wallamatch, null, null, MainActivity.class,4);*/
                }
                else if(type.equalsIgnoreCase(NOTIFICATION))
                {
                    Log.d("app2you","notification : " + message );
                    Notificacion.create(context, context.getResources().getString(R.string.app_name), "Loovers", "", R.drawable.logo_parejita_2, null, null, MainActivity.class,99);
                }
                else if(type.equalsIgnoreCase(MESSAGES))
                {
                    //message 1
                    String tmp = (GlobalData.isKey(Constant.COUNTER_MESSAGES,context))?GlobalData.get(Constant.COUNTER_MESSAGES,context):"1";
                    String countMessage = String.valueOf((tmp != null) ? Integer.parseInt(tmp) + 1 : "1");
                    GlobalData.set(Constant.COUNTER_MESSAGES,""+countMessage,context);

                    // Log.d("app2you","type message url picture: "+avatar + " mensaje: "+message + " type: "+type );
                   /* DatabaseHandler objDBHandler = new DatabaseHandler(this);
                    ChatMessageList objMessageData = new ChatMessageList();
                    objMessageData.setStrMessage(message);
                    objMessageData.setStrDateTime(strDateTime);
                    objMessageData.setStrSenderFacebookId(strFacebookId);
                    objMessageData.setStrSendername(strSenderName);
                    objMessageData.setStrFlagForMessageSuccess("1");
                    objMessageData.setStrSenderId(strSenderId);
                    objDBHandler.insertMessageData(objMessageData);*/


                    Intent homeIntent = new Intent("com.embed.anddroidpushntificationdemo11.push");

                    homeIntent.putExtra("MESSAGE_FOR_PUSH", message);
                    homeIntent.putExtra("MESSAGE_FOR_PUSH_ACTION", "");
                    homeIntent.putExtra("MESSAGE_FOR_PUSH_MESSAGETYPE", strMessageType);
                    homeIntent.putExtra("MESSAGE_FOR_PUSH_MESSAGEID", strMessageID);
                    homeIntent.putExtra("MESSAGE_FOR_PUSH_SENDERNAME", strSenderName);
                    homeIntent.putExtra("MESSAGE_FOR_PUSH_DATETIME", strDateTime);
                    homeIntent.putExtra("MESSAGE_FOR_PUSH_FACEBOOKID", strSenderId);
                    homeIntent.putExtra("MESSAGE_FOR_PUSH_SENDERID", strSenderId);
                    context.sendBroadcast(homeIntent);


                    if (GlobalData.get("notification_new", context) == null) {
                        GlobalData.set("notification_new", "Y", context);
                        Intent i = new Intent("LOCATION_UPDATED");
                        i.putExtra("name", "" + strSenderName);
                        i.putExtra("type","0");
                        context.sendBroadcast(i);
                    }

                    if(GlobalData.isKey(Constant.NOTIFICATION_MESSAGES, context))
                        if(GlobalData.get(Constant.NOTIFICATION_MESSAGES,context).equals("Y"))
                            Notificacion.create(context, context.getResources().getString(R.string.app_name),  context.getResources().getString(R.string.message_notification_message_text)+ " "+ strSenderName, "", R.drawable.logo_parejita_2, null, null, MainActivity.class,1);

                }//close type messages

            }//close type message GCM
        }
        //se completo el servicio
        //GCMBroadCast.completeWakefulIntent(intent);
    }



}
