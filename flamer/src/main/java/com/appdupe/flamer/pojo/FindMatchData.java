package com.appdupe.flamer.pojo;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class FindMatchData 
{
	
	@SerializedName("usersProfiles")
	public ArrayList<MatchesData>matches;

    @SerializedName("success")
    public Boolean  success;

    @SerializedName("accountTypeID")
    public String  accountTypeID;

    @SerializedName("currentLikes")
    public String  currentLikes;

    @SerializedName("errorNum")
	public int errNum;
		
	@SerializedName("errFlag")
	public int errFlag;

    @SerializedName("status")
    public String status;

    @Override
    public String toString() {
        return "error num : "+errNum + " errorFlag: "+ errFlag + " size matches: "+ matches.size();
    }
}
