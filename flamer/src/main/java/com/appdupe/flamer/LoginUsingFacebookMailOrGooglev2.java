/**
 * Copyright 2010-present Facebook.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.appdupe.flamer;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import android.util.Patterns;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.slidingmenu.MainActivity;
import com.android.slidingmenu.activities.GenereActivity;
import com.android.slidingmenu.adapter.AdapterListMail;
import com.android.slidingmenu.util.Common;
import com.android.slidingmenu.util.GlobalData;
import com.android.slidingmenu.util.KeyBoard;
import com.android.slidingmenu.util.TypesLetter;
import com.android.slidingmenu.util.Validate;
import com.android.slidingmenu.util.Visibility;
import com.android.slidingmenu.webservices.Services;
import com.appdupe.flamer.utility.ConnectionDetector;
import com.appdupe.flamer.utility.Constant;
import com.gitonway.lee.niftymodaldialogeffects.lib.Effectstype;
import com.gitonway.lee.niftymodaldialogeffects.lib.NiftyDialogBuilder;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.loovers.app.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class LoginUsingFacebookMailOrGooglev2 extends Activity implements AdapterView.OnItemSelectedListener {

	private static final String TAG = "app2you";

	public static final String EXTRA_MESSAGE = "message";
	public static final String PROPERTY_REG_ID = "registration_id";
	private static final String PROPERTY_APP_VERSION = "appVersion";
	private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final String PROPERTY_EXPIRATION_TIME = "onServerExpirationTimeMs";
    private static final String PROPERTY_USER = "user";
    public static final long EXPIRATION_TIME_MS = 1000 * 3600 * 24 * 7;
	public GoogleCloudMessaging gcm;
	//public Context context;
	private String regid;
    //private String SENDER_ID = "1077626696227";
    //private String SENDER_ID = "705758513230";//WALLAMATCH JUAN MARTIN id de proyecto - login
    private String SENDER_ID = "246265097728";
    private EditText regEmail;
    private EditText regPass;
    private EditText regNickname;
    private TextView lh_birthday,txtDate;
    public String mCity, province;
    public ImageView addMail;
    public String mCountry;
    public ListView listMail;
    public List<String> mailWallaMatches = new ArrayList<String>();
    public AdapterListMail adapterListMail;
    public LinearLayout container_toggle_channels;
    //public CallbackManager callbackManager;
    //public LoginButton loginButton;
    public String emailFacebook, idFacebook, nameFacebook, birthdayFacebook;
    public boolean status = true;
    public LinearLayout containerLocation;
    private Spinner spProvincias,spLocalidades;

    @Override
	public void onCreate(Bundle savedInstanceState) {
        //validar version del android
        int sdk = android.os.Build.VERSION.SDK_INT;
        if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN)
            super.setTheme(R.style.AppTheme);

        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.activity);
        initdata();


        if(!GlobalData.isKey(Constant.SESSION_TOKEN,LoginUsingFacebookMailOrGooglev2.this)) {
            if (!GlobalData.isKey(Constant.SERVER_PING_VIEW_REGISTER, LoginUsingFacebookMailOrGooglev2.this)) {
                String ip = "false";

                int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                if (currentapiVersion >= Build.VERSION_CODES.M) {
                    sendPingSever(ip);
                } else {
                    ip = (Common.getLocalIpAddress(LoginUsingFacebookMailOrGooglev2.this).length() > 0) ? Common.getLocalIpAddress(LoginUsingFacebookMailOrGooglev2.this) : "false";
                    sendPingSever(ip);
                }

            }
        }

        //FacebookSdk.sdkInitialize(this.getApplicationContext());


      //  context = this;
        /*callbackManager = CallbackManager.Factory.create();

                LoginManager.getInstance().registerCallback(callbackManager,
                        new FacebookCallback<LoginResult>() {
                            @Override
                            public void onSuccess(LoginResult loginResult) {
                                // App code

                                Log.d("app2you", "token: " + loginResult.getAccessToken().getToken());
                                AccessToken accessToken = loginResult.getAccessToken();

                                final GraphRequest request = new GraphRequest(loginResult.getAccessToken(), "me");
                                Bundle params = new Bundle();
                                params.putString("fields", "id,name,email,gender, birthday");
                                request.setParameters(params);
                                AsyncTask.execute(new Runnable() {
                                    public void run() {
                                        Log.d(TAG, "LoginCallback.onSuccess() lookup email");
                                        GraphResponse response = request.executeAndWait();
                                        JSONObject jsonResp = response.getJSONObject();
                                        Log.d("app2you", "data :" + jsonResp.toString());
                                        try {
                                            emailFacebook = jsonResp.getString("email");
                                            nameFacebook = jsonResp.getString("name");
                                            idFacebook = jsonResp.getString("id"); //url image facebook http://graph.facebook.com/10156394864230024/picture?type=large
                                            String gender = jsonResp.getString("gender");//male or female
                                            String birthday = jsonResp.getString("birthday");


                                            SimpleDateFormat fromUser = new SimpleDateFormat("MM/dd/yyyy");
                                            SimpleDateFormat myFormat = new SimpleDateFormat("yyyy/MM/dd");

                                            try {
                                                //final Calendar myCalendar = Calendar.getInstance();
                                                birthdayFacebook = myFormat.format(fromUser.parse(birthday));
                                                Log.d("app2you", "data :" + jsonResp.toString() + " new date : " + birthdayFacebook);
                                                String[] dateUser = birthdayFacebook.split("/");
                                                final Calendar userAge = new GregorianCalendar(Integer.parseInt(dateUser[0]), Integer.parseInt(dateUser[1]), Integer.parseInt(dateUser[2]));
                                                Calendar minAdultAge = new GregorianCalendar();
                                                minAdultAge.add(Calendar.YEAR, -18);
                                                if (minAdultAge.before(userAge)) {
                                                    Looper.prepare();
                                                    runOnUiThread(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            Common.errorMessage(LoginUsingFacebookMailOrGooglev2.this, "", ""+getResources().getString(R.string.higher_age));
                                                        }
                                                    });
                                                    Looper.loop();

                                                } else {

                                                    String imageUrl = "http://graph.facebook.com/" + idFacebook + "/picture?type=large";
                                                    GlobalData.set(Constant.URL_AVATAR_FACEBOOK, "" + imageUrl, LoginUsingFacebookMailOrGooglev2.this);
                                                    GlobalData.set(Constant.LOGIN_FACEBOOK, "Y", LoginUsingFacebookMailOrGooglev2.this);
                                                    gcm = GoogleCloudMessaging.getInstance(LoginUsingFacebookMailOrGooglev2.this);

                                                    //Obtenemos el Registration ID guardado
                                                    regid = getRegistrationId(context);

                                                    //Si no disponemos de Registration ID comenzamos el registro

                                                    new TareaRegistroGCM(emailFacebook, idFacebook, nameFacebook, birthdayFacebook).execute();
                                                    //Toast.makeText(LoginUsingFacebookMailOrGooglev2.this, "ejecutar registro tienes " + userAge.YEAR, Toast.LENGTH_SHORT).show();

                                                }
                                            } catch (ParseException e) {
                                                e.printStackTrace();
                                            }


                                        } catch (final Exception ex) {
                                            Log.e(TAG, "LoginCallback.onSuccess(): exception caught while getting identity", ex);
                                            runOnUiThread(new Runnable() {
                                                public void run() {
                                                    Toast.makeText(LoginUsingFacebookMailOrGooglev2.this, "Exception caught while getting identity: " +
                                                            ex.getMessage(), Toast.LENGTH_LONG).show();
                                                    LoginUsingFacebookMailOrGooglev2.this.finish();
                                                }
                                            });
                                        }
                                    }
                                });

                            }

                            @Override
                            public void onCancel() {
                                // App code
                                Toast.makeText(LoginUsingFacebookMailOrGooglev2.this, "cancelo facebook", Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onError(FacebookException exception) {
                                // App code
                                Toast.makeText(LoginUsingFacebookMailOrGooglev2.this, R.string.try_again, Toast.LENGTH_SHORT).show();
                            }
                        });*/

        //loginButton = (LoginButton) findViewById(R.id.login_button);
        //loginButton.setBackgroundResource(R.drawable.facebook_btn);
        //loginButton.setReadPermissions(Arrays.asList("public_profile, email, user_birthday"));




    }
    /*@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }*/
	@Override
	protected void onResume() {
		super.onResume();
		//checkPlayServices();
        // Logs 'install' and 'app activate' App Events.
       // AppEventsLogger.activateApp(this);


	}

    @Override
    protected void onPause() {
        super.onPause();
        // Logs 'app deactivate' App Event.
//        AppEventsLogger.deactivateApp(this);
    }

    /**
	 * Check the device to make sure it has the Google Play Services APK. If it
	 * doesn't, display a dialog that allows users to download the APK from the
	 * Google Play Store or enable it in the device's system settings.
	 */
	public boolean checkPlayServices() {
		// logDebug("checkPlayServices  ");
		int resultCode = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(this);

		if (resultCode != ConnectionResult.SUCCESS) {
			if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
				GooglePlayServicesUtil.getErrorDialog(resultCode, this,
						PLAY_SERVICES_RESOLUTION_REQUEST).show();
			} else {


				finish();
			}
			return false;
		}
		return true;
	}

    /**
     * Método que retorna el nombre del paquete de la app
     * @param context
     * @return
     */
    private static int getAppVersion(Context context)
    {
        try
        {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);

            return packageInfo.versionCode;
        }
        catch (NameNotFoundException e)
        {
            throw new RuntimeException("Error al obtener versión: " + e);
        }
    }

    /**
     * Retorna el id de registro del dispositivo para notificaciones push GCM
     * @param context
     * @return
     */
    private String getRegistrationId(Context context)
    {
        SharedPreferences prefs = getSharedPreferences(
                MainActivity.class.getSimpleName(),
                Context.MODE_PRIVATE);

        String registrationId = prefs.getString(PROPERTY_REG_ID, "");

        if (registrationId.length() == 0)
        {
            Log.d(TAG, "Registro GCM no encontrado.");
            return "";
        }

        String registeredUser =
                prefs.getString(PROPERTY_USER, "user");

        int registeredVersion =
                prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);

        long expirationTime =
                prefs.getLong(PROPERTY_EXPIRATION_TIME, -1);

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.getDefault());
        String expirationDate = sdf.format(new Date(expirationTime));

        Log.d(TAG, "Registro GCM encontrado (usuario=" + registeredUser +
                ", version=" + registeredVersion +
                ", expira=" + expirationDate + ")");

        int currentVersion = getAppVersion(context);

        if (registeredVersion != currentVersion)
        {
            Log.d(TAG, "Nueva versión de la aplicación.");
            return "";
        }
        else if (System.currentTimeMillis() > expirationTime)
        {
            Log.d(TAG, "Registro GCM expirado.");
            return "";
        }
        return registrationId;
    }


    /**
     * Inicializo los componentes de la vista
     */
	private void initdata() {


        spProvincias              = (Spinner) findViewById(R.id.sp_provincia);
        spLocalidades             = (Spinner) findViewById(R.id.sp_localidad);

        loadSpinnerProvincias();

        listMail                  = (ListView) findViewById(R.id.listMail);
        container_toggle_channels = (LinearLayout) findViewById(R.id.container_toggle_channels);
        containerLocation         = (LinearLayout)findViewById(R.id.containerLocation);
        regEmail                  = (EditText)findViewById(R.id.reg_email);
        regPass                   = (EditText)findViewById(R.id.reg_passw);
        regNickname               = (EditText)findViewById(R.id.reg_nick);
        addMail                   = (ImageView)findViewById(R.id.addMail);


        Button registerSend = (Button)findViewById(R.id.register_mail);
       // registerSend.setTransformationMethod(null);

        //int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        //currentapiVersion >= Build.VERSION_CODES.M
        if (GlobalData.isKey(Constant.CITY_USER,LoginUsingFacebookMailOrGooglev2.this)){
            Visibility.gone(containerLocation);
        }
        else
        {
            Visibility.gone(containerLocation);
        }
        Typeface openSans = TypesLetter.getSansLight(LoginUsingFacebookMailOrGooglev2.this);
        regEmail.setTypeface(openSans);
        regPass.setTypeface(openSans);
        regNickname.setTypeface(openSans);

        mailWallaMatches = getEmailPhone();

        //adapter
        adapterListMail = new AdapterListMail(LoginUsingFacebookMailOrGooglev2.this,mailWallaMatches);
        listMail.setAdapter(adapterListMail);
       // DisplayMetrics displaymetrics = new DisplayMetrics();
        //getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
       // int height = displaymetrics.heightPixels;

        int pixels = 0;
        final float scale = getResources().getDisplayMetrics().density;
        pixels = (int) (52 * scale + 0.5f);
        /*if (height > 800)
            pixels = (int) (52 * scale + 0.5f);
        else
            pixels = (int) (52 * scale + 0.5f);*/
        container_toggle_channels.getLayoutParams().height = pixels * mailWallaMatches.size();

        //container_toggle_channels.setVisibility((mailWallaMatches != null && mailWallaMatches.size() > 1)?View.VISIBLE:View.GONE);
        addMail.setVisibility((mailWallaMatches != null && mailWallaMatches.size() > 1)?View.VISIBLE:View.GONE);

        if(mailWallaMatches.size() > 0)
            regEmail.setText(""+mailWallaMatches.get(0));

        lh_birthday = (TextView)findViewById(R.id.lh_birthday);
        lh_birthday.setTypeface(openSans);
        txtDate  = (TextView)findViewById(R.id.txtDate);

        showDialogDate();

        registerSend.setTypeface(openSans);


        listMail.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                String mailWallaMatch = (String)listMail.getItemAtPosition(position);
                regEmail.setText(""+mailWallaMatch);
                Visibility.gone(container_toggle_channels);
                addMail.setImageResource(R.drawable.ic_add_white_24dp);

            }
        });
        spProvincias.setOnTouchListener(new View.OnTouchListener(){

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_DOWN){
                    closeKeyBoardGUI();
                    // Load your spinner here
                }
                return false;
            }

        });
        spLocalidades.setOnTouchListener(new View.OnTouchListener(){

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_DOWN){
                    closeKeyBoardGUI();
                    // Load your spinner here
                }
                return false;
            }

        });

        registerSend.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                final String email = regEmail.getText().toString().trim();
                String password    = regPass.getText().toString().trim();
                String nickname    = regNickname.getText().toString().trim();
                String dateTime    = lh_birthday.getText().toString().trim();
               // String spLocality  = spLocalidades.getSelectedItem().toString();
                //String spProvince  = spProvincias.getSelectedItem().toString();

                SimpleDateFormat fromUser = new SimpleDateFormat("dd/MM/yyyy");
                SimpleDateFormat myFormat = new SimpleDateFormat("yyyy/MM/dd");

                try {
                    dateTime = myFormat.format(fromUser.parse(dateTime));
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                if (!Validate.validateFieldsForm(regEmail, regPass, regNickname, dateTime, getApplicationContext())) {

                }
                /*else if(containerLocation.getVisibility() == View.VISIBLE && (spLocality.equals(getResources().getString(R.string.localidad_prompt)) || spProvince.equals(getResources().getString(R.string.provincia_prompt))))
                {
                    Common.errorMessage(LoginUsingFacebookMailOrGooglev2.this,"","Debes seleccionar provincia y localidad");
                }*/
                else{

                    //servicio registro
                    if(ConnectionDetector.isConnectingToInternet(LoginUsingFacebookMailOrGooglev2.this)) {

                        closeKeyBoardGUI();
                        gcm = GoogleCloudMessaging.getInstance(LoginUsingFacebookMailOrGooglev2.this);

                        //Obtenemos el Registration ID guardado
                        regid = getRegistrationId(LoginUsingFacebookMailOrGooglev2.this);

                        //Si no disponemos de Registration ID comenzamos el registro

                        new TareaRegistroGCM(email,password,nickname,dateTime).execute();


                    }else
                        Toast.makeText(LoginUsingFacebookMailOrGooglev2.this, R.string.check_internet, Toast.LENGTH_SHORT).show();


                }
            }
        });

    }
    /**
     * When the back button is pressed, check if user has clicked on the
     * "privacy policy" button. If clicked, then close the privacy policy layout
     * on back press. Otherwise close the activity.
     */
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        backToInit();
    }



    /**
     * Metodo que devuelve el correo gmail registrado en el dispositvo
     * @return
     */
    public List<String> getEmailPhone() {

        mailWallaMatches.clear();
        //String email = "";
        Pattern emailPattern = Patterns.EMAIL_ADDRESS; // API level 8+
        Account[] accounts = AccountManager.get(LoginUsingFacebookMailOrGooglev2.this).getAccounts();
        for (Account account : accounts) {
            if (emailPattern.matcher(account.name.trim()).matches()) {
                String possibleEmail = account.name.toLowerCase().trim();
                mailWallaMatches.add(possibleEmail);

            }
        }
        Set<String> hs = new HashSet<String>();
        hs.addAll(mailWallaMatches);
        mailWallaMatches.clear();
        mailWallaMatches.addAll(hs);

        return mailWallaMatches;
    }
    public void showDialogDate()
    {
        final Calendar myCalendar = Calendar.getInstance();

        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                Calendar userAge = new GregorianCalendar(year,monthOfYear,dayOfMonth);
                Calendar minAdultAge = new GregorianCalendar();
                minAdultAge.add(Calendar.YEAR, - 18);
                if (minAdultAge.before(userAge))
                {
                    Toast.makeText(LoginUsingFacebookMailOrGooglev2.this, R.string.higher_age, Toast.LENGTH_SHORT).show();
                }
                else {
                    myCalendar.set(Calendar.YEAR, year);
                    myCalendar.set(Calendar.MONTH, monthOfYear);
                    myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                    updateLabel(myCalendar);
                }
            }

        };

        lh_birthday.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                closeKeyBoardGUI();
                setDatePicker(date, myCalendar);

            }
        });


        txtDate.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                closeKeyBoardGUI();
                setDatePicker(date, myCalendar);

            }
        });

    }
    public void setDatePicker(DatePickerDialog.OnDateSetListener date,Calendar myCalendar)
    {
        int age = 0;
        if(lh_birthday.getText().toString().length() == 0)
           age = 18;

        new DatePickerDialog(LoginUsingFacebookMailOrGooglev2.this, date, myCalendar
                .get(Calendar.YEAR) - age, myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    /**
     * Actualiza el campo de la fecha
     * @param myCalendar
     */
    private void updateLabel(Calendar myCalendar) {

        String myFormat = "dd/MM/yyyy";//"yyyy/MM/dd" ;
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat);
        lh_birthday.setText(sdf.format(myCalendar.getTime()));
    }

    /**
     * Volver a la actividad inicial
     */
    public void backToInit()
    {
        GlobalData.delete(Constant.LOGIN_APP,LoginUsingFacebookMailOrGooglev2.this);
        Intent intent = new Intent(LoginUsingFacebookMailOrGooglev2.this,GenereActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.fade_in_slow, R.anim.fade_out);
        finish();
    }

    /**
     * Servicio para registrar un usuario en el sistema genere 0 male, 1 female
     */
    public void registerUser(final String email, String password, final String nickname,String age,String gcmId, final ProgressDialog progressDialog)
    {
        OkHttpClient client = new OkHttpClient();
        OkHttpClient clientWith30sTimeout = client.newBuilder()
                .readTimeout(30, TimeUnit.SECONDS).writeTimeout(30,TimeUnit.SECONDS).connectTimeout(30,TimeUnit.SECONDS)
                .build();

        final String model = Build.BRAND + " " +Build.MODEL;
        final String version = Common.getVersionDevice(LoginUsingFacebookMailOrGooglev2.this);
        String genere   =  GlobalData.get(Constant.GENERE, LoginUsingFacebookMailOrGooglev2.this);
        mCity           = (GlobalData.isKey(Constant.CITY_USER,LoginUsingFacebookMailOrGooglev2.this))?GlobalData.get(Constant.CITY_USER, LoginUsingFacebookMailOrGooglev2.this):"";
        mCountry        = (GlobalData.isKey(Constant.COUNTRY_USER,LoginUsingFacebookMailOrGooglev2.this))?GlobalData.get(Constant.COUNTRY_USER, LoginUsingFacebookMailOrGooglev2.this):"ES";
        province        = (GlobalData.isKey(Constant.PROVINCE,LoginUsingFacebookMailOrGooglev2.this))?GlobalData.get(Constant.PROVINCE, LoginUsingFacebookMailOrGooglev2.this):"";
        Log.d("app2you","gcm: " +gcmId);
        regid = gcmId;
        final  String wanted = (genere.equals("1") ? "2" : "1");

        RequestBody formBody = new FormBody.Builder()
                .add("email", "" + email)
                .add("password", "" + password)
                .add("firstName", "" + nickname)
                .add("genere", ""+genere)
                .add("dateOfBirth", "" + age)
                //.add("currentLatitude", "" + GlobalData.get(Constant.LATITUD_USER, LoginUsingFacebookMailOrGooglev2.this))
                //.add("currentLongitude",""+GlobalData.get(Constant.LONGITUD_USER, LoginUsingFacebookMailOrGooglev2.this))
                //.add("city",(mCity.isEmpty())?"":""+mCity)
                //.add("province",""+province)
                .add("country",(mCountry.isEmpty())?"ES":""+mCountry)
                .add("ip","" + Common.getLocalIpAddress(LoginUsingFacebookMailOrGooglev2.this))
                .add("registrationId",""+regid)
                .add("registrationIdOld",GlobalData.isKey(Constant.REGISTRATION_ID_GCM_OLD,LoginUsingFacebookMailOrGooglev2.this)?GlobalData.get(Constant.REGISTRATION_ID_GCM_OLD,LoginUsingFacebookMailOrGooglev2.this):"")
                       .add("so", Constant.SO)
                .add("model",""+model)
                .add("isFacebook","2")
                .add("version",""+version)
                .add("appVersion",""+LoginUsingFacebookMailOrGooglev2.this.getResources().getString(R.string.app_version))
                .build();


        Request request = new Request.Builder()
                .url(Services.REGISTER_SERVICE)
                .post(formBody)
                .build();

        clientWith30sTimeout.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                if(progressDialog != null)
                    progressDialog.dismiss();
                Looper.prepare();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Common.errorMessage(LoginUsingFacebookMailOrGooglev2.this,"",""+getResources().getString(R.string.check_internet));
                    }
                });
                Looper.loop();
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {


                final String result = response.body().string();
                Log.d("app2you", "respuesta: " + result);
                Looper.prepare();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            JSONObject json = new JSONObject(result);
                            String success = json.getString("success");
                            if (success.equals("true")) {
                                //Enviar dato de registro exitoso al servidor registro completo
                                registerComplete();

                                setRegistrationId(LoginUsingFacebookMailOrGooglev2.this,nickname, regid);
                                String sessionToken = (json.has("sessionToken")) ? json.getString("sessionToken") : "";
                                String currentLikes = json.getString("currentLikes");
                                String myUserId = json.getString("myUserId");
                                String emailVerified = json.has("emailVerified")?json.getString("emailVerified"):"N";


                                int creditsLoginDaily       = json.has("creditsLoginDaily")?json.getInt("creditsLoginDaily"):5;
                                GlobalData.set(Constant.CREDITS_LOGIN_DAY,""+creditsLoginDaily,LoginUsingFacebookMailOrGooglev2.this);

                                int creditsShareFacebook    = json.has("creditsShareFacebook")?json.getInt("creditsShareFacebook"):20;
                                GlobalData.set(Constant.CREDITS_SHARE_FACEBOOK,""+creditsShareFacebook,LoginUsingFacebookMailOrGooglev2.this);

                                GlobalData.set(Constant.REGISTRATION_ID_GCM_OLD,""+regid,LoginUsingFacebookMailOrGooglev2.this);
                                GlobalData.set(Constant.EMAIL_VERIFIED,""+emailVerified, LoginUsingFacebookMailOrGooglev2.this);
                                GlobalData.set(Constant.LOGIN_APP, "Y", LoginUsingFacebookMailOrGooglev2.this);
                                GlobalData.set(Constant.NICKNAME, "" + nickname, LoginUsingFacebookMailOrGooglev2.this);
                                GlobalData.set(Constant.SESSION_TOKEN, "" + sessionToken, LoginUsingFacebookMailOrGooglev2.this);
                                GlobalData.set(Constant.WANTED_FOUND, "" + wanted, LoginUsingFacebookMailOrGooglev2.this);
                                GlobalData.set(Constant.NAME_MODEL_DEVICE, "" + model, LoginUsingFacebookMailOrGooglev2.this);
                                GlobalData.set(Constant.VERSION_SO, "" + version, LoginUsingFacebookMailOrGooglev2.this);
                                GlobalData.set(Constant.CURRENT_LIKES, "" + currentLikes, LoginUsingFacebookMailOrGooglev2.this);
                                GlobalData.set(Constant.USER_STATUS, Constant.NOT_GOLD_BEFORE_TRIAL_NO_PICTURE, LoginUsingFacebookMailOrGooglev2.this); //usuario nuevo
                                GlobalData.set(Constant.FIRST_REGISTER, "Y", LoginUsingFacebookMailOrGooglev2.this); //usuario nuevo
                                GlobalData.set(Constant.EMAIL_ADDRESS,""+email,LoginUsingFacebookMailOrGooglev2.this);
                                GlobalData.set(Constant.COUNTER_MESSAGES,"1",LoginUsingFacebookMailOrGooglev2.this);
                                GlobalData.set(Constant.MY_USER_ID,""+myUserId,LoginUsingFacebookMailOrGooglev2.this);
                                GlobalData.set(Constant.BIRTHDAY_VERIFIED,"Y",LoginUsingFacebookMailOrGooglev2.this);


                                SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");
                                Date today = Calendar.getInstance().getTime();
                                String currentDate = myFormat.format(today);
                                GlobalData.set(Constant.LAST_CONNECTION,currentDate,LoginUsingFacebookMailOrGooglev2.this);

                                //se borran las preferencias antiguas
                                GlobalData.delete(Constant.RADIUS_PREFERENCE,LoginUsingFacebookMailOrGooglev2.this);
                                GlobalData.delete(Constant.MIN_AGE_PREFERENCE,LoginUsingFacebookMailOrGooglev2.this);
                                GlobalData.delete(Constant.MAX_AGE_PREFERENCE,LoginUsingFacebookMailOrGooglev2.this);

                               /* if(containerLocation.getVisibility() == View.VISIBLE)
                                {
                                    GlobalData.set(Constant.CITY_USER, "" + city, LoginUsingFacebookMailOrGooglev2.this);
                                    GlobalData.set(Constant.PROVINCE, "" + provin, LoginUsingFacebookMailOrGooglev2.this);
                                    Log.d("app2you","provincia: "+provin + "locality:  "+ city);
                                }*/

                                if(progressDialog != null)
                                    progressDialog.dismiss();

                                Intent intent = new Intent(LoginUsingFacebookMailOrGooglev2.this, MainActivity.class);
                                startActivity(intent);
                                overridePendingTransition(R.anim.fade_in_slow, R.anim.fade_out);
                                finish();
                            } else
                            {

                                if(progressDialog != null)
                                    progressDialog.dismiss();
                                int errorMessage = Integer.parseInt(json.getString("errorNum"));
                                //Toast.makeText(LoginUsingFacebookMailOrGooglev2.this, "Algo paso registro: "+ errorMessage, Toast.LENGTH_SHORT).show();
                                switch (errorMessage) {
                                    case 1:
                                        //error faltan campos key
                                        break;
                                    case 2:
                                        //error valor
                                        Toast.makeText(LoginUsingFacebookMailOrGooglev2.this, R.string.try_again, Toast.LENGTH_SHORT).show();
                                        break;
                                    case 3:
                                        // Toast.makeText(LoginUsingFacebookMailOrGooglev2.this, "El correo ya esta registrado", Toast.LENGTH_SHORT).show();
                                        final NiftyDialogBuilder dialog =  NiftyDialogBuilder.getInstance(LoginUsingFacebookMailOrGooglev2.this);
                                        dialog.withTitle(null);
                                        dialog.withMessage(null);
                                        dialog.isCancelableOnTouchOutside(false);
                                        dialog.withDialogColor(Color.TRANSPARENT);
                                        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                        dialog.withDuration(500);
                                        dialog.withEffect(Effectstype.Slidetop);
                                        dialog.setCustomView(R.layout.modal_general,LoginUsingFacebookMailOrGooglev2.this);

                                        TextView text = (TextView) dialog.findViewById(R.id.text);
                                        text.setText(""+getResources().getString(R.string.email_registreted));


                                        Button exit = (Button) dialog.findViewById(R.id.exit);
                                        Visibility.gone(exit);
                                        Button accept = (Button) dialog.findViewById(R.id.accept);
                                        // if button is clicked, close the custom dialog
                                        exit.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                dialog.dismiss();
                                            }
                                        });
                                        accept.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                dialog.dismiss();


                                            }
                                        });

                                        dialog.show();
                                        break;
                                    case 4:
                                        break;
                                    case 5:
                                        break;
                                    case 6:
                                        break;
                                    case 7:

                                        break;
                                    default:
                                        Toast.makeText(LoginUsingFacebookMailOrGooglev2.this, R.string.check_internet, Toast.LENGTH_SHORT).show();
                                        break;

                                }

                            }
                        } catch (JSONException e) {
                            if(progressDialog != null)
                                progressDialog.dismiss();
                            e.printStackTrace();
                            Toast.makeText(LoginUsingFacebookMailOrGooglev2.this, R.string.error_reponse_server, Toast.LENGTH_SHORT).show();
                        }
                        finally {
                            response.body().close();
                        }

                    }
                });

                Looper.loop();

            }
        });


    }

    /**
     * Método que envia ping para informar al servidor que termino el registro (una sola vez)
     */
    private void registerComplete() {
        if(!GlobalData.isKey(Constant.SESSION_TOKEN,LoginUsingFacebookMailOrGooglev2.this)) {
            if (!GlobalData.isKey(Constant.SERVER_PING_VIEW_REGISTER_COMPLETE, LoginUsingFacebookMailOrGooglev2.this)) {
                String ip = "false";

                int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                if (currentapiVersion >= Build.VERSION_CODES.M) {
                    sendPingSeverRegisterComplete(ip);
                } else {
                    ip = (Common.getLocalIpAddress(LoginUsingFacebookMailOrGooglev2.this).length() > 0) ? Common.getLocalIpAddress(LoginUsingFacebookMailOrGooglev2.this) : "false";
                    sendPingSeverRegisterComplete(ip);
                }

            }
        }
    }

    /**
     *
     * @param view
     */
    public void addMail(View view)
    {
        closeKeyBoardGUI();
        if(status)
        {
            status = false;
            addMail.setImageResource(R.drawable.ic_remove_white_24dp);

            Visibility.visible(container_toggle_channels);
        }
        else
        {
            status = true;
            addMail.setImageResource(R.drawable.ic_add_white_24dp);
            Visibility.gone(container_toggle_channels);
        }




    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        ((TextView) parent.getChildAt(0)).setTextColor(Color.WHITE);
        ((TextView) parent.getChildAt(0)).setTextSize(16);
        closeKeyBoardGUI();

        switch (parent.getId()) {
            case R.id.sp_provincia:

                // Retrieves an array
                TypedArray arrayLocalidades = getResources().obtainTypedArray(
                        R.array.array_provincia_a_localidades);

                int posTmp = 0;
                if(position > 0)
                    posTmp = position - 1;

                CharSequence[] localidades = arrayLocalidades.getTextArray(posTmp);
                arrayLocalidades.recycle();

                ArrayList<CharSequence> tmp = new ArrayList<>();
                for (CharSequence data:localidades)
                    tmp.add(data);

                tmp.add(0,getString(R.string.localidad_prompt));
                // Create an ArrayAdapter using the string array and a default
                // spinner layout
                ArrayAdapter<CharSequence> adapter = new ArrayAdapter<CharSequence>(
                        this, android.R.layout.simple_spinner_item,
                        android.R.id.text1, tmp);

                // Specify the layout to use when the list of choices appears
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                // Apply the adapter to the spinner
                this.spLocalidades.setAdapter(adapter);

                break;

            case R.id.sp_localidad:

                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    /**
     * Clase encargada de obtener el numero de GCM para identificar al dispositivo
     */
    class TareaRegistroGCM extends AsyncTask<String,String,String>
    {
        String email;
        String password;
        String nickname;
        String age;

        ProgressDialog progressDialog;
        boolean status = true;

        public TareaRegistroGCM(String email, String password, String nickname,String age)
        {
            this.email          = email;
            this.password       = password;
            this.nickname       = nickname;
            this.age            = age;

        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog = new ProgressDialog(LoginUsingFacebookMailOrGooglev2.this,R.style.CustomDialogTheme);
            progressDialog.setCancelable(false);
           // progressDialog.setMessage(""+getResources().getString(R.string.register_user));
            progressDialog.show();
            progressDialog.setContentView(R.layout.custom_progress_dialog);
            TextView txtView = (TextView) progressDialog.findViewById(R.id.txtProgressDialog);
            txtView.setText(""+getResources().getString(R.string.register_user));
        }

        @Override
        protected String doInBackground(String... params)
        {

            String msg = "";
            Log.d("app2you", "entro a background : " + email + " " + password + " " + nickname + " " + age);
            try
            {
                if (gcm == null)
                    gcm = GoogleCloudMessaging.getInstance(LoginUsingFacebookMailOrGooglev2.this);


                //Nos registramos en los servidores de GCM
                regid = gcm.register(SENDER_ID);

                Log.d(TAG, "Registrado en GCM: registration_id=" + regid);

            }
            catch (IOException ex)
            {
                status = false;
                ex.printStackTrace();
                Log.d(TAG, "Error registro en GCM:" + ex.getMessage());

            }

            return msg;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(status)
                registerUser(email, password, nickname, age,regid, progressDialog);
            else {
                progressDialog.dismiss();
                new TareaRegistroGCM(email,password,nickname,age).execute();
               // Toast.makeText(LoginUsingFacebookMailOrGooglev2.this, "error GCM ID", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void setRegistrationId(Context context, String user, String regId)
    {
        SharedPreferences prefs = getSharedPreferences(
                MainActivity.class.getSimpleName(),
                Context.MODE_PRIVATE);

        int appVersion = getAppVersion(context);

        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PROPERTY_USER, user);
        editor.putString(PROPERTY_REG_ID, regId);
        editor.putInt(PROPERTY_APP_VERSION, appVersion);
        editor.putLong(PROPERTY_EXPIRATION_TIME,
                System.currentTimeMillis() + EXPIRATION_TIME_MS);

        editor.commit();
    }
    @Override
    public void onDestroy() {
        super.onDestroy();

    }
    //LM+/CwtbttEZDZWstpKvEkOV5YA=

    /**
     * Cerrar teclado en la vista
     */
    public void closeKeyBoardGUI()
    {
        KeyBoard.hide(regEmail);
        KeyBoard.hide(regPass);
        KeyBoard.hide(regNickname);

    }

    /**
     * Envia datos de instalacion al servidor, por primera vez
     */
    public void sendPingSever(String ip)
    {

        OkHttpClient client = new OkHttpClient();

        RequestBody formBody = new FormBody.Builder()
                .add("hashSession",GlobalData.isKey(Constant.HASH_SESSION,LoginUsingFacebookMailOrGooglev2.this)?GlobalData.get(Constant.HASH_SESSION,LoginUsingFacebookMailOrGooglev2.this):"false")

                .add("brand",""+ Build.BRAND)
                .add("model",""+Build.MODEL)
                .add("so","1")
                .add("ip",""+ip)
                .add("deviceVersion", Common.getVersionDevice(LoginUsingFacebookMailOrGooglev2.this))
                .add("appVersion",""+getResources().getString(R.string.app_version))
                .add("typeId","4")
                .build();

        Request request = new Request.Builder()
                .url(Services.REGISTER_INSTALL)
                .post(formBody)
                .build();

        client.newCall(request).enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(Call call, IOException e) {


            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                final String result = response.body().string();
                //Log.d("app2you", "respuesta  ping  " + result);

                Looper.prepare();

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            JSONObject json = new JSONObject(result);

                            String success = json.getString("success");

                            if (success.equals("true")) {
                                GlobalData.set(Constant.SERVER_PING_VIEW_REGISTER,"Y",LoginUsingFacebookMailOrGooglev2.this);
                            } else {

                            }
                        } catch (JSONException ex) {
                            ex.printStackTrace();
                        }
                        finally {
                            response.body().close();
                        }

                    }
                });

                Looper.loop();
            }
        });
    }

    /**
     * Envia datos de instalacion al servidor, por primera vez
     */
    public void sendPingSeverRegisterComplete(String ip)
    {

        OkHttpClient client = new OkHttpClient();

        RequestBody formBody = new FormBody.Builder()
                .add("hashSession",GlobalData.isKey(Constant.HASH_SESSION,LoginUsingFacebookMailOrGooglev2.this)?GlobalData.get(Constant.HASH_SESSION,LoginUsingFacebookMailOrGooglev2.this):"false")
                .add("brand",""+ Build.BRAND)
                .add("model",""+Build.MODEL)
                .add("so","1")
                .add("ip",""+ip)
                .add("deviceVersion", Common.getVersionDevice(LoginUsingFacebookMailOrGooglev2.this))
                .add("appVersion",""+getResources().getString(R.string.app_version))
                .add("typeId","5")
                .build();

        Request request = new Request.Builder()
                .url(Services.REGISTER_INSTALL)
                .post(formBody)
                .build();

        client.newCall(request).enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                final String result = response.body().string();
                //Log.d("app2you", "respuesta  ping  " + result);

                Looper.prepare();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            JSONObject json = new JSONObject(result);

                            String success = json.getString("success");

                            if (success.equals("true")) {
                                GlobalData.set(Constant.SERVER_PING_VIEW_REGISTER_COMPLETE,"Y",LoginUsingFacebookMailOrGooglev2.this);
                            } else {

                            }
                        } catch (JSONException ex) {
                            ex.printStackTrace();
                        }
                        finally {
                            response.body().close();
                        }

                    }
                });

                Looper.loop();
            }
        });
    }

    /**
     * Populate the Spinner.
     */
    private void loadSpinnerProvincias() {

        // Create an ArrayAdapter using the string array and a default spinner
        // layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
                this, R.array.provincias, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spProvincias.setAdapter(adapter);

        // This activity implements the AdapterView.OnItemSelectedListener
        spProvincias.setOnItemSelectedListener(this);
        spLocalidades.setOnItemSelectedListener(this);

    }

}