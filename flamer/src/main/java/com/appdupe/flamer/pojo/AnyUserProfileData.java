package com.appdupe.flamer.pojo;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class AnyUserProfileData implements Serializable
{
	@SerializedName("status")
    private String  status;

    @SerializedName("oldMatches")
    private String  oldMatches;

    @SerializedName("newMatches")
    private String  newMatches;

    @SerializedName("userLiked")
    private String  userLiked;

    @SerializedName("messaNotRead")
    private String  messaNotRead;

    @SerializedName("age")
    private String  age;

    @SerializedName("user_id")
    private String  user_id;

    @SerializedName("errNum")
    private String  errNum;

    @SerializedName("errFlag")
    private String  errFlag;

    @SerializedName("errMsg")
    private String  errMsg;

    @SerializedName("nombre")
    private String  nombre;

    @SerializedName("apellidos")
    private String  apellidos;

    @SerializedName("sexo")
    private String  sexo;

    @SerializedName("pimgs")
    private ArrayList<String>  pimgs;

    @SerializedName("gimgs")
    private ArrayList<String>  gimgs;

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessaNotRead() {
        return messaNotRead;
    }

    public void setMessaNotRead(String messaNotRead) {
        this.messaNotRead = messaNotRead;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getErrNum() {
        return errNum;
    }

    public void setErrNum(String errNum) {
        this.errNum = errNum;
    }

    public String getErrFlag() {
        return errFlag;
    }

    public void setErrFlag(String errFlag) {
        this.errFlag = errFlag;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }


    public String getOldMatches() {
        return oldMatches;
    }

    public void setOldMatches(String oldMatches) {
        this.oldMatches = oldMatches;
    }

    public String getNewMatches() {
        return newMatches;
    }

    public void setNewMatches(String newMatches) {
        this.newMatches = newMatches;
    }

    public String getUserLiked() {
        return userLiked;
    }

    public void setUserLiked(String userLiked) {
        this.userLiked = userLiked;
    }
    public ArrayList<String> getGimgs() {
        return gimgs;
    }

    public void setGimgs(ArrayList<String> gimgs) {
        this.gimgs = gimgs;
    }

    public ArrayList<String> getPimgs() {

        return pimgs;
    }

    public void setPimgs(ArrayList<String> pimgs) {
        this.pimgs = pimgs;
    }
}
