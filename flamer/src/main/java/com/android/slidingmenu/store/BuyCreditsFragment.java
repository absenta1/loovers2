package com.android.slidingmenu.store;


import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.slidingmenu.MainActivity;
import com.android.slidingmenu.util.Common;
import com.android.slidingmenu.util.GlobalData;
import com.android.slidingmenu.util.TypesLetter;
import com.android.slidingmenu.util.Visibility;
import com.appdupe.flamer.utility.Constant;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;

import com.facebook.share.Sharer;

import com.facebook.share.model.ShareLinkContent;

import com.facebook.share.widget.ShareDialog;
import com.loovers.app.R;

/**
 * Created by Juan Martin Bernal on 11/3/16.
 */
public class BuyCreditsFragment extends Fragment
{

    public MainActivity mainActivity;
    public ListView listCredits;
    public BuyCreditsAdapter adapter;
    public LinearLayout containerSeeGold,containerTextPassToGold,containerPassToGold;
    public TextView txtHazteGold, txtDescriptionGold,txtDescriptionWallas;


    public BuyCreditsFragment() {
        // Required empty public constructor
    }

    public BuyCreditsFragment(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.buy_credits, container,false);

        if (mainActivity == null)
            mainActivity = (MainActivity) getActivity();

        listCredits                         = (ListView)view.findViewById(R.id.listCredits);
        containerSeeGold                    = (LinearLayout)view.findViewById(R.id.containerSeeGold);
        LinearLayout containerPassGoldStore = (LinearLayout)view.findViewById(R.id.containerPassGoldStore);
        mainActivity.btnShareFacebook       = (Button) view.findViewById(R.id.btnShareFacebook);
        containerTextPassToGold             =  (LinearLayout)view.findViewById(R.id.containerTextPassToGold);
        containerPassToGold                 =  (LinearLayout)view.findViewById(R.id.containerPassToGold);
        txtHazteGold                        = (TextView)view.findViewById(R.id.txtHazteGold);
        txtDescriptionGold                  = (TextView)view.findViewById(R.id.txtGoldDescription);
        txtDescriptionWallas                = (TextView)view.findViewById(R.id.textView21);
        txtDescriptionGold.setTypeface(TypesLetter.getSansRegular(mainActivity));
        mainActivity.btnShareFacebook.setTypeface(TypesLetter.getSansRegular(mainActivity));
        txtDescriptionWallas.setTypeface(TypesLetter.getSansRegular(mainActivity));
        txtHazteGold.setTypeface(TypesLetter.getSansBold(mainActivity));
        listCredits.setClickable(false);
        String numberCreditsFacebook = GlobalData.isKey(Constant.CREDITS_SHARE_FACEBOOK,mainActivity)?GlobalData.get(Constant.CREDITS_SHARE_FACEBOOK,mainActivity):"20";
        mainActivity.btnShareFacebook.setText("COMPARTE CON FACEBOOK Y GANA "+numberCreditsFacebook +" LIKES");

        String genere = GlobalData.get(Constant.GENERE, mainActivity);
        if(GlobalData.get(Constant.USER_STATUS,mainActivity).equals(Constant.GOLD) || genere.equals(Constant.WOMAN))
        {
            if(GlobalData.isKey(Constant.SHARE_FACEBOOK,mainActivity)) {
                Visibility.gone(containerPassToGold);
                Visibility.gone(containerTextPassToGold);
                Visibility.gone(mainActivity.btnShareFacebook);
            }
            else {
                Visibility.visible(containerTextPassToGold);
                txtDescriptionWallas.setText(""+mainActivity.getString(R.string.give_free_likes));
            }

        }
        else
        {
            if(GlobalData.isKey(Constant.SHARE_FACEBOOK,mainActivity)) {
                Visibility.visible(containerPassToGold);
                Visibility.gone(mainActivity.btnShareFacebook);
            }

            Visibility.visible(containerTextPassToGold);
            txtDescriptionWallas.setText(""+mainActivity.getString(R.string.give_free_likes));
        }

        containerSeeGold.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //pasar a la siguiente pestaña
                mainActivity.mTabHost.setCurrentTab(1);

            }
        });
        containerPassGoldStore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //pasar a la siguiente pestaña
                mainActivity.mTabHost.setCurrentTab(1);

            }
        });

        if(GlobalData.isKey(Constant.SHARE_FACEBOOK,mainActivity))
        {
            Visibility.gone(mainActivity.btnShareFacebook);
            txtDescriptionWallas.setText(""+mainActivity.getString(R.string.pass_to_gold));
            Visibility.visible(containerTextPassToGold);
            Visibility.visible(containerPassToGold);
        }


        mainActivity.btnShareFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainActivity.callbackManager = CallbackManager.Factory.create();
                ShareDialog shareDialog = new ShareDialog(mainActivity);

                // this part is optional
              shareDialog.registerCallback(mainActivity.callbackManager, new FacebookCallback<Sharer.Result>() {
                    @Override
                    public void onSuccess(Sharer.Result result) {

                            mainActivity.setCredits("", ""+Constant.FACEBOOK_SHARE_CREDITS_TIME);
                            String genere = GlobalData.get(Constant.GENERE, mainActivity);
                            Visibility.gone(mainActivity.btnShareFacebook);
                            if (GlobalData.get(Constant.USER_STATUS, mainActivity).equals(Constant.GOLD) || genere.equals(Constant.WOMAN)) {
                                Visibility.gone(containerPassToGold);
                                Visibility.gone(containerTextPassToGold);
                            } else {
                                Visibility.visible(containerTextPassToGold);
                                txtDescriptionWallas.setText(""+mainActivity.getString(R.string.pass_to_gold));
                                Visibility.visible(containerPassToGold);
                            }

                    }

                 /* @Override
                  public void onSuccess(AppInviteDialog.Result result) {
                      Toast.makeText(mainActivity, "publicacion compartida - invitacion amigos", Toast.LENGTH_SHORT).show();
                  }*/

                  @Override
                    public void onCancel() {
                        Toast.makeText(mainActivity, R.string.canceled_post, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onError(FacebookException error) {
                        Toast.makeText(mainActivity, R.string.error_share_post_facebook, Toast.LENGTH_SHORT).show();
                    }
                });
               if (ShareDialog.canShow(ShareLinkContent.class)) {
                    ShareLinkContent linkContent = new ShareLinkContent.Builder()
                            .setContentTitle(""+mainActivity.getString(R.string.app_name))
                            .setContentDescription(""+mainActivity.getString(R.string.description_app))
                            .setContentUrl(Uri.parse("https://play.google.com/store/apps/details?id=com.loovers.app"))
                            .setImageUrl(Uri.parse("http://ass2.loovers.com/support/piccompartir.png"))

                            .build();
                    shareDialog.show(linkContent);
                }

                /*if (AppInviteDialog.canShow()) {
                    AppInviteContent content = new AppInviteContent.Builder()
                            .setApplinkUrl("https://fb.me/1104821436243821")
                            .setPreviewImageUrl("http://ass2.wallamatch.com//p//es//6812_415881656_thumb.jpg")

                            .build();
                    AppInviteDialog.show(mainActivity,content);
                }*/

            }
        });

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        adapter = new BuyCreditsAdapter(mainActivity, Common.loadProductsWallamatch(mainActivity));
        listCredits.setAdapter(adapter);

    }

}
