package com.appdupe.flamer.pojo;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class MyProfileData implements Serializable
{
    @SerializedName("success")
    public Boolean  success;

    @SerializedName("accountTypeID")
    public String  accountTypeID;

    @SerializedName("status")
    public String  status;

    @SerializedName("descriptionOptions")
    public String  descriptionOptions;

    @SerializedName("errorNum")
    public int  errNum;

    @SerializedName("errFlag")
    public String  errFlag;

    @SerializedName("errorMsg")
    public String  errMsg;

    @SerializedName("age")
    public String  age;

    @SerializedName("pimgs")
    public ArrayList<PictureData>  pimgs;

    @SerializedName("gimgs")
    public ArrayList<PictureData>  gimgs;

    @SerializedName("currentLikes")
    public String  currentLikes;








}
