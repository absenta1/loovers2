package com.android.slidingmenu.util;

import android.content.Context;
import android.widget.EditText;
import android.widget.Toast;

import com.loovers.app.R;

/**
 * Created by Juan Martin Bernal on 10/11/15.
 */
public class Validate {

    private static final String EMAIL_PATTERN =
            "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@"
                    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    /**
     * Metodo para validar los campos del formulario
     * @return
     */
    public static boolean validateFieldsForm(EditText email, EditText password, EditText nickname, String datetime, Context cxt)
    {
        boolean message = true;
        String mail = email.getText().toString().trim();
        String pass = password.getText().toString().trim();
        String nickName = nickname.getText().toString().trim();
        if(!validEmail2(mail) || mail.isEmpty()) {
            email.setError(cxt.getResources().getString(R.string.error_mail));
            email.requestFocus();
            message = false;
        }
        if(pass.isEmpty() || pass.length() < 6 || !isValidPassword(pass))
        {
            password.setError(cxt.getResources().getString(R.string.error_password));
            password.requestFocus();
            message = false;
        }
        if(nickName.isEmpty() || nickName.length() < 2)
        {
            nickname.setError(cxt.getResources().getString(R.string.error_nickname));
            nickname.requestFocus();
            message = false;
        }
        if(datetime.isEmpty()) {
            Toast.makeText(cxt, R.string.error_date, Toast.LENGTH_SHORT).show();
            message = false;
        }

        return message;


    }
    public static boolean isValidPassword(String s) {
        String n = ".*[0-9].*";
        String a = ".*[A-Za-z].*";
        return s.matches(n) && s.matches(a);
    }
    public  static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    // returns true if the string does not have a number at the beginning
    public static boolean validEmail2(String mail){
        return mail.matches(EMAIL_PATTERN);
    }
}
