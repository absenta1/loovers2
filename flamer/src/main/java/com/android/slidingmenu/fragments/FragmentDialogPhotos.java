package com.android.slidingmenu.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.android.slidingmenu.MainActivity;
import com.android.slidingmenu.activities.ViewContent;
import com.android.slidingmenu.util.Visibility;
import com.appdupe.flamer.pojo.PictureData;
import com.squareup.picasso.Picasso;
import com.viewpagerindicator.CirclePageIndicator;
import com.loovers.app.R;

import java.util.ArrayList;

/**
 * Created by Juan Martin Bernal on 26/1/16.
 */
public class FragmentDialogPhotos  extends DialogFragment implements View.OnClickListener{

    public View view;
    public ViewPager pager;
    public MainActivity mainActivity;
    public ArrayList<PictureData> gimgs;
    public LinearLayout containerOptionsBackChat;
    public ImageView imgBack;


    public FragmentDialogPhotos(){}

    public FragmentDialogPhotos(MainActivity mainActivity, ArrayList<PictureData> gimgs)
    {
        this.mainActivity     = mainActivity;
        this.gimgs            = gimgs;
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.photos_gallery,container,false);
        if(mainActivity == null)
            mainActivity = (MainActivity)getActivity();

        pager                       = (ViewPager) view.findViewById(R.id.viewpager);
        containerOptionsBackChat    = (LinearLayout) view.findViewById(R.id.containerOptionsBackChat);
        imgBack                     = (ImageView) view.findViewById(R.id.imgBack);
        CustomPagerAdapter mCustomPagerAdapter = new CustomPagerAdapter(mainActivity,  gimgs);
        pager.setAdapter(mCustomPagerAdapter);
        CirclePageIndicator titleIndicator = (CirclePageIndicator) view.findViewById(R.id.titles);
        titleIndicator.setViewPager(pager);


        containerOptionsBackChat.setOnClickListener(this);
        imgBack.setOnClickListener(this);

        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_FRAME, android.R.style.Theme_Holo_Light_DarkActionBar);
    }

    @Override
    public void onClick(View view) {

        int id = view.getId();
        switch (id)
        {
            case R.id.containerOptionsBackChat:
                dismiss();
                break;
            case R.id.imgBack:
                dismiss();
                break;
            default:
                break;
        }


    }


    class CustomPagerAdapter extends PagerAdapter {

        Context mContext;
        LayoutInflater mLayoutInflater;
        ArrayList<PictureData> mLista = new ArrayList<PictureData>();

        public CustomPagerAdapter(Context context, ArrayList<PictureData> lista1) {

            this.mLista = lista1;

            /*for (PictureData pd :lista2)
                mLista.add(pd.imageUrl);*/

            mContext = context;
            mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return mLista.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((FrameLayout) object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View itemView = mLayoutInflater.inflate(R.layout.pic_viewpager_page, container, false);

            ImageView imageView = (ImageView) itemView.findViewById(R.id.user_gallery_detail_viewflow_item_imagen);
            final ProgressBar progressBarLoadGallery = (ProgressBar)itemView.findViewById(R.id.progressBarLoadGallery);
            final PictureData pictureData = mLista.get(position);
            Picasso.with(mContext).load(pictureData.imageUrl).error(R.drawable.no_foto).into(imageView, new com.squareup.picasso.Callback() {
                @Override
                public void onSuccess() {
                    Visibility.gone(progressBarLoadGallery);
                }

                @Override
                public void onError() {
                    Visibility.gone(progressBarLoadGallery);
                }
            });

            container.addView(itemView);

            //pasarlo a dialogFragment
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    launchViewDetail(""+pictureData.imageUrl,""+pictureData.name);
                }
            });



            return itemView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((FrameLayout) object);
        }

        /**
         * Metodo que permite visualizar la imagen en el tamaño original
         * @param urlImage
         * @param name
         */
        public void launchViewDetail(String urlImage, String name)
        {
            /*Intent intento = new Intent(mainActivity, ViewContent.class);
            intento.putExtra("urlImage",urlImage);
            intento.putExtra("name",name);
            mainActivity.startActivity(intento);*/
            ViewContent viewContent = new ViewContent(mainActivity,urlImage,name);
            viewContent.show(mainActivity.mFragmentManager,"ViewContent.TAG");
        }
    }
}
