package com.android.slidingmenu.util;

import android.content.Context;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;

import com.romainpiel.shimmer.Shimmer;
import com.romainpiel.shimmer.ShimmerTextView;
import com.loovers.app.R;

/**
 * Clase encargada de gestionar las animaciones de los componentes de la app
 *
 *
 *
 * Created by Juan Martin Bernal on 17/12/15.
 */
public class AppAnimation {


    public static void scale(Context cxt,View v)
    {
        Animation animation = AnimationUtils.loadAnimation(cxt,R.anim.scale_button);
        v.startAnimation(animation);
    }

    public static void fadeIn(View v, int duration)
    {
        Animation fadeIn = new AlphaAnimation(0, 1);
        fadeIn.setInterpolator(new DecelerateInterpolator()); //add this
        fadeIn.setDuration(duration);
        v.startAnimation(fadeIn);

    }
    public static void fadeOut(final View v, int duration)
    {
        Animation fadeOut = new AlphaAnimation(1, 0);
        fadeOut.setInterpolator(new AccelerateInterpolator()); //and this
      //  fadeOut.setStartOffset(1000);
        fadeOut.setDuration(duration);
        fadeOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                   Visibility.gone(v);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        v.startAnimation(fadeOut);
    }

    public static void apperView(Context cxt, View view)
    {
        Animation anim = AnimationUtils.loadAnimation(cxt, R.anim.apper_lock_chat);
        view.startAnimation(anim);
    }

    public static void effectShimmer(int duration, ShimmerTextView shimmerTextView)
    {
        Shimmer shimmer = new Shimmer();
        shimmer.setDuration(duration);
        shimmer.setRepeatCount(0);
        shimmer.start(shimmerTextView);
    }



}
