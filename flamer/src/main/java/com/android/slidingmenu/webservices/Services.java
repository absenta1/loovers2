package com.android.slidingmenu.webservices;

/**
 * Created by Juan Martin Bernal on 12/11/15.
 */
public class Services {

    //host de produccion
    //public static final String HOST = "http://sw00.wallamatch.com/api/";

    //host loovers 2
    //public static final String HOST = "http://sw02.wallamatch.com/api/";
    public static final String HOST = "http://api.loovers.es/api/";
    //host de desarrollo
    //public static final String HOST = "http://servwmdevel.wallamatch.com/api/";

    public static final String LOGIN_SERVICE = HOST + "login";

    public static final String REGISTER_SERVICE = HOST + "register";

    public static final String FORGOT_PASSWORD_SERVICE = HOST + "recoverPassword";

    public static final String UPDATE_PREFERENCES_SERVICE = HOST + "updateUserPreference";

    public static final String FIND_MATCHES_SERVICE = HOST + "findNewPeople";

    public static final String LIKES_SERVICE = HOST + "setUserLike";

    public static final String UPLOAD_PICTURE_SERVICE = HOST + "uploadPicture";

    public static final String GET_PROFILE = HOST + "getProfile";

    public static final String DELETE_USER_PICTURE = HOST + "deleteUserPicture";

    public static final String SET_USER_FAVORITE = HOST + "setUserFavorite";

    public static final String GET_MY_USERS = HOST + "getMyUsers";

  //  public static final String UPDATE_STATUS_USER = HOST + "updateAccountType";

    public static final String DELETE_RELATIONSHIP = HOST + "deleteRelationship";

    public static final String SEND_MESSAGE_CHAT =  HOST + "insertNewMessage";

    public static final String GET_ALL_CHATS = HOST + "getAllChats";

    public static final String GET_CHAT = HOST + "getChat";

    public static final String LOG_OUT = HOST + "logout";

    public static final String UPDATE_PROFILE = HOST + "updateProfile";

    public static final String SEND_FEEDBACK = HOST + "sendFeedback";

    public static final String DELETE_ACCOUNT = HOST + "deleteAccount";

    public static final String GET_NUM_WALLAS = HOST + "getNumWallas";

    public static final String CONFIRM_EMAIL = HOST + "confirmAccount";

    public static final String BUY_PRODUCT = HOST + "buyProduct";

    public static final String GET_WAIT_TIME = HOST + "getWaitTime";

    public static final String MODIFY_PASSWORD = HOST + "changePassword";

    public static final String CHECK_KEY = HOST + "checkKey";

    public static final String GET_CURRENT_LIKES = HOST + "getCurrentLikes";

    public static final String REPORT_USER = HOST + "reportUser";

    public static final String NEWEST_APP_VERSION = HOST + "newestAppVersion";

    public static final String SET_PRIVATE_PHOTO = HOST + "setPrivatePhoto";

    public static final String GET_PRIVATE_PICTURES = HOST + "getPrivatePictures";

    public static final String REQUEST_PRIVATE_PICTURES = HOST + "requestPrivatePictures";

    public static final String UPDATE_PURCHASE_DATA = HOST + "updatePurchaseData";

    public static final String RESPONSE_REQUEST = HOST + "responseRequest";

    public static final String REGISTER_INSTALL = HOST + "registerInstall";

    public static final String REQUEST_CHAT = HOST + "requestChat";

    public static final String SET_CREDITS = HOST + "setCredits";

}

