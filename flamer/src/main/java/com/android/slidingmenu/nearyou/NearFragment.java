package com.android.slidingmenu.nearyou;


import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TabHost;
import android.widget.TextView;

import com.android.slidingmenu.MainActivity;
import com.android.slidingmenu.util.AppAnimation;
import com.android.slidingmenu.util.Common;
import com.android.slidingmenu.util.GlobalData;
import com.android.slidingmenu.util.TypesLetter;
import com.android.slidingmenu.util.Visibility;
import com.appdupe.flamer.utility.Constant;
import com.loovers.app.R;
import com.wang.avi.AVLoadingIndicatorView;

/**
 * @author Juan Martin Bernal
 * @since 15-4-2016
 * A simple {@link Fragment} subclass.
 */
public class NearFragment extends Fragment {


    private FragmentTabHost mTabHost;
    public MainActivity mainActivity;
    public TextView titleTab, badgeTab;
    public RelativeLayout containerAnimatioMapNearToYou;
    public ImageView imgMap, imgMarker1,imgMarker2,imgMarker3;
    public TextView txtTitleLocation;
    public AVLoadingIndicatorView avloadingIndicatorView;


    public NearFragment() {
        // Required empty public constructor
    }

    public NearFragment(MainActivity mainActivity)
    {
        this.mainActivity = mainActivity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_near,container,false);

        imgMap              = (ImageView)view.findViewById(R.id.imageView28);
        imgMarker1          = (ImageView)view.findViewById(R.id.imageView27);
        imgMarker2          = (ImageView)view.findViewById(R.id.imageView29);
        imgMarker3          = (ImageView)view.findViewById(R.id.imageView30);
        txtTitleLocation    = (TextView)view.findViewById(R.id.textView29);
        avloadingIndicatorView = (AVLoadingIndicatorView)view.findViewById(R.id.avloadingIndicatorView);

        mTabHost            = (FragmentTabHost)view.findViewById(android.R.id.tabhost); //new FragmentTabHost(getActivity());
        mTabHost.setup(getActivity(), getChildFragmentManager(), android.R.id.tabcontent);

        mTabHost.addTab(mTabHost.newTabSpec("Personas").setIndicator(getTabIndicator(mainActivity,"Cercanos",0)),ListNearbyPersonFragment.class, null);
        mTabHost.addTab(mTabHost.newTabSpec("Nuevos").setIndicator(getTabIndicator(mainActivity,"Nuevos",1)),ListNewsPersonsFragment.class, null);
       // mTabHost.addTab(mTabHost.newTabSpec("Mapa").setIndicator(getTabIndicator(mainActivity,"Mapa")),MapFragment.class, null);

        return view;


    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if(mainActivity == null)
            mainActivity = (MainActivity)getActivity();

        mainActivity.tvTitle.setText("Cerca de ti");


        mTabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String tabId) {
                if(tabId.equals("Personas"))
                {

                    setStyleTextTabActive(mTabHost.getTabWidget().getChildAt(0));
                    setStyleTabHide(mTabHost.getTabWidget().getChildAt(1));

                }
                else
                {
                    setStyleTextTabActive(mTabHost.getTabWidget().getChildAt(1));
                    setStyleTabHide(mTabHost.getTabWidget().getChildAt(0));
                }
            }
        });

      /*  new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                executeAnimationMap();
            }
        },300);*/

    }

    /**
     * Tab activa
     * @param view
     */
    public void setStyleTextTabActive(View view)
    {
        titleTab = (TextView) view.findViewById(R.id.title);
        titleTab.setTextColor(Color.WHITE);
        titleTab.setTypeface(TypesLetter.getSansRegular(mainActivity), Typeface.BOLD);
    }

    /**
     * tab desactivada
     * @param view
     */
    public void setStyleTabHide(View view)
    {
        titleTab = (TextView) view.findViewById(R.id.title);
        titleTab.setTextColor(Color.parseColor("#c3c3c3"));
        titleTab.setTypeface(TypesLetter.getSansRegular(mainActivity));
    }

    @Override
    public void onResume() {
        super.onResume();
        String status = GlobalData.get(Constant.USER_STATUS,mainActivity);
        if(status.equals(Constant.GOLD))
            Visibility.enableDisableView(mTabHost,true);
    }

    /**
     * Método para agregar tabs personalizados a la vista
     * @param context
     * @param title
     * @return
     */
    private View getTabIndicator(Context context, String title, int tabId) {
        View view = LayoutInflater.from(context).inflate(R.layout.tab_layout, null);

        titleTab = (TextView) view.findViewById(R.id.title);
        badgeTab  = (TextView) view.findViewById(R.id.badge);
        if(tabId == 0) {
            titleTab.setTextColor(Color.WHITE);
            titleTab.setTypeface(TypesLetter.getSansRegular(mainActivity), Typeface.BOLD);
        }
        else
        {
            titleTab.setTextColor(Color.parseColor("#c3c3c3"));
            titleTab.setTypeface(TypesLetter.getSansRegular(mainActivity));
        }
        titleTab.setTypeface(TypesLetter.getSansRegular(mainActivity));
        badgeTab.setTypeface(TypesLetter.getSansRegular(mainActivity));
        titleTab.setTextSize(TypedValue.COMPLEX_UNIT_SP,13);
        titleTab.setText(title);
        return view;
    }

    /**
     *  Iniciar animacion acerca de mi (mapa)
     */
    public void executeAnimationMap()
    {

        Animation animMap = AnimationUtils.loadAnimation(mainActivity,R.anim.down_from_top);
        animMap.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                executeAnimationMarkers();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        Visibility.visible(imgMap);

        imgMap.startAnimation(animMap);
       // txtTitleLocation.startAnimation(animMap);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Visibility.visible(txtTitleLocation);
                Visibility.visible(avloadingIndicatorView);
            }
        },350);
    }

    /**
     *  Iniciar animacion acerca de mi marcas
     */
    public void executeAnimationMarkers()
    {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //Visibility.gone(avloadingIndicatorView);
              //  AppAnimation.fadeOut(avloadingIndicatorView,400);
                Visibility.visible(imgMarker1);
                AppAnimation.scale(mainActivity,imgMarker1);
            }
        },1000);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Visibility.visible(imgMarker2);
                AppAnimation.scale(mainActivity,imgMarker2);
            }
        },3000);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Visibility.visible(imgMarker3);
                AppAnimation.scale(mainActivity,imgMarker3);
            }
        },4000);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Common.errorMessage(mainActivity,"","sube tu foto....bla bla");
            }
        },5000);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mTabHost = null;
    }

}
