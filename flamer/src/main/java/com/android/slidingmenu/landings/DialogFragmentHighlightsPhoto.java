package com.android.slidingmenu.landings;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.slidingmenu.MainActivity;
import com.android.slidingmenu.store.DialogStore;
import com.android.slidingmenu.util.GlobalData;
import com.android.slidingmenu.util.TypesLetter;
import com.appdupe.flamer.utility.CircleTransform;
import com.appdupe.flamer.utility.Constant;
import com.loovers.app.R;
import com.squareup.picasso.Picasso;

/**
 * Created by Juan Martin Bernal on 17/5/16.
 */
public class DialogFragmentHighlightsPhoto extends DialogFragment implements View.OnClickListener {

    private TextView senderName,txtTitle, txtSubText,txtOtherMoment;
    private ImageView imgBack,imageUserLanding;
    public LinearLayout containerOptionsBackChat;
    public Button btnHighlightPhoto;
    public MainActivity mainActivity;


    public DialogFragmentHighlightsPhoto(MainActivity mainActivity)
    {
        this.mainActivity = mainActivity;
    }

    public DialogFragmentHighlightsPhoto(){}

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.landing_hightlights_photo, null);

        if(mainActivity == null)
            mainActivity = (MainActivity)getActivity();

        initComponents(view);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_FRAME, android.R.style.Theme_Holo_Light_DarkActionBar);
    }

    public void initComponents(View view)
    {
        senderName          = (TextView)view.findViewById(R.id.senderName);
        txtOtherMoment      = (TextView)view.findViewById(R.id.txtOtherMoment);
        senderName.setText(""+mainActivity.getString(R.string.highlight_your_photo));
        senderName.setTypeface(TypesLetter.getAvenir(mainActivity));

        btnHighlightPhoto               = (Button)view.findViewById(R.id.btnHighlightPhoto);
        containerOptionsBackChat        = (LinearLayout)view.findViewById(R.id.containerOptionsBackChat);
        imgBack                         = (ImageView)view.findViewById(R.id.imgBack);
        imageUserLanding                = (ImageView)view.findViewById(R.id.imageUserLanding);
        txtTitle                        = (TextView)view.findViewById(R.id.txtTitle);
        txtSubText                      = (TextView)view.findViewById(R.id.txtSubText);

        String url = GlobalData.isKey(Constant.AVATAR_USER,mainActivity)?GlobalData.get(Constant.AVATAR_USER,mainActivity):"";

        if(url.length() > 0)
            Picasso.with(mainActivity).load(url).error(R.drawable.no_foto).transform(new CircleTransform()).into(imageUserLanding);
        else
            Picasso.with(mainActivity).load(R.drawable.no_foto).transform(new CircleTransform()).into(imageUserLanding);

        txtTitle.setTypeface(TypesLetter.getSansBold(mainActivity));
        txtSubText.setTypeface(TypesLetter.getSansRegular(mainActivity));
        txtOtherMoment.setTypeface(TypesLetter.getSansRegular(mainActivity));
        btnHighlightPhoto.setTypeface(TypesLetter.getSansRegular(mainActivity));

        imgBack.setOnClickListener(this);
        containerOptionsBackChat.setOnClickListener(this);
        txtOtherMoment.setOnClickListener(this);
        btnHighlightPhoto.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.btnHighlightPhoto:
                DialogStore dialogStore = new DialogStore(mainActivity,1);
                dialogStore.show(mainActivity.mFragmentManager,"DialogStore.TAG");
                break;
            case R.id.containerOptionsBackChat:
                dismiss();
                break;
            case R.id.imgBack:
                dismiss();
                break;
            case R.id.txtOtherMoment:
                dismiss();
                break;
            default:
                break;
        }


    }

}