package com.appdupe.flamer.pojo;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class FaceBookAlbumData 
{
	@SerializedName("data")
    private ArrayList<FaceBookAlubumScr> alubumScrsList;

	public ArrayList<FaceBookAlubumScr> getAlubumScrsList() {
		return alubumScrsList;
	}

	public void setAlubumScrsList(ArrayList<FaceBookAlubumScr> alubumScrsList) {
		this.alubumScrsList = alubumScrsList;
	}
	
}
