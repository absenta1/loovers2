package com.appdupe;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.slidingmenu.MainActivity;
import com.android.slidingmenu.objects.Match;
import com.android.slidingmenu.util.GlobalData;
import com.android.slidingmenu.util.TypesLetter;
import com.appdupe.androidpushnotifications.ChatActivity;
import com.appdupe.flamer.pojo.MatchesData;
import com.appdupe.flamer.utility.CircleTransform;
import com.appdupe.flamer.utility.Constant;
import com.squareup.picasso.Picasso;
import com.loovers.app.R;

public class MatchFromFlechazosDialogFragment extends DialogFragment {

    public static final String TAG = "MatchFromFlechazosDialogFragment";
    public ImageView imgMatch, imgUser, backgroundImageUser;
    public TextView txtMatch, txtSubtitle;
    public Button btnChatear, btnPLay;
    private Match match;
    public MainActivity mainActivity;


    public MatchFromFlechazosDialogFragment() {
        // Empty constructor required for DialogFragment
    }

    public MatchFromFlechazosDialogFragment(MainActivity mainActivity, Match match) {
        // Empty constructor required for DialogFragment
        this.mainActivity = mainActivity;
        this.match = match;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        setStyle(DialogFragment.STYLE_NO_FRAME, android.R.style.Theme_Holo_Light_DarkActionBar);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.match_found_screen, container);

        if(mainActivity == null)
            mainActivity = (MainActivity)getActivity();

        Typeface openSansBold = TypesLetter.getSansBold(mainActivity);

        imgMatch = (ImageView) view.findViewById(R.id.imgMatch);
        imgUser = (ImageView) view.findViewById(R.id.imgUser);
        backgroundImageUser = (ImageView) view.findViewById(R.id.backgroundImageUser);
        btnChatear = (Button) view.findViewById(R.id.btnChatMatch);
        btnPLay = (Button) view.findViewById(R.id.btnPlay);

        btnChatear.setTransformationMethod(null);
        btnPLay.setTransformationMethod(null);

        txtMatch = (TextView) view.findViewById(R.id.txtMatch);
        txtSubtitle = (TextView) view.findViewById(R.id.txtSubTitle);

        txtSubtitle.setTypeface(TypesLetter.getSansRegular(mainActivity));
        txtMatch.setText(mainActivity.getResources().getString(R.string.have_match_with)+" " + match.username + "!");
        txtMatch.setTypeface(openSansBold);
        btnPLay.setTypeface(openSansBold);
        btnChatear.setTypeface(openSansBold);

        setImageBackGround(match.picture, backgroundImageUser);
        setImage(match.picture, imgMatch);

        setImage(GlobalData.get(Constant.AVATAR_USER, mainActivity), imgUser);

        btnPLay.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        btnChatear.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                MatchesData matchesData = new MatchesData();
                matchesData.userId      = match.idUser;
                matchesData.firstName   = match.username;
                matchesData.avatarUrl   = match.picture;

                ChatActivity dialog = new ChatActivity(mainActivity, matchesData, Constant.MATCH);
                dialog.show(mainActivity.mFragmentManager, "ChatActivity.TAG");
                dismiss();

            }
        });


        return view;
    }

    /**
     * Método encargado de cargar la imagen en la vista
     * @param url
     * @param image
     */
    private void setImage(String url, ImageView image) {
        Picasso.with(getActivity()).load(url).resize(200, 200).centerCrop().transform(new CircleTransform()).into(image);

    }

    /**
     * Método encargado de cargar la imagen en la vista
     * @param url
     * @param image
     */
    private void setImageBackGround(String url, ImageView image) {
        Picasso.with(getActivity()).load(url).into(image);

    }

}
