package com.android.slidingmenu.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.slidingmenu.Analitycs.MyApplication;
import com.android.slidingmenu.MainActivity;
import com.android.slidingmenu.adapter.GeneralAdapter;
import com.android.slidingmenu.objects.UserData;
import com.android.slidingmenu.util.Common;
import com.android.slidingmenu.util.GlobalData;
import com.android.slidingmenu.util.TypesLetter;
import com.android.slidingmenu.util.Visibility;
import com.android.slidingmenu.webservices.Services;
import com.appdupe.flamer.pojo.MatchesData;
import com.appdupe.flamer.utility.Constant;
import com.google.gson.Gson;
import com.loovers.app.R;
import com.wang.avi.AVLoadingIndicatorView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import jp.co.recruit_lifestyle.android.widget.WaveSwipeRefreshLayout;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class FragmentFavorites extends Fragment  implements WaveSwipeRefreshLayout.OnRefreshListener{

    //private FragmentManager mFragmentManager;
    private GridView gridview;
    public GeneralAdapter adapter;
    public MainActivity mainActivity;
    public WaveSwipeRefreshLayout mWaveSwipeRefreshLayout;
    public ArrayList<MatchesData> matchesDatas = new ArrayList<>();
    public static final int SHOW_DIALOG = 1;
    public static final int HIDE_DIALOG = 0;
    //public ProgressDialog progressDialog;
    public RelativeLayout containerMatchEmpty;
    public Button btnPLayWalla;
    public ImageView imgUser,imgInterrogante;
    public TextView txtTitle,txtSubTitle;
    public AVLoadingIndicatorView progressBarFavoritesWallas;

    public FragmentFavorites()
    {}

    public FragmentFavorites(MainActivity mainActivity)
    {
        this.mainActivity = mainActivity;
    }
    @Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.flechazosactivity, null);
        if(mainActivity == null)
            mainActivity = (MainActivity)getActivity();


        Visibility.visible(mainActivity.tvTitle);

       // mFragmentManager= ((FragmentActivity) inflater.getContext()).getSupportFragmentManager();

        mWaveSwipeRefreshLayout = (WaveSwipeRefreshLayout)view.findViewById(R.id.main_swipe);
        mWaveSwipeRefreshLayout.setColorSchemeColors(Color.WHITE, Color.WHITE);
        mWaveSwipeRefreshLayout.setWaveColor(Color.parseColor("#e2156c"));
        mWaveSwipeRefreshLayout.setOnRefreshListener(this);

        gridview = (GridView)view.findViewById(R.id.search_gridview);
        containerMatchEmpty = (RelativeLayout)view.findViewById(R.id.containerMatchEmpty);
        btnPLayWalla        = (Button)view.findViewById(R.id.btnPLayWalla);
        btnPLayWalla.setTransformationMethod(null);
        progressBarFavoritesWallas  = (AVLoadingIndicatorView)view.findViewById(R.id.progressBarFavoritesWallas);
        txtTitle            = (TextView)view.findViewById(R.id.txtTitle);
        txtSubTitle         = (TextView)view.findViewById(R.id.txtSubTitle);
        imgUser             = (ImageView)view.findViewById(R.id.imageView13);
        imgInterrogante     = (ImageView)view.findViewById(R.id.imageView17);
        imgInterrogante.setImageResource(R.drawable.corazon_favorite);
        txtTitle.setText(""+mainActivity.getResources().getString(R.string.title_favorites_empty));
        txtSubTitle.setText(""+mainActivity.getResources().getString(R.string.message_favorites_empty));
        txtTitle.setTypeface(TypesLetter.getSansBold(mainActivity));
        txtSubTitle.setTypeface(TypesLetter.getSansRegular(mainActivity));
        btnPLayWalla.setTypeface(TypesLetter.getSansRegular(mainActivity));


        String genere = (GlobalData.isKey(Constant.GENERE,mainActivity))?GlobalData.get(Constant.GENERE, mainActivity):Constant.MAN;
        imgUser.setImageResource(genere.equals(Constant.WOMAN) ? R.drawable.hombre : R.drawable.mujer);

        return view;
	}

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);



        if(GlobalData.isKey(Constant.REQUEST_FAVORITES,mainActivity))
        {

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    loadFavorites();
                }
            }, 260);
        }
        else {
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    getFavorites(SHOW_DIALOG);
                }
            }, Constant.TIME_EXCUTE_FRAGMENT);
        }


        btnPLayWalla.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mainActivity.replaceFragment(new FragmentGame(mainActivity));
            }
        });

    }


    /**
     * Cargar favoritos que están en memoria
     */
    public void loadFavorites()
    {
        String resultCards = GlobalData.get(Constant.REQUEST_FAVORITES,mainActivity);
        //Log.d("app2you","favorites resume: " + resultCards);
        Gson gson = new Gson();
        MatchesData []datas = gson.fromJson(resultCards, MatchesData[].class);

        matchesDatas = new ArrayList<MatchesData>(Arrays.asList(datas));
        if (matchesDatas != null && matchesDatas.size() > 0) {
            adapter = new GeneralAdapter(mainActivity, matchesDatas,Constant.FAVORITE);
            gridview.setAdapter(adapter);
            Visibility.visible(gridview);
            Visibility.gone(containerMatchEmpty);

        } else {
            Visibility.gone(gridview);
            Visibility.visible(containerMatchEmpty);
        }
    }
    @Override
    public void onResume() {
        super.onResume();
        MyApplication.getInstance().trackScreenView("fragment favorites");
    }

    /**
     * Metodo encargado de obtener los matches que tiene un usuario
     */
    public void getFavorites(final int option)
    {
        if(option == SHOW_DIALOG) {
            Visibility.visible(progressBarFavoritesWallas);
        }
        OkHttpClient client = new OkHttpClient();
        RequestBody formBody = new FormBody.Builder()
                .add("sessionToken", "" + GlobalData.get(Constant.SESSION_TOKEN, mainActivity))
                .add("serviceId", "" + Constant.FAVORITE)
                .build();

        Request request = new Request.Builder()
                .url(Services.GET_MY_USERS)
                .post(formBody)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
               // if(progressDialog != null)
                 //   progressDialog.dismiss();

                Looper.prepare();
                mainActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Common.errorMessage(mainActivity, "", mainActivity.getResources().getString(R.string.check_internet));
                        Visibility.gone(progressBarFavoritesWallas);
                        GlobalData.set(Constant.SERVICE_EXECUTION, "N", mainActivity);
                        mWaveSwipeRefreshLayout.setRefreshing(false);
                    }
                });

                Looper.loop();
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {

                final String result = response.body().string();
                Log.d("app2you", "respuesta favorite: " + result);
                Looper.prepare();
                if(response.isSuccessful()) {

                    mainActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Gson gson = new Gson();
                                final UserData userData = gson.fromJson(result, UserData.class);
                                if (userData.success) {
                                    GlobalData.set(Constant.SERVICE_EXECUTION, "N", mainActivity);
                                    mWaveSwipeRefreshLayout.setRefreshing(false);
                                    //true
                                    matchesDatas = userData.results;
                                    if (matchesDatas != null && matchesDatas.size() > 0) {
                                        adapter = new GeneralAdapter(mainActivity, matchesDatas, Constant.FAVORITE);
                                        gridview.setAdapter(adapter);
                                        Visibility.visible(gridview);
                                        Visibility.gone(containerMatchEmpty);

                                    } else {
                                        Visibility.gone(gridview);
                                        Visibility.visible(containerMatchEmpty);
                                    }
                                    Visibility.gone(progressBarFavoritesWallas);
                                } else {
                                    //false
                                    Toast.makeText(mainActivity, R.string.error_reponse_server, Toast.LENGTH_SHORT).show();
                                }
                                Visibility.gone(progressBarFavoritesWallas);

                            } catch (Exception ex) {
                                Visibility.gone(progressBarFavoritesWallas);
                                ex.printStackTrace();
                                Toast.makeText(mainActivity, R.string.error_reponse_server, Toast.LENGTH_SHORT).show();
                            }
                            finally {
                                response.body().close();
                            }
                        }
                    });


                }
                else
                    Toast.makeText(mainActivity, R.string.check_internet, Toast.LENGTH_SHORT).show();

                Looper.loop();
            }
        });
    }


    @Override
    public void onRefresh() {
        if(GlobalData.isKey(Constant.SERVICE_EXECUTION,mainActivity)) {
            if (!GlobalData.get(Constant.SERVICE_EXECUTION, mainActivity).equals("Y"))
                if(containerMatchEmpty.getVisibility() == View.GONE) {
                    GlobalData.set(Constant.SERVICE_EXECUTION, "Y", mainActivity);
                    getFavorites(HIDE_DIALOG);
                }
        }
        else {

            if(containerMatchEmpty.getVisibility() == View.GONE) {
                GlobalData.set(Constant.SERVICE_EXECUTION, "Y", mainActivity);
                getFavorites(HIDE_DIALOG);
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        if(matchesDatas != null && matchesDatas.size() > 0)
            GlobalData.set(Constant.REQUEST_FAVORITES,new Gson().toJson(matchesDatas),mainActivity);
    }

}

