package com.android.slidingmenu.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.slidingmenu.Analitycs.MyApplication;
import com.android.slidingmenu.MainActivity;
import com.android.slidingmenu.util.Common;
import com.android.slidingmenu.util.ConnectionDetector;
import com.android.slidingmenu.util.GlobalData;
import com.android.slidingmenu.util.TypesLetter;
import com.android.slidingmenu.webservices.Services;
import com.appdupe.flamer.pojo.MyProfileData;
import com.appdupe.flamer.utility.Constant;
import com.appyvet.rangebar.RangeBar;
import com.loovers.app.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by Juan Martin Bernal on 20/4/16.
 */
public class FragmentEditProfile extends DialogFragment implements View.OnClickListener {

    private TextView senderName,txtDulce,txtApasionada,txtAtrevida,txtTraviesa;
    private ImageView imgBack;
    public RangeBar rangeDulce, rangeApasionada, rangeAtrevida, rangeTraviesa;
    public ImageView imgDulce, imgApasionada, imgAtrevida, imgTraviesa, imgBtnOk;
    public LinearLayout containerOptionsBackChat;
    public MyProfileData myProfileData;
    public MainActivity mainActivity;


    public FragmentEditProfile(MainActivity mainActivity,MyProfileData myProfileData)
    {
        this.mainActivity = mainActivity;
        this.myProfileData = myProfileData;
    }

    public FragmentEditProfile(){}

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.edit_profile, null);

        if(mainActivity == null)
            mainActivity = (MainActivity)getActivity();

        initComponents(view);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_FRAME, android.R.style.Theme_Holo_Light_DarkActionBar);
    }

    /**
     * Inicialización de componentes de la vista
      * @param view
     */
    public void initComponents(View view)
    {
        senderName                  = (TextView)view.findViewById(R.id.senderName);

        containerOptionsBackChat    = (LinearLayout) view.findViewById(R.id.containerOptionsBackChat);
        imgBack                     = (ImageView) view.findViewById(R.id.imgBack);
        txtDulce                    = (TextView) view.findViewById(R.id.txtDulce);
        txtApasionada               = (TextView) view.findViewById(R.id.txtApasionada);
        txtAtrevida                 = (TextView) view.findViewById(R.id.txtAtrevida);
        txtTraviesa                 = (TextView) view.findViewById(R.id.txtTraviesa);
        rangeDulce                  = (RangeBar) view.findViewById(R.id.rangebar_dulce);
        rangeApasionada             = (RangeBar) view.findViewById(R.id.rangebar_apasionada);
        rangeAtrevida               = (RangeBar) view.findViewById(R.id.rangebar_atrevida);
        rangeTraviesa               = (RangeBar) view.findViewById(R.id.rangebar_traviesa);
        imgDulce                    = (ImageView) view.findViewById(R.id.img_dulce);
        imgApasionada               = (ImageView) view.findViewById(R.id.img_apasionada);
        imgAtrevida                 = (ImageView) view.findViewById(R.id.img_atrevida);
        imgTraviesa                 = (ImageView) view.findViewById(R.id.img_traviesa);
        imgBtnOk                    = (ImageView) view.findViewById(R.id.btn_questions);

        senderName.setText(""+mainActivity.getResources().getString(R.string.tell_about_me));
        senderName.setTypeface(TypesLetter.getAvenir(mainActivity));
        String gender = (GlobalData.isKey(Constant.GENERE,mainActivity))?GlobalData.get(Constant.GENERE,mainActivity):Constant.MAN;

        txtDulce.setText((gender.equals(Constant.MAN)) ? getResources().getString(R.string.romantic) : getResources().getString(R.string.sweet));
        txtApasionada.setText((gender.equals(Constant.MAN)) ? getResources().getString(R.string.retailer) : getResources().getString(R.string.passionate));
        txtAtrevida.setText((gender.equals(Constant.MAN)) ? getResources().getString(R.string.daring) : getResources().getString(R.string.bold));
        txtTraviesa.setText((gender.equals(Constant.MAN))?getResources().getString(R.string.seductive):getResources().getString(R.string.naughty));


        initRange(myProfileData.descriptionOptions);

        imgBack.setOnClickListener(this);
        containerOptionsBackChat.setOnClickListener(this);

        rangeDulce.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
            @Override
            public void onRangeChangeListener(RangeBar rangeBar, int leftPinIndex, int rightPinIndex, String leftPinValue, String rightPinValue) {
                //marca la position rightPinIndex
                setImageValue(imgDulce, rightPinIndex);

            }
        });
        rangeApasionada.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
            @Override
            public void onRangeChangeListener(RangeBar rangeBar, int leftPinIndex, int rightPinIndex, String leftPinValue, String rightPinValue) {
                //marca la position rightPinIndex
                setImageValue(imgApasionada, rightPinIndex);

            }

        });
        rangeAtrevida.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
            @Override
            public void onRangeChangeListener(RangeBar rangeBar, int leftPinIndex, int rightPinIndex, String leftPinValue, String rightPinValue) {
                //marca la position rightPinIndex
                setImageValue(imgAtrevida, rightPinIndex);

            }
        });
        rangeTraviesa.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
            @Override
            public void onRangeChangeListener(RangeBar rangeBar, int leftPinIndex, int rightPinIndex, String leftPinValue, String rightPinValue) {
                //marca la position rightPinIndex
                setImageValue(imgTraviesa, rightPinIndex);

            }
        });

        imgBtnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    JSONObject option1 = new JSONObject();
                    option1.put("option", "1");
                    option1.put("value", "" + rangeDulce.getRightPinValue());

                    JSONObject option2 = new JSONObject();
                    option2.put("option", "2");
                    option2.put("value", "" + rangeApasionada.getRightPinValue());

                    JSONObject option3 = new JSONObject();
                    option3.put("option", "3");
                    option3.put("value", "" + rangeAtrevida.getRightPinValue());

                    JSONObject option4 = new JSONObject();
                    option4.put("option", "4");
                    option4.put("value", "" + rangeTraviesa.getRightPinValue());

                    JSONArray jsonArray = new JSONArray();

                    jsonArray.put(option1);
                    jsonArray.put(option2);
                    jsonArray.put(option3);
                    jsonArray.put(option4);

                    if(ConnectionDetector.isConnectingToInternet(mainActivity))
                        updateDescriptionOptions(jsonArray);
                    else
                        Common.errorMessage(mainActivity,"",getResources().getString(R.string.check_internet));

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

    }
    /**
     * Metodo de cambiar la imagen de la intensidad del fuego en relacion a rangeBar
     *
     * @param image
     * @param value
     */
    public void setImageValue(ImageView image, int value) {

        switch (value) {
            case 0:
                image.setImageResource(R.drawable.fuego_0);
                break;
            case 1:
                image.setImageResource(R.drawable.fuego_1);
                break;
            case 2:
                image.setImageResource(R.drawable.fuego_2);
                break;
            case 3:
                image.setImageResource(R.drawable.fuego_3);
                break;
            case 4:
                image.setImageResource(R.drawable.fuego_4);
                break;
            case 5:
                image.setImageResource(R.drawable.fuego_5);
                break;
            case 6:
                image.setImageResource(R.drawable.fuego_6);
                break;
            case 7:
                image.setImageResource(R.drawable.fuego_7);
                break;
            case 8:
                image.setImageResource(R.drawable.fuego_8);
                break;
            case 9:
                image.setImageResource(R.drawable.fuego_9);
                break;
            case 10:
                image.setImageResource(R.drawable.fuego_10);
                break;

            default:
                image.setImageResource(R.drawable.fuego_5);
                break;
        }
    }
    /**
     * Metodo encargado de actualizar la descripcion del usuario
     */
    public void updateDescriptionOptions(JSONArray options) {

        final ProgressDialog progressDialog = new ProgressDialog(mainActivity,R.style.CustomDialogTheme);
        progressDialog.setCancelable(false);
        //  progressDialog.setMessage(""+getResources().getString(R.string.saving_preferences));
        progressDialog.show();
        progressDialog.setContentView(R.layout.custom_progress_dialog);
        TextView txtView = (TextView) progressDialog.findViewById(R.id.txtProgressDialog);
        txtView.setText(""+getResources().getString(R.string.saving_preferences));


        OkHttpClient client = new OkHttpClient();

        RequestBody formBody = new FormBody.Builder()
                .add("sessionToken", "" + GlobalData.get(Constant.SESSION_TOKEN, mainActivity))
                .add("descriptionOptions", "" + options.toString())
                .build();

        Request request = new Request.Builder()
                .url(Services.UPDATE_PREFERENCES_SERVICE)
                .post(formBody)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                progressDialog.dismiss();
                Looper.prepare();
                mainActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Common.errorMessage(mainActivity, "", "" + getResources().getString(R.string.check_internet));
                    }
                });

                Looper.loop();
            }

            @Override
            public void onResponse(Call call,final Response response) throws IOException {
                progressDialog.dismiss();
                final String result = response.body().string();

                Looper.prepare();

                mainActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //apago el dialogo
                        try {
                            JSONObject json = new JSONObject(result);
                            String success = json.getString("success");
                            if (success.equals("true")) {

                                GlobalData.set("dulce",""+rangeDulce.getRightPinValue(),mainActivity);
                                GlobalData.set("apasionada",""+rangeApasionada.getRightPinValue(),mainActivity);
                                GlobalData.set("atrevida",""+rangeAtrevida.getRightPinValue(),mainActivity);
                                GlobalData.set("traviesa",""+rangeTraviesa.getRightPinValue(),mainActivity);
                                dismiss();

                            } else {
                                Toast.makeText(mainActivity, R.string.check_internet, Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException ex) {
                            ex.printStackTrace();
                            Toast.makeText(mainActivity, R.string.error_reponse_server, Toast.LENGTH_SHORT).show();
                        }
                        finally {
                            response.body().close();
                        }
                    }
                });


                Looper.loop();
            }
        });
    }

    @Override
    public void onClick(View v) {

        if(v.getId() == R.id.containerOptionsBackChat)
        {
            dismiss();

        }
        else if(v.getId() == R.id.imgBack)
        {
            dismiss();

        }

    }

    /**
     * Inicializar valores de las barras
     * @param descriptionOptions
     */
    public void initRange(String descriptionOptions)
    {
        if(descriptionOptions!= null) {
            if (GlobalData.isKey("dulce", mainActivity)) {
                int op1 = Integer.parseInt(GlobalData.get("dulce", mainActivity));
                int op2 = Integer.parseInt(GlobalData.get("apasionada", mainActivity));
                int op3 = Integer.parseInt(GlobalData.get("atrevida", mainActivity));
                int op4 = Integer.parseInt(GlobalData.get("traviesa", mainActivity));
                rangeDulce.setSeekPinByIndex(op1);
                rangeApasionada.setSeekPinByIndex(op2);
                rangeAtrevida.setSeekPinByIndex(op3);
                rangeTraviesa.setSeekPinByIndex(op4);
            } else {
                try {

                    JSONArray json = new JSONArray(descriptionOptions);
                    int op1 = Integer.parseInt(json.getJSONObject(0).getString("value"));
                    int op2 = Integer.parseInt(json.getJSONObject(1).getString("value"));
                    int op3 = Integer.parseInt(json.getJSONObject(2).getString("value"));
                    int op4 = Integer.parseInt(json.getJSONObject(3).getString("value"));
                    rangeDulce.setSeekPinByIndex(op1);
                    rangeApasionada.setSeekPinByIndex(op2);
                    rangeAtrevida.setSeekPinByIndex(op3);
                    rangeTraviesa.setSeekPinByIndex(op4);
                } catch (JSONException e) {
                    e.printStackTrace();
                    rangeDulce.setSeekPinByIndex(5);
                    rangeApasionada.setSeekPinByIndex(5);
                    rangeAtrevida.setSeekPinByIndex(5);
                    rangeTraviesa.setSeekPinByIndex(5);
                }
            }
        }
        else {
            rangeDulce.setSeekPinByIndex(5);
            rangeApasionada.setSeekPinByIndex(5);
            rangeAtrevida.setSeekPinByIndex(5);
            rangeTraviesa.setSeekPinByIndex(5);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        MyApplication.getInstance().trackScreenView("fragment edit profile");
    }
}
