package com.android.slidingmenu.fragments;

import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

/**
 * Created by Juan Martin Bernal on 13/1/16.
 */
public class ViewHolder {
    public FrameLayout backgroundFrame;
    public TextView txtNameUser,txtGimgs;
    public ImageView cardImage, imageHeart;
    public ProgressBar progressBarCard;

}