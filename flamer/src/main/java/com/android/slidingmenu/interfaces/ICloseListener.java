package com.android.slidingmenu.interfaces;

/**
 * Created by manoli on 6/3/15.
 */
public interface ICloseListener {
    public void onDialogClose();
}
