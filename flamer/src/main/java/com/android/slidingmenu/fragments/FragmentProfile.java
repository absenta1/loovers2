package com.android.slidingmenu.fragments;


import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Looper;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.slidingmenu.Analitycs.MyApplication;
import com.android.slidingmenu.CameraPhotoCapture;
import com.android.slidingmenu.MainActivity;
import com.android.slidingmenu.activities.ViewContent;
import com.android.slidingmenu.adapter.ProfileGalleryAdapter;
import com.android.slidingmenu.util.Common;
import com.android.slidingmenu.util.ConnectionDetector;
import com.android.slidingmenu.util.GlobalData;
import com.android.slidingmenu.util.Visibility;
import com.android.slidingmenu.util.AppAnimation;
import com.android.slidingmenu.webservices.Services;
import com.appdupe.flamer.pojo.MyProfileData;
import com.appdupe.flamer.pojo.PictureData;
import com.appdupe.flamer.utility.Constant;
import com.gitonway.lee.niftymodaldialogeffects.lib.Effectstype;
import com.gitonway.lee.niftymodaldialogeffects.lib.NiftyDialogBuilder;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.loovers.app.R;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class FragmentProfile extends Fragment {


    private GridView mGridview ;
    private ImageView imageprofile, btn_change_profile,imageView11,imgEditProfile;
    public static final int  MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 12;
    private TextView profile_pic_text;
    public static final int PHOTO_NO_VALIDATE = 1;
    public static final int PHOTO_ACCEPT_TOP = 2;
    public static final int PHOTO_ACCEPT_EROTIC = 3;
    public static final int PHOTO_DENEGATE =  4;
    public static final int PHOTO_ACCEPT = 5;
    public PictureData profile;
    public Animation animationIconCameraUp, animationIconCameraDown;
    public ProgressBar progressBarImageProfile;
    public AVLoadingIndicatorView progressBarGetProfile;
    public MainActivity mainActivity;
    public ArrayList<PictureData>pictures;
    public String [] itemsPublic = {"Borrar foto","Hacer foto publica"};
    public String [] itemsPrivate = {"Borrar foto","Hacer foto privada"};
    public String [] items = null;
    public PictureData pic;
    public MyProfileData myProfileData;
    public int fromOpen = -1;

    public FragmentProfile()
    {

    }
    public FragmentProfile(MainActivity mainActivity)
    {
        this.mainActivity = mainActivity;
    }
    public FragmentProfile(MainActivity mainActivity, int fromOpen)
    {
        this.mainActivity = mainActivity;
        this.fromOpen     = fromOpen;
    }

    @Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_layout2, null);

        if(mainActivity == null)
            mainActivity = (MainActivity)getActivity();


        Visibility.visible(mainActivity.tvTitle);
        mainActivity.tvTitle.setText(mainActivity.getString(R.string.profile));

        imageprofile                = (ImageView)view.findViewById(R.id.imageprofile);
        imgEditProfile              = (ImageView)view.findViewById(R.id.imgEditProfile);
        imageView11                 = (ImageView)view.findViewById(R.id.imageView11);
        profile_pic_text            = (TextView)view.findViewById(R.id.profile_pic_text);
        mGridview                   = (GridView)view.findViewById(R.id.profile_gallery_gridview);
        btn_change_profile          = (ImageView)view.findViewById(R.id.btn_change_profile);
        animationIconCameraUp       = AnimationUtils.loadAnimation(mainActivity,R.anim.bounce);
        animationIconCameraDown     = AnimationUtils.loadAnimation(mainActivity,R.anim.second);
        progressBarImageProfile     = (ProgressBar)view.findViewById(R.id.progressBarImageProfile);
        progressBarGetProfile       = (AVLoadingIndicatorView)view.findViewById(R.id.progressBarGetProfile);

        GlobalData.delete("dulce",mainActivity);
        GlobalData.delete("apasionada",mainActivity);
        GlobalData.delete("atrevida",mainActivity);
        GlobalData.delete("traviesa",mainActivity);

        imgEditProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppAnimation.scale(mainActivity,imgEditProfile);
                FragmentEditProfile fragmentEditProfile = new FragmentEditProfile(mainActivity,myProfileData);
                fragmentEditProfile.show(mainActivity.mFragmentManager,"FragmentEditProfile.TAG");
            }
        });
        imageprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                optionProfile();
            }
        });
        imageView11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                optionProfile();

            }
        });
        mGridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                PictureData pic = (PictureData) parent.getAdapter().getItem(position);

                if (pic.imageUrl.equals("BAJAR")) {

                    if( GlobalData.isKey(Constant.PREFERENCES_IMAGES_GALLERY,mainActivity) &&  GlobalData.get(Constant.PREFERENCES_IMAGES_GALLERY,mainActivity).equals(""+Constant.NUM_IMAGES_GALLERY))
                        Common.errorMessage(mainActivity,"","Solo puedes subir 10 fotos de galeria, borra una de tu galeria dejando presionado sobre la foto que quieras");
                    else
                        openDialogSelectImage(Constant.TYPE_GALLERY,mainActivity.getResources().getString(R.string.msg_modal_update_gallery));
                } else {

                    //launchViewDetail("" + pic.imageUrl, "" + pic.name);
                    pictures = new ArrayList<PictureData>();
                    for (int i = 1, len = mainActivity.adapter.photolist.size(); i < len; ++i)
                        pictures.add(mainActivity.adapter.photolist.get(i));

                    ViewContent viewContent = new ViewContent(mainActivity,pictures ,(position - 1),0);
                    viewContent.show(mainActivity.mFragmentManager,"ViewContent.TAG");

                }
            }
        });
        mGridview.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, final View view, final int position, long l) {
                pic = (PictureData) parent.getAdapter().getItem(position);

                if(pic.isPrivate != null) {
                    if (pic.isPrivate.equals(Constant.PRIVATE_PHOTO))
                        items = itemsPublic;
                    else
                        items = itemsPrivate;


                    new AlertDialog.Builder(mainActivity)
                            .setSingleChoiceItems(items, 0, null)
                            .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    dialog.dismiss();
                                    int selectedPosition = ((AlertDialog) dialog).getListView().getCheckedItemPosition();
                                    switch (selectedPosition) {
                                        case 0:
                                            showDialogDeletePhoto(pic, position);
                                            break;
                                        case 1:
                                            //ejecutar servicio para quitar o poner privacidad
                                            setPrivatePhoto(pic, view);
                                            break;
                                        default:
                                            break;
                                    }
                                }
                            })
                            .show();
                }

                //showDialogDeletePhoto(pic,position);
                return true;
            }
        });


        btn_change_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(mainActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mainActivity, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions( mainActivity, new String[] {  android.Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA },MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
                }
                else {
                    //Animation animationBtnSend = AnimationUtils.loadAnimation(mainActivity,R.anim.scale_button);
                    //btn_change_profile.startAnimation(animationBtnSend);
                    AppAnimation.scale(mainActivity,btn_change_profile);
                    openDialogSelectImage(Constant.TYPE_PROFILE,mainActivity.getResources().getString(R.string.msg_modal_update_photo));
                }
            }
        });

        if(fromOpen == 1)
            openDialogSelectImage(Constant.TYPE_GALLERY,mainActivity.getResources().getString(R.string.msg_modal_update_gallery));




        return view;
    }

    /**
     * Método encargado de mostrar dialogo para borrar una foto de galeria
     * @param pic
     * @param position
     */
    public void showDialogDeletePhoto(final PictureData pic, final int position){

        final Dialog dialog =  new Dialog(mainActivity,R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        dialog.setContentView(R.layout.modal_general);

        TextView text = (TextView) dialog.findViewById(R.id.text);
        text.setText(""+mainActivity.getResources().getString(R.string.delete_photo_gallery));

        Button exit = (Button) dialog.findViewById(R.id.exit);
        Button accept = (Button) dialog.findViewById(R.id.accept);
        // if button is clicked, close the custom dialog
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deletePictureGallery(pic.id_foto, position, dialog);
            }
        });

        dialog.show();
    }

    /**
     * Método encargado de abrir dialogo para seleccionar foto
     */
    public void optionProfile()
    {
        if(profile != null) {
            // Toast.makeText(getActivity(), "" + profile.toString(), Toast.LENGTH_SHORT).show();
            launchViewDetail(""+profile.imageUrl, ""+profile.name);
           // GlobalData.set("tmp", "Y", mainActivity);
        }
        else {
           // Toast.makeText(getActivity(), "perfil nulo", Toast.LENGTH_SHORT).show();

            if (ActivityCompat.checkSelfPermission(mainActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mainActivity, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions( mainActivity, new String[] {  android.Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA  },MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
            }
            else {
                openDialogSelectImage(Constant.TYPE_PROFILE,mainActivity.getResources().getString(R.string.msg_modal_update_photo));
            }

        }
    }

    /**
     * Método para validar la foto dependiendo del estado del usuario
     */

    public void validateStatusPhoto(String userStatus)
    {
        String status = userStatus;//GlobalData.get(Constant.USER_STATUS,mainActivity);
        if(status.equals(Constant.NOT_GOLD_BEFORE_TRIAL_NO_PICTURE))
        {
           // if(!GlobalData.isKey(Constant.MESSAGE_NOT_PHOTO,mainActivity)) {

                final NiftyDialogBuilder dialog =  NiftyDialogBuilder.getInstance(mainActivity);
                dialog.withTitle(null);
                dialog.withMessage(null);
                dialog.isCancelableOnTouchOutside(false);
                dialog.withDialogColor(Color.TRANSPARENT);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.withDuration(500);
                dialog.withEffect(Effectstype.Slidetop);
                dialog.setCustomView(R.layout.modal_general,mainActivity);

                TextView text = (TextView) dialog.findViewById(R.id.text);
                TextView title = (TextView) dialog.findViewById(R.id.title);
                title.setText("" + Common.getNickName(mainActivity));
                text.setText(""+mainActivity.getResources().getString(R.string.msg_update_photo)+ "\n\n"+ mainActivity.getResources().getString(R.string.advantages_upload_photo));


                Button exit = (Button) dialog.findViewById(R.id.exit);
                Visibility.gone(exit);
                Button accept = (Button) dialog.findViewById(R.id.accept);
                // if button is clicked, close the custom dialog
                exit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                accept.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                       // GlobalData.set(Constant.MESSAGE_NOT_PHOTO, "Y", mainActivity);
                        if (ActivityCompat.checkSelfPermission(mainActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ) {
                            ActivityCompat.requestPermissions( mainActivity, new String[] {  android.Manifest.permission.WRITE_EXTERNAL_STORAGE  },MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
                        }
                        else {
                            openDialogSelectImage(Constant.TYPE_PROFILE,mainActivity.getResources().getString(R.string.msg_modal_update_photo));
                        }


                    }
                });

                dialog.show();
           // }
        }
        else if(status.equals(Constant.NOT_GOLD_BEFORE_TRIAL_PICTURE_PENDING))
        {
            if(!GlobalData.isKey(Constant.MESSAGE_PHOTO_PENDING,mainActivity)) {

                final NiftyDialogBuilder dialog =  NiftyDialogBuilder.getInstance(mainActivity);
                dialog.withTitle(null);
                dialog.withMessage(null);
                dialog.isCancelableOnTouchOutside(false);
                dialog.withDialogColor(Color.TRANSPARENT);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.withDuration(500);
                dialog.withEffect(Effectstype.Slidetop);
                dialog.setCustomView(R.layout.modal_general,mainActivity);

                TextView text = (TextView) dialog.findViewById(R.id.text);
                TextView title = (TextView) dialog.findViewById(R.id.title);
                title.setText(""+ Common.getNickName(mainActivity));
                text.setText(""+mainActivity.getResources().getString(R.string.advert_photo_pending_profile));

                Button exit = (Button) dialog.findViewById(R.id.exit);
                Visibility.gone(exit);
                Button accept = (Button) dialog.findViewById(R.id.accept);
                // if button is clicked, close the custom dialog
                exit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                accept.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        GlobalData.set(Constant.MESSAGE_PHOTO_PENDING, "Y", mainActivity);


                    }
                });

                dialog.show();
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        //consulta la informacion del usuario

            if (ConnectionDetector.isConnectingToInternet(mainActivity))
                getMyUserProfile();
            else
                Toast.makeText(mainActivity,R.string.check_internet, Toast.LENGTH_SHORT).show();

            animateCamera();

        MyApplication.getInstance().trackScreenView("fragment profile");

    }

    /**
     * Metodo encargado de consultar el perfil del usuario
     */
    public void deletePictureGallery(String idPhoto, final int position, final Dialog dialog)
    {

        OkHttpClient client = new OkHttpClient();

        final ProgressDialog progressDialog = new ProgressDialog(mainActivity,R.style.CustomDialogTheme);
        progressDialog.setCancelable(false);
      //  progressDialog.setMessage(mainActivity.getResources().getString(R.string.deleting_photo_from_gallery));
        progressDialog.show();
        progressDialog.setContentView(R.layout.custom_progress_dialog);
        TextView txtView = (TextView) progressDialog.findViewById(R.id.txtProgressDialog);
        txtView.setText(""+getResources().getString(R.string.deleting_photo_from_gallery));

        RequestBody formBody = new FormBody.Builder()
                .add("sessionToken", "" + GlobalData.get(Constant.SESSION_TOKEN, mainActivity))
                .add("ip", Common.getLocalIpAddress(mainActivity))
                .add("version", Common.getVersionDevice(mainActivity))
                .add("id", "" + idPhoto)
                .build();

        Request request = new Request.Builder()
                .url(Services.DELETE_USER_PICTURE)
                .post(formBody)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                progressDialog.dismiss();
                Looper.prepare();
                mainActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        dialog.dismiss();
                        Common.errorMessage(mainActivity, "",mainActivity.getResources().getString(R.string.check_internet));
                    }
                });

                Looper.loop();
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {

                progressDialog.dismiss();
                final String result = response.body().string();
                Log.d("app2you", "respuesta layout 2 delete: " + result);

                Looper.prepare();
                mainActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            JSONObject json = new JSONObject(result);

                            String success = json.getString("success");
                            if (success.equals("true")) {
                                dialog.dismiss();
                                int numGallery = json.getInt("numGalleryPics");
                                GlobalData.set(Constant.PREFERENCES_IMAGES_GALLERY,""+numGallery,mainActivity);
                                mainActivity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        mainActivity.adapter.photolist.remove(position);
                                        mainActivity.adapter.notifyDataSetChanged();
                                        mGridview.setAdapter(mainActivity.adapter);
                                    }
                                });

                            } else
                                Toast.makeText(mainActivity, R.string.try_again, Toast.LENGTH_SHORT).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(mainActivity, R.string.error_reponse_server, Toast.LENGTH_SHORT).show();
                        }
                        finally {
                            response.body().close();
                        }
                    }
                });

                Looper.loop();
            }
        });
    }

    /**
     * Metodo encargado de actualizar la privacidad de una foto privada/publica
     */
    public void setPrivatePhoto(final PictureData pic, final View view)
    {

        OkHttpClient client = new OkHttpClient();

        final ProgressDialog progressDialog = new ProgressDialog(mainActivity,R.style.CustomDialogTheme);
        progressDialog.setCancelable(false);
        //  progressDialog.setMessage(mainActivity.getResources().getString(R.string.deleting_photo_from_gallery));
        progressDialog.show();
        progressDialog.setContentView(R.layout.custom_progress_dialog);
        TextView txtView = (TextView) progressDialog.findViewById(R.id.txtProgressDialog);
        txtView.setText("Actualizando foto...");

        //1 es privada - 0 noes
        RequestBody formBody = new FormBody.Builder()
                .add("sessionToken", "" + GlobalData.get(Constant.SESSION_TOKEN, mainActivity))
                .add("picId", ""+pic.id_foto)
                .add("isPrivate", (pic.isPrivate.equals("1")?Constant.PUBLIC_PHOTO:Constant.PRIVATE_PHOTO))

                .build();

        Request request = new Request.Builder()
                .url(Services.SET_PRIVATE_PHOTO)
                .post(formBody)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                progressDialog.dismiss();
                Looper.prepare();
                mainActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Common.errorMessage(mainActivity, "",mainActivity.getResources().getString(R.string.check_internet));
                    }
                });

                Looper.loop();
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {

                progressDialog.dismiss();
                final String result = response.body().string();
                Log.d("app2you", "respuesta private photo " + result);

                Looper.prepare();
                mainActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            JSONObject json = new JSONObject(result);

                            String success = json.getString("success");
                            if (success.equals("true")) {
                                ImageView image = (ImageView) view.findViewById(R.id.imgLock);
                                //image.setVisibility(pic.isPrivate.equals("0")?View.VISIBLE:View.GONE);
                                if(pic.isPrivate != null && pic.isPrivate.equals("0")) {
                                    pic.isPrivate = Constant.PRIVATE_PHOTO;
                                    Visibility.visible(image);
                                }
                                else {
                                    pic.isPrivate = "0";
                                    Visibility.gone(image);
                                }
                                mainActivity.adapter.notifyDataSetChanged();


                            } else
                                Toast.makeText(mainActivity, R.string.try_again, Toast.LENGTH_SHORT).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(mainActivity, R.string.error_reponse_server, Toast.LENGTH_SHORT).show();
                        }
                        finally {
                            response.body().close();
                        }
                    }
                });

                Looper.loop();
            }
        });
    }
    /**
     * Metodo encargado de consultar el perfil del usuario
     */
    public void getMyUserProfile()
    {
        Visibility.visible(progressBarGetProfile);
        OkHttpClient client = new OkHttpClient();

        RequestBody formBody = new FormBody.Builder()
                .add("sessionToken", "" + GlobalData.get(Constant.SESSION_TOKEN, mainActivity))
                .build();

        Request request = new Request.Builder()
                .url(Services.GET_PROFILE)
                .post(formBody)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

                Looper.prepare();
                mainActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Visibility.gone(progressBarGetProfile);
                        Common.errorMessage(mainActivity,"",mainActivity.getResources().getString(R.string.check_internet));
                        Visibility.visible(btn_change_profile);
                    }
                });
                Looper.loop();
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {

                final String result = response.body().string();
                Log.d("app2you", "getMyUserProfile: " + result);
                Looper.prepare();

                    mainActivity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    try{
                                        Gson gson = new Gson();
                                        myProfileData = gson.fromJson(result, MyProfileData.class);
                                        if (myProfileData != null) {
                                            if (myProfileData.success) {
                                                if(myProfileData.currentLikes != null) {
                                                    int current = Integer.parseInt(myProfileData.currentLikes);
                                                    if(current > 0)
                                                        mainActivity.updateCredits(""+current);
                                                    else
                                                    {
                                                        GlobalData.set(Constant.CURRENT_LIKES,""+current,mainActivity);
                                                        if(mainActivity.txtCreditsStore != null)
                                                            mainActivity.txtCreditsStore.setText(""+current);

                                                        if(mainActivity.txtCredits != null)
                                                            mainActivity.txtCredits.setText(""+current);

                                                        if(mainActivity.imgCreditsStore != null)
                                                            mainActivity.imgCreditsStore.setImageResource(R.drawable.no_credito);

                                                        if(mainActivity.imgCredits != null)
                                                            mainActivity.imgCredits.setImageResource(R.drawable.no_credito);

                                                    }

                                                }
                                                else
                                                    mainActivity.txtCredits.setText(GlobalData.get(Constant.CURRENT_LIKES,mainActivity));

                                                Visibility.visible(imgEditProfile);
                                                AppAnimation.fadeIn(imgEditProfile,200);

                                                Visibility.gone(progressBarGetProfile);

                                                GlobalData.set(Constant.USER_STATUS, "" + myProfileData.accountTypeID, mainActivity);

                                                //mostrar mensaje dependiendo del estado del usuario
                                                validateStatusPhoto(myProfileData.accountTypeID);

                                                if(myProfileData.pimgs != null && myProfileData.pimgs.size() == 0)
                                                {
                                                    //Visibility.gone(imageView11);
                                                    GlobalData.set(Constant.AVATAR_USER, "", mainActivity);
                                                }
                                                else if (myProfileData.pimgs != null && myProfileData.pimgs.size() == 1) {
                                                    Visibility.gone(imageView11);

                                                    profile = myProfileData.pimgs.get(0);
                                                    //PictureData pictureDataProfile = myProfileData.pimgs.get(myProfileData.pimgs.size() - 1);
                                                    String url = profile.imageUrl;
                                                    String avatar = profile.avatarUrl;
                                                    GlobalData.set(Constant.AVATAR_USER, "" + avatar, mainActivity);
                                                 //   Log.d("app2you", "imageUrl: " + url);
                                                    switch (Integer.parseInt(profile.status)) {
                                                        case PHOTO_NO_VALIDATE:
                                                            loadImageProfile(url);
                                                            break;
                                                        case PHOTO_ACCEPT_TOP:
                                                            //Toast.makeText(mainActivity, "Tu foto será actualizada cuando sea validada por nuestro equipo caso 2", Toast.LENGTH_SHORT).show();
                                                            Common.errorMessage(mainActivity, "", mainActivity.getResources().getString(R.string.revise_photo_team_loovers));
                                                            break;
                                                        case PHOTO_ACCEPT_EROTIC:
                                                            // Toast.makeText(getActivity(), "Tu foto será actualizada cuando sea validada por nuestro equipo caso 3", Toast.LENGTH_SHORT).show();
                                                            loadImageProfile(url);
                                                            break;
                                                        case PHOTO_DENEGATE:

                                                            //loadImageProfile(url);
                                                            //Todo: hacer algo con la foto denegada mostrar mensaje porque razón fue denegada
                                                            Picasso.with(mainActivity).load(R.drawable.denied_photo).into(imageprofile);
                                                            GlobalData.delete(Constant.AVATAR_USER, mainActivity);
                                                           // Common.errorMessage(mainActivity, "", "" + mainActivity.getResources().getString(R.string.denegated_photo));
                                                            break;
                                                        case PHOTO_ACCEPT:
                                                            //Toast.makeText(getActivity(), "Tu foto será actualizada cuando sea validada por nuestro equipo caso 5", Toast.LENGTH_SHORT).show();
                                                            loadImageProfile(url);
                                                            break;
                                                        default:
                                                            break;

                                                    }
                                                    GlobalData.set(Constant.PREFERENCES_IMAGES_GALLERY,""+myProfileData.gimgs.size(),mainActivity);

                                                    mainActivity.adapter = new ProfileGalleryAdapter(myProfileData.gimgs, mainActivity);
                                                    mGridview.setAdapter(mainActivity.adapter);

                                                    SpannableStringBuilder longDescription = new SpannableStringBuilder();
                                                    PictureData pictureDataGallery = myProfileData.pimgs.get(0);
                                                    String status = (pictureDataGallery.status != null) ? pictureDataGallery.status : "";
                                                    String estado = "";
                                                    if (status.equals("1")) {
                                                        estado = "Pendiente de validar";
                                                        longDescription.append(estado);
                                                        //int start = longDescription.length();
                                                        longDescription.setSpan(new ForegroundColorSpan(0xFFe46b29), 0, longDescription.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                                                    } else if (status.equals("2")) {
                                                        estado = "";
                                                    } else if (status.equals("3")) {
                                                        estado = "";

                                                    } else if (status.equals("4")) {
                                                        estado = "";
                                                        longDescription.append(estado);
                                                        // int start = longDescription.length();
                                                        longDescription.setSpan(new ForegroundColorSpan(0xFFff0000), 0, longDescription.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                                                    }

                                                    profile_pic_text.setText((longDescription.toString().isEmpty()) ? "Perfil" : "" + longDescription);

                                                    //animar entrada icono camara
                                                    animateCamera();

                                                } else if (myProfileData.pimgs != null && myProfileData.pimgs.size() == 2) {
                                                    // Toast.makeText(mainActivity, "Han llegado 2 fotos en pimgs", Toast.LENGTH_SHORT).show();
                                                    Visibility.gone(imageView11);
                                                    profile = myProfileData.pimgs.get(1);
                                                    //PictureData pictureDataProfile = myProfileData.pimgs.get(myProfileData.pimgs.size() - 1);
                                                    String url = profile.imageUrl;
                                                    String avatar = profile.avatarUrl;
                                                    GlobalData.set(Constant.AVATAR_TEMPORAL, "" + avatar, mainActivity);
                                                    //Log.d("app2you", "imageUrl: " + url);
                                                    switch (Integer.parseInt(profile.status)) {
                                                        case PHOTO_NO_VALIDATE:
                                                            loadImageProfile(url);
                                                            break;
                                                        case PHOTO_ACCEPT_TOP:
                                                            //Toast.makeText(mainActivity, "Tu foto será actualizada cuando sea validada por nuestro equipo caso 2", Toast.LENGTH_SHORT).show();
                                                            Common.errorMessage(mainActivity, "", mainActivity.getResources().getString(R.string.revise_photo_team_loovers));
                                                            break;
                                                        case PHOTO_ACCEPT_EROTIC:
                                                            // Toast.makeText(getActivity(), "Tu foto será actualizada cuando sea validada por nuestro equipo caso 3", Toast.LENGTH_SHORT).show();
                                                            loadImageProfile(url);
                                                            break;
                                                        case PHOTO_DENEGATE:

                                                            //loadImageProfile(url);
                                                            //Todo: hacer algo con la foto denegada mostrar mensaje porque razón fue denegada
                                                            Picasso.with(mainActivity).load(myProfileData.pimgs.get(0).imageUrl).into(imageprofile);
                                                            GlobalData.delete(Constant.AVATAR_TEMPORAL, mainActivity);
                                                            Common.errorMessage(mainActivity, "", "" + mainActivity.getResources().getString(R.string.denegated_photo));
                                                            GlobalData.set(Constant.AVATAR_USER, "" + myProfileData.pimgs.get(0).avatarUrl, mainActivity);
                                                            break;
                                                        case PHOTO_ACCEPT:
                                                            //Toast.makeText(getActivity(), "Tu foto será actualizada cuando sea validada por nuestro equipo caso 5", Toast.LENGTH_SHORT).show();
                                                            loadImageProfile(url);
                                                            GlobalData.set(Constant.AVATAR_USER, "" + avatar, mainActivity);
                                                            break;
                                                        default:
                                                            break;

                                                    }
                                                    GlobalData.set(Constant.PREFERENCES_IMAGES_GALLERY,""+myProfileData.gimgs.size(),mainActivity);

                                                    mainActivity.adapter = new ProfileGalleryAdapter(myProfileData.gimgs, mainActivity);
                                                    mGridview.setAdapter(mainActivity.adapter);

                                                    SpannableStringBuilder longDescription = new SpannableStringBuilder();
                                                    PictureData pictureDataGallery = myProfileData.pimgs.get(1);
                                                    String status = (pictureDataGallery.status != null) ? pictureDataGallery.status : "";
                                                    String estado = "";
                                                    if (status.equals("1")) {
                                                        estado = "Pendiente de validar";
                                                        longDescription.append(estado);
                                                        //int start = longDescription.length();
                                                        longDescription.setSpan(new ForegroundColorSpan(0xFFe46b29), 0, longDescription.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                                                    } else if (status.equals("2")) {
                                                        estado = "";
                                                    } else if (status.equals("3")) {
                                                        estado = "";

                                                    } else if (status.equals("4")) {
                                                        estado = "" + mainActivity.getString(R.string.profile);//getResources().getString(R.string.lh_foto_validada_ko);
                                                        longDescription.append(estado);
                                                        // int start = longDescription.length();
                                                        longDescription.setSpan(new ForegroundColorSpan(0xFFff0000), 0, longDescription.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                                                    }

                                                    profile_pic_text.setText((longDescription.toString().isEmpty()) ? mainActivity.getString(R.string.profile) : "" + longDescription);

                                                    //animar entrada icono camara
                                                    animateCamera();
                                                } else {

                                                    Visibility.visible(btn_change_profile);
                                                    //Toast.makeText(mainActivity, "No hay imagen de perfil", Toast.LENGTH_SHORT).show();
                                                }
                                            } else {
                                                if (myProfileData.errNum == 100)
                                                    Common.closeSessionWallamatch(mainActivity);
                                                else {
                                                    Visibility.visible(btn_change_profile);
                                                    Toast.makeText(mainActivity, R.string.try_again, Toast.LENGTH_SHORT).show();
                                                }
                                            }
                                        } else {
                                            Visibility.visible(btn_change_profile);
                                            Toast.makeText(mainActivity, R.string.check_internet, Toast.LENGTH_SHORT).show();
                                        }
                                    }catch(Exception ex)
                                    {
                                        ex.printStackTrace();
                                        Toast.makeText(mainActivity, R.string.error_reponse_server, Toast.LENGTH_SHORT).show();
                                    }
                                    finally {
                                        response.body().close();
                                    }
                                }


                                });

                Looper.loop();
            }
        });
    }

    /**
     * Metodo encarago de cargar imagen
     * @param url
     */
    public void loadImageProfile(String url)
    {
        Visibility.visible(progressBarImageProfile);
        Picasso.with(mainActivity).load(url).error(R.drawable.no_foto).into(imageprofile, new com.squareup.picasso.Callback() {
            @Override
            public void onSuccess() {
                Visibility.gone(progressBarImageProfile);
            }

            @Override
            public void onError() {
                Visibility.gone(progressBarImageProfile);
            }
        });
    }

    /**
     * Metodo encargado de animacion del icono de camara
     */
    private void animateCamera() {
        Visibility.visible(btn_change_profile);
       /* animationIconCameraUp.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                btn_change_profile.startAnimation(animationIconCameraDown);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        btn_change_profile.startAnimation(animationIconCameraUp);*/
    }

    /**
     * Método encargado de enviar imagen ViewContent, para ver la foto completa
     * @param urlImage
     * @param name
     */
    public void launchViewDetail(String urlImage, String name)
    {
        /*Intent intento = new Intent(mainActivity, ViewContent.class);
        intento.putExtra("urlImage",urlImage);
        intento.putExtra("name", name);
        mainActivity.startActivity(intento);*/
        ViewContent viewContent = new ViewContent(mainActivity,urlImage,name);
        viewContent.show(mainActivity.mFragmentManager,"ViewContent.TAG");
    }

    @Override
    public void onPause() {
        super.onPause();
       // GlobalData.delete("tmp", mainActivity);
    }


    /**
     * Abrir modal para opciones de foto
     */
    public void openDialogSelectImage(final int profileOrGallery, String subTitle)
    {
        // custom dialog
        final Dialog dialog = new Dialog(mainActivity,R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.modal_select_image);

        TextView txtSubTitle = (TextView)dialog.findViewById(R.id.subtitle) ;
        txtSubTitle.setText(subTitle);
        Button gallery = (Button) dialog.findViewById(R.id.galeria);
        Button camera = (Button) dialog.findViewById(R.id.camara);
        Button cancel = (Button) dialog.findViewById(R.id.cancelar);
        gallery.setTransformationMethod(null);
        camera.setTransformationMethod(null);
        cancel.setTransformationMethod(null);
        // if button is clicked, close the custom dialog
        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
               // openGallery();
                Intent i = new Intent(mainActivity, CameraPhotoCapture.class);
                i.putExtra("TYPE", profileOrGallery);
                i.putExtra("OPEN",1); //abrir galeria del dispositivo
                startActivity(i);
                mainActivity.overridePendingTransition(R.anim.fade_in_slow, R.anim.fade_out);
            }
        });
        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                //takePhoto();nvb
                // Here, thisActivity is the current activity
                if (ActivityCompat.checkSelfPermission(mainActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mainActivity, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ) {
                    ActivityCompat.requestPermissions( mainActivity, new String[] {  android.Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA  },MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
                }
                else {
                    Intent i = new Intent(mainActivity, CameraPhotoCapture.class);
                    i.putExtra("TYPE", profileOrGallery);
                    i.putExtra("OPEN",2); //abrir camara
                    startActivity(i);
                    mainActivity.overridePendingTransition(R.anim.fade_in_slow, R.anim.fade_out);
                }



            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });

        dialog.show();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    Intent i = new Intent(mainActivity, CameraPhotoCapture.class);
                    i.putExtra("TYPE", Constant.TYPE_PROFILE);
                    i.putExtra("OPEN",2); //abrir camara
                    startActivity(i);
                    mainActivity.overridePendingTransition(R.anim.fade_in_slow, R.anim.fade_out);
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

}
