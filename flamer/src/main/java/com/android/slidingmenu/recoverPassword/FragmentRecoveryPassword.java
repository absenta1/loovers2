package com.android.slidingmenu.recoverPassword;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.slidingmenu.activities.LoginActivity;
import com.android.slidingmenu.util.Common;
import com.android.slidingmenu.util.KeyBoard;
import com.android.slidingmenu.util.Validate;
import com.android.slidingmenu.webservices.Services;
import com.loovers.app.R;

import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by Juan Martin Bernal on 23/2/16.
 */
public class FragmentRecoveryPassword extends FragmentActivity
{
    public String idPass;
    public EditText fieldPassword, fieldRepeatPassword;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.fragment_change_password);

        fieldPassword          = (EditText)findViewById(R.id.fieldSecurity);
        fieldRepeatPassword    = (EditText)findViewById(R.id.fieldRepeatSecurity);

        Log.d("app2you","app  restaurar contrasena");
        final Intent intent = getIntent();
        final String action = intent.getAction();
        Log.d("app2you","restaurar contrasena accion - " + action);
        if (Intent.ACTION_VIEW.equals(action)) {
            final List<String> segments = intent.getData().getPathSegments();
            if (segments.size() > 1) {
                String idChangePass = segments.get(1);
                idPass = idChangePass;
                confirmIdChangePassword(idChangePass);

            }
        }
    }
    public void modifyPassword(View view)
    {
        String password = fieldPassword.getText().toString().trim();
        String repeatPassword = fieldRepeatPassword.getText().toString().trim();

        if(password.isEmpty() || password.length() < 6 || !Validate.isValidPassword(password))
        {
            fieldPassword.setError(getResources().getString(R.string.error_password));
        }
        if(repeatPassword.isEmpty() || repeatPassword.length() < 6 || !Validate.isValidPassword(repeatPassword))
        {
            fieldRepeatPassword.setError(getResources().getString(R.string.error_password));
        }
        else {
            if (password.equalsIgnoreCase(repeatPassword)) {
                KeyBoard.hide(fieldPassword);
                KeyBoard.hide(fieldRepeatPassword);
                Log.d("app2you","idPass: " + idPass);
                serviceModifyPassword(password,repeatPassword);

            }
            else
                Toast.makeText(FragmentRecoveryPassword.this, R.string.error_password_equals, Toast.LENGTH_SHORT).show();
        }
    }
    public void serviceModifyPassword(String password,String repeatPassword )
    {
        final ProgressDialog progressDialog = new ProgressDialog(this,R.style.CustomDialogTheme);
        progressDialog.setCancelable(false);
        //progressDialog.setMessage();
        progressDialog.show();
        progressDialog.setContentView(R.layout.custom_progress_dialog);
        TextView txtView = (TextView) progressDialog.findViewById(R.id.txtProgressDialog);
        txtView.setText(""+getResources().getString(R.string.modifying_password));

        OkHttpClient client = new OkHttpClient();

        RequestBody formBody = new FormBody.Builder()
                .add("id", "" + idPass)
                .add("pwd",""+password)
                .add("confirmPwd",""+repeatPassword)

                .build();

        Request request = new Request.Builder()
                .url(Services.MODIFY_PASSWORD)
                .post(formBody)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                progressDialog.dismiss();
                Looper.prepare();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Common.errorMessage(FragmentRecoveryPassword.this, "", "" + getResources().getString(R.string.check_internet));
                    }
                });

                Looper.loop();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                progressDialog.dismiss();
                final String result = response.body().string();
                Log.d("app2you", "respuesta confirm status: " + result);

                Looper.prepare();

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //apago el dialogo
                        try {
                            JSONObject json = new JSONObject(result);
                            String success = json.getString("success");
                            if (success.equals("true")) {

                                Intent intento = new Intent(FragmentRecoveryPassword.this, LoginActivity.class);
                                intento.putExtra("success_password","OK");
                                startActivity(intento);
                                overridePendingTransition(R.anim.fade_in_slow, R.anim.fade_out);
                                finish();
                            } else {
                                //3-la key no pudo ser encontrada, 4- password no coinciden - 5 password no pudo ser actualizado
                                int error = json.getInt("errorNum");
                                if(error == 3)
                                    Toast.makeText(FragmentRecoveryPassword.this, R.string.link_invalid, Toast.LENGTH_SHORT).show();
                                else
                                    Toast.makeText(FragmentRecoveryPassword.this, "Tu contraseña no pudo ser actualizada, intenta más tarde.", Toast.LENGTH_SHORT).show();

                            }

                        } catch (Exception ex) {
                            ex.printStackTrace();
                            Toast.makeText(FragmentRecoveryPassword.this, R.string.error_reponse_server, Toast.LENGTH_SHORT).show();
                        }
                    }
                });


                Looper.loop();
            }
        });
    }

    /**
     * Metodo encargado validacion de email del usuario
     */
    public void confirmIdChangePassword(String id) {

        final ProgressDialog progressDialog = new ProgressDialog(this,R.style.CustomDialogTheme);
        progressDialog.setCancelable(false);
        //progressDialog.setMessage();
        progressDialog.show();
        progressDialog.setContentView(R.layout.custom_progress_dialog);
        TextView txtView = (TextView) progressDialog.findViewById(R.id.txtProgressDialog);
        txtView.setText(""+getResources().getString(R.string.validate_id_password));

        OkHttpClient client = new OkHttpClient();

        RequestBody formBody = new FormBody.Builder()
                .add("id", "" + id)
                .add("type","password")
                .build();

        Request request = new Request.Builder()
                .url(Services.CHECK_KEY)
                .post(formBody)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                progressDialog.dismiss();
                Looper.prepare();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Common.errorMessage(FragmentRecoveryPassword.this, "", "" + getResources().getString(R.string.check_internet));
                    }
                });

                Looper.loop();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                progressDialog.dismiss();
                final String result = response.body().string();
                Log.d("app2you", "respuesta confirm status: " + result);

                Looper.prepare();

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //apago el dialogo
                            try {
                                JSONObject json = new JSONObject(result);
                                String success = json.getString("success");
                                if (success.equals("true")) {
                                    //Toast.makeText(FragmentRecoveryPassword.this, "", Toast.LENGTH_SHORT).show();
                                } else {
                                    //errorNum
                                    //3 expiro la fecha 24 horas, 4 - no existe, codigo ya ha sido usado, 5- no valido
                                    Common.errorMessage(FragmentRecoveryPassword.this,"",""+getResources().getString(R.string.link_invalid));
                                    Intent intento = new Intent(FragmentRecoveryPassword.this, LoginActivity.class);
                                    startActivity(intento);
                                    overridePendingTransition(R.anim.fade_in_slow, R.anim.fade_out);
                                    finish();
                                }

                            } catch (Exception ex) {
                                ex.printStackTrace();

                                Toast.makeText(FragmentRecoveryPassword.this, R.string.error_reponse_server, Toast.LENGTH_SHORT).show();
                                Intent intento = new Intent(FragmentRecoveryPassword.this, LoginActivity.class);
                                startActivity(intento);
                                overridePendingTransition(R.anim.fade_in_slow, R.anim.fade_out);
                                finish();

                            }
                        }
                    });


                Looper.loop();
            }
        });
    }


}
