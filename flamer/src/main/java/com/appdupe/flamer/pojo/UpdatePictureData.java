package com.appdupe.flamer.pojo;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class UpdatePictureData implements Serializable
{
    @SerializedName("status")
    private String  status;

    @SerializedName("oldMatches")
    private String  oldMatches;

    @SerializedName("newMatches")
    private String  newMatches;

    @SerializedName("userLiked")
    private String  userLiked;

    @SerializedName("messaNotRead")
    private String  messaNotRead;

    public String getOldMatches() {
        return oldMatches;
    }

    public void setOldMatches(String oldMatches) {
        this.oldMatches = oldMatches;
    }

    public String getNewMatches() {
        return newMatches;
    }

    public void setNewMatches(String newMatches) {
        this.newMatches = newMatches;
    }

    public String getUserLiked() {
        return userLiked;
    }

    public void setUserLiked(String userLiked) {
        this.userLiked = userLiked;
    }

    public String getMessaNotRead() {
        return messaNotRead;
    }

    public void setMessaNotRead(String messaNotRead) {
        this.messaNotRead = messaNotRead;
    }

    public String getErrNum() {
        return errNum;
    }

    public void setErrNum(String errNum) {
        this.errNum = errNum;
    }

    public String getStatus() {

        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @SerializedName("errNum")
    private String  errNum;
	
	
	
	

	
}
