package com.android.slidingmenu.store;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.slidingmenu.MainActivity;
import com.android.slidingmenu.products.Product;
import com.android.slidingmenu.util.Common;
import com.android.slidingmenu.util.GlobalData;
import com.android.slidingmenu.util.TypesLetter;
import com.android.slidingmenu.util.Visibility;
import com.appdupe.flamer.utility.Constant;
import com.loovers.app.R;

import java.util.ArrayList;

/**
 * Created by Juan Martin on 11/3/16.
 */
public class BuyCreditsAdapter extends ArrayAdapter<Product> {
    private MainActivity mainActivity;
    private ArrayList<Product> products;

    public BuyCreditsAdapter(MainActivity mainActivity, ArrayList<Product> products) {
        super(mainActivity, R.layout.row_buy_credits, products);
        this.mainActivity = mainActivity;
        this.products = products;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) mainActivity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView                            = inflater.inflate(R.layout.row_buy_credits, parent, false);
        TextView txtProduct                     = (TextView) rowView.findViewById(R.id.txtTitle);
        TextView txtSubtitle                    = (TextView) rowView.findViewById(R.id.txtSubtitle);
        TextView txtPrice                       = (TextView) rowView.findViewById(R.id.txtPrice);
        ImageView imgProduct                    = (ImageView)rowView.findViewById(R.id.imgProduct);
        LinearLayout container                  = (LinearLayout)rowView.findViewById(R.id.containerBuy);
        LinearLayout containerBuyCredits        = (LinearLayout)rowView.findViewById(R.id.containerBuyCredits);
        TextView txtCounterWallas               = (TextView)rowView.findViewById(R.id.txtCounterWallas);
        RelativeLayout containerOffer           = (RelativeLayout)rowView.findViewById(R.id.relativeLayout8);

        txtProduct.setTypeface(TypesLetter.getSansBold(mainActivity));
        txtSubtitle.setTypeface(TypesLetter.getSansRegular(mainActivity));
        txtPrice.setTypeface(TypesLetter.getSansRegular(mainActivity));
        txtCounterWallas.setTypeface(TypesLetter.getSansRegular(mainActivity));
        final Product product = products.get(position);

        if (product != null) {

            txtSubtitle.setVisibility((product.description.equals("")?View.GONE:View.VISIBLE));
            imgProduct.setImageResource(product.image);
            txtProduct.setText("" + product.name);
            txtSubtitle.setText(""+product.description);
            txtPrice.setText("" + product.price);

            if(!product.offer.equals("")) {
                Visibility.visible(containerOffer);
                txtCounterWallas.setText(""+product.offer);
            }
            switch (product.productId)
            {

                case Constant.GOLD_24H:
                    LinearLayout.LayoutParams layout = new LinearLayout.LayoutParams(55 , ViewGroup.LayoutParams.WRAP_CONTENT);
                    imgProduct.setLayoutParams(layout);
                    break;
                case Constant.GOLD_1_MONTH:

                    LinearLayout.LayoutParams layoutPara = new LinearLayout.LayoutParams(70, ViewGroup.LayoutParams.WRAP_CONTENT);
                    imgProduct.setLayoutParams(layoutPara);
                    break;
                case Constant.GOLD_3_MONTHS:

                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(85, ViewGroup.LayoutParams.WRAP_CONTENT);
                    imgProduct.setLayoutParams(layoutParams);
                    break;

                default:
                    break;
            }

        }
        container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buyProduct(product.productId);
            }
        });

        containerBuyCredits.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buyProduct(product.productId);
            }
        });


        return rowView;
    }

    /**
     * Invoca al servicio de Google para comprar el producto
     * @param productId
     */
    public void buyProduct(int productId)
    {
        String status = GlobalData.get(Constant.USER_STATUS,mainActivity);
        String gender = GlobalData.get(Constant.GENERE,mainActivity);
        switch (productId) {
            case Constant.BUY_COINS_1:
                mainActivity.buy10Coins();

                break;
            case Constant.BUY_COINS_2:
                mainActivity.buy60Coins();

                break;
            case Constant.BUY_COINS_3:
                mainActivity.buy130Coins();

                break;

            case Constant.GOLD_24H:
                if(status.equals(Constant.GOLD) || gender.equals(Constant.WOMAN))
                    Common.errorMessage(mainActivity,"","Ya eres usuario GOLD");
                else
                    mainActivity.buyGOLD24h();
                break;
            case Constant.GOLD_1_MONTH:
                if(status.equals(Constant.GOLD) || gender.equals(Constant.WOMAN))
                    Common.errorMessage(mainActivity,"","Ya eres usuario GOLD");
                else
                    mainActivity.buyGOLD1MonthSubscription();
                break;
            case Constant.GOLD_3_MONTHS:
                if(status.equals(Constant.GOLD) || gender.equals(Constant.WOMAN))
                    Common.errorMessage(mainActivity,"","Ya eres usuario GOLD");
                else
                    mainActivity.buyGOLD3MonthSubscription();
                break;

            default:
                break;
        }
    }

}