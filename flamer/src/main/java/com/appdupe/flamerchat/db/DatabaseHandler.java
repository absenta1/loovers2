package com.appdupe.flamerchat.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.util.Log;

import com.appdupe.androidpushnotifications.ChatMessageList;
import com.appdupe.flamer.pojo.ImageDetail;
import com.appdupe.flamer.pojo.MatchesData;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;
import java.util.ArrayList;

public class DatabaseHandler {

	private static boolean mDebugLog = true;
	private static String mDebugTag = "MainActivity";

	void logDebug(String msg) {

		if (mDebugLog) {
			Log.d(mDebugTag, msg);
		}
	}

	void logError(String msg) {

		if (mDebugLog) {
			Log.e(mDebugTag, msg);
		}
	}

	private static String TAG = "DatabaseHandler";

	private static String DATABASE_NAME = "TinderModuldb";

	private static int DATABASE_VERSION = 1;

	// for CHAT DATA
	private static String CHAT_MESSAGE_TABLE = "chat_message_table";

	private static final String FACEBOOK_ID = "facebook_id";
	private static final String CHAT_MESSAGE = "chat_message";
	private static final String MESSAGE_DATE = "message_date";
	private static final String USERNAME_DATA = "username";
	private static final String SENDER_ID = "sender_id";
	private static final String RECEIVER_ID = "receiver_id";
	private static final String FLAG_FOR_SUCCESS = "message_success";

	// for MATCH LIST
	private static String MATCH_LIST_TABLE = "match_list_table";
    private static String FAVORITE_LIST_TABLE = "favorite_list_table";

	private static final String USER_FACEBOOK_ID = "user_facebook_id";
	private static final String SENDER_FACEBOOK_ID = "sender_facebook_id";
	private static final String SENDER_PIC_URL = "sender_pic_url";
	private static final String SENDER_FILE_PATH = "sender_file_path";
	private static final String SENDER_ID_NAME = "sender_id_name";
	private static final String SENDER_ladt = "sender_last_date";
	private static final String SENDER_flag = "flag";
    private static final String SENDER_age = "age";
    private static final String SENDER_MESSAGE = "message";

	// user profile info
	private static final String Current_User_info_TABLE = "user_infor_table";
	private static final String USERNAME = "user_name";
	private static final String USERAGE = "user_age";
	private static final String USERABOUT = "user_ablut";
	private static final String USERFACEBOOKID = "user_facebookid";

	// user ProfileImageinfor

	private static final String Current_User_Image_info_TABLE = "user_infor_table";
	private static final String Url = "Url";
	private static final String imageImafoTavble_columid = "colum_id";
	private static final String sdcardpath = "sdcardpath";
	private static final String imageorder = "imageoder";
    private static final String status = "status";


	//para guardar ultima conversacion
	/*private static String LASTEST_CHAT_USERS = "lastest_chat_user";
	private static final String ID_USER = "id_user";
	private static final String NAME_USER = "name_user";
	private static final String PICTURE = "picture";
	private static final String MESSAGE = "message";
	private static final String DATE = "date";
	private static final String COUNT_MESSAGES = "count_messages";
	private static final String TYPE_MESSAGE = "1";*/

    private SQLiteDatabase db;

	/*
	 * private static String firstPic="firstpic"; private static String
	 * secondPic="secondpic"; private static String thirdPic="thirdpic"; private
	 * static String fourthPic="fourthpic"; private static String
	 * fifthPic="fifthpic";
	 */
	private static String Image_info_USERFACEBOOKID = "user_facebookid";

	SQLiteDatabase tinderdatabase = null;

	DatabaseHelper mDBHelper = null;

	private Context mContext = null;

	public DatabaseHandler(Context aContext) {

		mContext = aContext;
		if (null == mDBHelper) {
			mDBHelper = new DatabaseHelper(mContext);

		}
		// Add sample data for testing
		// addSampleData();
	}



    private class DatabaseHelper extends SQLiteOpenHelper {

		DatabaseHelper(Context aContext) {


            super(aContext, "test", null, DATABASE_VERSION);

		}

		@Override
		public void onCreate(SQLiteDatabase db) {

			// for cate data
			db.execSQL("create table " + CHAT_MESSAGE_TABLE + " (" + SENDER_ID
					+ " VARCHAR," + RECEIVER_ID + " VARCHAR," + CHAT_MESSAGE
					+ " VARCHAR," + USERNAME_DATA + " VARCHAR," + FACEBOOK_ID
					+ " VARCHAR," + FLAG_FOR_SUCCESS + " VARCHAR,"
					+ MESSAGE_DATE + " VARCHAR)");

			// for Mached data
			db.execSQL("create table " + MATCH_LIST_TABLE + " ("
					+ USER_FACEBOOK_ID + " VARCHAR," + SENDER_FACEBOOK_ID
					+ " VARCHAR," + SENDER_PIC_URL + " VARCHAR,"
					+ SENDER_FILE_PATH + " VARCHAR,"+SENDER_MESSAGE + " VARCHAR," + SENDER_ID_NAME
					+ " VARCHAR," + SENDER_ladt + " VARCHAR," + SENDER_flag
					+ " VARCHAR)");
			db.execSQL("create table " + Current_User_Image_info_TABLE + " ("
					+ imageImafoTavble_columid
					+ " integer primary key autoincrement,"
					+ Image_info_USERFACEBOOKID + " VARCHAR," + Url
					+ " VARCHAR," + sdcardpath + " VARCHAR," + imageorder
					+ " integer," + status + " integer," + "id"
                    + " VARCHAR" + ")");//
            db.execSQL("create table " + FAVORITE_LIST_TABLE + " ("
                    + USER_FACEBOOK_ID + " VARCHAR," + SENDER_FACEBOOK_ID
                    + " VARCHAR," + SENDER_PIC_URL + " VARCHAR,"
                    + SENDER_FILE_PATH + " VARCHAR," + SENDER_ID_NAME
                    + " VARCHAR," + SENDER_ladt +" VARCHAR," + SENDER_age + " VARCHAR," + SENDER_flag
                    + " VARCHAR)");

			/*db.execSQL("create table " + LASTEST_CHAT_USERS + " ("
					+ ID_USER + " VARCHAR," +NAME_USER+" VARCHAR,"+ PICTURE
					+ " VARCHAR," + MESSAGE + " VARCHAR,"
					+ DATE + " VARCHAR," + COUNT_MESSAGES
					+ " VARCHAR," + TYPE_MESSAGE + " integer)");*/

		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			onCreate(db);
		}
	}

	// add detail with key

	public long addImagedetal(ArrayList<ImageDetail> imagelist) {

		ImageDetail imageDetail;
		String userfacebookid;
		String imageurl;
		String sdcardPath;
		int imageOrder;
		long count = 0;
		boolean detailsStored = true;

		try {
			tinderdatabase = mDBHelper.getWritableDatabase();
			ContentValues values = new ContentValues();
			if (imagelist != null && imagelist.size() > 0) {

				for (int i = 0; i < imagelist.size(); i++) {
					imageDetail = imagelist.get(i);
					userfacebookid = imageDetail.getUserFacebookid();
					imageurl = imageDetail.getImageUrl();
					sdcardPath = imageDetail.getSdcardpath();
					imageOrder = imageDetail.getImageOrder();

					values.put(Image_info_USERFACEBOOKID, userfacebookid);
					values.put(Url, imageurl);
					values.put(sdcardpath, sdcardPath);
					values.put(imageorder, imageOrder);
                    values.put(status, imageDetail.getStatus());

                	count = tinderdatabase.insertOrThrow(
							Current_User_Image_info_TABLE, null, values);

				}
			} else {
				logError("addImagedetal  imagelist null or empty");
			}

			// values.put("Category_id", catogory_id);

		} catch (SQLiteException lSqlEx) {

			logError("addImagedetal  Could not open addDetail database   SQLiteException "
					+ lSqlEx);

			detailsStored = false;
		} catch (SQLException lEx) {
			Log.e(TAG, "Could not insert addDetail data");
			Log.e(TAG, "Exception:" + lEx.getMessage());
			logError("addImagedetal  Could not insert addDetail data  SQLException "
					+ lEx);
			detailsStored = false;
		} finally {
			if (tinderdatabase != null)
				tinderdatabase.close();
		}

		return count;
	}

	// updateContact()
	// Updating single contact
	public String[] updateOrInsertImagepath(ImageDetail imageDetail) {
		String previousImageUrl = null;
		String imageSdCardpath = null;
		String[] imageUrlAndPath = new java.lang.String[2];
		try {

			String userfacebookid;
			String imageurl;
			String sdcardPath;
			int imageOrder;
			long count;
			Cursor cursor;
			tinderdatabase = mDBHelper.getWritableDatabase();

			ContentValues values = new ContentValues();

			userfacebookid = imageDetail.getUserFacebookid();
			imageurl = imageDetail.getImageUrl();
			sdcardPath = imageDetail.getSdcardpath();
			imageOrder = imageDetail.getImageOrder();

			// updating row
			String selectQuary = "select imageoder,Url,sdcardpath,id from user_infor_table where imageoder="
					+ "'" + imageOrder + "'";

			cursor = tinderdatabase.rawQuery(selectQuary, null);

			if (cursor != null && cursor.moveToFirst() && cursor.getCount() > 0) {

				previousImageUrl = cursor.getString(1);
				imageSdCardpath = cursor.getString(2);
				imageUrlAndPath[0] = previousImageUrl;
				imageUrlAndPath[1] = imageSdCardpath;

				values.put(Image_info_USERFACEBOOKID, userfacebookid);
				values.put(Url, imageurl);
				values.put(sdcardpath, sdcardPath);

				int isUpdated = tinderdatabase.update(
						Current_User_Image_info_TABLE, values, imageorder
								+ " = ?",
						new String[] { String.valueOf(imageOrder) });

			} else {
				previousImageUrl = "imageNotFount";
				imageUrlAndPath[0] = previousImageUrl;

				values.put(Image_info_USERFACEBOOKID, userfacebookid);
				values.put(Url, imageurl);
				values.put(sdcardpath, sdcardPath);
				values.put(imageorder, imageOrder);
				count = tinderdatabase.insertOrThrow(
						Current_User_Image_info_TABLE, null, values);

			}
		} catch (SQLiteException lSqlEx) {
			previousImageUrl = "imageNotFount";
			imageUrlAndPath[0] = previousImageUrl;
			logError("updateOrInsertImagepath  Could not open updateOrInsertImagepath database   SQLiteException "
					+ lSqlEx);

		} catch (SQLException lEx) {
			previousImageUrl = "imageNotFount";
			imageUrlAndPath[0] = previousImageUrl;
			Log.e(TAG, "Could not insert addDetail data");
			Log.e(TAG, "Exception:" + lEx.getMessage());
			logError("updateOrInsertImagepath  Could not insert updateOrInsertImagepath data  SQLException "
					+ lEx);

		} finally {
			if (tinderdatabase != null)
				tinderdatabase.close();
		}

		return imageUrlAndPath;

	}

    public String[] updateOrInsertMyProfileImagepath(ImageDetail imageDetail) {
        String previousImageUrl = null;
        String imageSdCardpath = null;
        String[] imageUrlAndPath = new java.lang.String[2];
        try {

            String userfacebookid;
            String imageurl;
            String status;
            int imageOrder;
            long count;
            Cursor cursor;
            tinderdatabase = mDBHelper.getWritableDatabase();

            ContentValues values = new ContentValues();

            userfacebookid = imageDetail.getUserFacebookid();
            imageurl = imageDetail.getImageUrl();
            imageOrder = imageDetail.getImageOrder();
            // updating row
            String selectQuary = "select * from user_infor_table where imageoder="
                    + "'" + imageOrder + "' and id='"+imageDetail.getId()+"'";

            cursor = tinderdatabase.rawQuery(selectQuary, null);

            if (cursor != null && cursor.moveToFirst() && cursor.getCount() > 0) {


                values.put("status", imageDetail.getStatus());
                values.put("Url", imageDetail.getImageUrl());

                int isUpdated = tinderdatabase.update(
                        Current_User_Image_info_TABLE, values, imageorder
                                + " = ?",
                        new String[] { String.valueOf(imageOrder) });

            }else {
                previousImageUrl = "imageNotFount";
                imageUrlAndPath[0] = previousImageUrl;

                values.put(Image_info_USERFACEBOOKID, userfacebookid);
                values.put(sdcardpath, imageDetail.getSdcardpath());
                values.put(imageorder, imageDetail.getImageOrder());
                values.put("id", imageDetail.getId());
                values.put("status", imageDetail.getStatus());
                values.put("Url", imageDetail.getImageUrl());

                count = tinderdatabase.insertOrThrow(
                        Current_User_Image_info_TABLE, null, values);

            }
        } catch (SQLiteException lSqlEx) {
            previousImageUrl = "imageNotFount";
            imageUrlAndPath[0] = previousImageUrl;
            logError("updateOrInsertImagepath  Could not open updateOrInsertImagepath database   SQLiteException "
                    + lSqlEx);

        } catch (SQLException lEx) {
            previousImageUrl = "imageNotFount";
            imageUrlAndPath[0] = previousImageUrl;
            Log.e(TAG, "Could not insert addDetail data");
            Log.e(TAG, "Exception:" + lEx.getMessage());
            logError("updateOrInsertImagepath  Could not insert updateOrInsertImagepath data  SQLException "
                    + lEx);

        } finally {
            if (tinderdatabase != null)
                tinderdatabase.close();
        }

        return imageUrlAndPath;

    }

	public ArrayList<ImageDetail> getImageDetail() {

		ArrayList<ImageDetail> infoList = null;
		Cursor cursor = null;
		try {
			// tinderdatabase = mDBHelper.getWritableDatabase();
			tinderdatabase = mDBHelper.getReadableDatabase();
			String[] columns = new String[] { imageorder, sdcardpath, Url,
					imageImafoTavble_columid };
			// String whereClause = "Category_id =?";
			// String[] whereArgs = new String[] {""+categoryid};
			cursor = tinderdatabase.query(Current_User_Image_info_TABLE,
					columns, null, null, null, null, null);

			if (cursor != null && cursor.moveToFirst() && cursor.getCount() > 0) {

				infoList = new ArrayList<ImageDetail>();
				do {
					ImageDetail imageDetail = new ImageDetail();
					int imageOrder = cursor.getInt(0);
					String sdcardPath = cursor.getString(1);
					String Url = cursor.getString(2);
					int imageImafoTavble_columid = cursor.getInt(3);

					imageDetail.setImageOrder(imageOrder);
					imageDetail.setSdcardpath(sdcardPath);
					imageDetail.setImageUrl(Url);
					imageDetail.setCoulumid(imageImafoTavble_columid);

					infoList.add(imageDetail);

				} while (cursor.moveToNext());
			} else {
				// System.out.println("getCategoryName Cursor is null or empty");
				logError("getImageDetail  Cursor is null or empty");
			}
		} catch (SQLiteException lSqlEx) {
			Log.e(TAG, "Could not open getCategoryName database");
			Log.e(TAG, "Exception:" + lSqlEx.getMessage());
			logError("getImageDetail  Exception:" + lSqlEx.getMessage());

			infoList = null;

		} catch (SQLException lEx) {
			Log.e(TAG, "Could not getCategoryName data");
			Log.e(TAG, "Exception:" + lEx.getMessage());
			logError("getImageDetail  Exception:" + lEx.getMessage());
			infoList = null;

		} finally {
			if (tinderdatabase != null)
				tinderdatabase.close();
			if (cursor != null)
				cursor.close();
		}
		return infoList;
	}

	public ArrayList<ImageDetail> getImageDetailByImageOrder(
			String[] imagrderarray) {

		ArrayList<ImageDetail> infoList = null;
		infoList = new ArrayList<ImageDetail>();
		String imageOrderFormUser;
		Cursor cursor = null;
		try {
			// tinderdatabase = mDBHelper.getWritableDatabase();
			tinderdatabase = mDBHelper.getReadableDatabase();
			String[] columns = new String[] { imageorder, sdcardpath, Url,
					imageImafoTavble_columid };
            String whereClause = "imageoder =?";
			for (int i = 0; i < imagrderarray.length; i++) {
				imageOrderFormUser = imagrderarray[i];

				String[] whereArgs = { imageOrderFormUser };

				cursor = tinderdatabase.query(Current_User_Image_info_TABLE,
						columns, whereClause, whereArgs, null, null, null);

				if (cursor != null && cursor.moveToFirst()
						&& cursor.getCount() > 0) {

					do {
						ImageDetail imageDetail = new ImageDetail();
						int imageOrder = cursor.getInt(0);
						String sdcardPath = cursor.getString(1);
						String Url = cursor.getString(2);
						int imageImafoTavble_columid = cursor.getInt(3);

						imageDetail.setImageOrder(imageOrder);
						imageDetail.setSdcardpath(sdcardPath);
						imageDetail.setImageUrl(Url);
						imageDetail.setCoulumid(imageImafoTavble_columid);

						infoList.add(imageDetail);

					} while (cursor.moveToNext());
				} else {
					// System.out.println("getCategoryName Cursor is null or empty");
					logError("getImageDetailByImageOrder  Cursor is null or empty");
				}

			}
		} catch (SQLiteException lSqlEx) {
			Log.e(TAG, "Could not open getCategoryName database");
			Log.e(TAG, "Exception:" + lSqlEx.getMessage());
			logError("getImageDetailByImageOrder  Exception:"
					+ lSqlEx.getMessage());

			infoList = null;

		} catch (SQLException lEx) {
			Log.e(TAG, "Could not getCategoryName data");
			Log.e(TAG, "Exception:" + lEx.getMessage());
			logError("getImageDetailByImageOrder  Exception:"
					+ lEx.getMessage());
			infoList = null;

		} finally {
			if (tinderdatabase != null)
				tinderdatabase.close();
			if (cursor != null)
				cursor.close();
		}
		return infoList;
	}

	public boolean deleteImagedetail(String[] imageOrderArray) {

		boolean favoritedatalist = false;
		String imageorder;
		// DealDetails d=null;
		Cursor cursor = null;
		try {
			tinderdatabase = mDBHelper.getReadableDatabase();
			// String[] columns = new String[]{"KEY_IMAGE"};
			String whereClause = "imageoder =?";
			for (int i = 0; i < imageOrderArray.length; i++) {
				imageorder = imageOrderArray[i];
				String[] whereArgs = { imageorder };
				tinderdatabase.delete(Current_User_Image_info_TABLE,
						whereClause, whereArgs);
				favoritedatalist = true;
				if (favoritedatalist) {

				}

			}
		} catch (SQLiteException lSqlEx) {
			Log.e(TAG, "deleteImagedetail Could not open database");
			Log.e(TAG, "deleteImagedetail Exception:" + lSqlEx.getMessage());
			favoritedatalist = false;

		} catch (SQLException lEx) {
			Log.e(TAG, "deleteImagedetail Could not fetch trip data");
			Log.e(TAG, "deleteImagedetail Exception:" + lEx.getMessage());
			favoritedatalist = false;

		} finally {
			if (tinderdatabase != null)
				tinderdatabase.close();
			if (cursor != null)
				cursor.close();
		}
		return favoritedatalist;

	}

	public boolean deleteUserData() {

		boolean favoritedatalist = false;

		// DealDetails d=null;
		Cursor cursor = null;
		try {
			tinderdatabase = mDBHelper.getReadableDatabase();
			// String[] columns = new String[]{"KEY_IMAGE"};
			/* String whereClause = "USERID =?"; */
			/* String[] whereArgs = new String[] {userid}; */
			tinderdatabase.delete(Current_User_Image_info_TABLE, null, null);
			favoritedatalist = true;
			if (favoritedatalist) {

			}

		} catch (SQLiteException lSqlEx) {
			Log.e(TAG, "deleteUserData Could not open database");
			Log.e(TAG, "deleteUserData Exception:" + lSqlEx.getMessage());
			favoritedatalist = false;

		} catch (SQLException lEx) {
			Log.e(TAG, "deleteUserData Could not fetch trip data");
			Log.e(TAG, "deleteUserData Exception:" + lEx.getMessage());
			favoritedatalist = false;

		} finally {
			if (tinderdatabase != null)
				tinderdatabase.close();
			if (cursor != null)
				cursor.close();
		}
		return favoritedatalist;

	}

	public boolean deleteMatchedlist() {

		boolean favoritedatalist = false;

		// DealDetails d=null;
		Cursor cursor = null;
		try {
			tinderdatabase = mDBHelper.getReadableDatabase();
			// String[] columns = new String[]{"KEY_IMAGE"};
			/* String whereClause = "USERID =?"; */
			/* String[] whereArgs = new String[] {userid}; */
			tinderdatabase.delete(MATCH_LIST_TABLE, null, null);
			favoritedatalist = true;
			if (favoritedatalist) {

			}

		} catch (SQLiteException lSqlEx) {
			Log.e(TAG, "deleteUserData Could not open database");
			Log.e(TAG, "deleteUserData Exception:" + lSqlEx.getMessage());
			favoritedatalist = false;

		} catch (SQLException lEx) {
			Log.e(TAG, "deleteUserData Could not fetch trip data");
			Log.e(TAG, "deleteUserData Exception:" + lEx.getMessage());
			favoritedatalist = false;

		} finally {
			if (tinderdatabase != null)
				tinderdatabase.close();
			if (cursor != null)
				cursor.close();
		}
		return favoritedatalist;

	}

	public boolean insertMessageData(ChatMessageList objMessageData) {
		// TODO Auto-generated method stub
		boolean detailsStored = true;

		tinderdatabase = mDBHelper.getWritableDatabase();
		try {

			// Log.i(TAG,
			// "insertMessageData getStrUniqueId....."+objMessageData.getStrUniqueId());
			String str = objMessageData.strSenderFacebookId;
			Log.i(TAG, "insertMessageData str...." + str);
			ContentValues values = new ContentValues();
			values.put(DatabaseHandler.FACEBOOK_ID, str);
			values.put(DatabaseHandler.CHAT_MESSAGE,
					objMessageData.strMessage);
			values.put(DatabaseHandler.MESSAGE_DATE,
					objMessageData.strDateTime);
			values.put(DatabaseHandler.USERNAME_DATA,
					objMessageData.strSendername);
			Log.i(TAG,
					"insertMessageData getStrSenderId....."
							+ objMessageData.strSenderId);
			values.put(DatabaseHandler.SENDER_ID,
					objMessageData.strSenderId);
			values.put(DatabaseHandler.RECEIVER_ID,
					objMessageData.strReceiverId);
			values.put(DatabaseHandler.FLAG_FOR_SUCCESS,
					objMessageData.strFlagForMessageSuccess);

			long count = tinderdatabase.insertOrThrow(CHAT_MESSAGE_TABLE, null,
					values);
			System.out.println("Inserted trip details row id:......" + count);
		} catch (Exception e) {
			e.printStackTrace();
			detailsStored = false;
		} finally {
			if (tinderdatabase != null)
				tinderdatabase.close();
		}

		return detailsStored;
	}
	/*public boolean insertChats(Chat chat) {
		// TODO Auto-generated method stub
		boolean detailsStored = true;

		tinderdatabase = mDBHelper.getWritableDatabase();
		try {
			ContentValues values = new ContentValues();
			values.put(DatabaseHandler.ID_USER, chat.id_user);
			values.put(DatabaseHandler.NAME_USER, chat.name);
			values.put(DatabaseHandler.PICTURE,chat.picture);
			values.put(DatabaseHandler.MESSAGE, chat.message);
			values.put(DatabaseHandler.DATE,chat.date);
			values.put(DatabaseHandler.COUNT_MESSAGES,chat.count_messages);
			values.put(DatabaseHandler.TYPE_MESSAGE,chat.typeMessage);


			long count = tinderdatabase.insertOrThrow(LASTEST_CHAT_USERS, null,
					values);
			System.out.println("Inserted trip details row id:......" + count);
		} catch (Exception e) {
			e.printStackTrace();
			detailsStored = false;
		} finally {
			if (tinderdatabase != null)
				tinderdatabase.close();
		}

		return detailsStored;
	}
	public void updateChats(Chat chat)
	{
		tinderdatabase = mDBHelper.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(DatabaseHandler.ID_USER, chat.id_user);
		values.put(DatabaseHandler.NAME_USER, chat.name);
		values.put(DatabaseHandler.PICTURE,chat.picture);
		values.put(DatabaseHandler.MESSAGE, chat.message);
		values.put(DatabaseHandler.DATE,chat.date);
		values.put(DatabaseHandler.COUNT_MESSAGES,chat.count_messages);
		values.put(DatabaseHandler.TYPE_MESSAGE,chat.typeMessage);
		tinderdatabase.update(LASTEST_CHAT_USERS, values, "id_user="+chat.id_user, null);


	}
	public List<String> getIdChats()
	{
		tinderdatabase = mDBHelper.getReadableDatabase();
		List<String> ids = new ArrayList<>();
		Cursor  cursor = tinderdatabase.rawQuery("select id_user from "+LASTEST_CHAT_USERS,null);
		if (cursor .moveToFirst()) {

			while (cursor.isAfterLast() == false) {
				String id = cursor.getString(cursor
						.getColumnIndex(ID_USER));

				ids.add(id);
				cursor.moveToNext();
			}
		}
		return ids;

	}
	public List<Chat> getChats()
	{
		tinderdatabase = mDBHelper.getReadableDatabase();
		List<Chat> chats = new ArrayList<>();
		Cursor  cursor = tinderdatabase.rawQuery("select * from "+LASTEST_CHAT_USERS+ " order by date DESC",null);
		if (cursor .moveToFirst()) {

			while (cursor.isAfterLast() == false) {

				String id = cursor.getString(cursor.getColumnIndex(ID_USER));
				String name = cursor.getString(cursor.getColumnIndex(NAME_USER));
				String picture = cursor.getString(cursor.getColumnIndex(PICTURE));
				String message = cursor.getString(cursor.getColumnIndex(MESSAGE));
				String date = cursor.getString(cursor.getColumnIndex(DATE));
				String count_messages = cursor.getString(cursor.getColumnIndex(COUNT_MESSAGES));
				int status = cursor.getInt(cursor.getColumnIndex(TYPE_MESSAGE));
				chats.add(new Chat(id,name,picture,message,date,count_messages,status));
				cursor.moveToNext();
			}
		}
		return chats;

	}
	public Chat getChatsUser(String idUser)
	{
		tinderdatabase = mDBHelper.getReadableDatabase();
		Chat chat = new Chat();
		Cursor  cursor = tinderdatabase.rawQuery("select * from "+LASTEST_CHAT_USERS+ " where id_user="+idUser,null);
		if (cursor .moveToFirst()) {

			while (cursor.isAfterLast() == false) {

				String id = cursor.getString(cursor.getColumnIndex(ID_USER));
				String name = cursor.getString(cursor.getColumnIndex(NAME_USER));
				String picture = cursor.getString(cursor.getColumnIndex(PICTURE));
				String message = cursor.getString(cursor.getColumnIndex(MESSAGE));
				String date = cursor.getString(cursor.getColumnIndex(DATE));
				String count_messages = cursor.getString(cursor.getColumnIndex(COUNT_MESSAGES));
				int status = cursor.getInt(cursor.getColumnIndex(TYPE_MESSAGE));
				chat.id_user = id;
				chat.name = name;
				chat.picture = picture;
				chat.message = message;
				chat.date = date;
				chat.count_messages = count_messages;
				chat.typeMessage = status;
				//chats.add(new Chat(id,name,picture,message,date,count_messages,status));
				cursor.moveToNext();
			}
		}
		return chat;

	}
	public boolean deleteChatUser(String userId)
	{
		boolean status = true;
		try {
			tinderdatabase = mDBHelper.getReadableDatabase();
			String whereClause = "id_user =?";
			String[] whereArgs = {userId};
			tinderdatabase.delete(LASTEST_CHAT_USERS, whereClause, whereArgs);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return status;
	}
	public boolean deleteChats()
	{
		boolean status = true;
		try {
			tinderdatabase = mDBHelper.getReadableDatabase();

			tinderdatabase.delete(LASTEST_CHAT_USERS, null, null);
			tinderdatabase.delete(CHAT_MESSAGE_TABLE, null, null);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return status;
	}*/




    public void deleteUserFavourite(DatabaseHandler DBHelper, String facebookID ) {


        Cursor cursor = null;
        try {
            // tinderdatabase = mDBHelper.getWritableDatabase();
            tinderdatabase = mDBHelper.getReadableDatabase();
            String[] columns = new String[] { USER_FACEBOOK_ID,
                    SENDER_FACEBOOK_ID, SENDER_PIC_URL, SENDER_FILE_PATH,
                    SENDER_ID_NAME };

            String whereClause = "sender_facebook_id =?";

            String[] whereArgs = { facebookID };
            tinderdatabase.delete("favorite_list_table",whereClause, whereArgs);

        } catch (SQLiteException lSqlEx) {
            Log.e(TAG, "Could not open getCategoryName database");
            Log.e(TAG, "Exception:" + lSqlEx.getMessage());
            logError("removeUserFavourite  Exception:" + lSqlEx.getMessage());



        } catch (SQLException lEx) {
            Log.e(TAG, "Could not getCategoryName data");
            Log.e(TAG, "Exception:" + lEx.getMessage());
            logError("removeUserFavourite  Exception:" + lEx.getMessage());
         //   infoList = null;

        } finally {
            if (tinderdatabase != null)
                tinderdatabase.close();
            if (cursor != null)
                cursor.close();
        }

    }


    // MATCH STUFF
	public boolean insertMatchList(
			ArrayList<MatchesData> matchlist,
			String usrFacebookId) {
		// TODO Auto-generated method stub
		boolean detailsStored = true;

		tinderdatabase = mDBHelper.getWritableDatabase();
		try {

			for (int i = 0, len =  matchlist.size(); i < len; ++i) {
                MatchesData objMatchData = matchlist.get(i);
				String fbId = objMatchData.userId;
				String sqlSelect = "SELECT * FROM " + MATCH_LIST_TABLE
						+ " WHERE sender_facebook_id=" + "'" + fbId + "'";

				Cursor mCursor1 = tinderdatabase.rawQuery("SELECT * FROM "
						+ MATCH_LIST_TABLE + " WHERE    sender_facebook_id=? ",
						new String[] { fbId });

                MatchesData matcheddataForListview = matchlist
						.get(i);

				ContentValues values = new ContentValues();

				values.put(DatabaseHandler.USER_FACEBOOK_ID, usrFacebookId);
				values.put(DatabaseHandler.SENDER_FACEBOOK_ID,
						matcheddataForListview.userId);
				values.put(DatabaseHandler.SENDER_PIC_URL,
						matcheddataForListview.img);
				//values.put(DatabaseHandler.SENDER_FILE_PATH,
				//		matcheddataForListview.getFilePath());
				values.put(DatabaseHandler.SENDER_ID_NAME,
						matcheddataForListview.firstName);
				values.put(DatabaseHandler.SENDER_ladt,
						matcheddataForListview.ladt);
                values.put(DatabaseHandler.SENDER_MESSAGE,
                        matcheddataForListview.message);
				//values.put(DatabaseHandler.SENDER_flag,
				//		matcheddataForListview.get());

				if (mCursor1 != null && mCursor1.getCount() > 0) {

					String whereClause1 = DatabaseHandler.SENDER_FACEBOOK_ID
							+ " =?";
					String[] whereArgs1 = new String[] { "" + fbId };

					long count = tinderdatabase.update(MATCH_LIST_TABLE,
							values, whereClause1, whereArgs1);

					if (count > 0) {

					} else {
						detailsStored = false;
						//break;
					}

					logError("insertMatchList  count " + count);

				} else {

					long count = tinderdatabase.insertOrThrow(MATCH_LIST_TABLE,
							null, values);

					if (count > 0) {

					} else {
						detailsStored = false;
						break;
					}
					System.out.println("Inserted trip details row id:......"
							+ count);
					logError("insertMatchList  count " + count);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			detailsStored = false;
			logError("insertMatchList  Exception " + e);
		} finally {
			if (tinderdatabase != null)
				tinderdatabase.close();
		}

		return detailsStored;
	}



	public ArrayList<ChatMessageList> getChatMessages(String strFriendFBId) {
		ArrayList<ChatMessageList> listChatMessage = new ArrayList<ChatMessageList>();
		Cursor cursor = null;
		try {


			tinderdatabase = mDBHelper.getReadableDatabase();


			final String MY_QUERY = "SELECT * FROM "
					+ " chat_message_table cmt  where " + "  cmt.sender_id="
					+ strFriendFBId + " or cmt.receiver_id=" + strFriendFBId;


			cursor = tinderdatabase.rawQuery(MY_QUERY, null);

			if (cursor != null && cursor.moveToFirst() && cursor.getCount() > 0) {

				// d= new DealDetails();
				do {
					ChatMessageList ObjChatMessage = new ChatMessageList();
					ObjChatMessage
							.strSenderFacebookId = cursor.getString(cursor
									.getColumnIndex(DatabaseHandler.SENDER_ID));
					ObjChatMessage.strMessage = cursor.getString(cursor
							.getColumnIndex(DatabaseHandler.CHAT_MESSAGE));
					ObjChatMessage.strDateTime = cursor.getString(cursor
							.getColumnIndex(DatabaseHandler.MESSAGE_DATE));
					ObjChatMessage.strSendername = cursor.getString(cursor
							.getColumnIndex(DatabaseHandler.USERNAME_DATA));
					// ObjChatMessage.setStrUniqueId(cursor.getString(cursor.getColumnIndex(DatabaseHandler.UNIQUE_USER_ID)));

					listChatMessage.add(ObjChatMessage);
					System.out.println("Getting favorote heritagspot detail");

				} while (cursor.moveToNext());
			} else {
				System.out.println("Cursor is null or empty");
			}
		} catch (SQLiteException lSqlEx) {
			Log.e(TAG, "Could not open database");
			Log.e(TAG, "Exception:" + lSqlEx.getMessage());

		} catch (SQLException lEx) {
			Log.e(TAG, "Could not fetch trip data");
			Log.e(TAG, "Exception:" + lEx.getMessage());
		} finally {
			if (tinderdatabase != null)
				tinderdatabase.close();
			if (cursor != null)
				cursor.close();
		}
		return listChatMessage;
	}

	public ArrayList<ChatMessageList> getChatMessages(String strFriendFBId,
			int start, int limit) {
		ArrayList<ChatMessageList> listChatMessage = new ArrayList<ChatMessageList>();
		Cursor cursor = null;
		try {

			tinderdatabase = mDBHelper.getReadableDatabase();

			final String MY_QUERY = "SELECT * FROM "
					+ " chat_message_table cmt where cmt.sender_id="
					+ strFriendFBId + " or cmt.receiver_id=" + strFriendFBId
					+ " order by " + MESSAGE_DATE + " asc limit " + start
					+ "," + limit;

			cursor = tinderdatabase.rawQuery(MY_QUERY, null);


			if (cursor != null && cursor.moveToFirst() && cursor.getCount() > 0) {

				// d= new DealDetails();
				do {
					ChatMessageList ObjChatMessage = new ChatMessageList();
					Log.i(TAG,
							"getChatMessages senderid FACEBOOK_ID......"
									+ cursor.getString(cursor
											.getColumnIndex(DatabaseHandler.FACEBOOK_ID)));
					ObjChatMessage
							.strSenderFacebookId = cursor.getString(cursor
									.getColumnIndex(DatabaseHandler.SENDER_ID));
					ObjChatMessage.strMessage  = cursor.getString(cursor
							.getColumnIndex(DatabaseHandler.CHAT_MESSAGE));
					ObjChatMessage.strDateTime = cursor.getString(cursor
							.getColumnIndex(DatabaseHandler.MESSAGE_DATE));
					ObjChatMessage.strSendername = cursor.getString(cursor
							.getColumnIndex(DatabaseHandler.USERNAME_DATA));
					// ObjChatMessage.setStrUniqueId(cursor.getString(cursor.getColumnIndex(DatabaseHandler.UNIQUE_USER_ID)));

					listChatMessage.add(ObjChatMessage);
					System.out.println("Getting favorote heritagspot detail");

				} while (cursor.moveToNext());
			} else {
				System.out.println("Cursor is null or empty");
			}
		} catch (SQLiteException lSqlEx) {
			Log.e(TAG, "Could not open database");
			Log.e(TAG, "Exception:" + lSqlEx.getMessage());

		} catch (SQLException lEx) {
			Log.e(TAG, "Could not fetch trip data");
			Log.e(TAG, "Exception:" + lEx.getMessage());
		} finally {
			if (tinderdatabase != null)
				tinderdatabase.close();
			if (cursor != null)
				cursor.close();
		}
		return listChatMessage;
	}
    public void exportDatabse(String databaseName) {
        try {
            File sd = Environment.getExternalStorageDirectory();
            File data = Environment.getDataDirectory();

            if (sd.canWrite()) {
                String currentDBPath = "//data//"+mContext.getPackageName()+"//databases//"+databaseName+"";
                String backupDBPath = "backupname.db";
                File currentDB = new File(data, currentDBPath);
                File backupDB = new File(sd, backupDBPath);

                if (currentDB.exists()) {
                    FileChannel src = new FileInputStream(currentDB).getChannel();
                    FileChannel dst = new FileOutputStream(backupDB).getChannel();
                    dst.transferFrom(src, 0, src.size());
                    src.close();
                    dst.close();
                }
            }
        } catch (Exception e) {

        }
    }

}
