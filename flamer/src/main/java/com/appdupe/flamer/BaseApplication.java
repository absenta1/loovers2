package com.appdupe.flamer;

import android.app.Application;

public abstract class BaseApplication extends Application {


	protected static BaseApplication INSTANCE;
	private Boolean applicationInForeground;

	@Override
	public void onCreate() {
		INSTANCE = this;
		super.onCreate();
	}
	
	public static BaseApplication getCurrent() {
		return INSTANCE;
	}

	public void setCurrentApplication(BaseApplication application){
		INSTANCE = application;
	}

    public  Boolean isActivityVisible() {
        return applicationInForeground;
    }
    public void applicationStarted() {
		applicationInForeground = true;
	}
	
	public void applicationStopped() {
		applicationInForeground = false;
	}

}