package com.appdupe.androidpushnotifications;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.Html;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.slidingmenu.MainActivity;
import com.android.slidingmenu.activities.ViewContent;
import com.android.slidingmenu.adapter.AdapterOptionsChat;
import com.android.slidingmenu.adapter.AdapterPrivatePhoto;
import com.android.slidingmenu.adapter.FriendsAdapter;
import com.android.slidingmenu.adapter.GeneralAdapter;
import com.android.slidingmenu.adapter.ReportAdapter;
import com.android.slidingmenu.fragments.DetailProfileFragment;
import com.android.slidingmenu.messages.MessageFragment;
import com.android.slidingmenu.messages.RequestChatsFragment;
import com.android.slidingmenu.objects.chatUser;
import com.android.slidingmenu.reports.Report;
import com.android.slidingmenu.store.DialogStore;
import com.android.slidingmenu.util.AppAnimation;
import com.android.slidingmenu.util.Common;
import com.android.slidingmenu.util.GlobalData;
import com.android.slidingmenu.util.KeyBoard;
import com.android.slidingmenu.util.TypesLetter;
import com.android.slidingmenu.util.Visibility;
import com.android.slidingmenu.webservices.Services;
import com.appdupe.flamer.pojo.MatchesData;
import com.appdupe.flamer.pojo.PictureData;
import com.appdupe.flamer.utility.CircleTransform;
import com.appdupe.flamer.utility.Constant;
import com.google.gson.Gson;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.loovers.app.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import github.ankushsachdeva.emojicon.EmojiconEditText;
import github.ankushsachdeva.emojicon.EmojiconGridView;
import github.ankushsachdeva.emojicon.EmojiconTextView;
import github.ankushsachdeva.emojicon.EmojiconsPopup;
import github.ankushsachdeva.emojicon.emoji.Emojicon;
import okhttp3.Call;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Clase encargada de gestionar los mensajes entre 2 usuarios
 * @author Juan Martín Bernal
 * @date 16/01/2016
 */
public class ChatActivity extends DialogFragment implements OnClickListener {

    private String mImagePath;
    private ListView chatList;
    private EmojiconEditText chatEditText;
    private ImageView sendChatMessageButton, imgMoreOptionsChat;
    private RelativeLayout containerImgMoreOptionsChat;
    private BroadcastReceiver receiver;
    private String strFriendFbId;
    private ChatMessageList objMessageData;
    private AwesomeAdapter messageAdapter;
    private ArrayList<ChatMessageList> listChatData = new ArrayList<ChatMessageList>();
    private String myUserId;
    public IntentFilter filter;
    private ImageView senderimage, imgBack, emoji_btn, imgUser;
    private TextView senderName, txtNumPrivatePictures, txtDescPrivatePictures, btnPassGoldRequest, txtProvince,textView26,textView27;
    private String currentImageurl;
    private Button btnFeedBack, btnClosePrivatePhotos;
    public ArrayList<String> messagesTmp = new ArrayList<>();
    public MainActivity mainActivity;
    public MatchesData matchWalla;
    public View viewMain;
    public LinearLayout bottom_write_bar,
            containerOptionsBackChat,
            containerOptionsChat, containerOptionsFeedBack,
            containerRequestUser, container_private_gallery_photos, containerPassGoldChat;
    public GridView gridPrivatePhotos;
    public RelativeLayout containerMoreVertical;
    public ProgressWheel progressBarChat;
    public int fromOpenChat = 0;
    public static final int NUM_MAX_MESSAGES = 5;
    public ProgressBar progressBarImageUser;
    public ListView listOptionChat;
    public RelativeLayout containerMainChat, containerPhotoPreviewChat;
    public AdapterPrivatePhoto adapterPrivatePhoto;
    public boolean unLockPictures = false;
    public DialogFragment dialogDetail;
    public int numPrivatePhotos = 0;
    public Animation animationBtnSend;
    public int numChatRequest = -1;
    public TextView txtNumRequest, txtTimeWaitRequest, txtTitleRequest;
    public int positionRequestChat = -1;
    public RequestChatsFragment requestChatsFragment;
    public CounterClass timer;
    public GeneralAdapter adapter;
    public FriendsAdapter friendsAdapter;
    public RequestChatsFragment.FriendsAdapter friendsAdapterRequest;
    public int fromMessages = -1;

    public ChatActivity() {

    }

    public ChatActivity(MainActivity mainActivity, MatchesData matchWalla) {
        this.mainActivity = mainActivity;
        this.matchWalla = matchWalla;
    }

    public ChatActivity(MainActivity mainActivity, MatchesData matchWalla, int fromOpenChat) {
        this.mainActivity = mainActivity;
        this.matchWalla = matchWalla;
        this.fromOpenChat = fromOpenChat;
    }

    public ChatActivity(MainActivity mainActivity, MatchesData matchWalla, int fromOpenChat, FriendsAdapter friendsAdapter) {
        this.mainActivity = mainActivity;
        this.matchWalla = matchWalla;
        this.fromOpenChat = fromOpenChat;
        this.friendsAdapter = friendsAdapter;
    }

    public ChatActivity(MainActivity mainActivity, MatchesData matchWalla, int fromOpenChat, FriendsAdapter friendsAdapter, int fromMessages) {
        this.mainActivity = mainActivity;
        this.matchWalla = matchWalla;
        this.fromOpenChat = fromOpenChat;
        this.friendsAdapter = friendsAdapter;
        this.fromMessages = fromMessages;
    }

    public ChatActivity(MainActivity mainActivity, MatchesData matchWalla, int fromOpenChat, GeneralAdapter adapter) {
        this.mainActivity = mainActivity;
        this.matchWalla = matchWalla;
        this.fromOpenChat = fromOpenChat;
        this.adapter = adapter;
    }

    public ChatActivity(MainActivity mainActivity, MatchesData matchWalla, int fromOpenChat, DialogFragment dialogDetail) {
        this.mainActivity = mainActivity;
        this.matchWalla = matchWalla;
        this.fromOpenChat = fromOpenChat;
        this.dialogDetail = dialogDetail;
    }

    public ChatActivity(MainActivity mainActivity, MatchesData matchWalla, int fromOpenChat, RequestChatsFragment requestChatsFragment, int positionRequestChat) {
        this.mainActivity = mainActivity;
        this.matchWalla = matchWalla;
        this.fromOpenChat = fromOpenChat;
        this.requestChatsFragment = requestChatsFragment;
        this.positionRequestChat = positionRequestChat;
    }

    public ChatActivity(MainActivity mainActivity, MatchesData matchWalla, int fromOpenChat, RequestChatsFragment requestChatsFragment, int positionRequestChat, RequestChatsFragment.FriendsAdapter friendsAdapterRequest) {
        this.mainActivity = mainActivity;
        this.matchWalla = matchWalla;
        this.fromOpenChat = fromOpenChat;
        this.requestChatsFragment = requestChatsFragment;
        this.positionRequestChat = positionRequestChat;
        this.friendsAdapterRequest = friendsAdapterRequest;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        viewMain = inflater.inflate(R.layout.chat_screen, container, false);

        if (mainActivity == null)
            mainActivity = (MainActivity) getActivity();

        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        animationBtnSend = AnimationUtils.loadAnimation(mainActivity, R.anim.scale_button);

        return viewMain;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //inicializo los componentes
        initComponent();

        strFriendFbId = matchWalla.userId;

        if (matchWalla.avatarUrl != null && matchWalla.avatarUrl.length() > 0) {
            mImagePath = matchWalla.avatarUrl;
            Picasso.with(mainActivity).load(mImagePath).error(R.drawable.no_foto).transform(new CircleTransform()).into(senderimage);
        }

        senderName.setText("" + matchWalla.firstName);


        myUserId = GlobalData.get(Constant.MY_USER_ID, mainActivity);
        currentImageurl = GlobalData.get(Constant.AVATAR_USER, mainActivity);

        filter = new IntentFilter();
        filter.addAction("com.embed.anddroidpushntificationdemo11.push");

        receiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {

                try {
                    Bundle bucket = intent.getExtras();

                    final String strMessage = (bucket.containsKey("MESSAGE_FOR_PUSH")) ? bucket.getString("MESSAGE_FOR_PUSH") : "";
                    String strSenderName = (bucket.containsKey("MESSAGE_FOR_PUSH_SENDERNAME")) ? bucket.getString("MESSAGE_FOR_PUSH_SENDERNAME") : "";
                    final String strDateTime = (bucket.containsKey("MESSAGE_FOR_PUSH_DATETIME")) ? bucket.getString("MESSAGE_FOR_PUSH_DATETIME") : "";
                    String strFacebookId = (bucket.containsKey("MESSAGE_FOR_PUSH_FACEBOOKID")) ? bucket.getString("MESSAGE_FOR_PUSH_FACEBOOKID") : "";
                    String strSenderId = (bucket.containsKey("MESSAGE_FOR_PUSH_SENDERID")) ? bucket.getString("MESSAGE_FOR_PUSH_SENDERID") : "";

                    //tipo de mensaje
                    String messageType = (bucket.containsKey("MESSAGE_FOR_PUSH_MESSAGE_TYPE")) ? bucket.getString("MESSAGE_FOR_PUSH_MESSAGE_TYPE") : "";
                    String imageUrl = (bucket.containsKey("MESSAGE_FOR_PUSH_MESSAGE_IMAGE")) ? bucket.getString("MESSAGE_FOR_PUSH_MESSAGE_IMAGE") : "";
                    String picId = (bucket.containsKey("MESSAGE_FOR_PUSH_MESSAGE_PIC_ID")) ? bucket.getString("MESSAGE_FOR_PUSH_MESSAGE_PIC_ID") : "";
                    String privatePic = (bucket.containsKey("MESSAGE_FOR_PUSH_MESSAGE_PRIVATE_PIC")) ? bucket.getString("MESSAGE_FOR_PUSH_MESSAGE_PRIVATE_PIC") : "";
                    String chatStatusId = (bucket.containsKey("MESSAGE_FOR_PUSH_MESSAGE_CHAT_STATUS_ID")) ? bucket.getString("MESSAGE_FOR_PUSH_MESSAGE_CHAT_STATUS_ID") : "";
                    boolean requestToMe = (bucket.containsKey("MESSAGE_REQUEST_TO_ME")) ? bucket.getBoolean("MESSAGE_REQUEST_TO_ME") : false;

                    if (privatePic.equals(Constant.ACCEPT_PHOTO))
                        unLockPictures = true;

                    if (requestToMe) {
                        if (matchWalla != null)
                            matchWalla.requestToMe = requestToMe;
                    }


                    ChatMessageList objMessageData = new ChatMessageList();
                    objMessageData.strMessage = strMessage;
                    objMessageData.strDateTime = strDateTime;
                    objMessageData.strSenderFacebookId = strFacebookId;
                    objMessageData.strSendername = strSenderName;
                    objMessageData.strFlagForMessageSuccess = "1";
                    objMessageData.strReceiverId = myUserId;
                    objMessageData.strSenderId = strSenderId;
                    objMessageData.messageTypeId = messageType;
                    objMessageData.imageUrl = imageUrl;
                    objMessageData.picId = picId;
                    objMessageData.privatePicStatusId = privatePic;


                    if (chatStatusId.length() > 0) {
                        int statusId = Integer.parseInt(chatStatusId);
                        //Toast.makeText(mainActivity, "chat status id: " + chatStatusId, Toast.LENGTH_SHORT).show();
                        objMessageData.chatStatusId = statusId;
                        if (matchWalla != null)
                            matchWalla.chatStatusId = statusId;
                        if (adapter != null) {
                            //Toast.makeText(mainActivity, "general adapter distinto de null " + chatStatusId, Toast.LENGTH_SHORT).show();
                            adapter.notifyDataSetChanged();
                        }

                        if (statusId == Constant.ACCEPT_CHAT_DIRECT)
                            Visibility.visible(containerImgMoreOptionsChat);
                        else {
                            if (strFriendFbId.equals(strSenderId)) {
                                GlobalData.set(Constant.COUNTER_MESSAGES, "0", mainActivity);

                                //desde lista de chats
                                matchWalla.senderIdLastestMessage = Integer.parseInt(strSenderId);
                                updateRowChatOtherUser(friendsAdapter, strMessage, strDateTime);
                                listChatData.add(objMessageData);
                            }
                        }

                        messageAdapter.refresh(listChatData);
                        messageAdapter.notifyDataSetChanged();

                    } else {
                        if (strFriendFbId.equals(strSenderId)) {
                            GlobalData.set(Constant.COUNTER_MESSAGES, "0", mainActivity);
                            //desde lista de chats
                            matchWalla.senderIdLastestMessage = Integer.parseInt(strSenderId);

                            updateRowChatOtherUser(friendsAdapter, strMessage, strDateTime);
                            listChatData.add(objMessageData);
                            GlobalData.delete("from", mainActivity);
                            Common.deleteNotifications(mainActivity);
                        }

                        messageAdapter.refresh(listChatData);
                        messageAdapter.notifyDataSetChanged();
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                    mainActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(mainActivity, R.string.error_reponse_server, Toast.LENGTH_SHORT).show();
                        }
                    });

                }

            }
        };

        sendChatMessageButton.setOnClickListener(this);
        senderimage.setOnClickListener(this);
        imgBack.setOnClickListener(this);
        senderName.setOnClickListener(this);
        btnFeedBack.setOnClickListener(this);
        chatEditText.setOnClickListener(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (listChatData != null)
            listChatData.clear();

        if (timer != null)
            timer.cancel();
    }

    /**
     * Método encargado de actualizar el listado de chat cuando se recibe un mensaje
     *
     * @param adapter
     * @param message
     * @param date
     */
    public void updateRowChat(final ArrayAdapter<MatchesData> adapter, final String message, final String date) {
        if (adapter != null) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    matchWalla.count_messages = "0";
                    matchWalla.lastestMessage = message;
                    matchWalla.date = date;
                    adapter.notifyDataSetChanged();
                }
            }, 400);


        }
    }

    /**
     * Método encargado de actualizar el listado de chat cuando se recibe un mensaje
     *
     * @param adapter
     * @param message
     * @param date
     */
    public void updateRowChatOtherUser(final ArrayAdapter<MatchesData> adapter, final String message, final String date) {
        if (adapter != null) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    matchWalla.count_messages = "0";
                    matchWalla.lastestMessage = message;
                    matchWalla.date = date;
                    adapter.notifyDataSetChanged();
                }
            }, 400);


        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setStyle(DialogFragment.STYLE_NO_FRAME, android.R.style.Theme_Holo_Light_DarkActionBar);
    }

    /**
     * Inicializacion de los componentes graficos de la vista
     */
    private void initComponent() {

        chatList                            = (ListView) viewMain.findViewById(R.id.listChats);
        listOptionChat                      = (ListView) viewMain.findViewById(R.id.listOptionChat);
        chatEditText                        = (EmojiconEditText) viewMain.findViewById(R.id.chat_editText);
        btnFeedBack                         = (Button) viewMain.findViewById(R.id.btnFeedBack);
        btnClosePrivatePhotos               = (Button) viewMain.findViewById(R.id.btnClosePrivatePhotos);
        btnPassGoldRequest                  = (TextView) viewMain.findViewById(R.id.btnPassGoldRequest);
        bottom_write_bar                    = (LinearLayout) viewMain.findViewById(R.id.bottom_write_bar);
        containerOptionsBackChat            = (LinearLayout) viewMain.findViewById(R.id.containerOptionsBackChat);
        containerOptionsChat                = (LinearLayout) viewMain.findViewById(R.id.containerOptionsChat);
        containerOptionsFeedBack            = (LinearLayout) viewMain.findViewById(R.id.containerOptionsFeedBack);
        containerRequestUser                = (LinearLayout) viewMain.findViewById(R.id.containerRequestUser);
        container_private_gallery_photos    = (LinearLayout) viewMain.findViewById(R.id.container_private_gallery_photos);
        containerPassGoldChat               = (LinearLayout) viewMain.findViewById(R.id.containerPassGoldChat);
        gridPrivatePhotos                   = (GridView) viewMain.findViewById(R.id.gridPrivatePhotos);
        containerMoreVertical               = (RelativeLayout) viewMain.findViewById(R.id.containerMoreVertical);
        containerImgMoreOptionsChat         = (RelativeLayout) viewMain.findViewById(R.id.containerImgMoreOptionsChat);
        sendChatMessageButton               = (ImageView) viewMain.findViewById(R.id.send_chat_message_button);
        emoji_btn                           = (ImageView) viewMain.findViewById(R.id.emoji_btn);
        imgUser                             = (ImageView) viewMain.findViewById(R.id.imgUser);
        imgMoreOptionsChat                  = (ImageView) viewMain.findViewById(R.id.imgMoreOptionsChat);
        progressBarImageUser                = (ProgressBar) viewMain.findViewById(R.id.progressBarImageUser);
        RelativeLayout rootView             = (RelativeLayout) viewMain.findViewById(R.id.root_view);
        containerMainChat                   = (RelativeLayout) viewMain.findViewById(R.id.containerMainChat);
        containerPhotoPreviewChat           = (RelativeLayout) viewMain.findViewById(R.id.containerPhotoPreviewChat);
        senderimage                         = (ImageView) viewMain.findViewById(R.id.senderimage);
        imgBack                             = (ImageView) viewMain.findViewById(R.id.imgBack);
        senderName                          = (TextView) viewMain.findViewById(R.id.senderName);
        txtNumPrivatePictures               = (TextView) viewMain.findViewById(R.id.txtNumPrivatePictures);
        txtDescPrivatePictures              = (TextView) viewMain.findViewById(R.id.txtDescPrivatePictures);
        txtNumRequest                       = (TextView) viewMain.findViewById(R.id.txtNumRequest);
        txtTimeWaitRequest                  = (TextView) viewMain.findViewById(R.id.txtTimeWaitRequest);
        txtTitleRequest                     = (TextView) viewMain.findViewById(R.id.txtTitleRequest);
        txtProvince                         = (TextView) viewMain.findViewById(R.id.txtProvince);
        textView26                          = (TextView) viewMain.findViewById(R.id.textView26);
        textView27                          = (TextView) viewMain.findViewById(R.id.textView27);
        progressBarChat                     = (ProgressWheel) viewMain.findViewById(R.id.progressBarChat);
        String genere = GlobalData.isKey(Constant.GENERE,mainActivity)?GlobalData.get(Constant.GENERE,mainActivity):Constant.MAN;
        if(genere.equals(Constant.WOMAN)) {
            textView26.setText(" y anticipate a él.");
            textView27.setText("Sorpréndelo enviando un mensaje.");

        }
        AdapterOptionsChat adapterOptionsChat = new AdapterOptionsChat(mainActivity, Common.loadOptionsChat(mainActivity));
        listOptionChat.setAdapter(adapterOptionsChat);

        senderName.setTypeface(TypesLetter.getSansRegular(mainActivity));
        txtProvince.setTypeface(TypesLetter.getSansRegular(mainActivity));
        btnClosePrivatePhotos.setTypeface(TypesLetter.getSansRegular(mainActivity));
        btnPassGoldRequest.setTypeface(TypesLetter.getSansBold(mainActivity));
        txtDescPrivatePictures.setTypeface(TypesLetter.getSansRegular(mainActivity));
        txtNumRequest.setTypeface(TypesLetter.getSansRegular(mainActivity));
        txtTimeWaitRequest.setTypeface(TypesLetter.getSansRegular(mainActivity));
        String mode_gold = "Activa el modo gold";
        SpannableString content = new SpannableString(mode_gold);
        content.setSpan(new UnderlineSpan(), 0, mode_gold.length(), 0);
        btnPassGoldRequest.setText(content);

        if (matchWalla != null) {
            if (matchWalla.userId.equals(Constant.ID_SUPPORT)) {
                Visibility.gone(containerOptionsChat);
                Visibility.visible(bottom_write_bar);
                Visibility.visible(containerOptionsFeedBack);
                Visibility.gone(containerImgMoreOptionsChat);
            } else if (matchWalla.userId.equals(Constant.ID_WALLAMATCH)) {
                Visibility.gone(bottom_write_bar);
                Visibility.gone(containerImgMoreOptionsChat);
            } else {
                Visibility.gone(containerOptionsFeedBack);
                Visibility.visible(containerOptionsChat);
                Visibility.visible(bottom_write_bar);
            }
        }
        txtDescPrivatePictures.setText(mainActivity.getResources().getString(R.string.touch_to_request_picture) + " " + matchWalla.firstName);


        mainActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (friendsAdapter != null)
                    friendsAdapter.notifyDataSetChanged();
                if (friendsAdapterRequest != null)
                    friendsAdapterRequest.notifyDataSetChanged();
            }
        });


        //emoticones
        // Give the topmost view of your activity layout hierarchy. This will be used to measure soft keyboard height
        final EmojiconsPopup popup = new EmojiconsPopup(rootView, mainActivity);

        //Will automatically set size according to the soft keyboard size
        popup.setSizeForSoftKeyboard();

        //If the emoji popup is dismissed, change emojiButton to smiley icon
        popup.setOnDismissListener(new PopupWindow.OnDismissListener() {

            @Override
            public void onDismiss() {
                changeEmojiKeyboardIcon(emoji_btn, R.drawable.ic_insert_emoticon_black_24dp);
            }
        });

        //If the text keyboard closes, also dismiss the emoji popup
        popup.setOnSoftKeyboardOpenCloseListener(new EmojiconsPopup.OnSoftKeyboardOpenCloseListener() {

            @Override
            public void onKeyboardOpen(int keyBoardHeight) {

            }

            @Override
            public void onKeyboardClose() {
                if (popup.isShowing())
                    popup.dismiss();
            }
        });

        //On emoji clicked, add it to edittext
        popup.setOnEmojiconClickedListener(new EmojiconGridView.OnEmojiconClickedListener() {

            @Override
            public void onEmojiconClicked(Emojicon emojicon) {
                if (chatEditText == null || emojicon == null) {
                    return;
                }

                int start = chatEditText.getSelectionStart();
                int end = chatEditText.getSelectionEnd();
                if (start < 0) {
                    chatEditText.append(emojicon.getEmoji());
                } else {
                    chatEditText.getText().replace(Math.min(start, end),
                            Math.max(start, end), emojicon.getEmoji(), 0,
                            emojicon.getEmoji().length());
                }
            }
        });

        //On backspace clicked, emulate the KEYCODE_DEL key event
        popup.setOnEmojiconBackspaceClickedListener(new EmojiconsPopup.OnEmojiconBackspaceClickedListener() {

            @Override
            public void onEmojiconBackspaceClicked(View v) {
                KeyEvent event = new KeyEvent(
                        0, 0, 0, KeyEvent.KEYCODE_DEL, 0, 0, 0, 0, KeyEvent.KEYCODE_ENDCALL);
                chatEditText.dispatchKeyEvent(event);
            }
        });
        // To toggle between text keyboard and emoji keyboard keyboard(Popup)
        emoji_btn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                //If popup is not showing => emoji keyboard is not visible, we need to show it
                if (!popup.isShowing()) {

                    //If keyboard is visible, simply show the emoji popup
                    if (popup.isKeyBoardOpen()) {
                        popup.showAtBottom();
                        changeEmojiKeyboardIcon(emoji_btn, R.drawable.ic_keyboard_black_24dp);
                    }

                    //else, open the text keyboard first and immediately after that show the emoji popup
                    else {
                        chatEditText.setFocusableInTouchMode(true);
                        chatEditText.requestFocus();
                        popup.showAtBottomPending();
                        final InputMethodManager inputMethodManager = (InputMethodManager) mainActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
                        inputMethodManager.showSoftInput(chatEditText, InputMethodManager.SHOW_IMPLICIT);
                        changeEmojiKeyboardIcon(emoji_btn, R.drawable.ic_keyboard_black_24dp);
                    }
                }

                //If popup is showing, simply dismiss it to show the undelying text keyboard
                else {
                    popup.dismiss();
                }
            }
        });

        imgMoreOptionsChat.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                KeyBoard.hide(chatEditText);
                if (numPrivatePhotos == 0)
                    Common.errorMessage(mainActivity, "", matchWalla.firstName + " " + mainActivity.getResources().getString(R.string.message_private_pictures));
                else {
                    String status = GlobalData.get(Constant.USER_STATUS, mainActivity);
                    String gender = GlobalData.get(Constant.GENERE, mainActivity);
                    if (fromOpenChat == Constant.MATCH || fromOpenChat == Constant.ACCEPT_CHAT_DIRECT || matchWalla.chatStatusId == Constant.ACCEPT_CHAT_DIRECT || status.equals(Constant.GOLD) || gender.equals(Constant.WOMAN))
                        addRequestPhoto();
                    else {
                        DialogStore dialogStore = new DialogStore(mainActivity, 1);
                        dialogStore.show(mainActivity.mFragmentManager, "DialogStore.TAG");
                    }
                }
            }
        });


        listOptionChat.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        Visibility.gone(containerMoreVertical);
                        addRequestPhoto();
                        break;
                    case 1:
                        //reportar usuario
                        Visibility.gone(containerMoreVertical);
                        openDialogReport(matchWalla.userId);
                        break;
                    default:
                        break;
                }
            }
        });

        chatList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Visibility.gone(containerMoreVertical);
            }
        });

        btnClosePrivatePhotos.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                if (adapterPrivatePhoto != null && adapterPrivatePhoto.picIds != null)
                    adapterPrivatePhoto.picIds.clear();

                Visibility.gone(container_private_gallery_photos);
            }
        });

        btnPassGoldRequest.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogStore dialogStore = new DialogStore(mainActivity, 1);
                dialogStore.show(mainActivity.mFragmentManager, "DialogStore.TAG");
            }
        });


    }

    /**
     * Metodo encargado de llamar al servicio para solicitar fotos privadas del usuario
     */
    private void addRequestPhoto() {
        getPrivatePictures(matchWalla.userId);
    }

    /**
     * Metodo de cambiar el icono cuando se abre el teclado con emoticones
     *
     * @param iconToBeChanged
     * @param drawableResourceId
     */
    private void changeEmojiKeyboardIcon(ImageView iconToBeChanged, int drawableResourceId) {
        iconToBeChanged.setImageResource(drawableResourceId);
    }

    @Override
    public void onResume() {
        super.onResume();


        if (receiver != null)
            mainActivity.registerReceiver(receiver, filter);

        if (matchWalla.chatStatusId == Constant.REJECT_CHAT_DIRECT)
            dismiss();
        else
            getChat(matchWalla.userId);

        if (getDialog() != null) {
            getDialog().setOnKeyListener(new DialogInterface.OnKeyListener() {
                @Override
                public boolean onKey(android.content.DialogInterface dialog, int keyCode,
                                     android.view.KeyEvent event) {


                    if ((keyCode == android.view.KeyEvent.KEYCODE_BACK)) {
                        //This is the filter
                        if (event.getAction() != KeyEvent.ACTION_DOWN) {

                            if (containerMoreVertical.getVisibility() == View.VISIBLE)
                                Visibility.gone(containerMoreVertical);
                            else
                                exitChat();


                            return true;
                        } else {
                            //Hide your keyboard here!!!!!!
                            //preguntar teclado esta abierto

                            InputMethodManager imm = (InputMethodManager) getActivity()
                                    .getSystemService(Context.INPUT_METHOD_SERVICE);

                            if (imm.isAcceptingText()) {
                                //writeToLog("Software Keyboard was shown");
                                KeyBoard.hide(chatEditText);
                            } else {
                                //writeToLog("Software Keyboard was not shown");
                                if (containerMoreVertical.getVisibility() == View.VISIBLE)
                                    Visibility.gone(containerMoreVertical);
                                else {
                                    exitChat();
                                   /* if(fromOpenChat == 1)
										mainActivity.replaceFragment(new FriendsFragment(mainActivity));*/
                                }

                            }

                            return true; // pretend we've processed it
                        }
                    } else
                        return false; // pass on to be processed as normal
                }
            });
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        if (receiver != null)
            mainActivity.unregisterReceiver(receiver);


    }

    /**
     * Adapter para la gestión de los mensajes de chat
     */
    public class AwesomeAdapter extends BaseAdapter {
        private Context mContext;
        private ArrayList<ChatMessageList> mMessages;

        public AwesomeAdapter(Context context,
                              ArrayList<ChatMessageList> messages) {
            super();
            this.mContext = context;
            this.mMessages = messages;
        }

        public void refresh(ArrayList<ChatMessageList> messages) {

            this.mMessages = messages;
            notifyDataSetChanged();
            chatList.setSelection(messages.size() + 1);
        }

        @Override
        public int getCount() {
            return mMessages.size();
        }

        @Override
        public Object getItem(int position) {
            return mMessages.get(position);
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final ChatMessageList message = (ChatMessageList) this.getItem(position);

            final ViewHolder holder;
            if (convertView == null) {
                holder                                  = new ViewHolder();
                convertView                             = LayoutInflater.from(mContext).inflate(R.layout.sms_row1, parent, false);

                holder.imageOther                       = (ImageView) convertView.findViewById(R.id.ivProfileOther);
                holder.imageMe                          = (ImageView) convertView.findViewById(R.id.ivProfileMe);
                holder.imgPrivatePhoto                  = (ImageView) convertView.findViewById(R.id.imgPrivatePhoto);
                holder.body                             = (EmojiconTextView) convertView.findViewById(R.id.tvBody);
                holder.txtDateChat                      = (TextView) convertView.findViewById(R.id.txtDateChat);
                holder.txtRequestPhotos                 = (TextView) convertView.findViewById(R.id.txtRequestPhotos);
                holder.txtTitleRequets                  = (TextView) convertView.findViewById(R.id.txtTitleRequets);
                holder.containerMessageChat             = (RelativeLayout) convertView.findViewById(R.id.containerMessageChat);
                holder.containerImageRequest            = (RelativeLayout) convertView.findViewById(R.id.containerImageRequest);
                holder.containerMainRowChat             = (LinearLayout) convertView.findViewById(R.id.containerMainRowChat);
                holder.containerRequestPrivatePhotos    = (LinearLayout) convertView.findViewById(R.id.containerRequestPrivatePhotos);
                holder.containerRequetsChat             = (LinearLayout) convertView.findViewById(R.id.containerRequetsChat);
                holder.containerButtonsRequestChat      = (LinearLayout) convertView.findViewById(R.id.containerButtonsRequestChat);
                holder.btnNoWant                        = (Button) convertView.findViewById(R.id.btnNoWant);
                holder.btnAcceptRequestPhotos           = (Button) convertView.findViewById(R.id.btnAcceptRequestPhotos);
                holder.btnAcceptRequestChat             = (Button) convertView.findViewById(R.id.btnAcceptRequestChat);
                holder.btnRejectRequestChat             = (Button) convertView.findViewById(R.id.btnRejectRequestChat);
                holder.containerButtonsRequestPhotos    = (LinearLayout) convertView.findViewById(R.id.containerButtonsRequestPhotos);
                holder.progressBarPrivateImage          = (ProgressBar) convertView.findViewById(R.id.progressBarPrivateImage);
                holder.separateRequestPicture           = (View) convertView.findViewById(R.id.separateRequestPicture);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            Visibility.gone(holder.containerMainRowChat);
            Visibility.gone(holder.containerRequestPrivatePhotos);
            Visibility.gone(holder.containerButtonsRequestPhotos);
            Visibility.gone(holder.containerRequetsChat);
            Visibility.gone(holder.txtRequestPhotos);

            String messageEncode = (message.strMessage != null) ? message.strMessage.trim() : "";
            try {
                messageEncode = URLDecoder.decode(messageEncode, "UTF-8");
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            holder.body.setTypeface(TypesLetter.getSansRegular(mainActivity));

            final boolean isMe = matchWalla.userId.equals("" + message.strSenderId);

            SimpleDateFormat fromUser = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMM hh:mm a");

            try {
                holder.txtDateChat.setText(simpleDateFormat.format(fromUser.parse(message.strDateTime)));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            holder.txtDateChat.setTypeface(TypesLetter.getSansRegular(mainActivity));
            //holder.txtDateChat.setTextSize(TypedValue.COMPLEX_UNIT_SP, 11);
            holder.containerMessageChat.setGravity(Gravity.CENTER_VERTICAL);
            //Log.d("app2you"," my id........." + myUserId+ "id del otro usuario"+ matchWalla.idUser);
            //Log.d("app2you","tipo de mensaje: " +  message.messageTypeId);

            if (message.messageTypeId != null && message.messageTypeId.equals(Constant.REQUEST_PRIVATE_PHOTOS)) {

                //logica para aparecer mensaje con peticion de fotos privadas
                if (!isMe) {
                    //yo
                    //Log.d("app2you","yo....privacity: " +  message.privatePicStatusId);
                    if (message.privatePicStatusId != null && message.privatePicStatusId.equals(Constant.ACCEPT_PHOTO)) {

                        Visibility.visible(holder.imgPrivatePhoto);
                        enableRowRequestPhoto(holder, message);
                        Visibility.visible(holder.containerImageRequest);

                        if (Common.getScreen(mainActivity, 1) <= 480) {
                            //4.3 - 4.7 pulgadas
                            LinearLayout.LayoutParams linearParams = new LinearLayout.LayoutParams(280, 355);
                            linearParams.setMargins(8, 0, 16, 0);
                            holder.containerRequestPrivatePhotos.setLayoutParams(linearParams);
                        } else {
                            LinearLayout.LayoutParams linearParams = new LinearLayout.LayoutParams(380, 455);
                            linearParams.setMargins(8, 0, 16, 0);
                            holder.containerRequestPrivatePhotos.setLayoutParams(linearParams);
                        }

                        Visibility.visible(holder.txtRequestPhotos);
                        Visibility.gone(holder.txtTitleRequets);
                        Visibility.gone(holder.containerButtonsRequestPhotos);
                        Visibility.gone(holder.separateRequestPicture);
                        holder.txtRequestPhotos.setText("" + mainActivity.getResources().getString(R.string.the_picture_accepted));

                    } else {
                        Visibility.gone(holder.containerRequestPrivatePhotos);
                        Visibility.visible(holder.containerMainRowChat);
                        holder.imageMe.setVisibility(View.VISIBLE);
                        holder.imageOther.setVisibility(View.INVISIBLE);
                        holder.body.setGravity(Gravity.CENTER_VERTICAL | Gravity.RIGHT);
                        holder.containerMessageChat.setBackgroundResource(R.drawable.speech_bubble_green);
                        holder.body.setTextColor(Color.parseColor("#FFFFFF"));
                        holder.containerMainRowChat.setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
                        holder.body.setGravity(Gravity.RIGHT);

                        holder.txtDateChat.setGravity(Gravity.LEFT);
                        // CODE FOR ADD MARGINS
                        LinearLayout.LayoutParams relativeParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                        relativeParams.setMargins(16, 0, 5, 0);
                        holder.containerMessageChat.setLayoutParams(relativeParams);
                        if(currentImageurl.isEmpty())
                            Picasso.with(getContext()).load(R.drawable.no_foto).transform(new CircleTransform()).into(holder.imageMe);
                        else
                            Picasso.with(getContext()).load(currentImageurl).error(R.drawable.no_foto).transform(new CircleTransform()).into(holder.imageMe);

                        //yo
                        holder.body.setText("" + message.strMessage);
                        holder.containerRequestPrivatePhotos.setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
                        holder.containerRequestPrivatePhotos.setBackgroundResource(R.drawable.speech_bubble_green);
                    }
                } else {
                    //otro usuario - persona con la que hablo
                    //Log.d("app2you","ella....privacity: " +  message.privatePicStatusId);
                    if (message.privatePicStatusId != null && message.privatePicStatusId.equals(Constant.REJECT_PHOTO)) {
                        Visibility.gone(holder.imgPrivatePhoto);
                        enableRowRequestPhoto(holder, message);
                        Visibility.gone(holder.containerImageRequest);
                        LinearLayout.LayoutParams linearParams = new LinearLayout.LayoutParams(350, 92);
                        linearParams.setMargins(8, 0, 16, 0);
                        holder.containerRequestPrivatePhotos.setLayoutParams(linearParams);
                        //holder.containerRequestPrivatePhotos.setBackgroundResource(R.drawable.speech_bubble_green);

                        Visibility.visible(holder.txtRequestPhotos);
                        Visibility.gone(holder.txtTitleRequets);
                        Visibility.gone(holder.containerButtonsRequestPhotos);
                        Visibility.gone(holder.separateRequestPicture);
                        holder.txtRequestPhotos.setText("" + mainActivity.getResources().getString(R.string.reject_picture));

                    } else if (message.privatePicStatusId != null && message.privatePicStatusId.equals(Constant.ACCEPT_PHOTO)) {
                        Visibility.visible(holder.imgPrivatePhoto);
                        enableRowRequestPhoto(holder, message);
                        Visibility.visible(holder.containerImageRequest);
                        if (Common.getScreen(mainActivity, 1) <= 480) {
                            //4.3 -4.7 pulgadas
                            LinearLayout.LayoutParams linearParams = new LinearLayout.LayoutParams(280, 355);
                            linearParams.setMargins(8, 0, 16, 0);
                            holder.containerRequestPrivatePhotos.setLayoutParams(linearParams);
                        } else {
                            LinearLayout.LayoutParams linearParams = new LinearLayout.LayoutParams(380, 455);
                            linearParams.setMargins(8, 0, 16, 0);
                            holder.containerRequestPrivatePhotos.setLayoutParams(linearParams);
                        }

                        Visibility.visible(holder.txtRequestPhotos);
                        Visibility.gone(holder.txtTitleRequets);
                        Visibility.gone(holder.containerButtonsRequestPhotos);
                        Visibility.gone(holder.separateRequestPicture);
                        holder.txtRequestPhotos.setText("" + mainActivity.getResources().getString(R.string.accepted_picture));

                    } else {
                        enableRowRequestPhoto(holder, message);
                        Visibility.visible(holder.containerImageRequest);
                        Visibility.visible(holder.imgPrivatePhoto);
                        if (Common.getScreen(mainActivity, 1) <= 480) {
                            //4.3 -4.7 pulgadas
                            LinearLayout.LayoutParams linearParams = new LinearLayout.LayoutParams(300, 375);
                            linearParams.setMargins(8, 0, 16, 0);
                            holder.containerRequestPrivatePhotos.setLayoutParams(linearParams);
                        } else {
                            LinearLayout.LayoutParams linearParams = new LinearLayout.LayoutParams(400, 475);
                            linearParams.setMargins(8, 0, 16, 0);
                            holder.containerRequestPrivatePhotos.setLayoutParams(linearParams);
                        }
                    }
                }
            } else if (message.messageTypeId != null && message.messageTypeId.equals(Constant.REQUEST_CHAT)) {
                //peticiones de chat
                int chatStatusId = matchWalla.chatStatusId;

                if (chatStatusId == Constant.REJECT_CHAT_DIRECT) {
                    //no haga nada
                } else if (chatStatusId == Constant.ACCEPT_CHAT_DIRECT) {
                    //Log.d("app2you","aceptada solicitud : " + listChatData.size());

                    for (int i = 0, len = listChatData.size(); i < len; ++i) {
                        if (i == 0)
                            drawTypeText(holder, mainActivity.getString(R.string.request_chat_accept) + " " + matchWalla.firstName, isMe);
                    }


                } else {


                    if (matchWalla.requestToMe && chatStatusId == Constant.REQUEST_CHAT_PENDING) {
                        Visibility.visible(holder.containerRequetsChat);
                        Visibility.visible(holder.containerButtonsRequestChat);
                        sendChatMessageButton.setEnabled(false);
                    } else {
                        Visibility.gone(holder.containerRequetsChat);
                        sendChatMessageButton.setEnabled(true);
                        //Log.d("app2you", "estado del chat: " + chatStatusId);
                        switch (chatStatusId) {
                            case Constant.NOT_REQUEST_CHAT:

                                break;
                            case Constant.REQUEST_CHAT_PENDING:
                                String gender = GlobalData.get(Constant.GENERE, mainActivity);
                                String messageRequest = gender.equals(Constant.MAN) ? mainActivity.getString(R.string.send_message_to, matchWalla.firstName) : "" + mainActivity.getString(R.string.send_sucess_message);
                                drawTypeText(holder, messageRequest, isMe);
                                break;
                            case Constant.ACCEPT_CHAT_DIRECT:
                                drawTypeText(holder, mainActivity.getString(R.string.request_chat_accept) + " " + matchWalla.firstName, isMe);
                                break;
                            case Constant.REJECT_CHAT_DIRECT:
                                Common.errorMessage(mainActivity, "", "" + mainActivity.getString(R.string.request_chat_rejected));
                                //drawTypeText(holder, "Tu solicitud de chat fue rechazada", isMe);
                                break;
                            default:

                                break;
                        }
                    }
                }


            } else {
                //chat normal
                drawTypeText(holder, messageEncode, isMe);
            }

            holder.btnAcceptRequestPhotos.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {

                    responseRequest(matchWalla.userId, message.picId, Constant.ACCEPT_PHOTO, holder, message);
                }
            });
            holder.btnNoWant.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {

                    responseRequest(matchWalla.userId, message.picId, Constant.REJECT_PHOTO, holder, message);
                }
            });


            holder.imgPrivatePhoto.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    ViewContent viewContent = new ViewContent(mainActivity, message.imageUrl, message.strSendername, 1);
                    viewContent.show(mainActivity.mFragmentManager, "ViewContent.TAG");

                }
            });
            holder.btnAcceptRequestChat.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {

                    responseRequestChat(matchWalla.userId, Constant.ACCEPT_CHAT_DIRECT, holder, message);
                }
            });
            holder.btnRejectRequestChat.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {

                    responseRequestChat(matchWalla.userId, Constant.REJECT_CHAT_DIRECT, holder, message);
                }
            });

            holder.body.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    Common.loadLink(message.strMessage, mainActivity);
                }
            });

            return convertView;
        }

        private class ViewHolder {

            public ImageView imageOther, imageMe, imgPrivatePhoto;
            public TextView txtDateChat, txtRequestPhotos, txtTitleRequets;
            public RelativeLayout containerMessageChat, containerImageRequest;
            public LinearLayout containerMainRowChat, containerRequestPrivatePhotos, containerButtonsRequestPhotos, containerRequetsChat, containerButtonsRequestChat;
            public EmojiconTextView body;
            public Button btnAcceptRequestPhotos, btnNoWant, btnRejectRequestChat, btnAcceptRequestChat;
            public ProgressBar progressBarPrivateImage;
            public View separateRequestPicture;

        }

        /**
         * Método para agregar un mensaje normal a la conversación
         *
         * @param holder
         * @param messageEncode
         * @param isMe
         */
        public void drawTypeText(final ViewHolder holder, String messageEncode, boolean isMe) {
            holder.body.setText(Html.fromHtml(messageEncode));
            Visibility.visible(holder.containerMainRowChat);
            if (!isMe) {
                holder.imageMe.setVisibility(View.VISIBLE);
                holder.imageOther.setVisibility(View.INVISIBLE);
                holder.body.setGravity(Gravity.CENTER_VERTICAL | Gravity.RIGHT);
                holder.containerMessageChat.setBackgroundResource(R.drawable.speech_bubble_green);
                holder.body.setTextColor(Color.parseColor("#FFFFFF"));
                holder.containerMainRowChat.setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
                holder.body.setGravity(Gravity.RIGHT);

                holder.txtDateChat.setGravity(Gravity.LEFT);
                // CODE FOR ADD MARGINS
                LinearLayout.LayoutParams relativeParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                relativeParams.setMargins(16, 0, 5, 0);
                holder.containerMessageChat.setLayoutParams(relativeParams);
                if(currentImageurl.isEmpty())
                    Picasso.with(getContext()).load(R.drawable.no_foto).transform(new CircleTransform()).into(holder.imageMe);
                else
                    Picasso.with(getContext()).load(currentImageurl).error(R.drawable.no_foto).transform(new CircleTransform()).into(holder.imageMe);

            } else {
                holder.imageOther.setVisibility(View.VISIBLE);
                holder.imageMe.setVisibility(View.INVISIBLE);
                holder.containerMainRowChat.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
                holder.body.setGravity(Gravity.CENTER_VERTICAL | Gravity.LEFT);
                holder.containerMessageChat.setBackgroundResource(R.drawable.speech_bubble_orange);
                holder.body.setTextColor(Color.parseColor("#606264"));
                holder.body.setGravity(Gravity.LEFT);
                holder.txtDateChat.setGravity(Gravity.RIGHT);
                LinearLayout.LayoutParams relativeParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                relativeParams.setMargins(5, 0, 16, 0);
                holder.containerMessageChat.setLayoutParams(relativeParams);
                if (mImagePath != null && mImagePath.length() > 0)
                    Picasso.with(getContext()).load(mImagePath).error(R.drawable.no_foto).transform(new CircleTransform()).into(holder.imageOther);
                else
                    Picasso.with(getContext()).load(R.drawable.no_foto).transform(new CircleTransform()).into(holder.imageOther);
            }
        }

        /**
         * Método para habilitar la visualización de la una foto solicitada
         *
         * @param holder
         * @param message
         */
        public void enableRowRequestPhoto(final ViewHolder holder, ChatMessageList message) {
            Visibility.gone(holder.containerMainRowChat);
            Visibility.gone(holder.txtRequestPhotos);
            Visibility.visible(holder.containerRequestPrivatePhotos);
            Visibility.visible(holder.txtTitleRequets);
            Visibility.visible(holder.containerButtonsRequestPhotos);
            Visibility.visible(holder.separateRequestPicture);


            if (message.imageUrl != null && message.imageUrl.length() > 0) {
                Visibility.visible(holder.progressBarPrivateImage);
                Picasso.with(mainActivity).load(message.imageUrl).error(R.drawable.no_foto).into(holder.imgPrivatePhoto, new Callback() {
                    @Override
                    public void onSuccess() {
                        Visibility.gone(holder.progressBarPrivateImage);
                    }

                    @Override
                    public void onError() {
                        Visibility.gone(holder.progressBarPrivateImage);
                    }
                });
            }

            holder.containerRequestPrivatePhotos.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
            holder.containerRequestPrivatePhotos.setBackgroundResource(R.drawable.shape_preferences);
        }

        /**
         * Método encargado de cambiar el estado de una foto privada
         *
         * @param otherUserId
         * @param picId
         * @param requestStatusId
         */
        public void responseRequest(String otherUserId, String picId, final String requestStatusId, final ViewHolder holder, final ChatMessageList message) {
            final ProgressDialog progressDialog = new ProgressDialog(mainActivity, R.style.CustomDialogTheme);
            progressDialog.setCancelable(false);

            progressDialog.show();
            progressDialog.setContentView(R.layout.custom_progress_dialog);
            TextView txtView = (TextView) progressDialog.findViewById(R.id.txtProgressDialog);
            txtView.setText("" + mainActivity.getResources().getString(R.string.change_state));

            OkHttpClient client = new OkHttpClient();
            //2 aceptado - 3 reject
            RequestBody formBody = new FormBody.Builder()
                    .add("sessionToken", "" + GlobalData.get(Constant.SESSION_TOKEN, mainActivity))
                    .add("otherUserId", "" + otherUserId)
                    .add("picId", "" + picId)
                    .add("requestStatusId", "" + requestStatusId)
                    .add("requestTypeId", "" + Constant.TYPE_REQUEST_PHOTO)
                    .add("deviceVersion", Common.getVersionDevice(mainActivity))
                    .add("appVersion", "" + getResources().getString(R.string.app_version))
                    .build();

            Request request = new Request.Builder()
                    .url(Services.RESPONSE_REQUEST)
                    .post(formBody)
                    .build();

            client.newCall(request).enqueue(new okhttp3.Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    progressDialog.dismiss();
                    Looper.prepare();
                    mainActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Common.errorMessage(mainActivity, "", "" + mainActivity.getResources().getString(R.string.check_internet));
                        }
                    });
                    Looper.loop();
                }

                @Override
                public void onResponse(Call call, final Response response) throws IOException {
                    progressDialog.dismiss();
                    final String result = response.body().string();
                    Log.d("app2you", "response  responseRequest  " + result);

                    Looper.prepare();

                    mainActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                JSONObject json = new JSONObject(result);

                                String success = json.getString("success");

                                if (success.equals("true")) {
                                    if (requestStatusId.equals(Constant.REJECT_PHOTO)) {
                                        message.privatePicStatusId = Constant.REJECT_PHOTO;
                                        holder.txtRequestPhotos.setText("" + mainActivity.getResources().getString(R.string.reject_request));

                                    } else {
                                        unLockPictures = true;
                                        message.privatePicStatusId = Constant.ACCEPT_PHOTO;
                                        holder.txtRequestPhotos.setText("" + mainActivity.getResources().getString(R.string.accepted_request));
                                    }

                                    Visibility.visible(holder.txtRequestPhotos);
                                    Visibility.gone(holder.containerButtonsRequestPhotos);
                                    if (messageAdapter != null)
                                        notifyDataSetChanged();


                                } else {
                                    Toast.makeText(mainActivity, R.string.error_reponse_server, Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException ex) {
                                ex.printStackTrace();
                                Toast.makeText(mainActivity, R.string.error_reponse_server, Toast.LENGTH_SHORT).show();
                            }
                            finally {
                                response.body().close();
                            }

                        }
                    });


                    Looper.loop();
                }
            });
        }

        /**
         * Método encargado de cambiar el estado de petición de chat
         *
         * @param otherUserId
         * @param requestStatusId
         */
        public void responseRequestChat(String otherUserId, final int requestStatusId, final ViewHolder holder, final ChatMessageList message) {
            final ProgressDialog progressDialog = new ProgressDialog(mainActivity, R.style.CustomDialogTheme);
            progressDialog.setCancelable(false);

            progressDialog.show();
            progressDialog.setContentView(R.layout.custom_progress_dialog);
            TextView txtView = (TextView) progressDialog.findViewById(R.id.txtProgressDialog);
            txtView.setText("" + mainActivity.getResources().getString(R.string.change_state));

            OkHttpClient client = new OkHttpClient();
            //2 aceptado - 3 reject
            RequestBody formBody = new FormBody.Builder()
                    .add("sessionToken", "" + GlobalData.get(Constant.SESSION_TOKEN, mainActivity))
                    .add("otherUserId", "" + otherUserId)
                    .add("requestStatusId", "" + requestStatusId)
                    .add("requestTypeId", "" + Constant.TYPE_REQUEST_CHAT)
                    .add("deviceVersion", Common.getVersionDevice(mainActivity))
                    .add("appVersion", "" + getResources().getString(R.string.app_version))
                    .build();

            Request request = new Request.Builder()
                    .url(Services.RESPONSE_REQUEST)
                    .post(formBody)
                    .build();

            client.newCall(request).enqueue(new okhttp3.Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    progressDialog.dismiss();
                    Looper.prepare();
                    mainActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Common.errorMessage(mainActivity, "", "" + mainActivity.getResources().getString(R.string.check_internet));
                        }
                    });
                    Looper.loop();
                }

                @Override
                public void onResponse(Call call, final Response response) throws IOException {
                    progressDialog.dismiss();
                    final String result = response.body().string();
                    Log.d("app2you", "response  responseRequest  chat" + result);

                    Looper.prepare();

                    mainActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                JSONObject json = new JSONObject(result);

                                String success = json.getString("success");

                                if (success.equals("true")) {
                                    if (requestStatusId == Constant.ACCEPT_CHAT_DIRECT) {
                                        //stPersons",mainActivity);
                                        //GlobalData.delete("listnewpersons",mainActivity);
                                        enableButtonChat(true);
                                        if (matchWalla != null)
                                            matchWalla.chatStatusId = Constant.ACCEPT_CHAT_DIRECT;
                                        if (adapter != null)
                                            adapter.notifyDataSetChanged();

                                        //matchWalla.chatStatusId = Constant.ACCEPT_CHAT_DIRECT;
                                        message.chatStatusId = Constant.ACCEPT_CHAT_DIRECT;
                                        Visibility.gone(holder.containerButtonsRequestChat);
                                        Visibility.visible(containerImgMoreOptionsChat);
                                        AppAnimation.apperView(mainActivity, containerImgMoreOptionsChat);

                                        if (positionRequestChat != -1)
                                            mainActivity.listRequestChat.remove(positionRequestChat);


                                        if (requestChatsFragment != null && requestChatsFragment.friendsAdapter != null)
                                            requestChatsFragment.friendsAdapter.notifyDataSetChanged();

                                        int listSize = mainActivity.listRequestChat.size();
                                        if (listSize == 0)
                                            Visibility.gone(mainActivity.badgeTab);
                                        else {
                                            if (mainActivity.badgeTab != null)
                                                mainActivity.badgeTab.setText("" + listSize);
                                        }

                                        //actualizar adapter
                                        if (messageAdapter != null)
                                            notifyDataSetChanged();
                                    } else {
                                        //borrar relación de chat y cerrar el chat
                                        enableButtonChat(false);
                                        if (matchWalla != null)
                                            matchWalla.chatStatusId = Constant.REJECT_CHAT_DIRECT;
                                        if (adapter != null)
                                            adapter.notifyDataSetChanged();

                                        deleteRelationship("" + Constant.DELETE_DIRECT_CHAT, matchWalla.userId, positionRequestChat);
                                    }


                                } else {
                                    Toast.makeText(mainActivity, R.string.error_reponse_server, Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException ex) {
                                ex.printStackTrace();
                                Toast.makeText(mainActivity, R.string.error_reponse_server, Toast.LENGTH_SHORT).show();
                            }
                            finally {
                                response.body().close();
                            }

                        }
                    });

                    Looper.loop();
                }
            });
        }

        @Override
        public long getItemId(int position) {
            // Unimplemented, because we aren't using Sqlite.
            return 0;
        }
    }

    /**
     * Metodo  para enviar mensaje de chat al servidor
     */
    public void sendMessageOkhttp3(String otherUserId, final String message, final String messageTypeId) {
        if (messageTypeId.equals(Constant.REQUEST_PRIVATE_PHOTOS) || messageTypeId.equals(Constant.REQUEST_CHAT))
            Visibility.visible(progressBarChat);

        okhttp3.OkHttpClient client = new okhttp3.OkHttpClient();

        okhttp3.RequestBody formBody = new FormBody.Builder()
                .add("sessionToken", "" + GlobalData.get(Constant.SESSION_TOKEN, mainActivity))
                .add("otherUserId", "" + otherUserId)
                .add("message", "" + message)
                .add("messageTypeId", "" + messageTypeId)
                .add("deviceId", "1")
                .build();

        okhttp3.Request request = new okhttp3.Request.Builder()
                .url(Services.SEND_MESSAGE_CHAT)
                .post(formBody)
                .build();


        client.newCall(request).enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Looper.prepare();
                mainActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Visibility.gone(progressBarChat);
                        sendChatMessageButton.setEnabled(true);
                        Common.errorMessage(mainActivity, "", "" + mainActivity.getResources().getString(R.string.check_internet));
                    }
                });
                Looper.loop();
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                final String result = response.body().string();
                Log.d("app2you", "respuesta send message: " + result);

                if (response.isSuccessful()) {
                    Looper.prepare();
                    mainActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Visibility.gone(progressBarChat);
                                Visibility.gone(containerRequestUser);
                                sendChatMessageButton.setEnabled(true);
                                // Log.d("app2you","chat insertado  "+result );
                                addMessageToList(message, messageTypeId);

                                if (!GlobalData.isKey(Constant.RATE_APP, mainActivity)) {
                                    for (int i = 0, len = messageAdapter.mMessages.size(); i < len; ++i) {
                                        ChatMessageList chatMessageList = messageAdapter.mMessages.get(i);

                                        if (!myUserId.equals(chatMessageList.strSenderId))
                                            messagesTmp.add(chatMessageList.strMessage);
                                    }

                                    if (messagesTmp != null && messagesTmp.size() >= NUM_MAX_MESSAGES) {
                                        if (!GlobalData.isKey(Constant.RATE_APP, mainActivity)) {
                                            GlobalData.set(Constant.RATE_APP, "Y", mainActivity);
                                            mainActivity.openDialogRateWallamatch();
                                        }
                                    }
                                }

                            } catch (Exception ex) {

                                ex.printStackTrace();
                                Toast.makeText(mainActivity, R.string.error_reponse_server, Toast.LENGTH_SHORT).show();
                            } finally {
                                response.body().close();
                            }
                        }
                    });


                    Looper.loop();
                }
            }
        });

    }

    /**
     * Método encargado de añadir mensaje al listado del chat
     *
     * @param message
     * @param messageTypeId
     */
    public void addMessageToList(final String message, String messageTypeId) {
        String msg = ""; /*messageTypeId.equals(Constant.REQUEST_PRIVATE_PHOTOS)?""+mainActivity.getResources().getString(R.string.request_send):message;*/
        int chatStatusId = -1;

        if (messageTypeId.equals(Constant.REQUEST_PRIVATE_PHOTOS)) {
            msg = "" + mainActivity.getResources().getString(R.string.request_send);
        } else if (messageTypeId.equals(Constant.REQUEST_CHAT)) {
            msg = "" + mainActivity.getResources().getString(R.string.request_send);
            chatStatusId = Constant.REQUEST_CHAT_PENDING;
        } else {
            msg = message;
        }

        objMessageData = new ChatMessageList();
        objMessageData.strSenderFacebookId = "";
        objMessageData.strMessage = msg;
        // 2013-12-05 07:49:26
        final String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());

        objMessageData.strDateTime = date;
        objMessageData.strSendername = "";
        objMessageData.strSenderId = myUserId;
        objMessageData.strReceiverId = strFriendFbId;
        objMessageData.messageTypeId = messageTypeId;
        objMessageData.chatStatusId = chatStatusId;
        objMessageData.strFlagForMessageSuccess = "1";
        listChatData.add(objMessageData);

        chatEditText.setText("");
        messageAdapter.refresh(listChatData);
        messageAdapter.notifyDataSetChanged();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                matchWalla.senderIdLastestMessage = Integer.parseInt(myUserId);
                updateRowChat(friendsAdapter, message, date);
                if (friendsAdapterRequest != null)
                    updateRowChat(friendsAdapterRequest, message, date);
            }
        }, 1000);
    }

    /**
     * Salir del chat, limpiar lista de chats y cerrar teclado
     */
    public void exitChat() {
        if (listChatData != null)
            listChatData.clear();

        if (timer != null)
            timer.cancel();

        KeyBoard.hide(chatEditText);
        Visibility.gone(containerMoreVertical);
        dismiss();

        if (fromOpenChat == 1)
            mainActivity.replaceFragment(new MessageFragment(mainActivity));
        else if (fromOpenChat == Constant.DIRECT_CHAT) {
            //Log.d("app2you","from chat directo");
			/*if(!GlobalData.isKey("listPersons",mainActivity))
				mainActivity.replaceFragment(new NearFragment(mainActivity));*/

        } else {
            if (unLockPictures) {
                if (fromMessages == 1) {

                } else {
                    unLockPictures = false;

                    DetailProfileFragment dialog = new DetailProfileFragment(mainActivity, matchWalla);
                    dialog.show(mainActivity.mFragmentManager, "DetailProfileFragment.TAG");

                    if (dialogDetail != null)
                        dialogDetail.dismiss();
                }
            }
        }

    }

    /**
     * Enviar mensaje a otro usuario
     */
    public void sendChatToUser() {
        String chatMessage = chatEditText.getText().toString().trim();

        if (chatMessage != null && chatMessage.length() > 0) {

            try {
                chatMessage = chatMessage.replaceAll("href", "");
                chatMessage = URLEncoder.encode(chatMessage, "UTF-8");
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            sendChatMessageButton.startAnimation(animationBtnSend);
            sendChatMessageButton.setEnabled(false);
            sendMessageOkhttp3(strFriendFbId, chatMessage, Constant.NORMAL_MESSAGE);

        }
    }

    /**
     * Manejo de eventos de los componentes de la vista
     *
     * @param view
     */
    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.send_chat_message_button) {

            if (adapterPrivatePhoto != null && adapterPrivatePhoto.picIds != null && adapterPrivatePhoto.picIds.size() > 0) {
                sendChatMessageButton.startAnimation(animationBtnSend);
                JSONObject jsonObj = new JSONObject();

                for (int i = 0, len = adapterPrivatePhoto.picIds.size(); i < len; ++i) {
                    try {
                        jsonObj.put("" + (i + 1), "" + adapterPrivatePhoto.picIds.get(i));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                //ejecutar servicio de mandar los ids de las fotos privadas y luego borrar el listado
                requestPrivatePictures(jsonObj.toString());
            } else {
                Visibility.gone(containerMoreVertical);
                String sex      = GlobalData.get(Constant.GENERE, mainActivity);
                String status   = GlobalData.get(Constant.USER_STATUS, mainActivity);
                //versión anterior a la 3
				/*if(sex.equals(Constant.WOMAN))
				{
					sendChatToUser();
				}
				else if(sex.equals(Constant.MAN) && (status.equals(Constant.GOLD_TRIAL) || status.equals(Constant.GOLD)))
				{
					sendChatToUser();
				}
				else
				{
					if(status.equals(Constant.NOT_GOLD_AFTER_TRIAL))
					{
						//abrir fragment para pago
						FragmentGOLD fragmentGOLD = new FragmentGOLD(mainActivity,matchWalla);
						fragmentGOLD.show(mainActivity.mFragmentManager, "fragmentGOLD.TAG");
					}
					else
					{
						//pasate a trial - prueba gratis
						FragmentPassToGOLD FragmentPassToGOLD = new FragmentPassToGOLD(mainActivity, matchWalla);
						FragmentPassToGOLD.show(mainActivity.mFragmentManager, "FragmentPassToGOLD.TAG");
					}

				}*/

                //lógica para chatear versión 3.0
                //Toast.makeText(mainActivity, "from open : " + fromOpenChat + " chatStatus : "+ matchWalla.chatStatusId, Toast.LENGTH_SHORT).show();
                if (fromOpenChat == Constant.MATCH) {
                    //Hay match entre los usuarios se debe dejar chatear libre
                    sendChatToUser();
                } else if (fromOpenChat == 1) {
                    //Hay match entre los usuarios se debe dejar chatear libre
                    sendChatToUser();
                } else if (fromOpenChat == Constant.ACCEPT_CHAT_DIRECT) {
                    sendChatToUser();
                } else if (matchWalla.chatStatusId == Constant.ACCEPT_CHAT_DIRECT) {
                    sendChatToUser();
                } else if (matchWalla.chatStatusId == Constant.REJECT_CHAT_DIRECT) {
                    final Dialog dialog = new Dialog(mainActivity, R.style.PauseDialog);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
                    dialog.setCancelable(false);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                    dialog.setContentView(R.layout.modal_general);

                    TextView text = (TextView) dialog.findViewById(R.id.text);
                    text.setText(matchWalla.firstName + " " + mainActivity.getString(R.string.other_user_rejected_chat));


                    Button exit = (Button) dialog.findViewById(R.id.exit);
                    Visibility.gone(exit);
                    Button accept = (Button) dialog.findViewById(R.id.accept);
                    // if button is clicked, close the custom dialog
                    exit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });
                    accept.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                            dismiss();


                        }
                    });

                    dialog.show();
                } else if (fromOpenChat == Constant.DIRECT_CHAT) {
                    //Revisar el estado de la solicitud
                    if (matchWalla.chatStatusId == Constant.ACCEPT_CHAT_DIRECT) {
                        sendChatToUser();
                    } else if (numChatRequest > 0 && (status.equals(Constant.GOLD) || sex.equals(Constant.WOMAN)) && matchWalla.chatStatusId == Constant.NOT_REQUEST_CHAT) {

                        if (matchWalla.chatStatusId == Constant.REQUEST_CHAT_PENDING) {
                            sendChatMessageButton.startAnimation(animationBtnSend);
                            String gender = GlobalData.get(Constant.GENERE, mainActivity);
                            String messageRequest = gender.equals(Constant.MAN) ? "Tu mensaje fue enviado a " + matchWalla.firstName + ", debes esperar a que responda, para poder hablar con ella." : "Tu mensaje fue enviado a " + matchWalla.firstName + ", debes esperar a que responda, para poder hablar con él.";
                            Common.errorMessage(mainActivity, "", messageRequest);
                        } else {
                            //Log.d("app2you", "enviando chat directo : " +matchWalla.chatStatusId);
                            if (listChatData != null && listChatData.size() > 0 && matchWalla.chatStatusId == Constant.ACCEPT_CHAT_DIRECT)
                                sendChatToUser();
                            else {
                                //Enviar peticion de chat - luego de enviar insertar mensaje
                                sendChatMessageButton.startAnimation(animationBtnSend);
                                String chatMessage = chatEditText.getText().toString().trim();
                                if (chatMessage.length() >= 4)
                                    requestChat(matchWalla.userId, chatMessage);
                                else
                                    Common.errorMessage(mainActivity, "", "" + mainActivity.getString(R.string.write_a_longer_message));
                                //Toast.makeText(mainActivity, "Dejale un mensaje un poco más largo", Toast.LENGTH_SHORT).show();

                            }
                        }
                    } else if (matchWalla.chatStatusId == Constant.REQUEST_CHAT_PENDING) {
                        sendChatMessageButton.startAnimation(animationBtnSend);
                        String gender = GlobalData.get(Constant.GENERE, mainActivity);
                        String messageRequest = gender.equals(Constant.MAN) ? "Tu mensaje fue enviado a " + matchWalla.firstName + ", debes esperar a que responda, para poder hablar con ella." : "Tu mensaje fue enviado a " + matchWalla.firstName + ", debes esperar a que responda, para poder hablar con él.";
                        Common.errorMessage(mainActivity, "", messageRequest);
                    } else {
                        //lanzar tienda en opción de comprar gold
                        sendChatMessageButton.startAnimation(animationBtnSend);
                        //DialogStore dialogStoreWallamatch = new DialogStore(mainActivity,1);
                        //dialogStoreWallamatch.show(mainActivity.mFragmentManager,"DialogStore.TAG");
                    }
                }


            }
        } else if (view.getId() == R.id.senderimage) {

            exitChat();

        } else if (view.getId() == R.id.imgBack) {
            exitChat();
        } else if (view.getId() == R.id.containerOptionsBackChat) {
            exitChat();

        } else if (view.getId() == R.id.btnFeedBack) {
            openDialogFeedBack();
        } else if (view.getId() == R.id.senderName) {
            if (containerMoreVertical.getVisibility() == View.VISIBLE)
                Visibility.gone(containerMoreVertical);
            else {
                if (matchWalla.userId.equals(Constant.ID_SUPPORT) || matchWalla.userId.equals(Constant.ID_WALLAMATCH)) {
                    //Toast.makeText(mainActivity, "Soporte no tiene perfil", Toast.LENGTH_SHORT).show();
                    Common.errorMessage(mainActivity, "", "" + mainActivity.getResources().getString(R.string.message_no_profile));
                } else {
                    DetailProfileFragment dialogDetailProfileFragment = new DetailProfileFragment(mainActivity, matchWalla, Constant.GAME);
                    dialogDetailProfileFragment.show(mainActivity.mFragmentManager, "DetailProfileFragment.TAG");

                }

            }
        }
        else if(view.getId() == R.id.chat_editText)
        {
            if(!chatEditText.isFocusable())
            {
                DialogStore dialogStore = new DialogStore(mainActivity, 1);
                dialogStore.show(mainActivity.mFragmentManager, "DialogStore.TAG");
            }
        }


    }

    /**
     * Abrir dialogo para enviar feedback
     */
    public void openDialogFeedBack() {

        final Dialog dialog = new Dialog(mainActivity, R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_dontlike_app);

        TextView txtTitle               = (TextView) dialog.findViewById(R.id.txtTitleReport);
        TextView txtDescription         = (TextView) dialog.findViewById(R.id.textView13);
        Button btnCancel                = (Button) dialog.findViewById(R.id.btnCancel);
        Button btnSend                  = (Button) dialog.findViewById(R.id.btnSend);
        final EditText fieldComment     = (EditText) dialog.findViewById(R.id.fieldComment);

        txtTitle.setText("" + mainActivity.getResources().getString(R.string.title_dialog_support_amanda));
        txtTitle.setTypeface(TypesLetter.getSansBold(mainActivity));
        txtDescription.setTypeface(TypesLetter.getSansRegular(mainActivity));
        btnSend.setTypeface(TypesLetter.getSansBold(mainActivity));
        btnCancel.setTypeface(TypesLetter.getSansRegular(mainActivity));
        fieldComment.setTypeface(TypesLetter.getSansRegular(mainActivity));

        btnCancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                KeyBoard.hide(fieldComment);
                dialog.dismiss();
            }
        });
        btnSend.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                String comment = fieldComment.getText().toString().trim();
                if (comment.length() > 0) {
                    KeyBoard.hide(fieldComment);
                    sendFeedBack("" + comment, dialog);
                } else
                    Toast.makeText(mainActivity, R.string.empty_fields, Toast.LENGTH_SHORT).show();

            }
        });
        dialog.show();
    }

    /**
     * obtiene los chats del usuario
     */
    public void getChat(String otherUserId) {
        Visibility.visible(progressBarChat);
        OkHttpClient client = new OkHttpClient();

        RequestBody formBody = new FormBody.Builder()
                .add("sessionToken", "" + GlobalData.get(Constant.SESSION_TOKEN, mainActivity))
                .add("otherUserId", "" + otherUserId)
                .build();

        Request request = new Request.Builder()
                .url(Services.GET_CHAT)
                .post(formBody)
                .build();

        client.newCall(request).enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

                Looper.prepare();
                mainActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Visibility.gone(progressBarChat);
                        Common.errorMessage(mainActivity, "", "" + mainActivity.getResources().getString(R.string.check_internet));
                    }
                });

                Looper.loop();
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {

                final String result = response.body().string();
                Log.d("app2you", "respuesta  chats  " + result);

                Looper.prepare();

                mainActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Gson gson = new Gson();
                            final chatUser chat = gson.fromJson(result, chatUser.class);
                            //numPrivatePics
                            Visibility.gone(containerPassGoldChat);
                            if (chat.success) {
                                enableButtonChat(true);
                                numChatRequest = chat.numChatRequests;
                                matchWalla.chatStatusId = chat.chatStatusId;
                                matchWalla.requestToMe = chat.requestToMe;

                                if (matchWalla.chatStatusId == Constant.REJECT_CHAT_DIRECT) {
                                    if (adapter != null)
                                        adapter.notifyDataSetChanged();

                                    dismiss();
                                }
                                //solo usuarios gold tienen derecho a solicitar fotos privadas
                                if (matchWalla.userId.equals(Constant.ID_SUPPORT) || matchWalla.userId.equals(Constant.ID_WALLAMATCH)) {
                                    Visibility.gone(containerImgMoreOptionsChat);
                                } else {
                                    if (chat.age != 0)
                                        senderName.setText(matchWalla.firstName + ", " + chat.age + " " + mainActivity.getString(R.string.age));

                                    txtProvince.setText("" + chat.city);
                                    txtProvince.setVisibility((chat.city != null && chat.city.length() > 0) ? View.VISIBLE : View.GONE);

                                    //Log.d("app2you","from2: " + fromOpenChat + " chatStatusId:  "+ matchWalla.chatStatusId);
                                    if (fromOpenChat == Constant.MATCH || fromOpenChat == Constant.ACCEPT_CHAT_DIRECT || matchWalla.chatStatusId == Constant.ACCEPT_CHAT_DIRECT) {
                                        Visibility.visible(containerImgMoreOptionsChat);
                                        AppAnimation.apperView(mainActivity, containerImgMoreOptionsChat);
                                    } else
                                        Visibility.gone(containerImgMoreOptionsChat);


                                }
                                if (chat.numPrivatePics == 0) {
                                    //no hay fotos
                                    numPrivatePhotos = chat.numPrivatePics;
                                    txtNumPrivatePictures.setText("" + numPrivatePhotos);
                                    imgMoreOptionsChat.setImageResource(R.drawable.icono_candado_gris);
                                } else {
                                    numPrivatePhotos = chat.numPrivatePics;
                                    txtNumPrivatePictures.setText("" + numPrivatePhotos);
                                    txtNumPrivatePictures.setTypeface(null, Typeface.BOLD);
                                    imgMoreOptionsChat.setImageResource(R.drawable.icono_candado_rojo_bola);
                                }

                                listChatData = chat.listChatData;
                                if (listChatData != null && listChatData.size() > 0) {
                                    Visibility.gone(containerRequestUser);

                                } else {
                                    Visibility.visible(containerRequestUser);
                                    Animation anim2 = AnimationUtils.loadAnimation(mainActivity, R.anim.apper_scale);
                                    containerPhotoPreviewChat.startAnimation(anim2);
                                    Visibility.visible(progressBarImageUser);
                                    Picasso.with(mainActivity).load(matchWalla.avatarUrl).error(R.drawable.no_foto).transform(new CircleTransform()).into(imgUser, new Callback() {
                                        @Override
                                        public void onSuccess() {
                                            Visibility.gone(progressBarImageUser);
                                        }

                                        @Override
                                        public void onError() {
                                            Visibility.gone(progressBarImageUser);
                                        }
                                    });

                                    if (fromOpenChat == Constant.DIRECT_CHAT) {

                                        String gender = GlobalData.get(Constant.GENERE, mainActivity);
                                        String status = GlobalData.get(Constant.USER_STATUS, mainActivity);

                                        if (status.equals(Constant.GOLD) || gender.equals(Constant.WOMAN)) {
                                            txtTitleRequest.setText(mainActivity.getString(R.string.call_attendent_message));
                                            Visibility.visible(txtNumRequest);
                                            enableButtonChat(true);
                                            if (numChatRequest == 0) {
                                                //llamar a servicio
                                                Visibility.gone(txtTitleRequest);
                                                Visibility.visible(txtTimeWaitRequest);
                                                txtTimeWaitRequest.setText(mainActivity.getString(R.string.miss_next_reload_init));
                                                requestWaitTime(Constant.TIME_RELOAD_REQUEST_CHAT);
                                                Visibility.gone(containerOptionsChat);
                                                txtNumRequest.setText(mainActivity.getString(R.string.have_exhausted_your_daily_requests));

                                            } else {
                                                Visibility.visible(txtTitleRequest);
                                                Visibility.invisible(txtTimeWaitRequest);
                                                if (numChatRequest == 1)
                                                    txtNumRequest.setText(getString(R.string.missing_one_request, "" + numChatRequest));
                                                else
                                                    txtNumRequest.setText(getString(R.string.missing_request, "" + numChatRequest));
                                            }
                                        } else {
                                            Visibility.visible(txtTitleRequest);
                                            Visibility.visible(containerPassGoldChat);
                                            txtTitleRequest.setText("No tienes Match con " + matchWalla.firstName + "." + "\nSigue jugando a Loovers...");
                                            enableButtonChat(false);
                                            chatEditText.setHint(mainActivity.getString(R.string.active_mode_gold));
                                        }

                                    } else {
                                        //es match
                                        Visibility.visible(txtTitleRequest);
                                        txtTitleRequest.setText(mainActivity.getResources().getString(R.string.say_to));
                                        enableButtonChat(true);
                                    }


                                }
                                messageAdapter = new AwesomeAdapter(mainActivity, listChatData);
                                chatList.setAdapter(messageAdapter);
                                KeyBoard.hide(chatEditText);

                            } else {
                                //try again send message
                            }
                            Visibility.gone(progressBarChat);
                        } catch (Exception ex) {
                            Visibility.gone(progressBarChat);
                            ex.printStackTrace();
                            Toast.makeText(mainActivity, R.string.error_reponse_server, Toast.LENGTH_SHORT).show();
                        }
                        finally {
                            response.body().close();
                        }

                    }
                });

                Looper.loop();
            }
        });
    }

    /**
     * Método encargado de enviar retroalimentación (feedback) al servidor
     *
     * @param message
     * @param dialog
     */
    public void sendFeedBack(String message, final Dialog dialog) {
        final ProgressDialog progressDialog = new ProgressDialog(mainActivity, R.style.CustomDialogTheme);
        progressDialog.setCancelable(false);
        //progressDialog.setMessage(""+mainActivity.getResources().getString(R.string.sending_message));
        progressDialog.show();
        progressDialog.setContentView(R.layout.custom_progress_dialog);
        TextView txtView = (TextView) progressDialog.findViewById(R.id.txtProgressDialog);
        txtView.setText("" + mainActivity.getResources().getString(R.string.sending_message));

        OkHttpClient client = new OkHttpClient();

        RequestBody formBody = new FormBody.Builder()
                .add("sessionToken", "" + GlobalData.get(Constant.SESSION_TOKEN, mainActivity))
                .add("message", "" + message)
                .add("type", "2")
                .add("so", "1")
                .add("deviceVersion", Common.getVersionDevice(mainActivity))
                .add("appVersion", "" + getResources().getString(R.string.app_version))
                .build();

        Request request = new Request.Builder()
                .url(Services.SEND_FEEDBACK)
                .post(formBody)
                .build();

        client.newCall(request).enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                progressDialog.dismiss();
                Looper.prepare();
                mainActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Common.errorMessage(mainActivity, "", "" + mainActivity.getResources().getString(R.string.check_internet));
                    }
                });
                Looper.loop();
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                progressDialog.dismiss();
                final String result = response.body().string();
               // Log.d("app2you", "respuesta  feed  " + result);

                Looper.prepare();

                mainActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            JSONObject json = new JSONObject(result);

                            String success = json.getString("success");

                            if (success.equals("true")) {
                                dialog.dismiss();
                                //Toast.makeText(mainActivity, "Tu mensaje ha sido recibido. ;)", Toast.LENGTH_SHORT).show();
                                Common.errorMessage(mainActivity, "", "" + mainActivity.getResources().getString(R.string.send_message_success));
                            } else {
                                //Toast.makeText(mainActivity, "Intenta de nuevo", Toast.LENGTH_SHORT).show();
                                Common.errorMessage(mainActivity, "", "" + mainActivity.getResources().getString(R.string.send_message_failure));
                            }
                        } catch (JSONException ex) {
                            ex.printStackTrace();
                            Toast.makeText(mainActivity, R.string.error_reponse_server, Toast.LENGTH_SHORT).show();
                        }
                        finally {
                            response.body().close();
                        }

                    }
                });
                Looper.loop();
            }
        });
    }


    /**
     * Método  para ver las fotos privadas que tiene el usuario
     *
     * @param otherUserId
     */
    public void getPrivatePictures(String otherUserId) {
        final ProgressDialog progressDialog = new ProgressDialog(mainActivity, R.style.CustomDialogTheme);
        progressDialog.setCancelable(false);
        progressDialog.show();
        progressDialog.setContentView(R.layout.custom_progress_dialog);
        TextView txtView = (TextView) progressDialog.findViewById(R.id.txtProgressDialog);
        txtView.setText("" + mainActivity.getResources().getString(R.string.fecth_private_pictures));

        okhttp3.OkHttpClient client = new okhttp3.OkHttpClient();

        okhttp3.RequestBody formBody = new FormBody.Builder()
                .add("sessionToken", "" + GlobalData.get(Constant.SESSION_TOKEN, mainActivity))
                .add("otherUserId", "" + otherUserId)
                .build();

        okhttp3.Request request = new okhttp3.Request.Builder()
                .url(Services.GET_PRIVATE_PICTURES)
                .post(formBody)
                .build();


        client.newCall(request).enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                progressDialog.dismiss();
                Looper.prepare();
                mainActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Common.errorMessage(mainActivity, "", "" + mainActivity.getResources().getString(R.string.check_internet));
                    }
                });
                Looper.loop();
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                progressDialog.dismiss();
                final String result = response.body().string();
                Log.d("app2you", "respuesta request photos: " + result);

                Looper.prepare();

                mainActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            JSONObject json = new JSONObject(result);

                            String success = json.getString("success");

                            if (success.equals("true")) {
                                String gimgs = json.getString("gimgs");
                                Gson gson = new Gson();
                                PictureData[] pictureDatas = gson.fromJson(gimgs, PictureData[].class);
                                if (pictureDatas != null && pictureDatas.length > 0) {
                                    ArrayList<PictureData> tmp = new ArrayList<PictureData>();
                                    for (PictureData pic : pictureDatas) {
                                        pic.isChecked = false;
                                        tmp.add(pic);
                                    }

                                    adapterPrivatePhoto = new AdapterPrivatePhoto(mainActivity, tmp);
                                    gridPrivatePhotos.setAdapter(adapterPrivatePhoto);

                                    Visibility.visible(container_private_gallery_photos);
                                    Animation anim = AnimationUtils.loadAnimation(mainActivity, R.anim.up_to_bottom);
                                    container_private_gallery_photos.startAnimation(anim);
                                } else
                                    Common.errorMessage(mainActivity, "", matchWalla.firstName + " " + mainActivity.getResources().getString(R.string.message_private_pictures));
                            } else
                                Common.errorMessage(mainActivity, "", "" + mainActivity.getResources().getString(R.string.try_again));


                        } catch (JSONException ex) {

                            ex.printStackTrace();
                            Toast.makeText(mainActivity, R.string.error_reponse_server, Toast.LENGTH_SHORT).show();
                        }
                        finally {
                            response.body().close();
                        }
                    }
                });
                Looper.loop();
            }
        });

    }

    /**
     * Método  para solicitar fotos privadas del usuario
     *
     * @param ids
     */
    public void requestPrivatePictures(String ids) {
        final ProgressDialog progressDialog = new ProgressDialog(mainActivity, R.style.CustomDialogTheme);
        progressDialog.setCancelable(false);
        progressDialog.show();
        progressDialog.setContentView(R.layout.custom_progress_dialog);
        TextView txtView = (TextView) progressDialog.findViewById(R.id.txtProgressDialog);
        txtView.setText("" + mainActivity.getResources().getString(R.string.sending_request_private_pictures));

        okhttp3.OkHttpClient client = new okhttp3.OkHttpClient();

        okhttp3.RequestBody formBody = new FormBody.Builder()
                .add("sessionToken", "" + GlobalData.get(Constant.SESSION_TOKEN, mainActivity))
                .add("otherUserId", "" + matchWalla.userId)
                .add("picsIds", "" + ids)
                .build();

        okhttp3.Request request = new okhttp3.Request.Builder()
                .url(Services.REQUEST_PRIVATE_PICTURES)
                .post(formBody)
                .build();


        client.newCall(request).enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                progressDialog.dismiss();
                Looper.prepare();
                mainActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Common.errorMessage(mainActivity, "", "" + mainActivity.getResources().getString(R.string.check_internet));
                    }
                });
                Looper.loop();
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                progressDialog.dismiss();
                final String result = response.body().string();
                //Log.d("app2you", "respuesta request picsIds: " + result);

                Looper.prepare();

                mainActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {

                            JSONObject json = new JSONObject(result);

                            String success = json.getString("success");

                            if (success.equals("true")) {
                                Visibility.gone(container_private_gallery_photos);
                                for (int i = 0, len = adapterPrivatePhoto.picIds.size(); i < len; ++i)
                                    sendMessageOkhttp3(matchWalla.userId, "" + adapterPrivatePhoto.picIds.get(i), Constant.REQUEST_PRIVATE_PHOTOS);

                                if (adapterPrivatePhoto.picIds != null)
                                    adapterPrivatePhoto.picIds.clear();

                            } else
                                Common.errorMessage(mainActivity, "", "" + mainActivity.getResources().getString(R.string.try_again));


                        } catch (JSONException ex) {
                            ex.printStackTrace();
                            Toast.makeText(mainActivity, R.string.error_reponse_server, Toast.LENGTH_SHORT).show();
                        }
                        finally {
                            response.body().close();
                        }
                    }
                });


                Looper.loop();
            }
        });

    }

    /**
     * Abrir cuadro de dialogo con las opciones para reportar usuario
     *
     * @param userId
     */
    private void openDialogReport(final String userId) {

        final Dialog dialog = new Dialog(mainActivity, R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_report_user);

        TextView txtTitleReport = (TextView) dialog.findViewById(R.id.txtTitleReport);
        final ListView listReport = (ListView) dialog.findViewById(R.id.listReport);
        txtTitleReport.setTypeface(TypesLetter.getSansBold(mainActivity));

        ReportAdapter reportAdapter = new ReportAdapter(mainActivity, Common.loadCategoriesReport(mainActivity));
        listReport.setAdapter(reportAdapter);

        listReport.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.progressBarReportUser);
                Visibility.visible(progressBar);

                final Report report = (Report) listReport.getItemAtPosition(position);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        serviceReportUser(userId, report.idCategory, progressBar, dialog);
                    }
                }, 200);

            }
        });

        dialog.show();
    }

    /**
     * Método encargado de ejecutar servicio para reportar  usuario
     *
     * @param userId
     * @param categoryId
     * @param progressBar
     */
    public void serviceReportUser(String userId, int categoryId, final ProgressBar progressBar, final Dialog dialog) {
        OkHttpClient client = new OkHttpClient();

        RequestBody formBody = new FormBody.Builder()
                .add("sessionToken", "" + GlobalData.get(Constant.SESSION_TOKEN, mainActivity))
                .add("otherUserId", "" + userId)
                .add("categoryId", "" + categoryId)
                .build();

        Request request = new Request.Builder()
                .url(Services.REPORT_USER)
                .post(formBody)
                .build();

        client.newCall(request).enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

                Looper.prepare();
                mainActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Visibility.gone(progressBar);
                        Common.errorMessage(mainActivity, "", mainActivity.getResources().getString(R.string.check_internet));
                    }
                });
                Looper.loop();
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {

                final String result = response.body().string();
                //Log.d("app2you", "respuesta  report user: " + result);

                Looper.prepare();

                mainActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //apago el dialogo
                        try {
                            dialog.dismiss();
                            Visibility.gone(progressBar);

                            JSONObject json = new JSONObject(result);
                            String success = json.getString("success");
                            if (success.equals("true")) {
                                openDialogConfirmReport("" + mainActivity.getResources().getString(R.string.report_receiver));
                            } else {
                                openDialogConfirmReport(""+mainActivity.getString(R.string.report_user_done));
                            }

                        } catch (JSONException ex) {
                            ex.printStackTrace();

                            openDialogConfirmReport("" + mainActivity.getResources().getString(R.string.report_receiver));
                            //Toast.makeText(mainActivity, R.string.try_again, Toast.LENGTH_SHORT).show();

                        }
                        finally {
                            response.body().close();
                        }
                    }
                });
                Looper.loop();
            }
        });
    }


    /**
     * Método encargado de abrir el dialogo de confirmación despues de enviar reporte
     *
     * @param message
     */
    public void openDialogConfirmReport(String message) {
        final Dialog dialog = new Dialog(mainActivity, R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_confirm_report);

        TextView txtTitleReport = (TextView) dialog.findViewById(R.id.txtTitleReport);
        txtTitleReport.setTypeface(TypesLetter.getSansRegular(mainActivity));
        txtTitleReport.setText("" + message);
        String status = "" + GlobalData.get(Constant.GENERE, mainActivity);
        Button btnClose = (Button) dialog.findViewById(R.id.btnClose);
        ImageView imgReportPerson = (ImageView) dialog.findViewById(R.id.imgReportPerson);
        imgReportPerson.setImageResource(status.equals(Constant.MAN) ? R.drawable.denunciado_chica : R.drawable.denunciado_chico);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    /**
     * Método  para solicitar chat directo
     *
     * @param otherUserId
     */
    public void requestChat(String otherUserId, final String message) {
        final ProgressDialog progressDialog = new ProgressDialog(mainActivity, R.style.CustomDialogTheme);
        progressDialog.setCancelable(false);
        progressDialog.show();
        progressDialog.setContentView(R.layout.custom_progress_dialog);
        TextView txtView = (TextView) progressDialog.findViewById(R.id.txtProgressDialog);
        txtView.setText("" + mainActivity.getString(R.string.sending_request_chat));

        okhttp3.OkHttpClient client = new okhttp3.OkHttpClient();

        okhttp3.RequestBody formBody = new FormBody.Builder()
                .add("sessionToken", "" + GlobalData.get(Constant.SESSION_TOKEN, mainActivity))
                .add("otherUserId", "" + otherUserId)
                .build();

        okhttp3.Request request = new okhttp3.Request.Builder()
                .url(Services.REQUEST_CHAT)
                .post(formBody)
                .build();


        client.newCall(request).enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                progressDialog.dismiss();
                Looper.prepare();
                mainActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Common.errorMessage(mainActivity, "", "" + mainActivity.getResources().getString(R.string.check_internet));
                    }
                });
                Looper.loop();
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                progressDialog.dismiss();
                final String result = response.body().string();
                Log.d("app2you", "response request chat " + result);

                Looper.prepare();

                mainActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {

                            JSONObject json = new JSONObject(result);

                            String success = json.getString("success");

                            if (success.equals("true")) {
                                //al ingresar a cerca de mi se piden nuevamente
                                //GlobalData.delete("listPersons",mainActivity);
                                //GlobalData.delete("listnewpersons",mainActivity);
                                //borrar relación de chat y cerrar el chat
                                if (matchWalla != null)
                                    matchWalla.chatStatusId = Constant.REQUEST_CHAT_PENDING;
                                if (adapter != null)
                                    adapter.notifyDataSetChanged();

                                matchWalla.chatStatusId = Constant.REQUEST_CHAT_PENDING;
                                sendMessageOkhttp3(matchWalla.userId, "" + message, Constant.REQUEST_CHAT);
                                KeyBoard.hide(chatEditText);
                            } else {
                                int errorNum = json.getInt("errorNum");
                                if (errorNum == 4)
                                    Common.errorMessage(mainActivity, "", "" + mainActivity.getString(R.string.have_exhausted_your_daily_requests));
                                else
                                    Common.errorMessage(mainActivity, "", "" + mainActivity.getResources().getString(R.string.try_again));
                            }


                        } catch (JSONException ex) {
                            ex.printStackTrace();
                            Toast.makeText(mainActivity, R.string.error_reponse_server, Toast.LENGTH_SHORT).show();
                        }
                        finally {
                            response.body().close();
                        }
                    }
                });


                Looper.loop();
            }
        });

    }

    /**
     * Metodo encargado de eliminar la relación de chat Mathc o  chat directo
     *
     * @param userId
     * @param positionRequestChat
     */
    public void deleteRelationship(final String relationId, final String userId, final int positionRequestChat) {
        final ProgressDialog progressDialog = new ProgressDialog(mainActivity);
        progressDialog.setMessage("" + mainActivity.getResources().getString(R.string.delete_relation));
        progressDialog.setCancelable(false);
        progressDialog.show();
        OkHttpClient client = new OkHttpClient();
        RequestBody formBody = new FormBody.Builder()
                .add("sessionToken", "" + GlobalData.get(Constant.SESSION_TOKEN, mainActivity))
                .add("serviceId", relationId)
                .add("otherUserId", userId)
                .build();

        Request request = new Request.Builder()
                .url(Services.DELETE_RELATIONSHIP)
                .post(formBody)
                .build();

        client.newCall(request).enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                progressDialog.dismiss();
                Looper.prepare();
                mainActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Common.errorMessage(mainActivity, "", mainActivity.getResources().getString(R.string.check_internet));
                    }
                });

                Looper.loop();
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                progressDialog.dismiss();
                final String result = response.body().string();
               // Log.d("app2you", "respuesta eliminar relacion: " + result);

                Looper.prepare();


                mainActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        JSONObject json = null;
                        try {
                            json = new JSONObject(result);

                            String success = json.getString("success");
                            if (success.equals("true")) {

                                GlobalData.delete("listPersons", mainActivity);
                                GlobalData.delete("listnewpersons", mainActivity);

                                mainActivity.listRequestChat.remove(positionRequestChat);

                                if (friendsAdapterRequest != null)
                                    friendsAdapterRequest.notifyDataSetChanged();

                                int listSize = mainActivity.listRequestChat.size();
                                if (listSize == 0) {
                                    Visibility.gone(mainActivity.badgeTab);
                                    if (mainActivity.containerNoRequestChat != null)
                                        Visibility.visible(mainActivity.containerNoRequestChat);
                                } else {
                                    if (mainActivity.badgeTab != null)
                                        mainActivity.badgeTab.setText("" + listSize);
                                }

                                dismiss();

                            } else {
                                Common.errorMessage(mainActivity, "", mainActivity.getResources().getString(R.string.check_internet));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        finally {
                            response.body().close();
                        }
                    }
                });

                Looper.loop();
            }
        });
    }

    /**
     * Metodo encargado de obtener el tiempo de espera en mili segundos para solicitar chats
     */
    public void requestWaitTime(String serviceId) {
        OkHttpClient client = new OkHttpClient();

        RequestBody formBody = new FormBody.Builder()
                .add("sessionToken", "" + GlobalData.get(Constant.SESSION_TOKEN, mainActivity))
                .add("serviceId", serviceId)
                .build();

        Request request = new Request.Builder()
                .url(Services.GET_WAIT_TIME)
                .post(formBody)
                .build();

        client.newCall(request).enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

                Looper.prepare();
                //Toast.makeText(mainActivity, "Error favorito", Toast.LENGTH_SHORT).show();
                mainActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        Common.errorMessage(mainActivity, "", mainActivity.getResources().getString(R.string.check_internet));
                    }
                });
                Looper.loop();
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {

                final String result = response.body().string();
                Log.d("app2you", "respuesta  getTime: " + result);

                Looper.prepare();

                mainActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //apago el dialogo
                        try {

                            JSONObject json = new JSONObject(result);
                            String success = json.getString("success");
                            if (success.equals("true")) {
                                int millis = json.getInt("time");
                                String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis),
                                        TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                                        TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
                                txtTimeWaitRequest.setText(getString(R.string.miss_next_reload, "" + hms));

                                timer = new CounterClass(millis, 1000, txtTimeWaitRequest);
                                timer.start();

                            } else {
                                Common.errorMessage(mainActivity, "", mainActivity.getResources().getString(R.string.check_internet));
                            }

                        } catch (JSONException ex) {
                            ex.printStackTrace();
                            Toast.makeText(mainActivity, R.string.error_reponse_server, Toast.LENGTH_SHORT).show();
                        }
                        finally {
                            response.body().close();
                        }
                    }
                });
                Looper.loop();
            }
        });
    }

    /**
     * Clase encargada de llevar contador de tiempo para recarga de nuevas solicitudes
     */
    public class CounterClass extends CountDownTimer {
        public TextView txtTimeWaitRequest;

        public CounterClass(long millisInFuture, long countDownInterval, TextView txtTime) {
            super(millisInFuture, countDownInterval);
            this.txtTimeWaitRequest = txtTime;

        }

        @Override
        public void onFinish() {

            if (GlobalData.get(Constant.USER_STATUS, mainActivity).equals(Constant.GOLD)) {
                txtTitleRequest.setText("" + mainActivity.getString(R.string.call_attendent_message));
                Visibility.visible(txtTitleRequest);
                numChatRequest = Constant.REQUEST_CHAT_USER_GOLD;
                txtNumRequest.setText(getString(R.string.missing_request, "" + numChatRequest));
            } else {
                txtTitleRequest.setText("" + mainActivity.getString(R.string.call_attendent_message));
                Visibility.visible(txtTitleRequest);
                numChatRequest = Constant.REQUEST_CHAT_WOMAN;
                txtNumRequest.setText(getString(R.string.missing_request, "" + numChatRequest));
            }

            Visibility.invisible(txtTimeWaitRequest);
            Visibility.visible(containerOptionsChat);

        }

        @Override
        public void onTick(long millisUntilFinished) {
            long millis = millisUntilFinished;
            String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis),
                    TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                    TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));

            txtTimeWaitRequest.setText(getString(R.string.miss_next_reload, "" + hms));
        }
    }

    /**
     * Habilita/deshabilita los componentes del chat footer
     *
     * @param enable
     */
    public void enableButtonChat(boolean enable) {
        chatEditText.setFocusable(enable);
        sendChatMessageButton.setEnabled(enable);
        emoji_btn.setEnabled(enable);
    }


}
