package com.appdupe.flamer.utility;

public class Constant {

    // Update the server End point here

    private static final String urlPath = "http://gestion.lovinghood.com/app_services/";

    public static String hostUrl = urlPath;//+ "process.php/";
    public static final String userlike_url = hostUrl + "setUserLike";
    public static final String blockuser_url = hostUrl + "blockUser";
    public static String register_gcm_url = "gcm_register";

    public static final String login_url = hostUrl + "login";
    public static final String UpdatePrefrence_url = hostUrl + "updateEntity";
    public static final String uploadImage_url = hostUrl + "uploadImage";
    public static final String findMatch_url = hostUrl + "findMatches";
    public static final String newpeople_url = hostUrl + "lookNewPeople";
    public static final String getMatchesFromUser_url = hostUrl + "getMatchesFromUser";
    public static final String myprofile_url = hostUrl + "getMyUserProfile";
    public static final String userprofile_url = hostUrl + "getUserProfile";
    public static final String referrer_url = hostUrl + "sendReferrer";

    public static final String findFlechazos_url = hostUrl + "findFlechazos";
    public static final String findMatchesFromUser_url = hostUrl + "getMatchesFromUser";

    public static final String getProfile_url = hostUrl + "getProfile";
    public static final String editUseProfile_url = hostUrl + "editProfile";
    public static final String getliked_url = hostUrl + "getProfileMatches";
    public static final String imagedelete_url = hostUrl + "deleteImage";
    public static final String UpdateToken_url = hostUrl + "updateSession";
    public static final String inviteaction_url = hostUrl + "inviteAction";
    public static final String updatePrefrence_url = hostUrl
            + "updateUserPreferences";
    public static final String logout_url = hostUrl + "logout";
    public static final String deleteUserAccount_url = hostUrl
            + "deleteAccount";
    public static final String uploadChunk_url = hostUrl + "uploadChunk";
    public static final String updateTotrial = hostUrl + "updateUserToTrial";

    // Added
    public static final String sendMessage_url = hostUrl + "sendMessage";
    public static final String getChatHistory_url = hostUrl + "getChatHistory";
    public static final String getChatMessage_url = hostUrl + "getChatMessage";

    public static final String ImageHostUrl = urlPath + "pics/";
    public static final int ChunkSize = 524288;

    public static final String methodeName = "POST";
    // Update the Flurry Key Here:
    // http://support.flurry.com/index.php?title=Analytics/GettingStarted/TechnicalQuickStart/Android
    public static final String flurryKey = "6Z4Z9HMGHKBW3Q88WCFZ";


    public static final String facebooAuthenticationType = "1";
    public static final String deviceType = "2";
    public static final String FromLogin = "fromLogin";
    public static final String Fromsignup = "fromsignup";
    public static final String Fromsplash = "fromsplash";
    public static final String ALUBUMID = "lubumid";
    public static final String ALUBUMNAME = "alubumName";
    public static final String FRIENDFACEBOOKID = "friendfacebookid";
    public static final String CHECK_FOR_PUSH_OR_NOT = "check_for_push";
    public static boolean isMatchedFound = false;
    public static String isFromChatScreen = "isfromChateScreen";
    public static String isLikde = "1";
    public static String isDisliked = "2";

    public static final String FACEBOOK_ID = "1553570614909785";


    /*
    Login -- Juan Martin Bernal
     */
    public static final String SESSION_TOKEN = "sessionToken";
    public static final String MY_USER_ID = "my_user_id";
    public static final String NICKNAME = "nickname";
    public static final String LOGIN_APP = "login_app";
    public static final String WANTED_FOUND = "wanted";
    public static final String FIRST_REGISTER = "first_register";
    public static final String PERSONAL_STATUS = "personal_status";


    public static final String GENERE = "genere";
    public static final String MAN = "1";
    public static final String WOMAN = "2";

    //Datos del dispositivo
    public static final String SO = "ANDROID";
    public static final String VERSION_SO = "version";
    public static final String NAME_MODEL_DEVICE = "name_model_device";


    //usuario
    public static final String USER_STATUS = "user_status";
    public static final String CURRENT_LIKES = "currentLikes";
    public static final String AVATAR_USER = "avatar_user";
    public static final String AVATAR_TEMPORAL = "avatar_temporal";

    public static final String LATITUD_USER = "lat_user";
    public static final String LONGITUD_USER = "lng_user";
    public static final String CITY_USER = "city_user";
    public static final String COUNTRY_USER = "country_user";
    public static final String PROVINCE = "province";
    public static final String POSTAL_CODE = "postal_code";

    //Estados usuario dentro de la app
    public static final String NOT_GOLD_BEFORE_TRIAL_NO_PICTURE = "1";
    public static final String NOT_GOLD_BEFORE_TRIAL_PICTURE_PENDING = "2";
    public static final String NOT_GOLD_BEFORE_TRIAL_PICTURE_VALIDATE = "3";
    //public static final String GOLD_TRIAL = "4";
    //public static final String NOT_GOLD_AFTER_TRIAL = "5";
    public static final String GOLD = "4";


    //constantes info usuario wallas- favorites - match

    public static final int WALLAS = 1;
    public static final int FAVORITE = 2;
    public static final int MATCH = 3;
    public static final int GAME = 4;
    public static final int LIST_PERSONS = 5;
    public static final int MESSAGE = 4;


    //primer MATCH
    public static final String FIRST_MATCH = "first_match";

    //servicio en ejecucion
    public static final String SERVICE_EXECUTION = "service_execute";

    //contadores
    public static final String COUNTER_WALLAS = "counter_wallas";
    public static final String COUNTER_MATCHS = "counter_matchs";
    public static final String COUNTER_FAVORITES = "counter_favorites";
    public static final String COUNTER_MESSAGES = "counter_messages";
    public static final String APP_ON = "app_on";

    //preferencias notificaciones

    public static final String NOTIFICATION_WALLAS = "notification_wallas";
    public static final String NOTIFICATION_FAVORITES = "notification_favorites";
    public static final String NOTIFICATION_MATCHES = "notification_matches";
    public static final String NOTIFICATION_MESSAGES = "notification_messages";


    //preferences
    public static final String EMAIL_ADDRESS = "email_address";

    public static final String MIN_AGE_PREFERENCE = "min_age_preferences";
    public static final String MAX_AGE_PREFERENCE = "max_age_preferences";
    public static final String RADIUS_PREFERENCE = "radius_preferences";


    public static final String TUTORIAL_GAME = "tutorial_game";

    public static final String ID_SUPPORT = "1";//amanda
    public static final String ID_WALLAMATCH = "2";//WALLAMATCH

    public static final int CHATEAR_UN_RATO = 10;
    public static final int INVITARME_A_SALIR = 11;
    public static final int SALTARME_LOS_PRELIMINARES = 12;

    public static final String FIRST_FAVORITE = "first_favorite";
    public static final String DELETE_FIRST_FAVORITE = "delete_first_favorite";

    //fotos
    public static final int TYPE_PROFILE = 1;
    public static final int TYPE_GALLERY = 2;
    public static final String MESSAGE_PHOTO_PENDING = "message_photo_pending";
    public static final String MESSAGE_NOT_PHOTO = "message_not_photo";


    public static final String LOGIN_FACEBOOK = "login_facebook";
    public static final String SHARE_FACEBOOK = "share_facebook";
    public static final String URL_AVATAR_FACEBOOK = "url_avatar_facebook";
    public static final String EMAIL_VERIFIED = "email_verified";
    public static final String BIRTHDAY_VERIFIED = "birthday_verified";

    public static final int TIME_EXCUTE_FRAGMENT = 300;

    //seguridad
    public static final String PIN_SECURITY = "pin_security";
    public static final String NUMBER_ATTEMPS = "0";
    public static final int NUMBER_MAX_ATTEMPS = 5;

    //Productos loovers
    /*ID - Service Name
		1 - Wallas
		2 - Gold
	*/
    public static final int PRODUCT_RELOAD_WALLAS = 7;
    public static final int PRODUCT_GOLD_SUBSCRIPTION = 8;

    public static final int BUY_COINS_1 = 1;
    public static final int BUY_COINS_2 = 2;
    public static final int BUY_COINS_3 = 3;
    public static final int GOLD_24H = 4;
    public static final int GOLD_1_MONTH = 5;
    public static final int GOLD_3_MONTHS = 6;


    //categorias reporte usuario
    public static final int PROFILE_FAKE = 1;
    public static final int PERSON_RUDE = 2;
    public static final int YOUNGER = 3;
    public static final int SEXUAL_PHOTOS = 4;
    public static final int OTHER_REASON = 5;
    public static final int FRAUD_PRIVATE_PRIVATE_PHOTO = 6;


    //cameraPhotoCapture activity

    public static final String ACTIVITY_FROM_CAMERA = "activity_from_camera";

    public static final String CARDS_GAME = "cards_game";
    public static final String CARDS = "cards";

    public static final String KEY_APPS_FLYER = "D5pM9WPraXtQiSnVJuHEu3";


    //CONSTANTES FINDNEWPEOPLE
    public static final int GAME_WALLAMATCH = 1;
    public static final int NEAR_TO_YOU = 2;
    public static final int MAP = 3;
    public static final int NEW_PEOPLE = 4;

    //Variables usuario estado conectado/desconectado


    public static final int ONLINE = 1;
    public static final int OFFLINE = 2;


    //num max de imagenes de galeria
    public static final int NUM_IMAGES_GALLERY = 10;
    public static final String PREFERENCES_IMAGES_GALLERY = "preferences_images_gallery";

    //Global data valorar la aplicación
    public static final String RATE_APP = "rate_app";


    //error 100 abrir sesion en otro dispositivo
    public static final int ERROR_OPEN_SESSION_OTHER_DEVICE = 100;


    //animaciones
    public static final int DURATION_ANIMATION = 400;


    //fotos
    public static final String PRIVATE_PHOTO = "1";
    public static final String PUBLIC_PHOTO = "2";


    //soporte para tipo de mensajes en el chat
    public static final String NORMAL_MESSAGE = "1";
    public static final String REQUEST_CHAT = "2";
    public static final String REQUEST_PRIVATE_PHOTOS = "3";
    public static final String TYPE_MESSAGE_IMAGE = "4";

    //private status id
    public static final String REQUEST_PHOTO = "1";
    public static final String ACCEPT_PHOTO = "2";
    public static final String REJECT_PHOTO = "3";

    //1- chat , 2 - foto typeRequest
    public static final String TYPE_REQUEST_CHAT = "1";
    public static final String TYPE_REQUEST_PHOTO = "2";


    //razones por las que se elimina loovers
    public static final int DONT_LIKE_APP = 1; //No es lo que esperaba
    public static final int NOT_FOUND_PEOPLE = 2; //No encuentro gente en mi zona
    public static final int NOT_RESPONSE_CHAT = 3; //No contestan a los chats
    public static final int OTHER_REASON_DELETE_APP = 4; //Otra razon (y cuadro de dialogo para escribir)
    public static final int APP_DOES_NOT_WORK = 5; //La aplicacion no funciona


    //constante para enviar ping al servidor
    public static final String SERVER_PING = "server_ping";
    public static final String SERVER_PING_REGISTER = "server_ping_register";
    public static final String SERVER_PING_GENERE = "server_ping_genere";
    public static final String SERVER_PING_VIEW_REGISTER = "server_ping_view_register";
    public static final String SERVER_PING_VIEW_REGISTER_COMPLETE = "server_ping_view_register_complete";
    public static final String SERVER_PING_CLICK_FACEBOOK = "server_ping_click_facebook";
    public static final String SERVER_PING_CLICK_FACEBOOK_SUCCESS = "server_ping_click_facebook_success";
    public static final String SERVER_PING_LOGIN_FACEBOOK_SUCCESS = "ping_login_facebook_success";

    public static final String SERVER_PING_LOGIN_SUCCESS = "ping_login_success";
    public static final String HASH_SESSION = "hash_session";


    //chat directo
    public static final int DIRECT_CHAT = 4;
    //Chat status ids:
    public static final int NOT_REQUEST_CHAT = 0;
    public static final int REQUEST_CHAT_PENDING = 1;
    public static final int ACCEPT_CHAT_DIRECT = 2;
    public static final int REJECT_CHAT_DIRECT = 3;


    //eliminar relaciones
	public static final int DELETE_MATCH = 5;
    public static final int DELETE_DIRECT_CHAT = 7;


    //Tiempo animaciones
    public static final int TIME_SHIMMER = 1300;

    //constantes para pedir servicio para recargar creditos o  solicitudes de chat
    public static final String TIME_RELOAD_CREDITS = "1";
    public static final String TIME_RELOAD_REQUEST_CHAT = "2";


    //variables globales para pedir quien te quiere conocer, maches o favoritos
    public static final String REQUEST_MATCH        = "request_match";
    public static final String REQUEST_WANT_TO_ME   = "request_want_to_me";
    public static final String REQUEST_FAVORITES    = "request_favorites";


    //numero de solicitudes gratuitas de chat
    public static final int REQUEST_CHAT_USER_GOLD = 5;
    public static final int REQUEST_CHAT_WOMAN     = 3;


    //registration id GCM
    public static final String REGISTRATION_ID_GCM_OLD = "registration_id_gcm_old";


    //promociones
    public static final String PROMOTION_ID = "-1";
    public static final int FEATURE_1 = 1;
    public static final int FEATURE_2 = 2;
    public static final int FEATURE_3 = 3;
    public static final int FEATURE_4 = 4;
    public static final int FEATURE_5 = 5;
    public static final int FEATURE_6 = 6;
    public static final int FEATURE_7 = 7;
    public static final int FEATURE_8 = 8;


    public static final String LAST_CONNECTION = "last_connection";

    //VARIABLES
    public static final String LOGIN_CREDITS_TIME = "1";
    public static final String FACEBOOK_SHARE_CREDITS_TIME = "2";
    public static final String RELOAD_CREDITS_TIME = "3";

    public static final String CREDITS_LOGIN_DAY = "credits_day_login";
    public static final String CREDITS_SHARE_FACEBOOK = "credits_share_facebook";


    //landings
    public static final String VIEW_LANDING_WANT_TO_KNOW = "view_want_to_know";

}
