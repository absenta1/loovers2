package com.android.slidingmenu.objects;

import com.appdupe.androidpushnotifications.ChatMessageList;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Juan Martin Bernal on 18/1/16.
 */
public class chatUser implements Serializable {

    @SerializedName("success")
    public Boolean  success;

    @SerializedName("results")
    public ArrayList<ChatMessageList> listChatData;

    @SerializedName("numPrivatePics")
    public int numPrivatePics;

    @SerializedName("numChatRequests")
    public int numChatRequests;

    @SerializedName("chatStatusId")
    public int chatStatusId;

    @SerializedName("requestToMe")
    public boolean requestToMe;

    @SerializedName("age")
    public int age;

    @SerializedName("city")
    public String city;
}




