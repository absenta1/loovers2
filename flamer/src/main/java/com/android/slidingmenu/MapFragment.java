package com.android.slidingmenu;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.slidingmenu.fragments.DetailProfileFragment;
import com.android.slidingmenu.util.Common;
import com.android.slidingmenu.util.GPSTracker;
import com.android.slidingmenu.util.GlobalData;
import com.android.slidingmenu.webservices.Services;
import com.appdupe.flamer.pojo.FindMatchData;
import com.appdupe.flamer.pojo.MatchesData;
import com.appdupe.flamer.utility.CircleTransform;
import com.appdupe.flamer.utility.Constant;
import com.appyvet.rangebar.RangeBar;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.google.maps.android.ui.IconGenerator;
import com.squareup.picasso.Picasso;
import com.loovers.app.R;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


public class MapFragment extends Fragment implements OnMapReadyCallback {

    private GoogleMap mMap;
    public MainActivity mainActivity;
    public GPSTracker gps;
    public LatLng myPosition;
    public static final int LEVEL_ZOOM = 11;
    private ClusterManager<MatchesData> mClusterManager;
    public CustomRenderer customRenderer;
    public ArrayList<MatchesData> nearbyPersons = new ArrayList<>();
    public View view;
    public com.appyvet.rangebar.RangeBar range_bar;
    public TextView txtLocatedPersons;
    public final static int MY_PERMISSIONS_REQUEST_READ_GPS = 7852;


    public MapFragment() {
    }

    public MapFragment(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // View view = inflater.inflate(R.layout.activity_maps, container, false);

        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            view = inflater.inflate(R.layout.activity_maps, container, false);
            range_bar = (com.appyvet.rangebar.RangeBar) view.findViewById(R.id.rangebar);


            range_bar.setEnabled(false);
            txtLocatedPersons = (TextView) view.findViewById(R.id.txtLocatedPersons);
        } catch (InflateException e) {
        /* map is already there, just return view as it is */
            e.printStackTrace();
        }


        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (mainActivity == null)
            mainActivity = (MainActivity) getActivity();


    }

    /**
     * Método encargado de dibujar las personas cercanas en el mapa
      * @param my_latlong
     * @param persons
     * @param distance
     */
    public void getNearbyPersons(LatLng my_latlong, ArrayList<MatchesData> persons, String distance) {

        ArrayList<MatchesData> matchesDatas = new ArrayList<>();
        double distanceRangeBar = Double.parseDouble(distance) * 1000;
        Location l1 = new Location("One");
        l1.setLatitude(my_latlong.latitude);
        l1.setLongitude(my_latlong.longitude);

        for (MatchesData matches : persons) {
            // MatchesData matchesData = persons.get(i);
            Location l2 = new Location("Two");
            l2.setLatitude(Double.parseDouble(matches.latitud));
            l2.setLongitude(Double.parseDouble(matches.longitud));

            float distancePerson = l1.distanceTo(l2);

            if (distancePerson <= distanceRangeBar) {
                //Log.d("app2you", "distancia con " + matches.firstName + " a " + distancePerson);
                matchesDatas.add(matches);

            }

        }
        // for (Marker marker : markersNearbyPersons)
        //   marker.remove();

        for (final MatchesData data : matchesDatas) {
            data.latitud = data.latitud + (Math.random() - .5) / 1500;
            data.longitud = data.longitud + (Math.random() - .5) / 1500;
        }

        Log.d("app2you", "marcas a guardar en cluster: " + matchesDatas.size());
        if(mClusterManager != null) {
            mClusterManager.clearItems();
            mClusterManager.addItems(matchesDatas);
            mClusterManager.cluster();
        }
        txtLocatedPersons.setText(getString(R.string.location_persons, "" + matchesDatas.size(), "" + distance));


    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (mMap != null) {
            // setUpMap();
            mMap.getUiSettings().setAllGesturesEnabled(true);
            mMap.getUiSettings().setZoomGesturesEnabled(true);

            if (ActivityCompat.checkSelfPermission(mainActivity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mainActivity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions( mainActivity, new String[] {  android.Manifest.permission.ACCESS_COARSE_LOCATION,android.Manifest.permission.ACCESS_FINE_LOCATION  },MY_PERMISSIONS_REQUEST_READ_GPS);
            }
            else
            {
                mMap.getUiSettings().setMyLocationButtonEnabled(true);
                mMap.setMyLocationEnabled(true);
            }
            mMap.getUiSettings().setZoomControlsEnabled(true);
            mMap.getUiSettings().setMapToolbarEnabled(true);

            String lat = GlobalData.get(Constant.LATITUD_USER,  mainActivity);
            String lon = GlobalData.get(Constant.LONGITUD_USER, mainActivity);
            Log.d("app2you","paso por aqui onMapReady ");

            if(mClusterManager != null) {
                Log.d("app2you","pause map cluster");
                mClusterManager.clearItems();
                mClusterManager.addItems(new ArrayList<MatchesData>());
                mClusterManager.cluster();
            }
            if(mMap != null)
                mMap.clear();


            if(!lat.equals("0.0") && !lon.equals("0.0"))
            {
                double latD = Double.parseDouble(lat);
                double lonD = Double.parseDouble(lon);
                setPositionPerson(latD,lonD);
                findNewPeople();

            }
            else
                gps();

            range_bar.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
                @Override
                public void onRangeChangeListener(RangeBar rangeBar, int leftPinIndex, int rightPinIndex, String leftPinValue, String rightPinValue) {

                    getNearbyPersons(myPosition,nearbyPersons,rightPinValue);
                }
            });


        }
    }

    //setea la posicion actual del usuario y lo ubica en el mapa
    public void setPositionPerson(final double latitude, final double longitude)
    {
        myPosition = new LatLng(latitude, longitude);

        /*Glide.with(mainActivity)
                .load(""+GlobalData.get(Constant.AVATAR_USER,mainActivity))
                .asBitmap()
                .transform(new CropCircleTransformation(mainActivity))
                .into(new SimpleTarget<Bitmap>(90,90) {
                    @Override
                    public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {

                        mMap.addMarker(new MarkerOptions()
                                .title(""+GlobalData.get(Constant.NICKNAME,mainActivity))

                                .position(myPosition)
                                .icon(BitmapDescriptorFactory.fromBitmap(resource))
                        );

                    }
                });*/

        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), LEVEL_ZOOM));
    }

    /**
     * Comprobar si el gps del dispositivo está disponible
     */
    public void gps()
    {
        // create class object
        gps = new GPSTracker(mainActivity);

        // check if GPS enabled
        if(gps.canGetLocation()){

            double latitude  = gps.getLatitude();
            double longitude = gps.getLongitude();

            setPositionPerson(latitude,longitude);

            findNewPeople();


        }
        else
        {
            gps.showSettingsAlert();
        }
    }



    /**
     * Metodo encargado de obtener las personas que están a sua alrededor
     */
    public void findNewPeople()
    {
        final ProgressDialog progressDialog = new ProgressDialog(mainActivity,R.style.CustomDialogTheme);
        progressDialog.setCancelable(false);
        progressDialog.show();
        progressDialog.setContentView(R.layout.custom_progress_dialog);
        TextView txtView = (TextView) progressDialog.findViewById(R.id.txtProgressDialog);
        txtView.setText("Encontrando personas a tu alrededor....");

        OkHttpClient client = new OkHttpClient();
        RequestBody formBody = new FormBody.Builder()
                .add("sessionToken", "" + GlobalData.get(Constant.SESSION_TOKEN, mainActivity))
                .add("latitud",""+myPosition.latitude)
                .add("longitud",""+myPosition.longitude)
                .add("typeId", ""+Constant.MAP)

                .build();

        Request request = new Request.Builder()
                .url(Services.FIND_MATCHES_SERVICE)
                .post(formBody)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                if (progressDialog != null)
                    progressDialog.dismiss();
                Looper.prepare();

                mainActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Common.errorMessage(mainActivity, "", mainActivity.getResources().getString(R.string.check_internet));
                    }
                });

                Looper.loop();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (progressDialog != null)
                    progressDialog.dismiss();
                final String result = response.body().string();
                Log.d("app2you", "respuesta findNewPeople map: " + result);

                Looper.prepare();

                mainActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {

                            Gson gson = new Gson();
                            FindMatchData mFindMatchData = gson.fromJson(result,FindMatchData.class);
                            if(mFindMatchData.success)
                            {

                                nearbyPersons = mFindMatchData.matches;

                                Toast.makeText(mainActivity,"total personas: " + nearbyPersons.size(),Toast.LENGTH_LONG).show();
                                new DownloadImages(nearbyPersons).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                                range_bar.setEnabled(true);
                            }
                            else
                            {
                                Toast.makeText(mainActivity, "false response", Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {

                            e.printStackTrace();
                            Toast.makeText(mainActivity, R.string.error_reponse_server, Toast.LENGTH_SHORT).show();
                        }


                    }


                });

                Looper.loop();
            }
        });
    }

    /**
     * Clase encargada de renderizar las marcas personalizadas en el mapa
     */
    class CustomRenderer extends DefaultClusterRenderer<MatchesData>
    {
        private final IconGenerator mIconGenerator = new IconGenerator(mainActivity);
        //private final IconGenerator mClusterIconGenerator = new IconGenerator(mainActivity);
        private final ImageView mImageView;
        //private final ImageView mClusterImageView;
        private final int mDimension;

        public CustomRenderer(Context context, GoogleMap map, ClusterManager<MatchesData> clusterManager) {
            super(context, map, clusterManager);
           // View multiProfile = mainActivity.getLayoutInflater().inflate(R.layout.multiple_profile, null);
            //mClusterIconGenerator.setContentView(multiProfile);
            //mClusterImageView = (ImageView) multiProfile.findViewById(R.id.image);

            mImageView = new ImageView(mainActivity);
            mDimension = 160;
            mImageView.setLayoutParams(new ViewGroup.LayoutParams(mDimension, mDimension));
            int padding = 2;
            mImageView.setPadding(padding, padding, padding, padding);
            mIconGenerator.setContentView(mImageView);
        }



        @Override
        protected void onBeforeClusterItemRendered(final MatchesData item, final MarkerOptions markerOptions) {
            super.onBeforeClusterItemRendered(item, markerOptions);
            // Draw a single person.
            // Set the info window to show their name.
            if(item.bitmap != null) {
                mImageView.setImageBitmap(item.bitmap);
                Bitmap icon = mIconGenerator.makeIcon();
                markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon)).draggable(true).title(item.firstName);
            }


        }

        @Override
        protected void onBeforeClusterRendered(Cluster<MatchesData> cluster, final MarkerOptions markerOptions) {
            super.onBeforeClusterRendered(cluster, markerOptions);


        }

        @Override
        protected boolean shouldRenderAsCluster(Cluster<MatchesData> cluster) {
            //start clustering if at least 2 items overlap
            return cluster.getSize() > 1;
        }
    }

    class DownloadImages extends AsyncTask<String, String, String>
    {
        public ArrayList<MatchesData> data;
        public ProgressDialog progressDialog;

        public DownloadImages(ArrayList<MatchesData> data)
        {
            this.data = data;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(mainActivity,R.style.CustomDialogTheme);
            progressDialog.setCancelable(false);
            progressDialog.show();
            progressDialog.setContentView(R.layout.custom_progress_dialog);
            TextView txtView = (TextView) progressDialog.findViewById(R.id.txtProgressDialog);
            txtView.setText("Encontrando personas...");
        }

        @Override
        protected String doInBackground(String... params) {

            for (MatchesData d:data) {
                try {
                    d.bitmap = Picasso.with(mainActivity).load(d.avatarUrl).transform(new CircleTransform()).get();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
            mClusterManager = new ClusterManager<MatchesData>(mainActivity, mMap);
            customRenderer = new CustomRenderer(mainActivity, mMap, mClusterManager);
            mClusterManager.setRenderer(customRenderer);

            mMap.setOnCameraChangeListener(mClusterManager);

            mMap.setOnMarkerClickListener(mClusterManager);
            mMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
                @Override
                public void onMarkerDragStart(Marker marker) {
                    Log.d("dragApp2you","cogio " + marker.getTitle());
                }

                @Override
                public void onMarkerDrag(Marker marker) {
                    Log.d("dragApp2you","hizo drag");
                }

                @Override
                public void onMarkerDragEnd(Marker marker) {
                    Log.d("dragApp2you","termino con " + marker.getTitle());
                }
            });

            mClusterManager.setOnClusterItemClickListener(new ClusterManager.OnClusterItemClickListener<MatchesData>() {
                @Override
                public boolean onClusterItemClick(MatchesData item) {
                    //clickedClusterItem = item;
                    DetailProfileFragment dialog = new DetailProfileFragment(mainActivity,item,Constant.LIST_PERSONS);
                    dialog.show(mainActivity.mFragmentManager,"DetailProfileFragment.TAG");
                    return false;
                }
            });
            getNearbyPersons(myPosition,nearbyPersons,"1000");
        }
    }


    @Override
    public void onResume() {
        super.onResume();

        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("app2you","pause onDestroy");
    }
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {

            switch (requestCode) {

                case MY_PERMISSIONS_REQUEST_READ_GPS: {
                    // If request is cancelled, the result arrays are empty.
                    if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                        // permission was granted, yay! Do the
                        // contacts-related task you need to do.

                    } else {

                        // permission denied, boo! Disable the
                        // functionality that depends on this permission.
                    }
                    return;
                }

            }

    }

    @Override
    public void onPause() {
        super.onPause();
        if(nearbyPersons != null && nearbyPersons.size() > 0)
            GlobalData.set("personsMap",new Gson().toJson(nearbyPersons),mainActivity);
    }
}
