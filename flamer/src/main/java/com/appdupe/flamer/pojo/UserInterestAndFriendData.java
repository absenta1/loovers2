package com.appdupe.flamer.pojo;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class UserInterestAndFriendData {
	@SerializedName("data")
	ArrayList<UserInterestAndFriendQuaryData> datalist;

	public ArrayList<UserInterestAndFriendQuaryData> getDatalist() {
		return datalist;
	}

	public void setDatalist(ArrayList<UserInterestAndFriendQuaryData> datalist) {
		this.datalist = datalist;
	}

}
