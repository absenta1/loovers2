package com.android.slidingmenu.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.slidingmenu.MainActivity;
import com.android.slidingmenu.util.Common;
import com.android.slidingmenu.util.GlobalData;
import com.android.slidingmenu.util.TouchImageView;
import com.android.slidingmenu.util.Visibility;
import com.android.slidingmenu.webservices.Services;
import com.appdupe.flamer.pojo.PictureData;
import com.appdupe.flamer.utility.Constant;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.viewpagerindicator.CirclePageIndicator;
import com.loovers.app.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


/**
 * @author juan.bernal@iptotal.com( Juan Martin Bernal)
 * @version 1.0
 * @class ViewContent
 * @date 15 de Diciembre del 2014
 * @description Clase encargada de visiaulizar un contenido dependiendo de su
 * tipo. (url,video, imagen)
 */
public class ViewContent extends DialogFragment {

    /**
     * Se inicializa los componentes y los listeners
     */

    public TouchImageView imgPost;
    public ImageView imgSave, imgShare;
    public ProgressBar prgSaveImage, prgShareImage, progressBarImageZoom;
    public String urlImage = "", name = "";
    public Bitmap bitmap;
    public MainActivity mainActivity;
    public ArrayList<PictureData>pictures;
    public ViewPager viewpagerUserProfile;
    public View view;
    public int position = 0;
    public int from = 0;
    public int fromChat = 0;

    public ViewContent(){}
    public ViewContent(MainActivity mainActivity, String urlImage, String name)
    {
        this.mainActivity = mainActivity;
        this.urlImage = urlImage;
        this.name     = name;

    }
    public ViewContent(MainActivity mainActivity, String urlImage, String name, int fromChat)
    {
        this.mainActivity = mainActivity;
        this.urlImage = urlImage;
        this.name     = name;
        this.fromChat = fromChat;

    }
    public ViewContent(MainActivity mainActivity,ArrayList<PictureData>pictures, int position, int from)
    {
        this.mainActivity = mainActivity;
        this.pictures     = pictures;
        this.position     = position;
        this.from         = from;

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
         view = inflater.inflate(R.layout.view_image,container,false);

        if(mainActivity == null)
            mainActivity = (MainActivity)getActivity();

        progressBarImageZoom    = (ProgressBar) view.findViewById(R.id.progressBarImageZoom);
        imgSave                 = (ImageView) view.findViewById(R.id.btn_save_image);
        imgShare                = (ImageView) view.findViewById(R.id.btn_share_image);
        prgSaveImage            = (ProgressBar) view.findViewById(R.id.progressBar_save_image);
        prgShareImage           = (ProgressBar) view.findViewById(R.id.progressBar_share_image);
        imgPost                 = (TouchImageView) view.findViewById(R.id.touchImageView);
        viewpagerUserProfile    = (ViewPager) view.findViewById(R.id.viewpagerUserProfile);

        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //cargar imagen

        if(pictures!= null && pictures.size()>0)
        {
            Visibility.gone(imgPost);
            Visibility.gone(progressBarImageZoom);


            CustomPagerAdapter mCustomPagerAdapter2 = new CustomPagerAdapter(mainActivity,  pictures);

            viewpagerUserProfile.setAdapter(mCustomPagerAdapter2);
            viewpagerUserProfile.setCurrentItem(position,true);
            CirclePageIndicator titleIndicator2 = (CirclePageIndicator) view.findViewById(R.id.titlesUserProfile);
            titleIndicator2.setViewPager(viewpagerUserProfile);
        }
        else {
            Visibility.visible(imgPost);
            Picasso.with(mainActivity).load(urlImage).error(R.drawable.no_foto).into(imgPost, new Callback() {
                @Override
                public void onSuccess() {
                    Visibility.gone(progressBarImageZoom);
                }

                @Override
                public void onError() {
                    Visibility.gone(progressBarImageZoom);
                }
            });
            imgPost.setBackgroundColor(Color.parseColor("#000000"));
            imgPost.setMaxZoom(4f);

            /*imgSave.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                    new AsyncTaskSaveImage(0)
                            .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                }
            });
            imgShare.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                    new AsyncTaskSaveImage(1)
                            .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                }
            });*/

            imgPost.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {

                    dismiss();
                }
            });

        }
    }
    public class AsyncTaskSaveImage extends AsyncTask<String, String, String> {

        File file;
        int option = 0;

        public AsyncTaskSaveImage(int option) {
            this.option = option;
            // TODO Auto-generated constructor stub
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();

            if (option == 1) {
                // share image
                Visibility.gone(imgShare);
                Visibility.visible(prgShareImage);

            } else {
                // save image
                file = getOutputMediaFile(name, getResources().getString(R.string.app_name));
                Visibility.gone(imgSave);
                Visibility.visible(prgSaveImage);

            }
        }

        @Override
        protected String doInBackground(String... params) {

            try {
                if (option == 0) {
                    FileOutputStream fOut = new FileOutputStream(file);
                    bitmap = Common.getBitmapFromURL(urlImage);
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 80, fOut);
                    fOut.flush();
                    fOut.close();
                    mainActivity.sendBroadcast(new Intent(
                            Intent.ACTION_MEDIA_SCANNER_SCAN_FILE,
                            Uri.fromFile(file)));
                }

               /* if (option == 0)
                    sendBroadcast(new Intent(
                            Intent.ACTION_MEDIA_SCANNER_SCAN_FILE,
                            Uri.fromFile(file)));*/

                return "true";
            } catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return "false";
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return "false";
            }

        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);

            String string = (result.equals("true")) ? getResources().getString(R.string.image_saved_on_device) : getResources().getString(R.string.error_save_image_device);

            if (option == 1) {

                // Uri imageUri = Uri.fromFile(file); // Uri.parse(file.getAbsolutePath());
                Visibility.visible(imgShare);
                Visibility.gone(prgShareImage);

                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                //intent.setType("image/png");
                intent.setType("text/plain");
                intent.putExtra(android.content.Intent.EXTRA_SUBJECT, getResources()
                        .getString(R.string.app_name));
                // intent.putExtra(Intent.EXTRA_STREAM, imageUri);
                intent.putExtra(Intent.EXTRA_TEXT, getResources().getString(R.string.photo_from_loovers)+" " + urlImage);
                startActivity(Intent.createChooser(intent, getResources()
                        .getString(R.string.app_name)));

            } else {
                Visibility.visible(imgSave);
                Visibility.gone(prgSaveImage);
                Toast.makeText(mainActivity, string, Toast.LENGTH_SHORT)
                        .show();
            }

        }

    }

    /**
     * Create a File for saving an image or video
     */
    private File getOutputMediaFile(String post_id, String folderName) {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        File mediaStorageDir = new File(
                Environment.getExternalStorageDirectory() + "/"
                        + Environment.DIRECTORY_PICTURES + "/" + folderName);

        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        // Create a media file name
        // String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmm")
        // .format(new Date());
        File mediaFile;
        String mImageName = getResources().getString(R.string.app_name) + post_id + ".jpg";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator
                + mImageName);
        return mediaFile;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (imgPost != null)
            imgPost.setImageDrawable(null);
    }


    class CustomPagerAdapter extends PagerAdapter {

        Context mContext;
        LayoutInflater mLayoutInflater;
        ArrayList<PictureData> mLista = new ArrayList<PictureData>();

        public CustomPagerAdapter(Context context, ArrayList<PictureData> lista1) {

            this.mLista = lista1;

            mContext = context;
            mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return mLista.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((FrameLayout) object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View itemView = mLayoutInflater.inflate(R.layout.pic_viewpager_page, container, false);

            TouchImageView imageView                         = (TouchImageView) itemView.findViewById(R.id.user_gallery_detail_viewflow_item_imagen);
            final ImageView imgLock                           = (ImageView) itemView.findViewById(R.id.imgLock);
            final ProgressBar progressBarLoadGallery    = (ProgressBar)itemView.findViewById(R.id.progressBarLoadGallery);
            final RelativeLayout containerLock          = (RelativeLayout)itemView.findViewById(R.id.containerLock);

            Visibility.gone(containerLock);
            Visibility.gone(imgLock);

            final PictureData pictureData = mLista.get(position);

            //viene de visitar el perfil de otro usuario
            if(from == 1) {
                if (pictureData.isPrivate != null && pictureData.isPrivate.equals(Constant.PRIVATE_PHOTO)) {
                    Visibility.visible(containerLock);
                }
            }
            //viene de mi perfil
            if(from == 0) {
                Visibility.visible(imgLock);
                if (pictureData.isPrivate != null && pictureData.isPrivate.equals(Constant.PRIVATE_PHOTO)) {
                    imgLock.setImageResource(R.drawable.candado);

                } else {
                    imgLock.setImageResource(R.drawable.unlock);

                }
            }

            Picasso.with(mContext).load(pictureData.imageUrl).error(R.drawable.no_foto).into(imageView, new com.squareup.picasso.Callback() {
                @Override
                public void onSuccess() {
                    Visibility.gone(progressBarLoadGallery);
                }

                @Override
                public void onError() {
                    Visibility.gone(progressBarLoadGallery);
                }
            });

            container.addView(itemView);

            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(from == 1) {
                        if(pictureData.isPrivate != null && pictureData.isPrivate.equals(Constant.PRIVATE_PHOTO))
                        {
                            Common.errorMessage(mainActivity, "", "Esta foto es privada, debes solicitarla en el chat");
                        } else {
                            dismiss();
                        }
                    }
                    else {
                        if(mainActivity.adapter != null)
                            mainActivity.adapter.notifyDataSetChanged();

                        dismiss();
                    }
                }
            });
            imgLock.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {

                    setPrivatePhoto(pictureData,imgLock);
                }
            });

            return itemView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((FrameLayout) object);
        }
        /**
         * Metodo encargado de actualizar la privacidad de una foto privada/publica
         */
        public void setPrivatePhoto(final PictureData pic,final ImageView imgLock)
        {

            OkHttpClient client = new OkHttpClient();

            final ProgressDialog progressDialog = new ProgressDialog(mainActivity,R.style.CustomDialogTheme);
            progressDialog.setCancelable(false);
            //  progressDialog.setMessage(mainActivity.getResources().getString(R.string.deleting_photo_from_gallery));
            progressDialog.show();
            progressDialog.setContentView(R.layout.custom_progress_dialog);
            TextView txtView = (TextView) progressDialog.findViewById(R.id.txtProgressDialog);
            txtView.setText("Actualizando foto...");

            //1 es privada - 0 noes
            RequestBody formBody = new FormBody.Builder()
                    .add("sessionToken", "" + GlobalData.get(Constant.SESSION_TOKEN, mainActivity))
                    .add("picId", ""+pic.id_foto)
                    .add("isPrivate", (pic.isPrivate.equals("1")?Constant.PUBLIC_PHOTO:Constant.PRIVATE_PHOTO))

                    .build();

            Request request = new Request.Builder()
                    .url(Services.SET_PRIVATE_PHOTO)
                    .post(formBody)
                    .build();

            client.newCall(request).enqueue(new okhttp3.Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    progressDialog.dismiss();
                    Looper.prepare();
                    mainActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Common.errorMessage(mainActivity, "",mainActivity.getResources().getString(R.string.check_internet));
                        }
                    });

                    Looper.loop();
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {

                    progressDialog.dismiss();
                    final String result = response.body().string();
                    Log.d("app2you", "respuesta private photo " + result);

                    Looper.prepare();
                    mainActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                JSONObject json = new JSONObject(result);

                                String success = json.getString("success");
                                if (success.equals("true")) {

                                    if(pic.isPrivate != null && pic.isPrivate.equals("0")) {
                                        pic.isPrivate = Constant.PRIVATE_PHOTO;
                                        imgLock.setImageResource(R.drawable.candado);
                                    }
                                    else {
                                        pic.isPrivate = "0";
                                        imgLock.setImageResource(R.drawable.unlock);
                                    }
                                } else
                                    Toast.makeText(mainActivity, R.string.try_again, Toast.LENGTH_SHORT).show();

                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(mainActivity, R.string.error_reponse_server, Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

                    Looper.loop();
                }
            });
        }

    }


    @Override
    public void onResume() {
        super.onResume();
       /* getDialog().setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(android.content.DialogInterface dialog, int keyCode,
                                 android.view.KeyEvent event) {


                if ((keyCode == android.view.KeyEvent.KEYCODE_BACK)) {
                    //This is the filter


                    if (event.getAction() != KeyEvent.ACTION_DOWN) {
                        Log.d("app2you","entrando con :" + fromChat);
                            if (fromChat == 1)
                                dismiss();
                            else
                                notifyAdapterGallery();

                            return true;
                        } else {
                        Log.d("app2you","entrando cdfdjnfljdnfjnon :" + fromChat);
                            //Hide your keyboard here!!!!!!
                            //preguntar teclado esta abierto
                            if (fromChat == 1) {
                                dismiss();

                            }
                            else
                                notifyAdapterGallery();


                            return true; // pretend we've processed it
                        }


                } else
                    return false; // pass on to be processed as normal

            }

        });*/
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new Dialog(getActivity(), getTheme()){
            @Override
            public void onBackPressed() {
                // On backpress, do your stuff here.
                if (fromChat == 1) {
                    dismiss();

                }
                else
                    notifyAdapterGallery();
            }
        };
    }

    private void notifyAdapterGallery() {
        if(mainActivity.adapter !=  null)
            mainActivity.adapter.notifyDataSetChanged();

        dismiss();
    }

}



