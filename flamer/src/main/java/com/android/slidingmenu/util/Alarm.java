package com.android.slidingmenu.util;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.android.slidingmenu.services.LocationService;

/**
 * Created by Juan Martin Bernal on 2/12/15.
 */
public class Alarm {

    static PendingIntent pending;
    static final int TIME_ALARM = 60000 * 2; // 2 minutos

    /**
     * Crea alarma lanzando el servicio para encontrar personas
     * @param cxt
     */
    public static void createAlarm(Context cxt)
    {
        if(!GlobalData.isKey("alarm", cxt))
            GlobalData.set("alarm", "Y", cxt);


        Log.d("lovinghood", "SE ACTIVO LA  ALARMA");
        AlarmManager alarmManager = (AlarmManager) cxt.getSystemService(Context.ALARM_SERVICE);
        Intent alarmIntent = new Intent(cxt, LocationService.class);
        pending = PendingIntent.getService(cxt, 0, alarmIntent, 0);
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), TIME_ALARM, pending);


    }

    /**
     * Metodo encargado de detener la busqueda de las personas
     * @param cxt
     */
    public static void cancelAlarm(Context cxt)
    {

        AlarmManager manager = (AlarmManager) cxt.getSystemService(Context.ALARM_SERVICE);
        if(pending != null) {
            Log.d("lovinghood", "CANCELANDO ALARMA");
            manager.cancel(pending);
        }
    }
}
