package com.android.slidingmenu.activities;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.slidingmenu.util.Common;
import com.android.slidingmenu.util.KeyBoard;
import com.android.slidingmenu.util.Validate;
import com.android.slidingmenu.webservices.Services;
import com.appdupe.flamer.utility.ConnectionDetector;
import com.loovers.app.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class RecoveryPasswordActivity extends Activity {

    public AutoCompleteTextView emailRecovery;
    public Button btn_recovery;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        int sdk = android.os.Build.VERSION.SDK_INT;
        if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            super.setTheme(R.style.AppTheme);

        }
        super.onCreate(savedInstanceState);
        //Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_recovery_password);
        emailRecovery = (AutoCompleteTextView)findViewById(R.id.email);
        btn_recovery = (Button)findViewById(R.id.btn_recovery);
        btn_recovery.setTransformationMethod(null);
    }
    public void recoveryPassword(View view)
    {
        String emailText = emailRecovery.getText().toString();
        //lanzar servicio
        if (!emailText.isEmpty()) {

            if (Validate.validEmail2(emailText)) {
                KeyBoard.hide(emailRecovery);
                //dialog.dismiss();
                if (ConnectionDetector.isConnectingToInternet(RecoveryPasswordActivity.this))
                    forgotPasswordService(emailText);
                else
                    Toast.makeText(RecoveryPasswordActivity.this, R.string.check_internet, Toast.LENGTH_SHORT).show();
            } else
                emailRecovery.setError(getResources().getString(R.string.error_mail));
        } else
            emailRecovery.setError(getResources().getString(R.string.error_mail));
    }

    /**
     * Servicio para recuperar contraseña del usuario en el sistema
     */
    public void forgotPasswordService(String email) {
        OkHttpClient client = new OkHttpClient();
        final ProgressDialog progressDialog = new ProgressDialog(RecoveryPasswordActivity.this,R.style.CustomDialogTheme);
        progressDialog.setCancelable(false);
       // progressDialog.setMessage(getResources().getString(R.string.recovering_password));
        progressDialog.show();
        progressDialog.setContentView(R.layout.custom_progress_dialog);
        TextView txtView = (TextView) progressDialog.findViewById(R.id.txtProgressDialog);
        txtView.setText(""+getResources().getString(R.string.recovering_password));

        RequestBody formBody = new FormBody.Builder()
                .add("email", "" + email)
                .add("ip", Common.getLocalIpAddress(RecoveryPasswordActivity.this))
                .add("version", Common.getVersionDevice(RecoveryPasswordActivity.this))
                .build();

        Request request = new Request.Builder()

                .url(Services.FORGOT_PASSWORD_SERVICE)
                .post(formBody)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                progressDialog.dismiss();
                Looper.prepare();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Common.errorMessage(RecoveryPasswordActivity.this, "", getResources().getString(R.string.check_internet));
                    }
                });
                //Toast.makeText(RecoveryPasswordActivity.this, "Ocurrio un error en recuperar contraseña", Toast.LENGTH_SHORT).show();
                Looper.loop();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                progressDialog.dismiss();


                final String result = response.body().string();
                Log.d("app2you", "respuesta: " + result);
                Looper.prepare();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            JSONObject json = new JSONObject(result);
                            String success = json.getString("success");
                            if (success.equals("true")) {

                                Common.errorMessage(RecoveryPasswordActivity.this,"",""+getResources().getString(R.string.confirm_recovery_email));


                            } else {

                                int errorMessage = Integer.parseInt(json.getString("errorNum"));
                                //Toast.makeText(LoginActivity.this, "Algo paso recuperar: " + errorMessage, Toast.LENGTH_SHORT).show();
                                switch (errorMessage) {
                                    case 1:
                                        break;
                                    case 2:
                                        break;
                                    case 3:
                                        Toast.makeText(RecoveryPasswordActivity.this, R.string.invalid_data, Toast.LENGTH_SHORT).show();
                                        break;
                                    default:
                                        Toast.makeText(RecoveryPasswordActivity.this, R.string.check_internet, Toast.LENGTH_SHORT).show();
                                        break;


                                }

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();

                            Toast.makeText(RecoveryPasswordActivity.this, R.string.error_reponse_server, Toast.LENGTH_SHORT).show();


                        }
                    }
                });

                Looper.loop();
            }
        });


    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        goToLogin();

    }

    /**
     * Metodo encargado de regresar a la pantalla de login
     */
    public void goToLogin()
    {
        Intent mIntent = new Intent(RecoveryPasswordActivity.this,
                LoginActivity.class);
        startActivity(mIntent);
        overridePendingTransition(R.anim.fade_in_slow, R.anim.fade_out);
        finish();
    }
}
